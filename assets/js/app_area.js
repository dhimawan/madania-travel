jQuery(document).ready(function(){
  // $("#field_city_id_chzn").hide();
});

function getCities(province_id,city_id_name){
  var base_url = $("#base_url").val();
  if(province_id!=""){
      jQuery.ajax({
        dataType:'html',
        success: function(data){
          var newview = data.split("{{<<--PISAHDISINI-->>}}");
          $("#"+city_id_name).removeAttr('disabled');
          $("#"+city_id_name).html(newview[0]);
          $("#"+city_id_name).trigger('liszt:updated');
          // console.log(newview[0]);
          // $("#field_city_id_chzn .chzn-drop .chzn-results").html(newview[1]);
          // console.log(city_id_name);
        },
        error:function(data){
          ShowNotifyMessage(data);
          $('#spinner').hide()
        },
        type:'get',
        url:base_url+'cities/getCities/'+province_id
      });
    }
}

function getDistricts(city_id,id_name){
  var base_url = $("#base_url").val();
  if(city_id!=""){
      jQuery.ajax({
        dataType:'html',
        success: function(data){
          // var newview = data.split("{{<<--PISAHDISINI-->>}}");
          $("#"+id_name).removeAttr('disabled');
          $("#"+id_name).html(data);
          $("#"+id_name).trigger('liszt:updated');
          // console.log(newview[0]);
          // $("#field_city_id_chzn .chzn-drop .chzn-results").html(newview[1]);
          // console.log(city_id_name);
        },
        error:function(data){
          ShowNotifyMessage(data);
          $('#spinner').hide()
        },
        type:'get',
        url:base_url+'shareds/getdistricts/'+city_id
      });
    }
}

function getVillages(district_id,id_name){
  var base_url = $("#base_url").val();
  if(district_id!=""){
      jQuery.ajax({
        dataType:'html',
        success: function(data){
          // var newview = data.split("{{<<--PISAHDISINI-->>}}");
          $("#"+id_name).removeAttr('disabled');
          $("#"+id_name).html(data);
          $("#"+id_name).trigger('liszt:updated');
         console.log(id_name);
        },
        error:function(data){
          ShowNotifyMessage(data);
          $('#spinner').hide()
        },
        type:'get',
        url:base_url+'shareds/getVillages/'+district_id
      });
    }
}