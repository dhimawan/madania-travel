<div class="modal fade" id="detailContactus_<?=$data->id?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?=$data->title?></h4>
      </div>
      <div class="modal-body">
        
        <fieldset id="detailEmail_<?=$data->id?>">
          <div class="control-group">
            <label class="control-label" for="focusedInput">Nama Pengirim</label>
            <div class="controls">
              <input id="focusedInput" class="input-xlarge focused" type="text" value="<?=$data->name?>" disabled>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="focusedInput">Email Pengirim</label>
            <div class="controls">
              <input id="focusedInput" class="input-xlarge focused" type="text" value="<?=$data->email?>"disabled>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="focusedInput">No Telpehone</label>
            <div class="controls">
              <input id="focusedInput" class="input-xlarge focused" type="text" value="<?=$data->telp?>" disabled>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="focusedInput">Pesan</label>
            <div class="controls">
              <textarea id="focusedInput" class="input-xlarge focused" disabled> <?=$data->description?> </textarea>
            </div>
          </div>
        </fieldset>
        <form class="form-horizontal" id="formReplayEmail_<?=$data->id?>" action="<?=base_url("dashboard/send_mail")?>">
          <fieldset id="replayEmail_<?=$data->id?>" class="replayEmail" >
            <div class="control-group">

              <label class="control-label" for="focusedInput">Email</label>
              <div class="controls">
                <input id="email" class="input-xlarge focused" type="text"  name="sendmail[email]" value="<?=$data->email?>">
                <span class="help-inline">Gunakan Tanda koma(,) untuk menambahkan email lainnya.</span>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="focusedInput">Judul Pesan</label>
              <div class="controls">
                <input id="title" class="input-xlarge focused" type="text" name="sendmail[title]" value="[Replay]<?=$data->title?>">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="focusedInput">Pesan</label>
              <div class="controls">
                <textarea id="description" class="input-xlarge focused" name="sendmail[description]"></textarea>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="focusedInput">Attachment</label>
              <div class="controls">
                <input type="file" name="userdata" id="attachment_<?=$data->id?>">
                <input type="hidden" name="sendmail[attachments]" class="filename_<?=$data->id?>" id="attachments">
                <input type="hidden" name="sendmail[mail_type]" value="reply">
                <input type="hidden" name="sendmail[parent_id]" value="<?=$data->id?>">
                <span id="div_attachment_<?=$data->id?>"> </span>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <input type="reset" class="btn btn-default" data-dismiss="modal" value="close"/>
        <button type="button" class="btn btn-primary" onclick="replayEmail(<?=$data->id?>)" id="reply_<?=$data->id?>">Balas</button>
        <button id="sendButton_<?=$data->id?>" type="button" class="btn btn-primary" onclick="sendEmail(<?=$data->id?>)" style="display:none;">Kirim</button>
        <span id="loading_mail_<?=$data->id?>"></span>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(document).ready(function(){
  $("#attachment_<?=$data->id?>").uploadify({
      'multi'     : true,
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/mail',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#div_attachment_<?=$data->id?>").append('<span class="clearfix" id="attFile_'+ID+'"><i class="glyphicon-attach" rel="tooltip" data-original-title=".glyphicon-attach"></i> '+fileObj['name']+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a onclick="deleteAttachment('+ID+');return false" class="glyphicon-remove-2" > </a> <span>');

        var last = $(".filename_<?=$data->id?>").val();
        if(last == undefined){
          last = "";
        }else{
          last = last+";"; 
        }
        $(".filename_<?=$data->id?>").val(last+''+fileObj['name']);
        
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
$("#sendButton_<?=$data->id?>").button('loading');
});
 

tinymce.init({
  mode : "exact",
  elements :"page_incontent",
});
</script>