<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "frontends";
$route['404_override'] = '';
$route['news/index/add'] = "news/addCustomNews";
$route['products/index/edit/:num'] = "products/editCustomProducts";
$route['jamaah_registrations/index/add'] = "jamaah_registrations/importManivest";

$route['products/product_employees/add'] = "products/add_product_employees";
$route['products/product_customers/add'] = "products/add_product_customers";

$route['kontak_kami'] = "frontends/contact_us";
$route['news'] = "frontends/pages/news";
$route['profiles'] = "frontends/pages/profiles";
$route['product_and_service_madania_travel'] = "frontends/pages/product_and_service_madania_travel";
$route['Gallery_selama_kegiatan_umroh_dan_haji'] = "frontends/pages/Gallery_selama_kegiatan_umroh_dan_haji";
$route['add_registration'] = "frontends/add_registration";
$route['FAQ'] = "frontends/pages/FAQ";
$route['news/:num/:num/:any'] = "frontends/detail_news";
$route['news/category/:any'] = "frontends/categories";
$route['paket/:any'] = "frontends/detail_service";
$route['app_config/index/add'] = "app_config/addCustomConfig";
$route['app_config/index/edit/:num'] = "app_config/editCustomConfig";

/* End of file routes.php */
/* Location: ./application/config/routes.php */