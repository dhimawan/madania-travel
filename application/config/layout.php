<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// $config['folder_layout'] = 'frontend';
// $config['themes'] = 'frontend';
$config['folder'] = 'themes';
$config['themes'] = 'themes';
$config['assets'] = 'default';
$config['layout'] = 'application';

$config['frontend_layout'] = 'elearning';
$config['frontend_assets'] = 'elearning';
$config['frontend_themes'] = 'elearning';
// $config['frontend_assets'] = 'frontend';

$config['backend_layout'] = 'backend';
$config['backend_assets'] = 'backend';

// $config['folder_layout'] = 'frontend';