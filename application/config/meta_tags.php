<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['meta_tags']['doctype'] = 'xhtml';
$config['meta_tags']['tags'] = array('tagname'=>'tagcontent', 'another tag'=>'some other content');
$config['meta_tags']['robots'] = array('NOINDEX', 'FOLLOW', 'NOARCHIVE');
$config['meta_tags']['keywords'] = array('great', 'php', 'framework');

?>