<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_comment_news extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		
		if(! $CI->db->table_exists('comment_news')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'comment' => array('type' => 'TEXT', 'constraint' => '200', 'null' => FALSE),
				'email' => array('type' => 'VARCHAR', 'constraint' => '200', 'null' => FALSE),
				'news_id' => array('type' => 'INT', 'null' => FALSE),
				'updated_at' => array('type' => 'TEXT', 'null' => FALSE)
			);
			
			// Setup Keys
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->add_field("created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
			$CI->dbforge->create_table('comment_news', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table comment_news...";
		$CI->dbforge->drop_table('comment_news');
	}
}

?>