<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_departures extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('departures')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'name' => array('type' => 'VARCHAR', 'constraint' => '200','null' => FALSE),
				'description' => array('type' => 'TEXT', 'null' => FALSE),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'startdate' => array('type' => 'DATETIME', 'null' => FALSE),
				'enddate' => array('type' => 'DATETIME', 'null' => FALSE),
				'total_tax' => array('type' => 'INT', 'null' => FALSE),
				'attribute_page_id' => array('type' => 'INT', 'null' => FALSE)
			);
			
			// Setup Keys
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('departures', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table departures...";
		$CI->dbforge->drop_table('departures');
	}
}

?>
