<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_requirements extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('requirements')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'name' => array('type' => 'VARCHAR', 'constraint' => '200','null' => FALSE),
				'description' => array('type' => 'TEXT', 'null' => FALSE),
				'status' => array('type' => 'enum','constraint'=>"'all','packet'"),
				'tag' => array('type' => 'VARCHAR','constraint'=>"100", 'null' => FALSE),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)
			);
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('requirements', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table requirements...";
		$CI->dbforge->drop_table('requirements');
	}
}

?>
