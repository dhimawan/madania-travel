<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_parent_id_on_contactus extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
      'parent_id' => array('type' => 'INT'),
      'attachments' => array('type' => 'TEXT'),
      'mail_type' => array('type' => 'VARCHAR','constraint' => '100'),
      'user_id' => array('type' => 'INT')
		);
		$CI->dbforge->add_column('contactus', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table contactus...";
			$this->dbforge->drop_column('contactus', array('parent_id','attachments','mail_type','user_id','mail_type'));
	}

}
?>