<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_Jamaah_registrations extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('jamaah_registrations')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'registration_id' => array('type' => 'INT'),
				'jamaah_id' => array('type' => 'INT'),
				'user_id' => array('type' => 'INT'),
				'age' => array('type' => 'INT'),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)
			);
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('jamaah_registrations', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table jamaah_registrations...";
		$CI->dbforge->drop_table('jamaah_registrations');
	}
}

?>
