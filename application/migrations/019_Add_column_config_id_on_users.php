<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_config_id_on_users extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
      'config_id' => array('type' => 'INT'),
		);
		$CI->dbforge->add_column('users', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table users...";
			$this->dbforge->drop_column('users', array('config_id'));
	}

}
?>