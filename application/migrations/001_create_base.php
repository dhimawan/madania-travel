<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_base extends CI_Migration {

	public function up() {

		## Create Table academic_year
		$this->dbforge->add_field("`id` int(11) NOT NULL ");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`created_at` datetime NULL ");
		$this->dbforge->add_field("`updated_at` datetime NULL ");
		$this->dbforge->add_field("`value` varchar(45) NULL ");
		$this->dbforge->add_field("`status` enum('active','nonactive') NULL ");
		$this->dbforge->create_table("academic_year", TRUE);
		$this->db->query('ALTER TABLE  `academic_year` ENGINE = InnoDB');
		## Create Table app_categories
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`title` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`description` varchar(100) NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL ");
		$this->dbforge->add_field("`controller_name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`method_name` varchar(100) NOT NULL ");
		$this->dbforge->create_table("app_categories", TRUE);
		$this->db->query('ALTER TABLE  `app_categories` ENGINE = InnoDB');
		## Create Table app_menus
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`description` text NOT NULL ");
		$this->dbforge->add_field("`feature_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`app_category_id` int(11) NOT NULL ");
		$this->dbforge->create_table("app_menus", TRUE);
		$this->db->query('ALTER TABLE  `app_menus` ENGINE = InnoDB');
		## Create Table attribute_pages
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`value` text NOT NULL ");
		$this->dbforge->add_field("`description` text NOT NULL ");
		$this->dbforge->add_field("`type` enum('image','text') NOT NULL ");
		$this->dbforge->add_field("`page_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`tag` text NOT NULL ");
		$this->dbforge->add_field("`user_created` text NOT NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL ");
		$this->dbforge->add_field("`updated_at` datetime NOT NULL ");
		$this->dbforge->create_table("attribute_pages", TRUE);
		$this->db->query('ALTER TABLE  `attribute_pages` ENGINE = InnoDB');
		## Create Table attribute_products
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`value` text NOT NULL ");
		$this->dbforge->add_field("`description` text NOT NULL ");
		$this->dbforge->add_field("`type` enum('image','text') NOT NULL ");
		$this->dbforge->add_field("`product_id` int(11) NOT NULL ");
		$this->dbforge->create_table("attribute_products", TRUE);
		$this->db->query('ALTER TABLE  `attribute_products` ENGINE = InnoDB');
		## Create Table categories
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->add_field("`deskripsi` text NULL ");
		$this->dbforge->add_field("`date_created` datetime NULL ");
		$this->dbforge->add_field("`date_updated` datetime NULL ");
		$this->dbforge->create_table("categories", TRUE);
		$this->db->query('ALTER TABLE  `categories` ENGINE = InnoDB');
		## Create Table cities
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->add_field("`province_id` int(11) NULL ");
		$this->dbforge->create_table("cities", TRUE);
		$this->db->query('ALTER TABLE  `cities` ENGINE = InnoDB');
		## Create Table classes
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(45) NULL ");
		$this->dbforge->add_field("`description` text NULL ");
		$this->dbforge->add_field("`created_at` varchar(45) NULL ");
		$this->dbforge->add_field("`updated_at` datetime NULL ");
		$this->dbforge->create_table("classes", TRUE);
		$this->db->query('ALTER TABLE  `classes` ENGINE = InnoDB');
		## Create Table config
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` text NOT NULL ");
		$this->dbforge->add_field("`slogan` text NOT NULL ");
		$this->dbforge->add_field("`email` text NOT NULL ");
		$this->dbforge->add_field("`facebook` text NOT NULL ");
		$this->dbforge->add_field("`twiter` text NOT NULL ");
		$this->dbforge->add_field("`telp` int(11) NOT NULL ");
		$this->dbforge->add_field("`alamat` text NOT NULL ");
		$this->dbforge->create_table("config", TRUE);
		$this->db->query('ALTER TABLE  `config` ENGINE = InnoDB');
		## Create Table curriculums
		$this->dbforge->add_field("`id` int(11) NOT NULL ");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(45) NULL ");
		$this->dbforge->add_field("`description` text NULL ");
		$this->dbforge->add_field("`created_at` datetime NOT NULL ");
		$this->dbforge->add_field("`updated_at` datetime NOT NULL ");
		$this->dbforge->add_field("`status` enum('active','nonactive') NOT NULL ");
		$this->dbforge->create_table("curriculums", TRUE);
		$this->db->query('ALTER TABLE  `curriculums` ENGINE = InnoDB');
		## Create Table districts
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->add_field("`city_id` int(11) NULL ");
		$this->dbforge->add_field("`province_id` int(11) NOT NULL ");
		$this->dbforge->create_table("districts", TRUE);
		$this->db->query('ALTER TABLE  `districts` ENGINE = InnoDB');
		## Create Table features
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`key` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`description` text NOT NULL ");
		$this->dbforge->add_field("`controller_name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`method_name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`second_method_name` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ");
		$this->dbforge->create_table("features", TRUE);
		$this->db->query('ALTER TABLE  `features` ENGINE = InnoDB');
		## Create Table group_features
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`group_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`feature_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ");
		$this->dbforge->create_table("group_features", TRUE);
		$this->db->query('ALTER TABLE  `group_features` ENGINE = MyISAM');
		## Create Table groups
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->add_field("`status` enum('aktif','nonaktif') NULL ");
		$this->dbforge->add_field("`deskripsi` text NULL ");
		$this->dbforge->add_field("`date_created` datetime NULL ");
		$this->dbforge->add_field("`date_updated` datetime NULL ");
		$this->dbforge->create_table("groups", TRUE);
		$this->db->query('ALTER TABLE  `groups` ENGINE = InnoDB');
		## Create Table news
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(100) NOT NULL ");
		$this->dbforge->create_table("news", TRUE);
		$this->db->query('ALTER TABLE  `news` ENGINE = InnoDB');
		## Create Table pages
		$this->dbforge->add_field("`id` int(10) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(100) NOT NULL ");
		$this->dbforge->add_field("`intitle` varchar(100) NOT NULL ");
		$this->dbforge->add_field("`category` enum('static','dinamic','collaboration') NOT NULL DEFAULT 'static' ");
		$this->dbforge->add_field("`incontent` text NOT NULL ");
		$this->dbforge->add_field("`url` varchar(50) NOT NULL ");
		$this->dbforge->add_field("`parent_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`date_created` datetime NOT NULL ");
		$this->dbforge->add_field("`date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ");
		$this->dbforge->add_field("`is_menu` tinyint(1) NOT NULL ");
		$this->dbforge->create_table("pages", TRUE);
		$this->db->query('ALTER TABLE  `pages` ENGINE = InnoDB');
		## Create Table provinces
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->create_table("provinces", TRUE);
		$this->db->query('ALTER TABLE  `provinces` ENGINE = InnoDB');
		## Create Table religions
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`name` varchar(50) NULL ");
		$this->dbforge->add_field("`date_created` datetime NULL ");
		$this->dbforge->add_field("`date_updated` datetime NULL ");
		$this->dbforge->create_table("religions", TRUE);
		$this->db->query('ALTER TABLE  `religions` ENGINE = InnoDB');
		## Create Table students
		$this->dbforge->add_field("`nis` int(11) NOT NULL ");
		$this->dbforge->add_key("nis",true);
		$this->dbforge->add_field("`name` varchar(45) NULL ");
		$this->dbforge->add_field("`religion_id` int(11) NOT NULL ");
		$this->dbforge->add_key("religion_id",true);
		$this->dbforge->add_field("`created_at` datetime NULL ");
		$this->dbforge->add_field("`updated_at` datetime NULL ");
		$this->dbforge->add_field("`birthdate` date NULL ");
		$this->dbforge->add_field("`password` varchar(45) NULL ");
		$this->dbforge->create_table("students", TRUE);
		$this->db->query('ALTER TABLE  `students` ENGINE = InnoDB');
		## Create Table teachers
		$this->dbforge->add_field("`nik` int(11) NOT NULL ");
		$this->dbforge->add_key("nik",true);
		$this->dbforge->add_field("`name` varchar(45) NULL ");
		$this->dbforge->add_field("`religion_id` int(11) NOT NULL ");
		$this->dbforge->add_key("religion_id",true);
		$this->dbforge->add_field("`created_at` datetime NULL ");
		$this->dbforge->add_field("`updated_at` datetime NULL ");
		$this->dbforge->add_field("`password` varchar(45) NULL ");
		$this->dbforge->create_table("teachers", TRUE);
		$this->db->query('ALTER TABLE  `teachers` ENGINE = InnoDB');
		## Create Table users
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`username` varchar(50) NULL ");
		$this->dbforge->add_field("`email` varchar(30) NULL ");
		$this->dbforge->add_field("`password` varchar(50) NULL ");
		$this->dbforge->add_field("`group_id` int(11) NULL ");
		$this->dbforge->create_table("users", TRUE);
		$this->db->query('ALTER TABLE  `users` ENGINE = InnoDB');
		## Create Table villages
		$this->dbforge->add_field("`id` int(11) NOT NULL auto_increment");
		$this->dbforge->add_key("id",true);
		$this->dbforge->add_field("`nama` varchar(50) NULL ");
		$this->dbforge->add_field("`district_id` int(11) NULL ");
		$this->dbforge->add_field("`province_id` int(11) NOT NULL ");
		$this->dbforge->add_field("`city_id` int(11) NOT NULL ");
		$this->dbforge->create_table("villages", TRUE);
		$this->db->query('ALTER TABLE  `villages` ENGINE = InnoDB');
	 }

	public function down()	{
		### Drop table academic_year ##
		$this->dbforge->drop_table("academic_year", TRUE);
		### Drop table app_categories ##
		$this->dbforge->drop_table("app_categories", TRUE);
		### Drop table app_menus ##
		$this->dbforge->drop_table("app_menus", TRUE);
		### Drop table attribute_pages ##
		$this->dbforge->drop_table("attribute_pages", TRUE);
		### Drop table attribute_products ##
		$this->dbforge->drop_table("attribute_products", TRUE);
		### Drop table categories ##
		$this->dbforge->drop_table("categories", TRUE);
		### Drop table cities ##
		$this->dbforge->drop_table("cities", TRUE);
		### Drop table classes ##
		$this->dbforge->drop_table("classes", TRUE);
		### Drop table config ##
		$this->dbforge->drop_table("config", TRUE);
		### Drop table curriculums ##
		$this->dbforge->drop_table("curriculums", TRUE);
		### Drop table districts ##
		$this->dbforge->drop_table("districts", TRUE);
		### Drop table features ##
		$this->dbforge->drop_table("features", TRUE);
		### Drop table group_features ##
		$this->dbforge->drop_table("group_features", TRUE);
		### Drop table groups ##
		$this->dbforge->drop_table("groups", TRUE);
		### Drop table news ##
		$this->dbforge->drop_table("news", TRUE);
		### Drop table pages ##
		$this->dbforge->drop_table("pages", TRUE);
		### Drop table provinces ##
		$this->dbforge->drop_table("provinces", TRUE);
		### Drop table religions ##
		$this->dbforge->drop_table("religions", TRUE);
		### Drop table students ##
		$this->dbforge->drop_table("students", TRUE);
		### Drop table teachers ##
		$this->dbforge->drop_table("teachers", TRUE);
		### Drop table users ##
		$this->dbforge->drop_table("users", TRUE);
		### Drop table villages ##
		$this->dbforge->drop_table("villages", TRUE);

	}
}