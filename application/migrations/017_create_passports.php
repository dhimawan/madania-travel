<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_Passports extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('passports')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				"jamaah_id" => array('type' => 'INT'),
				"no_passport" => array('type' => 'VARCHAR', 'constraint' => '40'),
				'berlaku' => array('type' => 'DATE', 'null' => FALSE),
				'berakhir' => array('type' => 'DATE', 'null' => FALSE),
				'status' => array('type' => 'enum','constraint'=>"'active','nonactive'"),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)

			);
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('passports', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table passports...";
		$CI->dbforge->drop_table('passports');
	}
}

?>
