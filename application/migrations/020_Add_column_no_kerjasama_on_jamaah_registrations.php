<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_no_kerjasama_on_jamaah_registrations extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
      'no_kerjasama' => array('type' => 'VARCHAR','constraint' => '40'),
		);
		$CI->dbforge->add_column('jamaah_registrations', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table jamaah_registrations...";
			$this->dbforge->drop_column('jamaah_registrations', array('no_kerjasama'));
	}

}
?>