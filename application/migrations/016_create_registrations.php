<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_Registrations extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('registrations')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				"total_jamaah" => array('type' => 'INT'),
				"departure_id" => array('type' => 'INT'),
				"packet_id" => array('type' => 'INT'),
				"attribute_page_id" => array('type' => 'INT'),
				"departure_start" => array('type' => 'DATETIME', 'null' => FALSE),
				"departure_end" => array('type' => 'DATETIME', 'null' => FALSE),
				"order_name" => array('type' => 'VARCHAR', 'constraint' => '40'),
				"order_telp" => array('type' => 'VARCHAR', 'constraint' => '20'),
				"order_hp" => array('type' => 'VARCHAR', 'constraint' => '20'),
				"order_email" => array('type' => 'VARCHAR', 'constraint' => '40'),
				"address" => array('type' => 'TEXT', 'constraint' => '200'),
				"order_mou" => array('type' => 'VARCHAR', 'constraint' => '50'),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)

			);
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('registrations', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table registrations...";
		$CI->dbforge->drop_table('registrations');
	}
}

?>
