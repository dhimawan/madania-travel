<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_Ci_sessions extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('ci_sessions')) {
			$cols = array(
				"session_id" => array('type' => 'VARCHAR', 'constraint' => '40'),
				"ip_address" => array('type' => 'VARCHAR', 'constraint' => '45'),
				"user_agent" => array('type' => 'VARCHAR', 'constraint' => '120'),
				'last_activity' => array('type' => 'INT'),
				'user_data' => array('type' => 'TEXT')

			);
			// $CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('ci_sessions', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table ci_sessions...";
		$CI->dbforge->drop_table('ci_sessions');
	}
}

?>





user_data