<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_news extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		
		if(! $CI->db->table_exists('news')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'name' => array('type' => 'VARCHAR', 'constraint' => '100','null' => TRUE),
				'title' => array('type' => 'VARCHAR', 'constraint' => '100','null' => TRUE),
				'url' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),
				'description' => array('type' => 'TEXT','null' => TRUE),
				'user' => array('type' => 'INT', 'null' => TRUE),
				'news_category_id' => array('type' => 'INT', 'null' => TRUE),
				'tag' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => TRUE),
				'status' => array('type' => 'ENUM','constraint' => "'publish','unpublish'"),
				'comment_id' => array('type' => 'INT',  'null' => TRUE),
				'created_at' => array('type' => 'DATETIME'),
				'updated_at' => array('type' => 'DATETIME')
			);
			
			// Setup Keys
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('news', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table news...";
		$CI->dbforge->drop_table('news');
	}
}

?>
