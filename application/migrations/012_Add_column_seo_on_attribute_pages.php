<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_seo_on_attribute_pages extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
        'seo_keywords' => array('type' => 'varchar','constraint' => '200'),
        'seo_description' => array('type' => 'varchar','constraint' => '200'),
        'title'	=> array('type' => 'varchar','constraint' => '200')
		);
		$CI->dbforge->add_column('attribute_pages', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table attribute_pages...";
			$this->dbforge->drop_column('attribute_pages', array('title','seo_description','seo_keywords'));
	}

}
?>