<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_type_on_departures extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
        'type' => array('type' => 'varchar','constraint' => '200'),
		);
		$CI->dbforge->add_column('departures', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table departures...";
			$this->dbforge->drop_column('departures', array('type'));
	}

}
?>