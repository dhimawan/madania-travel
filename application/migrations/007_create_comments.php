<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_comments extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		
		if(! $CI->db->table_exists('comments')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'news_id' => array('type' => 'INT', 'null' => FALSE),
				'descriptions' => array('type' => 'TEXT', 'constraint' => '200', 'null' => FALSE),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'name' => array('type' => 'VARCHAR', 'constraint' => '200','null' => FALSE),
				'email' => array('type' => 'VARCHAR', 'constraint' => '200','null' => TRUE)
			);
			
			// Setup Keys
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('comments', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table comments...";
		$CI->dbforge->drop_table('comments');
	}
}

?>
