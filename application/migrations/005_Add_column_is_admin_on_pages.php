<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_is_admin_on_pages extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
        'is_admin' => array('type' => 'BOOLEAN'),
        'frontend_menu_order' => array('type' => 'INTEGER')
        
		);
		$CI->dbforge->add_column('pages', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table comment_news...";
			$this->dbforge->drop_column('pages', array('is_admin','frontend_menu_order'));
	}

}
?>