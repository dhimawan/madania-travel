<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_seo_on_pages extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		$fields = array(
        'seo_keywords' => array('type' => 'TEXT'),
        'seo_description' => array('type' => 'TEXT')
        
		);
		$CI->dbforge->add_column('pages', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table comment_news...";
			$this->dbforge->drop_column('pages', array('seo_keywords','seo_description'));
	}

}
?>