<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_image_on_news extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
        'image' => array('type' => 'varchar','constraint' => '100'),
        'seo_keywords' => array('type' => 'varchar','constraint' => '200'),
        'seo_description' => array('type' => 'varchar','constraint' => '200')
		);
		$CI->dbforge->add_column('news', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table news...";
			$this->dbforge->drop_column('news', array('image','seo_description','seo_keywords'));
	}

}
?>