<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Add_column_contact_person_on_config extends CI_Migration {
	function up()  
	{
		$CI =& get_instance();
		$fields = array(
        'is_office' => array('type' => 'BOOLEAN'),
        'contact_person' => array('type' => 'varchar','constraint' => '100','default'=>"N/A")
		);
		$CI->dbforge->add_column('config', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
			echo "Dropping table config...";
			$this->dbforge->drop_column('config', array('is_office','contact_person'));
	}

}
?>