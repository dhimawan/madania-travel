<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_add_column_config extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		$fields = array(
        'long' => array('type' => 'TEXT'),
        'lat' => array('type' => 'TEXT'),
        'google_plus' => array('type' => 'TEXT'),
        'youtube' => array('type' => 'TEXT'),
        'flicker'  => array('type' => 'TEXT')
		);
		$CI->dbforge->add_column('config', $fields);
	}

	function down() 
	{
		$CI =& get_instance();
		// if($CI->migrate->verbose)
			echo "Dropping table comment_news...";
			$this->dbforge->drop_column('config', array('long','lat','google_plus','youtube','flicker'));
		
	}

}
?>