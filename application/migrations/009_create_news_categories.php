<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_news_categories extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		
		if(! $CI->db->table_exists('news_categories')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				'name' => array('type' => 'VARCHAR', 'constraint' => '200','null' => FALSE),
				'descriptions' => array('type' => 'TEXT', 'null' => FALSE),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)
			);
			
			// Setup Keys
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('news_categories', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table news_categories...";
		$CI->dbforge->drop_table('news_categories');
	}
}

?>
