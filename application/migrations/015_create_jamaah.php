<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class  Migration_Create_Jamaah extends CI_Migration {
	function up() 
	{
		$CI =& get_instance(); 
		if(! $CI->db->table_exists('jamaah')) {
			$cols = array(
				'id' => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE),
				"no_ktp" => array('type' => 'VARCHAR', 'constraint' => '30') ,
				"nama_lengkap" => array('type' => 'VARCHAR', 'constraint' => '200') ,
				"nama_ayah_kandung" => array('type' => 'VARCHAR', 'constraint' => '200') ,
				"tempat_lahir" => array('type' => 'VARCHAR', 'constraint' => '200') ,
				"tanggal_lahir" => array('type' => 'DATE') ,
				"address" => array('type' => 'VARCHAR', 'constraint' => '200'),
				"propinsi" => array('type' => 'INT', 'constraint' => '200'),
				"kota" => array('type' => 'INT', 'constraint' => '200'),
				"kecamatan" => array('type' => 'VARCHAR', 'constraint' => '200') ,
				"kelurahan" => array('type' => 'VARCHAR', 'constraint' => '200') ,
				"kode_pos" => array('type' => 'VARCHAR', 'constraint' => '20') ,
				"no_telp" => array('type' => 'VARCHAR', 'constraint' => '20') ,
				"nama_mahram" => array('type' => 'VARCHAR', 'constraint' => '60') ,
				"no_mahram" => array('type' => 'VARCHAR', 'constraint' => '40') ,
				"pernikahan" => array('type' => 'VARCHAR', 'constraint' => '20') ,
				"jk" => array('type' => 'VARCHAR', 'constraint' => '10') ,
				"wn" => array('type' => 'VARCHAR', 'constraint' => '10') ,
				"pendidikan" => array('type' => 'VARCHAR', 'constraint' => '40') ,
				"pekerjaan" => array('type' => 'VARCHAR', 'constraint' => '40') ,
				"pergi_haji" => array('type' => 'VARCHAR', 'constraint' => '10') ,
				"hubungan_mahram" => array('type' => 'VARCHAR', 'constraint' => '30') ,
				"golongan_darah" => array('type' => 'VARCHAR', 'constraint' => '2'),
				'created_at' => array('type' => 'DATETIME', 'null' => FALSE),
				'updated_at' => array('type' => 'DATETIME', 'null' => FALSE)

			);
			$CI->dbforge->add_key('id', TRUE);
			$CI->dbforge->add_field($cols);
			$CI->dbforge->create_table('jamaah', TRUE);
		}
	}

	function down() 
	{
		$CI =& get_instance();
		if($CI->migrate->verbose)
			echo "Dropping table jamaah...";
		$CI->dbforge->drop_table('jamaah');
	}
}

?>
