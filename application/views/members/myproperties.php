<div class="container clearfix">
  <div id="banner-wrapper">
    <div class="banner-container">
      <a href="#"><img src="<?=base_url("assets/images/banner-image.jpg")?>" alt="banner" /></a>
    </div>
  <div class="title-container">
    <h2 class="page-title"><?=$title_content?></h2>
    <p class="sub-heading"><?//=$detail[0]->name?></p>
  </div>
  </div>
  <div id="content">
    <div class="breadcrumb-bar">
      <p class="breadcrumb-wrapper">
        <a href="<?=base_url()?>">Home</a> :: 
        <a class="active" href="#"><?=$title_content?></a>
      </p>
    </div>
    <div class="post-wrapper">
       <h3 class="title">Daftar Propertiku</h3>
      <p>Berikut adalah daftar properti yang sedang atau telah anda ikuti</p>
      <table class="table" border="2">
        <thead class="">
          <tr class=" head">
            <th class="table-cols">No</th>
            <th class="table-cols">Nama</th>
            <th class="table-cols">Keterangan</th>
            <th class="table-cols">Feature</th>
            <th class="table-cols">Syarat</th>
            <th class="table-cols">Tanggal Lelang</th>
            <th class="table-cols">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($product_customer as $key => $val){?>
          <tr class="grey-filled">
            <td class="table-cols"><?=($key+1)?></td>
            <td class="table-cols"><?=$val->name?></td>
            <td class="table-cols"><?=$val->description?></td>
            <td class="table-cols"><?=get_attribute_products($val->id,-1,"text",array("open" => "<dd>","end"=>"</dd>"))?></td>
            <td class="table-cols"><?php echo product_requirment($val->id)?></td>
            <td class="table-cols"><?=date("d-m-Y",strtotime($val->on_auction))?></td>
            <td class="table-cols"><?=$val->status?></td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  
  <div class="sidebar">
      <div class="side-widget">
        <?= $this->load->view($widget["sidepage_search"]);?>
        <?= $this->load->view($widget["sidepage_employee"]);?>
        <?= $this->load->view($widget["sidepage_populerproperti"]);?>
        <?= $this->load->view($widget["sidepage_newsletter"]);?>
      </div>
  </div><!-- SIDEBAR CLOSED -->   
</div><!-- CONTAINER CLOSED --> 
