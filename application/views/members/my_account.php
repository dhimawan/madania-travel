<script type="text/javascript" src="<?=base_url()?>/assets/js/app_area_product.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $("#procces_account").hide();
  $("#edit_account").click(function(){
    $(".member_label").hide();
    $("#edit_account").hide();
    $(".member_input").show();
    $("#procces_account").show();
  });

  var contact_options = { 
    target: '#message-sent',
    success : showResponse
    // success: function(request){
    //       $('#contact-loader').fadeOut('fast');
    //       $('#message-sent').fadeIn('fast');   
    //        window.location.replace = "'"+$("#base_url").val()+"'";     
    //        window.location.href =   "'"+$("#base_url").val()+"'";
    //        return true;
    // }

  }; 

  $('#member_form').validate({
    // success: function(label) {
    //   $('#contact-loader').fadeOut('fast');
    //   $('#message-sent').fadeIn('fast');
     
    // },
    submitHandler: function(form) {
        $(form).ajaxSubmit(contact_options);
        return true;
     }
  });

  // post-submit callback 


});

function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxSubmit method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxSubmit method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
        '\n\nThe output div should have already been updated with the responseText.'); 
} 
</script>

 <div class="container clearfix">
  <div id="banner-wrapper">
    <div class="banner-container">
        <a href="#"><img src="<?=base_url("assets/images/banner-image.jpg")?>" alt="banner" /></a>
    </div>
    <div class="title-container">
      <h2 class="page-title"><?=$title_content?></h2>
      <p class="sub-heading"><?=$subtitle_content?></p>
    </div>
  </div>
  <div id="content">
    <!-- <form method="post" action="<?=base_url("members/update_account")?>" > -->
    <?=form_open('members/update_account',array("id"=>"member_form"))?>
    <div class="address-block clearfix">
       <div class="contact-view">
        <h3 class="title">Data Login</h3>
        <ul class="address-line">
          <li>
            <strong>Email:</strong>
            <label id="lemail" class="member_label"><?=$member->email?></label>
            <div id="demail" class="member_input" style="display:none;"><?=form_input(array("name" => "member[email]","value"=>"$member->email",'required' => "true",'email' => "true"));?></div>
          </li>
          <li>
            <strong>Password:</strong>
            <label id="lbirthdate" class="member_label">*************</label>
            <div id="dbirthdate" class="member_input" style="display:none;">
              <?=form_password(array("name" => "member[password]","value"=>"",'required' => "true"));?></div>
          </li>
          <li >
            <strong>Konfirmasi Password:</strong>
            <label id="lbirthdate" class="member_label">*************</label>
            <div id="dbirthdate" class="member_input" style="display:none;">
              <?=form_password(array("name" => "confirm_password","value"=>"",'required' => "true"));?>
            </div>
          </li>
        </ul>
      </div> 
      <div class="contact-detail">
        
        <h3 class="title">Data Pribadi</h3>
        <ul class="address-line">
          <li>
            <strong>No Ktp:</strong>
            <label id="lno_ktp" class="member_label"><?=$member->no_ktp?></label>
            <div id="dno_ktp" class="member_input" style="display:none;"><?=form_input(array("name" => "member[no_ktp]","value"=>"$member->no_ktp",'required' => "true"));?></div>
          </li>
          <li>
            <strong>Nama:</strong>
            <label id="lname" class="member_label"><?=$member->name?></label>
            <div id="dname" class="member_input" style="display:none;"><?=form_input(array("name" => "member[name]","value"=>"$member->name",'required' => "true"));?></div>
          </li>
          <li>
            <strong>Tgl Lahir:</strong>
            <label id="lbirthdate" class="member_label"><?=date("d-m-Y",strtotime($member->birthdate))?></label>
            <div id="dbirthdate" class="member_input" style="display:none;"><?=form_input(array("name" => "member[birthdate]","value"=>"$member->birthdate",'required' => "true"));?></div>
          </li>
          <li>
            <strong>Kelamin:</strong>
            <label id="lsex" class="member_label"><?=$member->sex?></label>
            <div id="dsex" class="member_input" style="display:none;"><?=form_dropdown('member[sex]',array("pria","wanita"),"$member->sex")?></div>
          </li>
          <li>
            <strong>Agama:</strong>
            <label id="lreligion_id" class="member_label"><?=$member->religion_id?></label>
            <div id="dno_ktp" class="member_input" style="display:none;"><?=dropdown_religions("sex","member[sex]",$member->religion_id)?></div>
          </li>
          <li>
            <strong>No Telp:</strong>
            <label id="ltelp" class="member_label"><?=$member->telp?></label>
            <div id="dtelp" class="member_input" style="display:none;"><?=form_input(array("name" => "member[telp]","value"=>"$member->telp",'required' => "true"));?></div>
          </li>
          <li>
            <strong>Propinsi:</strong>
            <label id="lprovince" class="member_label"><?=$member->no_ktp?></label>
            <div id="dno_ktp" class="member_input" style="display:none;"><?=select_area("province","province",1)?></div>
          </li>
          <li>
            <strong>Kota:</strong>
            <label id="lcity" class="member_label"><?=$member->no_ktp?></label>
            <div id="dno_ktp" class="member_input" style="display:none;"><?=select_area("city","city_id",1)?></div>
          </li>
          <li>
            <strong>Kecamatan:</strong>
            <label id="ldistrict" class="member_label"><?=$member->village_id?></label>
            <div id="dno_ktp" class="member_input" style="display:none;"><?=select_area("district","district_id",$member->village_id)?></div>
          </li>
          <li>
            <strong>Kelurahan:</strong>
            <label id="lvillage" class="member_label"><?=$member->village_id?></label>
            <div id="dvillage_id" class="member_input" style="display:none;"><?=select_area("village","member[village_id]",$member->village_id)?></div>
          </li>
          <li>
            <strong>Alamat:</strong>
            <label id="laddress" class="member_label"><?=$member->address?></label>
            <div id="daddress" class="member_input" style="display:none;"><?=form_input(array("name" => "member[address]","value"=>"$member->address",'required' => "true"));?></div>
          </li>
        </ul>
      </div>
      <input class="submit-btn" name="button" type="button" id="edit_account"  value="Edit"/>
      <input class="submit-btn" name="submit" type="submit" id="procces_account"  value="Simpan" style="disply:none !important"/>
    </div>
  </div>
  <!-- </form> -->
  <?=form_close()?>
</div>
