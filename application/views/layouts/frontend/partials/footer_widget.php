<footer id="footer">
<div class="light"></div>
  <div class="foot-cont">   
    <div class="one-fourth">
      <div class="widget twit">
        <div class="header">Komentar</div>
        <div class="reviews-t">
          <div class="coda-slider-wrapper">
            <div class="coda-slider preload" id="coda-slider-1">                  
              <div class="panel">
                <div class="panel-wrapper">
                  <p>Curabitur scelerisque mauriquis diam gravida eu placerat ligula scelerisque. In vitae sem nec massa imperdiet condimentum. Donec ut mauris vel risus rutrum.</p>
                </div>
                <div class="panel-author">
                  Guest 1, designer
                </div>
              </div>
        
              <div class="panel">
                <div class="panel-wrapper">
                  <p>Donec ut mauris vel risus rutrum commodo.Curabitur scelerisque mauris quis diam gravida eu placerat ligula scelerisque. mauris quis diam gravida eu placerat ligula scelerisque. </p>
                </div>
                <div class="panel-author">
                  Guest 2, developer
                </div>
              </div>       

              <div class="panel">
                <div class="panel-wrapper">
                  <p>Nonummy a velit. Duis dapibus pulvinar. Aliquam egestas vivamus consectetuer. Donec ut mauris vel risus rutrum commodo.Vivamus dolor pede. Pharetra nec in. Vivamus quis ipsum.</p>
                </div>
                <div class="panel-author">
                  Guest 3, architector
                </div>
              </div>
                    
              <div class="panel">
                <div class="panel-wrapper">
                  <p>Facilisis pulvinar turpis. Et fusce in risus nibh ellentesque deleniti sagittis. Tincidunt nisl nunc.Vivamus dolor pede. Pharetra nec in. Vivamus quis ipsum.</p>
                </div>
                <div class="panel-author">
                  Guest 4, designer
                </div>
              </div> 
                    
              <div class="panel">
                <div class="panel-wrapper">
                  <p> Vivamus dolor pede. Pharetra nec in. Vivamus quis ipsum. Sed arcu aenean aliquam quibusdam quam!Vivamus dolor pede. Pharetra nec in. Vivamus quis ipsum.</p>
                </div>
                <div class="panel-author">
                  Guest 5, designer
                </div>
              </div>                    
            </div>
          </div>
    
          <div class="reviews-b"></div> 
        </div>                   
        <p class="autor">Anonimus</p>
      </div>
    </div>

    <div class="one-fourth">
      <div class="widget">
        <div class="header">Recent posts</div>
        <div class="post first">
          <a href="#">Nonummy a velit duis dapibus pulvinar aliquam egestas vivamus.</a>
            <div class="goto-post">
              <span class="ico-link date">22 Maret 2013</span>
            </div>
        </div>
        <div class="post">            
          <a href="#">Vestibulum nec aliquam sonec ut mauris vel risus rutrum commodo.</a>
          <div class="goto-post">
              <span class="ico-link date">22 Maret 2013</span>
          </div>
        </div>
        <div class="post last">
          <a href="#">Vivamus dolor pede</a>
          <div class="goto-post">
              <span class="ico-link date">22 Maret 2013</span>
          </div>
        </div>     
      </div>
    </div>
    <div class="one-fourth">
      <div class="widget">
          <div class="header">Now working on</div>
          <div class="navig-small">
      <a class="SliderNamePrev" href="javascript:void(0);"></a>
      <a class="SliderNameNext" href="javascript:void(0);"></a>
    </div>
          <div class="slider_container_1">
      <div class="widget_slider">
        <a href="#1">
          <img src="<?=$assets_img_path?>202x202.jpg" title="#caption-one" width="202" height="202" alt="" />
        </a>
        <a href="#2">
          <img src="<?=$assets_img_path?>202x202-1.jpg" width="202" height="202" alt="" title="#caption-four"  />
        </a>
        <img src="<?=$assets_img_path?>202x202-2.jpg" width="202" height="202" alt="" title="#caption-two" />
        <img src="<?=$assets_img_path?>202x202-3.jpg" width="202" height="202" alt="" title="#caption-three" />
      </div>
      <div id="caption-one" class="nivo-html-caption">
          <p>Curabitur scelerisque mauris quis.</p>
      </div>
      <div id="caption-two" class="nivo-html-caption">
          <p>Placerat ligula scelerisque rutrum.</p>
      </div>
      <div id="caption-three" class="nivo-html-caption">
          <p>In vitae sem nec massa imperdiet.</p>
      </div>
      <div id="caption-four" class="nivo-html-caption">
          <p>Donec ut mauris vel risus rutrum.</p>
      </div>
    </div>
      </div>
    </div>

    <div class="one-fourth">
        <div class="widget">
          <div class="header">Get in touch!</div>    
          <form class="uniform get-in-touch">
              <div class="i-h"><input name="" type="text" class="validate[required]" /><div class="i-l"><span>Nama</span></div></div>
              <div class="i-h"><input name="" type="text" class="validate[required]" /><div class="i-l"><span>E-mail</span></div></div>
              <div class="t-h"><textarea name="" id="message1" class="validate[required]"></textarea></div>
              <a href="#" class="button" title="Submit"><span><i class="submit"></i>Kirim Pesan</span></a>
          </form>
      </div>
    </div>
  </div>
  <div id="bottom">
    <div class="bottom-cont">
      <a href=""><img src="<?=$assets_img_path?>grunge/Maya-Mee-bottom.png" /></a>
    </div>
  </div>
</footer>