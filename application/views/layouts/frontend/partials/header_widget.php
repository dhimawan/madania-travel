<header id="header">
  <a class="logo" href="#"><img alt="" src="<?=$assets_img_path?>grunge/logo.png" width="349" height="60" /></a>       
  <nav>
    <ul id="nav">
      <li class="first"><a href="<?=site_url("/frontends")?>" class="act">HOMEPAGES</a></li>
      <li><a href="<?=site_url("/frontends/blogs/")?>">BLOG<span></span></a>
        <!-- <div>
          <ul>
            <li class="first"><a href="<?=site_url("/frontends/blogs/")?>">Posts List</a></li>
              <li><a href="./post.html">Post Page</a></li>
          </ul>
          <i></i>
        </div> -->
      </li>
      <li><a href="<?=site_url("/frontends/contactus/")?>">CONTACT</a></li>
    </ul>
    
        <ul class="soc-ico">
          <li><a class="rss trigger" href="#"><span>Our RSS</span></a></li>
            <li><a class="dribble trigger" href="#"><span>Follow Us on Dribbble</span></a></li>
            <li><a class="facebook trigger" href="#"><span>Follow Us on Facebook</span></a></li>
            <li><a class="twitter trigger" href="#"><span>Follow Us on Twitter</span></a></li>
            <li><a class="google trigger" href="#"><span>Follow Us on Google+</span></a></li>
                <li><a class="delicious trigger" href="#"><span>Follow Us on Delicious</span></a></li>
            <li><a class="flickr trigger" href="#"><span>Follow Us on Flickr</span></a></li>
            <li><a class="forrst trigger" href="#"><span>Follow Us on Forrst</span></a></li>
                <li><a class="lastFM trigger" href="#"><span>Follow Us on LastFM</span></a></li>
            <li><a class="linkedin trigger" href="#"><span>Follow Us on Linkedin</span></a></li>
            <li><a class="mySpace trigger" href="#"><span>Follow Us on MySpace</span></a></li>
                <li><a class="vimeo trigger" href="#"><span>Follow Us on Vimeo</span></a></li>
            <li><a class="youTube trigger" href="#"><span>Follow Us on YouTube</span></a></li>
        </ul> 
  </nav>
</header>