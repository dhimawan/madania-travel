<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class members extends Member_Controller {

  public function member_login(){
    if((!$this->input->post('email') =='') && (!$this->input->post('password')=='') ){
      $data['email']=$this->input->post('email');
      $data['password']=$this->encode($this->input->post('password'));
      $data["status"] = "aktif";
      $rows=$this->customer->find($data);
      if(count($rows) > 0){
        $member = $rows[0]->email;
        $this->session->set_flashdata('success', ' Anda berhasil login');
        $this->session->set_userdata('customer_login',$rows[0]->email);
        $this->session->set_userdata('customer_name',$rows[0]->name);
        $this->session->set_userdata('customer_no_ktp',$rows[0]->no_ktp);
        $this->session->set_flashdata('message', 'Anda berhasil login');
        redirect('/');
      }else{
        $cek_aktifasi = $this->customer->find(array("email" => $data["email"]),1);
        if($cek_aktifasi[0]->status == "new"){
          $this->session->set_flashdata('message', 'Silakahkan Aktifikan terlebih dahulu akun anda!');
          redirect("/");
        }else{
          $this->session->set_flashdata('message', 'Email / Password anda salah silakan ulangi kembali '); 
          redirect("/");
        }
      }
    }else{
        $this->session->set_flashdata('message', 'Email / Password anda salah silakan ulangi kembali ');
    redirect("/");
    }
    
  }

  public function member_logout(){
    $array_items = array('customer_login' => '', 'customer_name' => '',"customer_no_ktp" => "");
    $this->session->unset_userdata($array_items);
    $this->session->sess_destroy();
    $this->session->set_flashdata('message', 'Anda berhasil logout');
    redirect('/');
  }

  public function member_activation(){
    $data = $this->input->post();
    if(isset($data["no_ktp"])){
      $member = $this->customer->find(array("no_ktp"=>$data["no_ktp"],"status" => "new"),1);
      if(count($member)>0){
        $this->my_account($member[0]->no_ktp);
      }else{
        $member2 = $this->customer->find(array("no_ktp"=>$data["no_ktp"]),1);
        if(count($member2)>0){
          $this->session->set_flashdata('message', 'Akun Anda Telah aktif silahkan login.');
          redirect('/');

        }else{
          $this->session->set_flashdata('message', 'Maaf No ktp anda tidak terdaftar');
        redirect('/');

        }
        $this->session->set_flashdata('message', 'Silahkan Masukkan ');
        redirect('/');
      }
    }else{
      $this->session->set_flashdata('message', 'Isi no ktp terlebih dahulu!');
      redirect('/');
    }
    
  }

  public function my_account($no_ktp){
    $data["content_path"] = "members/my_account";
    $data["slideshow_widget"] = true;
    $data["title"] = "detail properti ";
    $member = $this->customer->find(array("no_ktp"=>$no_ktp),1);

    $data["content"]["title_content"] = " Akun ";
    $data["content"]["subtitle_content"] = $member[0]->name;
    $data["content"]["member"] = $member[0];
    $this->layouts->render($data);
  }

  public function update_account(){
    $this->load->library('form_validation');
    $data = $this->input->post();
    $data["member"]["password"] = $this->encode($data["member"]["password"]);
    $data["member"]["status"] = "aktif";
    $this->customer->update_attribute(array("no_ktp"=>$data["member"]["no_ktp"]),$data["member"]);
    return true;
  }

  public function update_product_customer(){
    $data = $this->input->post();
    $data["customer_no_ktp"] = $this->session->userdata("customer_no_ktp");
    $cek = $this->db->get_where("product_customers",$data)->result();
    if(count($cek)>0){
      $this->session->set_flashdata('message', 'Maaf anda telah terdaftar pada properti ini');
      redirect("frontends/detail/".$data["product_id"]);
    }else{
      $data["status"] = "minat";
      $data["created_at"] = date("Y-m-d H:i:s");
      $this->db->insert("product_customers",$data);
      $this->session->set_flashdata('message', 'data telah ditambahkan.');
      redirect("frontends/detail/".$data["product_id"]);
    }
  }

  public function myproperties(){
    $data["content_path"] = "members/myproperties";
    $data["slideshow_widget"] = true;
    $data["title"] = "Properiku";
    $data["content"]["title_content"] = " My Properti ";
    $pc = $this->db
    ->order_by("product_customers.created_at","desc")
    ->join("products","products.id = product_customers.product_id")
    ->get_where("product_customers",array("customer_no_ktp" => $this->session->userdata("customer_no_ktp")))
    ->result();
    $data["content"]["product_customer"] =  $pc;
    $this->layouts->render($data);
  }

  public function search(){
    $data= $this->input->post();

    $cari["village_id"] = array();
    if($data["province_id"] != 00){
      if($data["city_id"]!= 00){
        if($data["district_id"]!= 00){
          if($data["product"]["village_id"]!= 00){
            $cari["village_id"][] = $data["product"]["village_id"];
          }else{
            $cek_vill = $this->db->query("select villages.id as village_id  from villages where district_id = '".$data["district_id"]."' ")->result();
            if(count($cek_vill) > 0 ){
              foreach ($cek_vill as $key => $value) {
                $cari["village_id"][] = $value->village_id;
              }
            }
          }
        }else{
          $cek_vill = $this->db->query("select villages.id as village_id  
            from districts 
            join villages on villages.district_id = districts.id
            where city_id = '".$data["city_id"]."'")->result();
            if(count($cek_vill) > 0 ){
              foreach ($cek_vill as $key => $value) {
                $cari["village_id"][] = $value->village_id;
              }
            }
        }
      }else{
        $cek_vill = $this->db->query("select villages.id as village_id  
          from cities 
          join districts on districts.city_id = cities.id
          join villages on villages.district_id = districts.id
          where cities.province_id = '".$data["province_id"]."' ")->result();
            if(count($cek_vill) > 0 ){
              foreach ($cek_vill as $key => $value) {
                $cari["village_id"][] = $value->village_id;
              }
            }
        // }
      }
    }
    if(count($cari["village_id"]) >0){
      $cari["villages"] = implode(",",$cari["village_id"]);
      $where[] = "village_id in (".implode(",",$cari["village_id"]).")";
    }
    if($data["product"]["category_id"] != 00){
      $cari["category_id"] = $data["product"]["category_id"] ;
      $where[] = "category_id = ".$data["product"]["category_id"];
    }
    if(($data["product"]["selling_price"] != 00) || ($data["product"]["selling_price"] != "")){
      $cari["selling_price"] = $data["product"]["selling_price"] ;
      $where[] = "selling_price > ".$data["product"]["selling_price"];

    }
    if(count($where) > 0){
      $data["content"]["detail"] = $this->db->where(implode(" AND ", $where))->get("products")->result();
      // print_r($search); 
      $data["content_path"] = "frontends/search";
      $data["slideshow_widget"] = true;
      // $data["content"]["detail"] = $this->product->find(array('id' => $id),1);
      // $data["content"]["product_customer"] = $this->db->get_where("product_customers",array("product_id" => $id,"customer_no_ktp" => $this->session->userdata("customer_no_ktp")))->row();

      $data["content"]["title_content"] = " Pencarian ";
      // $data["content"]["subtitle_content"] = $member[0]->name;
      // $data["content"]["member"] = $member[0];
      $this->layouts->render($data);
    }else{
      $this->session->set_flashdata('message', 'Pencarian Tidak Ditemukan');
      redirect("/");
    }
    
    // print_r($cari);

  }


  public function forget_password(){
    $this->load->model("customers/customer","member");
    $data = $this->input->post();
    $dbmember = $this->db->query("select * from customers where email='".$data["email"]."'")->row();
    if(count($dbmember)){
    $new_pass = rand();
    $send_data = array(
        "from" => 'Admin Harfarogensi' ,
        "to"   =>  "$dbmember->email" ,
        "subject" => "Reset",
        "message" => "Username : $dbmember->email  <br/>  Password baru ".$new_pass." <br/> "
      );
      $this->load->library("sendmail");
      $this->sendmail->send($send_data);
      $this->member->update_attribute(array("no_ktp" => $dbmember->no_ktp),array("password" => $this->encode($new_pass)));
    }
    redirect("/");
  }

}