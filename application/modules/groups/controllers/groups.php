<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends Backend_Controller {
  function __construct()
    {
        parent::__construct();
    }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('groups');
    $crud->set_subject('group');
    // $crud->set_relation("id != '99'");
    $crud->where('id !=', '99');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>