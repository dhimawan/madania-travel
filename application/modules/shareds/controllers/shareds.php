<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shareds extends Backend_Controller {
  function __construct()
    {
        parent::__construct();
        // $widget = array(
        //   "left_menu" => "themes/backend/partials/master_sidebar_left_widget",
        //   "header_menu" => "themes/backend/partials/setting_header_widget"
        //   );
        // $this->layouts->get_widget($widget);
    }

  public function getCities($province_id){
   $data["data"] = $this->city->findall_entity_by("province_id",$province_id);
   $html = $this->load->view("cities/getOptions",$data);
    return $html;
  }

  public function getDistricts($city_id){
   $data["data"] = $this->district->findall_entity_by("city_id",$city_id);
   $html = $this->load->view("shareds/getOptions",$data);
    return $html;
  }

  public function getVillages($district_id){
   $data["data"] = $this->village->findall_entity_by("district_id",$district_id);
   $html = $this->load->view("shareds/getOptions",$data);
    return $html;
  }

 public function get_all_area_option($village){
  $vill = $this->village->find_entity_by_id($village);
  $district = $this->district->find_entity_by_id($vill->district_id);
  $city = $this->city->find_entity_by_id($district->city_id);
  // $province = $this->province->find_by_entity_by("id",$district->city_id);
  $data["province_id"] = $city->province_id;
  $data["city_id"] = $district->city_id;
  $data["district_id"] = $vill->district_id;
  echo json_encode($data);
 }

  
}
?>