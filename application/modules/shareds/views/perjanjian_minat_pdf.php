<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 3.5  (Linux)">
	<META NAME="AUTHOR" CONTENT="MYcom">
	<META NAME="CREATED" CONTENT="20120809;3520000">
	<META NAME="CHANGEDBY" CONTENT="MYcom">
	<META NAME="CHANGED" CONTENT="20130520;5310000">
	<META NAME="AppVersion" CONTENT="12.0000">
	<META NAME="DocSecurity" CONTENT="0">
	<META NAME="HyperlinksChanged" CONTENT="false">
	<META NAME="LinksUpToDate" CONTENT="false">
	<META NAME="ScaleCrop" CONTENT="false">
	<META NAME="ShareDoc" CONTENT="false">
	<STYLE TYPE="text/css">
	<!--
		@page { size: 8.27in 11.69in; margin: 1in }
		P { margin-bottom: 0.08in; direction: ltr; color: #000000; line-height: 115%; widows: 2; orphans: 2 }
	-->
	</STYLE>
</HEAD>
<BODY LANG="id-ID" TEXT="#000000" DIR="LTR">
<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><FONT SIZE=4><B>SURAT
PERNYATAAN MINAT</B></FONT></P>
<P ALIGN=CENTER STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in">Yang bertanda tangan dibawah ini,</P>
<P STYLE="margin-bottom: 0.14in">Nama            : <?=$customer->name?></P>
<P STYLE="margin-bottom: 0.14in">Alamat          : <?=$customer->address?></P>
<P STYLE="margin-bottom: 0.14in">No. KTP         : <?=$customer->no_ktp?></P>
<P STYLE="margin-bottom: 0.14in">Dengan ini menyatakan berminat
serius tehadap objek lelang tanah dan bangunan yang berada di lokasi
<B> <?=$product->address?> </B>,
dengan <B>harga</B><I><B> Rp. <?=number_format($product->selling_price)?> </B></I></P>
<P STYLE="margin-bottom: 0.14in"><I><B>(...................................................................................),</B></I></P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in">Demikian Surat Pernyataan Minat ini
saya buat dengan sebenar-benarnya dan untuk di pergunakan sebaik
mungkin.</P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in">Bandung,<?=date("d")?> <?=date("M")?> <?=date("Y")?> </P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<P STYLE="margin-bottom: 0.14in">(<?=$customer->name?>)</P>
<P STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
</BODY>
</HTML>