<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE></TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 3.5  (Linux)">
	<META NAME="AUTHOR" CONTENT="MYcom">
	<META NAME="CREATED" CONTENT="20120809;3560000">
	<META NAME="CHANGEDBY" CONTENT="dani himawan">
	<META NAME="CHANGED" CONTENT="20130731;19202200">
	<META NAME="AppVersion" CONTENT="12.0000">
	<META NAME="DocSecurity" CONTENT="0">
	<META NAME="HyperlinksChanged" CONTENT="false">
	<META NAME="LinksUpToDate" CONTENT="false">
	<META NAME="ScaleCrop" CONTENT="false">
	<META NAME="ShareDoc" CONTENT="false">
	<META NAME="CHANGEDBY" CONTENT="dani himawan">
	<STYLE TYPE="text/css">
	<!--
		@page { size: 8.27in 11.69in; margin: 2in }
		P { margin-bottom: 0.08in; direction: ltr; color: #000000; line-height: 115%; widows: 2; orphans: 2 }
		P.western { so-language: en-US }
		A:link { so-language: zxx }
	-->
	</STYLE>
</HEAD>
<BODY LANG="id-ID" TEXT="#000000" DIR="LTR">
<P LANG="en-US" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.14in">
<FONT FACE="Times new roman, serif"><B>SURAT PERJANJIAN KERJASAMA</B></FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0.14in">
<BR><BR>
</P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.14in">
<FONT FACE="Times new roman, serif">Kami yang bertanda tangan di
bawah ini:</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.14in">
<FONT FACE="Times new roman, serif">PIHAK I</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Haryanto, HARFA PROGENCY (Mitra
Lelang), yang beralamat kantor di Jl. <SPAN LANG="id-ID">Batununggal
Indah Raya No.23</SPAN> Bandung, No.KTP 3273222609700003</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Selanjutnya dalam perjanjian ini
disebut sebagai PIHAK KESATU</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<BR>
</P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0.14in">
<FONT FACE="Times new roman, serif">PIHAK II</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif"><?=$customer->name?> Pembeli objek
lelang, yang beralamat di <?=$customer->address?>. No KTP <?=$customer->no_ktp?></FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Selanjutnya dalam perjanjian ini
disebut sebagai PIHAK KEDUA</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<BR>
</P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Selanjutnya dalam perjanjian ini
Pihak Kesatu dan Pihak Kedua disebut PARA PIHAK</FONT></P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<BR>
</P>
<P LANG="en-US" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Kedua belah pihak telah
bersepakat untuk melakukan pembelian dan proses pengurusan objek
lelang tanah dan bangunan yang beralamat di <B>Jl</B> <SPAN LANG="id-ID"><B><?=$product->address?></B></SPAN> dengan harga sebesar <BR><I><B>RP.
</B></I><SPAN LANG="id-ID"><I><B><?=number_format($product->selling_price)?>
</B></I></SPAN><!-- <I><B>(</B></I><SPAN LANG="id-ID"><I><B>...................................................................................</B></I></SPAN><I><B>)</B></I> --></FONT></P>
<OL>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">Pelunasan
	dilakukan pada saat selesai pelaksanaan lelang,setelah diterima
	<B>Surat Penunjukan Pemenang Lelang</B></FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">PIHAK
	KESATU akan melakukan proses pengurusan objek lelang tanah dan
	bangunan yang beralamat di   <SPAN LANG="id-ID"><B><?=$product->address?> </B></SPAN>
	yang dimaksud sampai selesai.</FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">PIHAK
	KESATU akan menserah terimakan objek lelang tersebut secara
	keseluruhan dengan dalam waktu <SPAN LANG="id-ID">5 </SPAN>bulan
	setelah pelaksanaan lelang.</FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">Dalam
	masa menunggu pengurusan objek lelang yang selama <SPAN LANG="id-ID">5</SPAN>
	bulan tersebut, PIHAK KESATU berkewajiban memberikan laporan
	perkembangan terakhir yang berhak diketahui oleh PIHAK <BR>KEDUA,
	apabila <SPAN LANG="id-ID">5</SPAN> bulan lebih tidak terlaksana
	oleh PIHAK KESATU maka PIHAK KESATU akan mengembalikan uang dari
	PIHAK KEDUA.</FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">Apabila
	ada keterlambatan dalam masalah waktu ketika masa pengurusan objek
	lelang tersebut, maka PIHAK KESATU akan menyampaikan hal tersebut
	kepada PIHAK KEDUA dan PIHAK KESATU berkewajiban menyelesaikan
	masalah tersebut sampai selesai.</FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">Jika
	terjadi Pelunasan Objek Lelang oleh Debitur yang menyebabkan
	terjadinya Cancel Lelang, maka PIHAK KESATU akan mengembalikan
	seluruh uang yang sudah dikeluarkan oleh PIHAK KEDUA sebelumnya.</FONT></P>
	<LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 150%"><FONT FACE="Times new roman, serif">Apabila
	Pihak Kedua membatalkan pembelian objek lelang ini, maka Down Paymen
	hangus.</FONT></P>
</OL>
<P ALIGN=JUSTIFY STYLE="margin-left: 0.5in; margin-bottom: 0in; line-height: 150%">
<BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-left: 0.25in; margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Demikian perjanjian kerjasama ini
dibuat dengan sebenarnya dan untuk dapat dipergunakan sebagaimana
mestinya.</FONT></P>
<P ALIGN=JUSTIFY STYLE="margin-left: 0.25in; margin-bottom: 0in; line-height: 150%">
<BR>
</P>
<P ALIGN=JUSTIFY STYLE="margin-left: 0.25in; margin-bottom: 0in; line-height: 150%">
<FONT FACE="Times new roman, serif">Bandung, <SPAN LANG="id-ID"><?=date("d")?> <?=date("M")?> <?=date("Y")?></SPAN></FONT></P>

<TABLE WIDTH=100% CELLPADDING=7 CELLSPACING=0>
	<COL WIDTH=294>
	<COL WIDTH=293>
	<TR VALIGN=TOP>
		<TD WIDTH=50% STYLE="border: none; padding: 0in">
			<P ALIGN=JUSTIFY STYLE="margin-left: 0.25in; margin-bottom: 0in"><FONT FACE="Times new roman, serif">PIHAK
			KESATU</FONT></P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=JUSTIFY>            <FONT FACE="Times new roman, serif">Haryanto</FONT></P>
		</TD>
		<TD WIDTH=50% STYLE="border: none; padding: 0in">
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><FONT FACE="Times new roman, serif">PIHAK
			KEDUA</FONT></P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=CENTER STYLE="margin-bottom: 0in"><BR>
			</P>
			<P ALIGN=CENTER><FONT FACE="Times new roman, serif"><?=$customer->name?></FONT></P>
		</TD>
	</TR>
</TABLE>
<P ALIGN=JUSTIFY STYLE="margin-left: 0.25in; margin-bottom: 0in; line-height: 150%">
<BR>
</P>

<P LANG="en-US" CLASS="western" STYLE="margin-bottom: 0.14in"><FONT FACE="Times new roman, serif"><FONT SIZE=5><U><B>Tahapan
Lelang </B></U></FONT><FONT SIZE=5><B>:</B></FONT></FONT></P>
<P LANG="en-US" CLASS="western" STYLE="margin-bottom: 0.14in"><BR><BR>
</P>
<OL>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>a.Menyerahkan Surat
	Minat Penawaran Harga All-in Lelang</FONT></FONT></P>
</OL>
<P LANG="en-US" CLASS="western" STYLE="margin-left: 0.5in; margin-top: 0.08in; line-height: 100%">
<FONT FACE="Times new roman, serif"><FONT SIZE=4>b.Membayar Down
Payment sebesar </FONT><FONT SIZE=4><SPAN LANG="id-ID">10</SPAN></FONT><FONT SIZE=4>%
dari nilai transaksi</FONT></FONT></P>
<OL START=2>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>Menunggu
	penjadwalan lelang.</FONT></FONT></P>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>H-1 Lelang membayar
	setoran jaminan sebesar 35%</FONT></FONT></P>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>Hari H Lelang
	membayar Pelunasan Lelang setelah menerima Surat Penunjukan Pemenang
	Lelang</FONT></FONT></P>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>H+10 Menerima Surat
	–surat Lengka</FONT><FONT SIZE=4><SPAN LANG="id-ID">p</SPAN></FONT><FONT SIZE=4>,antara
	lain :</FONT></FONT></P>
</OL>
<P LANG="en-US" CLASS="western" STYLE="margin-left: 0.5in; margin-top: 0.08in; line-height: 100%">
<FONT FACE="Times new roman, serif"><FONT SIZE=4>Sertifikat,
Sertifikat Hak Tanggungan, IMB, Risalah Lelang, dll</FONT></FONT></P>
<OL START=6>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>Menunggu proses
	eksekusi / pengosongan objek lelang</FONT></FONT></P>
	<LI><P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in; line-height: 100%">
	<FONT FACE="Times new roman, serif"><FONT SIZE=4>Serah terima kunci,
	yang dilakukan paling lambat </FONT><FONT SIZE=4><SPAN LANG="id-ID">5</SPAN></FONT>
	<FONT SIZE=4>bulan setelah pelaksanaan lelang</FONT> <FONT SIZE=4>dalam
	keadaan kosong (clear and clean) di lokasi objek lelang</FONT></FONT></P>
</OL>
<P LANG="en-US" CLASS="western" STYLE="margin-top: 0.08in"><BR><BR>
</P>
<P LANG="en-US" CLASS="western" STYLE="margin-bottom: 0.14in"><BR><BR>
</P>

</BODY>
</HTML>