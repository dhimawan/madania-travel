<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Group_Features extends Backend_Controller {
  function __construct(){
      parent::__construct();
    }

  // function index(){
  //   $crud = new grocery_CRUD();
  //   $crud->set_table('group_features');
  //   $crud->set_subject('group_feauters');
  //   $crud->set_relation('group_id','groups','nama');
  //   $crud->set_relation('feature_id','features','name');
  //   // $crud->set_relation('officeCode','offices','city');
  //   // $crud->set_rules('email','email','required');
  //   // $crud->required_fields("username","password","email");
  //   $data["content"]["groc_view"] = $crud->render();
  //   $this->layouts->render($data);
  // }

  public function index(){
    $data["content"]["title_content"] = "Group Features";
    $data["content"]["groups"] = $this->db->get_where("groups",array("id !="=>"99"))->result();
    $data["content"]["features"] = $this->feature->find(array());
    $data["content_path"] = "group_features/index";
    $this->layouts->render($data);
  }

  public function create(){
    $post = $this->input->post();
    $this->db->query("delete from group_features");
    foreach ($post["group_id"] as $key => $value) {
      foreach ($value as $index => $val) {
        $this->db->insert("group_features",array("feature_id" => $key,"group_id"=>$val,"created_at"=>date("Y-m-d H:i:s")));
      }
    }
    $this->session->set_flashdata('message', 'Data berhasil disimpan');
    redirect("group_features/index");
  }
}
?>