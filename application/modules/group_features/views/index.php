<script type="text/javascript">
$(document).ready(function(){
  $(".checkall").click(function(){
    var id = $(this).attr("id");
    if($(this).is(":checked")==true){
      // console.log(); 
      $("."+id).attr("checked",true); 
    }else{
      $("."+id).attr("checked",false); 
    }
  });
});


</script>
<div class="row-fluid" id="main-content-row">
  <h2><?=$title_content?></h2>
  <div class="span12" id="main-content-span">
    <div class="ev-form form-div">
      <form method="post" action="<?=base_url("group_features/create")?>">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th rowspan="2">Fitur Nama(C/M/M)</th>
            <th colspan="<?=count($groups)?>" style="text-align:center">Group</th>
          </tr>
          <tr>
            <?php foreach ($groups as $g) { ?>
              <td style="text-align:center"><?=$g->nama?> <input type="checkbox" name="checkall" value="<?=$g->id?>" id="cek_group_<?=$g->id?>" class="checkall" checked="true" /> </td>
            <?php }?>
          </tr>
        </thead>
        <tbody id="tbody_detail">
          <?php foreach ($features as $f) { ?>
          <tr>
            <td><?=$f->key?>(<?=$f->controller_name?>/<?=$f->method_name?>/<?=$f->second_method_name?>)</th>
            <?php foreach ($groups as $g) { ?>
            <?php $gf = $this->db->get_where("group_features",array("group_id" => $g->id,"feature_id" => $f->id))->row(); ?>
              <td style="text-align:center"><input type="checkbox" name="group_id[<?=$f->id?>][]" value="<?=$g->id?>" <?=(empty($gf->group_id) ? "" : 'checked="true"')?> class="cek_group_<?=$g->id?>" /></td>
            <?php }?>
          </tr>
          <?php }?>
        </tbody>
       </table>
       <input type="submit" value="simpan">
     </form>
   </div>
  </div>
</div>

