<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curriculums extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('curriculums');
    $crud->set_subject('Kurikulum');
    $data["content"]["title_content"] = "Kurikulum";
    $crud->set_theme('datatables');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>