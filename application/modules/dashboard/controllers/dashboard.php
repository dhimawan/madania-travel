<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Backend_Controller {
  
  function __construct(){
    parent::__construct();
    $this->load->library('grocery_CRUD'); 
    $this->load->library("Gravatar");
  }

  public function index(){
    $this->load->model("packets/packet","packet_model");
    $pages = $this->db->get_where("pages",array("url" => "daftar_paket"))->row();
    $data["content"]["attribute_pages"] = $this->db->get_where("attribute_pages",array("page_id"=> $pages->id,"type" => "text"))->result();
    $data["content"]["packets"] =$this->packet_model->packet_prices_array();
    $data["content"]["jamaah"] = $this->db->limit(5)->order_by("updated_at desc")->get("jamaah")->result();
    $data["content"]["contactus"] = $this->db->limit(5)->where("parent_id","")->order_by("updated_at desc")->get("contactus")->result();
    $data["content"]["title_content"] = "Tambah Properti Marketing";
    $data["content_path"] = "dashboard/index";
    $this->layouts->render($data);
    
  }
  
  function pages(){
    $crud = new grocery_CRUD();

      $crud->set_theme('datatables');
      $crud->set_table('pages');
      $crud->set_subject('Pages');      
      $output = $crud->render();
      $this->_example_output($output);
  }

  function blog(){
    $crud = new grocery_CRUD();

      $crud->set_theme('datatables');
      $crud->set_table('blogs');
      // $crud->set_relation('officeCode','offices','city');
      // $crud->display_as('officeCode','Office City');
      $crud->set_subject('blog');
      
      // $crud->required_fields('lastName');
      
      // $crud->set_field_upload('file_url','assets/uploads/files');
      
      $output = $crud->render();
      $this->_example_output($output);
  }

  function _example_output($output = null)
  {
    $this->load->view('example.php',$output); 
  }
  

  function make_base(){
    $this->load->library('VpxMigration');
    $this->vpxmigration->generate();
  }

  function migration(){
    // $this->load->library('migration');
    if ( ! $this->migration->current())
    {
      show_error($this->migration->error_string());
    }
  }

  public function get_total_jamaah(){
    $current = date("Y");
    $prev= $current - 1;
    $group = $this->user_model->group_name($this->session->userdata("email"));
    if($group == "superadmin" || $group == "company_admin"){
       $curr_data = $this->db
        ->select("count(jamaah_registrations.id) as current_total")
        ->join("registrations","registrations.id = jamaah_registrations.registration_id and YEAR(registrations.departure_start) =$current ")
        ->get("jamaah_registrations")->row();
        $prev_data = $this->db
        ->select("count(jamaah_registrations.id) as current_total")
        ->join("registrations","registrations.id = jamaah_registrations.registration_id and YEAR(registrations.departure_start) =$prev ")
        ->get("jamaah_registrations")->row();
    }else{
      $nokerjasama = $this->user_model->no_kerjasama($this->session->userdata("email"));
      $curr_data = $this->db
        ->select("count(jamaah_registrations.id) as current_total")
        ->where("jamaah_registrations.no_kerjasama",$no_kerjasama)
        ->join("registrations","registrations.id = jamaah_registrations.registration_id and YEAR(registrations.departure_start) =$current ")
        ->get("jamaah_registrations")->row();
        $prev_data = $this->db
        ->select("count(jamaah_registrations.id) as current_total")
        ->where("jamaah_registrations.no_kerjasama",$no_kerjasama)
        ->join("registrations","registrations.id = jamaah_registrations.registration_id and YEAR(registrations.departure_start) =$prev ")
        ->get("jamaah_registrations")->row();
    }
    echo $prev_data->current_total."{{pisah}}".$curr_data->current_total;
  }

  public function sendEmail(){
    // $this->load->model("app_config/app_config","");
    // user_id
    $email = $this->session->userdata("email");
    $user_id = $this->user_model->user_id($email);
    $params = $this->input->post();
    $insert = array(
      "email"         => $params["sendmail"]["email"] ,
      "title"         => $params["sendmail"]["title"] ,
      "description"   => $params["sendmail"]["description"] ,
      "attachments"   => $params["sendmail"]["attachments"] ,
      "mail_type"     => $params["sendmail"]["mail_type"] ,
      "parent_id"     => $params["sendmail"]["parent_id"] ,
      "user_id"       => $this->user_model->user_id($email),
      "created_at"    => date("Y-m-d H:i:s"),
      "updated_at"    => date("Y-m-d H:i:s")
    );
    if($this->db->insert("contactus",$insert)){
      $this->load->library("sendmail");
      // $data = $this->sendmail->send($email);
      $this->procces_sendMail($insert);
      // echo "sendmail";
    }
    // print_r($insert);
 // [sendmail] => Array
 //        (
 //            [email] => danzztakezo@gmail.com
 //            [title] => [Replay]Test halaman kontak
 //            [description] => asdasd
 //            [attachments] => 
 //            [mail_type] => reply
 //            [parent_id] => 12
 //        )

    // print_r($params);

  }

  public function procces_sendMail($params){
    $this->load->library("sendmail");
    $this->load->helper("frontend");
    $data["email"]["packet"] = $this->page->get_pages("daftar_paket");
    $data["email"]["news"] = $this->db->limit("5")->order_by("updated_at","desc")->get("news")->result();
    $data["params"]["mail"] = $this->db->get_where("contactus",array("parent_id"=> $params["parent_id"]))->row();
    $data["params"]["data"] = $params;
    $split = explode(";",$params["attachments"]);
    $attachments = array();
    // $path = set_realpath('assets/uploads/mail/');
    foreach ($split as $key => $value) {
      if(!empty($value)){
        $attachments[] =  FCPATH."assets/uploads/mail/".$value."";
      }
    }
      // if(!empty($value)){
        $email = array(
        "from"        => 'info@madaniatravel.com' ,
        "to"          => "".$params["email"]."",
        "subject"     => "".$params['title']."",
        "message"     => $this->load->view("themes/".$this->config->item("backend_layout")."/partials/_tmp_contact_sendEmail",$data,true),
        "attachments" => $attachments
        );
        // if(!empty($params["attachments"])){
        //   $email["attachments"] = 
        // }
        $response = $this->sendmail->send($email);
        print_r($response);
    //   }
      
    // }
    
    
   
    // $this->load->view("themes/".$this->config->item("backend_layout")."/partials/_tmp_contact_sendEmail",$data,true);
    // return true;
  }

  public function example_email(){
    $this->load->helper("frontend");
    $data["email"]["packet"] = $this->page->get_pages("daftar_paket");
    $data["email"]["news"] = $this->db->limit("5")->order_by("updated_at","desc")->get("news")->result();
    $data["params"]["mail"] = $this->db->get_where("contactus",array("parent_id"=> 1))->row();
    $this->load->view("themes/".$this->config->item("backend_layout")."/partials/_tmp_contact_sendEmail",$data);
  }

  public function get_departures(){
    $data = $this->db
            ->select("departures.name,departures.startdate,departures.enddate,departures.id,,departures.total_tax,attribute_pages.name as attribute_pages_name,attribute_pages.value")
            ->join("attribute_pages","attribute_pages.id = departures.attribute_page_id")
            ->order_by("departures.startdate desc")
            ->where("departures.status","active")
            ->get("departures")
            ->result();
    $year = date('Y');
    $month = date('m');

    $event = array();
    foreach ($data as $key => $value) {
      $event[] = array(
                  'id' => $value->id,
                  'title' => "$value->attribute_pages_name $value->name (".date("d",strtotime($value->startdate))." S/D ". date("d",strtotime($value->enddate)).")",
                  'start' => "".date("Y-m-d",strtotime($value->startdate))."",
                  'name'  => "$value->attribute_pages_name",
                  'total_tax' => "$value->total_tax",
                  'icon'  => "<img src='".base_url("assets/uploads/pages/".$value->value)."'"
                );
    }
  echo json_encode($event);
  }

  public function detail_departure(){
    $params = $this->input->post();
    $data["data"] = $this->db
            ->select("departures.name,departures.startdate,departures.enddate,departures.id,,departures.total_tax,attribute_pages.name as attribute_pages_name,attribute_pages.value,attribute_pages.description")
            ->join("attribute_pages","attribute_pages.id = departures.attribute_page_id")
            ->order_by("departures.startdate desc")
            ->where("departures.id",$params["id"])
            ->get("departures")
            ->row();
    return $this->load->view("dashboard/partials/_pop_detail_departure",$data);
  }
 
  public function detai_contactus(){
    $params = $this->input->post();
    $data["data"] = $this->db
        ->where("id",$params["id"])
        ->get("contactus")->row();
    return $this->load->view("dashboard/partials/_detailContactUs",$data);
  }

}
