<div class="span8" id="main-content-span">
  <div class="tabbable">

    <ul class="nav nav-tabs">
      <?php foreach ($attribute_pages as $key => $value) { ?>
        <li class="<?=($key==0 ? 'active' : '' )?>">
          <a href="#tab_<?=$value->id?>" data-toggle="tab"><?=$value->name?></a>
        </li>
      <?php } ?>
    </ul>

    <div class="tab-content">
      <?php foreach ($attribute_pages as $key => $value) { ?>
        <div class="tab-pane <?=($key==0 ? 'active' : '' )?>" id="tab_<?=$value->id?>" >
          <h3><?=$value->name?></h3>
          <?=$value->description?>
          <table class="table table-striped">
            <thead>
              <tr>
                <th rowspan="2" style="vertical-align:top;text-align:center" >#</th>
                <th rowspan="2" style="vertical-align:top;text-align:center">Nama</th>
                <th width="300px" colspan="4" style="text-align:center">Biaya</th>
                <th rowspan="2" style="vertical-align:top;text-align:center">Deskripsi</th>
              </tr>
              <tr>
                <th> Single</th>
                <th> Double</th>
                <th> Triple</th>
                <th> Quad</th>
              </tr>
            </thead>
            <tbody class="tbody_depature">
              <?php if(count($packets[$value->id]) > 0){ ?>
                <?php  foreach($packets[$value->id] as $index => $packet) { ?>
                <tr>
                  <td><?=($index+1)?></td>
                  <td><?=$packet["packet_name"]?></td>
                  <td><?=$packet["min_single_price"]?> s/d <?=$packet["max_single_price"]?> </td>
                  <td><?=$packet["min_double_price"]?> s/d <?=$packet["max_double_price"]?> </td>
                  <td><?=$packet["min_triple_price"]?> s/d <?=$packet["max_triple_price"]?> </td>
                  <td><?=$packet["min_quad_price"]?> s/d <?=$packet["max_quad_price"]?> </td>
                  <td><?=$packet["packet_description"]?></td>
                </tr>
                <?php } ?>
              <?php } ?>
            </tbody>
          </table>
        </div>
      <?php } ?>
    </div>
  </div>
</div>