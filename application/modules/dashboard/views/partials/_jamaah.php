<div class="span4" id="main-content-span">
  <div class="span12" id="main-content-span">
    <h2><i class="glyphicon-pie-chart"></i>Jamaah</h2>
    <div class="well">
      <a class="btn btn-warning btn-dashboard-big" href="#">
        <i class="icon-user icon-large"></i>
        Total Jamaah <?=date("Y")-1?>
        <label id="prev_data"> 0 </label>
      </a>
      <a class="btn btn-success btn-dashboard-big" href="#">
        <i class="icon-user icon-large"></i>
        Total Jamaah <?=date("Y")?>
        <label id="curr_data"> 0 </label>
      </a>
      <a class="btn btn-danger btn-dashboard-big-text" href="<?=base_url("jamaah_registrations/importManivest")?>">
        <i class="icon-user icon-large"></i>
        <label id="curr_data">Import Manivest</label>
      </a>
    </div>
  </div>
</div><!-- END #main-content-span -->