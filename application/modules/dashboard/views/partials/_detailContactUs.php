
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?=$data->title?></h4>
      </div>
      <div class="modal-body">
        
        <fieldset id="detailEmail_<?=$data->id?>">

        </fieldset>
        <ul class="media-list">
          <li class="media">
            <a class="pull-left" href="#">
              <img class="media-object"  src="<?=$this->gravatar->get_gravatar("$data->email");?>" alt="<?=$data->name?>">
            </a>
            <div class="media-body">
              <h4 class="media-heading"><?=$data->email?>(<?=$data->name?> / <?=$data->telp?>)</h4>
              <p><?=strip_tags($data->description )?></p>
            </div>
          </li>
        </ul>

        <hr/>
        Balas Pesan <br/>
        <form class="form-horizontal" id="formReplayEmail_<?=$data->id?>" action="<?=base_url("dashboard/send_mail")?>">
          <fieldset id="<?=$data->id?>" class="" >
            <div class="form-group clearfix">
              <label  for="focusedInput">Email</label>
              <div class="form-group">
                <input id="email" class="focused form-control" style="width:100%" type="text"  name="sendmail[email]" value="<?=$data->email?>">
                <i class="help-inline">Gunakan Tanda koma(,) untuk menambahkan email lainnya.</i>
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="" for="focusedInput">Judul Pesan</label>
              <div class="form-group">
                <input id="title" class="focused form-control" style="width:100%"  type="text" name="sendmail[title]" value="[Replay]<?=$data->title?>">
              </div>
            </div>
            <div class="form-group clearfix">
              <label class="" for="focusedInput">Pesan</label>
                <textarea id="description" class="focused form-control" style="width:100%" name="sendmail[description]"></textarea>
            </div>
            <div class="form-group">
              <label class="" for="focusedInput">Attachment</label>
              <div class="form-group">
                <input type="file" name="userdata" id="attachment_<?=$data->id?>">
                <input type="hidden" name="sendmail[attachments]" class="filename_<?=$data->id?>" id="attachments">
                <input type="hidden" name="sendmail[mail_type]" value="reply">
                <input type="hidden" name="sendmail[parent_id]" value="<?=$data->id?>">
                <span id="div_attachment_<?=$data->id?>"> </span>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
        <input type="reset" class="btn btn-default" data-dismiss="modal" value="close"/>
        <!-- <button type="button" class="btn btn-primary" onclick="replayEmail(<?=$data->id?>)" id="reply_<?=$data->id?>">Balas</button> -->
        <button id="sendButton_<?=$data->id?>" type="button" class="btn btn-primary" onclick="sendEmail(<?=$data->id?>)" >Kirim</button>
        <span id="loading_mail_<?=$data->id?>"></span>
      </div>
    </div><!-- /.modal-content -->


<script type="text/javascript">
$(document).ready(function(){
  $("#attachment_<?=$data->id?>").uploadify({
      'multi'     : true,
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/mail',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#div_attachment_<?=$data->id?>").append('<span class="clearfix" id="attFile_'+ID+'"><i class="glyphicon-attach" rel="tooltip" data-original-title=".glyphicon-attach"></i> '+fileObj['name']+' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a onclick="deleteAttachment('+ID+');return false" class="glyphicon-remove-2" > </a> <span>');

        var last = $(".filename_<?=$data->id?>").val();
        if(last == undefined){
          last = "";
        }else{
          last = last+";"; 
        }
        $(".filename_<?=$data->id?>").val(last+''+fileObj['name']);
        
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
});
 

tinymce.init({
  mode : "exact",
  elements :"page_incontent",
});
</script>