<ul class="media-list">
  <li class="media">
    <a class="pull-left" href="#">
      <img class="media-object"  src="<?=base_url("assets/uploads/pages/$data->value")?>" alt="<?=$data->attribute_pages_name?>" width="200px">
    </a>
    <div class="media-body">
      <h4 class="media-heading"><?=$data->attribute_pages_name?>(<?=$data->name?>)</h4>
      <i><?=date("d-m-Y",strtotime($data->startdate))?> s/d <?=date("d-m-Y",strtotime($data->enddate))?></i>
      <p><?=strip_tags($data->description)?></p>
    </div>
  </li>
</ul>