
<?=$this->load->view("dashboard/partials/_jamaah")?>
<?=$this->load->view("dashboard/partials/_info_packet")?>
</div><!-- END main-content-row -->

<!-- BEGIN #main-content-row -->
<div class="row-fluid" id="main-content-row">
  <div class="span8" id="main-content-span">
    <h2><i class="glyphicon-pie-chart"></i> Jadwal</h2>
    <div class="well">
      <div id='calendar'></div>
    </div>
  </div>

  <div class="span4" id="main-content-span">
    <div class="span12" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>Pesan via Kontak Kami</h2>
    </div>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nama</th>
          <th>Judul</th>
          <th>Deskripsi</th>
          <th>Perlakuan</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($contactus as $key => $value) { ?>
        <tr>
          <td><?=$value->name?></td>
          <td><?=$value->title?></td>
          <td><?=word_limiter($value->description,6)?></td>
          <td>
            <a href="#" class="btn btn-info" onclick="detailContactUs(<?=$value->id?>);return false;">
              <i class="glyphicon-message-flag"></i> Detail
            </a>
          </td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>

  
</div><!-- END main-content-row -->

<!-- BEGIN #main-content-row -->
<!-- <div class="row-fluid" id="main-content-row">
  <div class="span6" id="main-content-span">
    <div class="span12" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>Data Jamaah Baru</h2>
    </div>
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Nama</th>
          <th>Alamat</th>
          <th>Jenis Kelamin</th>
          <th>Tgl Lahir</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($jamaah as $key => $value) { ?>
        <tr>
          <td><?=$value->nama_lengkap?></td>
          <td><?=$value->address?></td>
          <td><?=$value->jk?></td>
          <td><?=date("d-m-Y",strtotime($value->tanggal_lahir))?></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div> -->
<!-- END #main-content-span -->
<div id="detailDeparture" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="mySignupModalLabel">Jadwal <strong>Keberangkatan</strong></h4>
  </div>
  <div class="modal-body" id="bodyDetailDeparture">
    
  </div>
</div>

<div class="modal fade" id="detailContactus">
  <div class="modal-dialog" id="detailContactUsBody">
    
  </div>
</div>

<script type="text/javascript">

function detailContactUs(id){
  var image = "<img src='<?=base_url("assets/images/load.gif")?>'>Loading ....";

  if(id!=""){
    $("#detailContactus").modal("show");
    $("#detailContactUsBody").html(image);
    $.ajax({
        type: "POST",
        url: "<?=base_url('dashboard/detai_contactus')?>",
        data: { id: id }
      }).done(function(msg){
          $("#detailContactUsBody").html(msg);
          // $('#detailDeparture').modal('show');
      });
    // $("#detailEmail_"+id).show();
    // $("#replayEmail_"+id).hide();
  }
}

function replayEmail(id){
  if(id!=""){
    $("#detailEmail_"+id).hide();
    $("#replayEmail_"+id).show();
    $("#sendButton_"+id).show();
    $("#reply_"+id).hide();

  }
}

function sendEmail(id){
  var data = $("#formReplayEmail_"+id).serialize();
  var image = "<img src='<?=base_url("assets/images/load.gif")?>'>Mengirim ....";
  $("#sendButton_"+id).show();
  $("#loading_mail_"+id).html("");
  $.ajax({
    type: "POST",
    url: "<?=base_url("dashboard/sendEmail")?>",
    data: data,
    success: function(response){
      // alert(response);
      window.location.href ="<?=base_url('dashboard/index')?>";
    },
    beforeSend: function(){

      $("#sendButton_"+id).hide();
      $("#loading_mail_"+id).html(image);
    }
  });


}

$(document).ready(function(){
  var base_url=$("#base_url").val();
  var url="<?=base_url()?>";
    $('#calendar').fullCalendar({
    
      editable: false,
      events: base_url+"dashboard/get_departures",
      eventDrop: function(event, delta) {
        
      },
      eventClick: function(event) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('dashboard/detail_departure')?>",
          data: { id: event.id }
        }).done(function(msg){
            $("#bodyDetailDeparture").html(msg);
            $('#detailDeparture').modal('show');
        });
      },
      loading: function(bool) {
        if (bool) $('#loading').show();
        else $('#loading').hide();
      }
      
    });
  $.get(url+"dashboard/get_total_jamaah", function(data) {
    var record = data.split("{{pisah}}");
    $("#prev_data").html(record[0]);
    $("#curr_data").html(record[1]);
  });
});
</script>

<style type="text/css"> 
.replayEmail{
  display: none;
}
</style>