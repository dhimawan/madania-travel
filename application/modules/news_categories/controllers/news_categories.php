<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News_categories extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('news_categories');
    $crud->set_subject('news_categories');
    $data["content"]["title_content"] = "Kategori Berita";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }
}
?>