<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once ('application/modules/commons/models/common_model.php');

class User extends Common_Model{
  function __construct(){
    parent::__construct("users");
  }

  public function is_admin($email){
   $user = $this->db->get_where("users",array("email"=>$email))->row();
   $group = $this->db->get_where("groups",array("id"=>$user->group_id))->row();
   $status = false;
   if($group->nama=="superadmin" || $group->nama=="admin_perusahaan"){
    $status = true;
   }
   return $status;
  }

  public function group_id($email){
   $user = $this->db->get_where("users",array("email"=>$email))->row();
   return $user->group_id;
  }

  public function group_name($email){
   $user = $this->db->get_where("users",array("email"=>$email))->row();
   $group = $this->db->get_where("groups",array("id"=>$user->group_id))->row();
   return $group->nama;
  }

  public function user_id($email){
   $user = $this->db->get_where("users",array("email"=>$email))->row();
   return $user->id;
  }

  public function no_kerjasama($email){
    $this->db->select("config.no_kerjasama");
    $this->db->where(array("users.email"=>$email));
    $this->db->join("config","config.id = users.config_id");
    $user = $this->db->get("users")->row();
   return $user->no_kerjasama;
  }

  public function get_features($email){
    $group = $this->user_model->group_id($email);    
    if($group==99){
      $group_feature = $this->db->select("id as feature_id")->get("features")->result();
    }else{
      $group_feature = $this->db
      ->select("features.id as feature_id")
      ->join("features","features.id = group_features.feature_id")
      ->where("group_id",$group)
      ->get("group_features")
      ->result();

    }
    
    $feature = array();
    foreach ($group_feature as $key => $value) {
      $feature[] = $value->feature_id;
    }
    return $feature;
  }

  // public function has_features($){
  //   $ses_features = $this->session->userdata("features");
  //   if(in_array('309', $ses_features, true)){
  //     echo "mempunyai features";
  //   }
  // }
}
?>