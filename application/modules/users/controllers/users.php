<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Backend_Controller {
  function __construct()
    {
        parent::__construct();
        $this->load->model("users/user","user_model");
    }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('users');
    $crud->set_subject('users');
    $crud->set_relation('group_id','groups','nama',"groups.id != 99");
    $crud->set_relation('config_id','config','nama');
    $crud->where('users.username !=', 'super_administrator');
    $crud->set_rules('email','email','required');
    $crud->required_fields("username","password","email");
    $crud->change_field_type('password', 'password');
     $crud->callback_before_insert(array($this,'encrypt_password_callback'));
     $crud->callback_before_update(array($this,'encrypt_password_callback'));
     $crud->unset_delete();
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  function encrypt_password_callback($post_array) {
    // $this->load->library('encrypt');
    // $key = 'super-secret-key';
    // $data["password"] = $this->encode($data["password"]);
    $post_array['password'] =  $this->encode($post_array['password']);
   
    return $post_array;
  }     

}
?>