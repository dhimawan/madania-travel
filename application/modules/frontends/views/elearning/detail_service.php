<div class="top-decor"></div>

<section class="callaction">
  <div class="">
    <div class="row-fluid">
      <div class="container">
        <div class="span8">
          <article>
            <div class="row-fluid">
              <div class="no-margin-left span12">
                <img class="align-left packet_image" src="<?=base_url("assets/uploads/pages/$detail->value")?>" alt="<?=$detail->value?>">
                <?=$detail->description?>
              </div>
              <div class="no-margin-left span12">
                <div class="solidline"></div>
              </div>
              <div class="no-margin-left span12">
                <?php foreach ($packets as $key => $value) { ?>
                  <div class="packet-box no-margin-left span4">
                    <div class="pricing-box-alt">
                      <div class="pricing-heading">
                        <h3><?=$value->name?></h3>
                      </div>
                      <div class="pricing-terms">
                        <h6><?=get_price_packet($value->id)?></h6>
                      </div>  
                      <div class="pricing-content">
                        <?=$value->description?>
                      </div>  
                      <div class="pricing-action">  
                        <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Daftar </a>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
              <div class="no-margin-left span12">
                <div class="solidline"></div>
              </div>
              <div class="no-margin-left span12">
                <div id="calendar"></div>
              </div>
            </div>
          </article> 
        </div>  
      <div class="span4"><?=generate_side_requirements()?></div>        
    </div>
  </div>
</section>

<div class="row-fluid features-content">
  <div class="container">
    <div class="span12 analyze-blue">
      <h2> Paket lain </h2>
      <div class="analyze-strip">
        <?php foreach ($packet as $key => $value) { ?>
          <?php if($value->id != $detail->id){ ?>
          <div class="span3">
          <div class="box aligncenter">
            <div class="aligncenter icon">
              <?php if($value->type=="link"){?>
                <a href="<?=$value->title?>" target="_blank">
              <?php }else{?>
                <a href="<?=base_url("paket/".underscore($value->name)."")?>">
              <?php }?>
                  <img src="<?=base_url("assets/uploads/pages/".$value->value)?>" style="width:221px;height:164px;margin-top: 10px;" alt="<?=$value->description?>"/>
                  <div class="owner-bg"></div>
                </a>
            </div>
            <div class="text">
              <h6><?=$value->name?></h6>
            </div>
          </div>
          </div>
          <?}?>
        <?}?>
        
      </div>
    </div>
  </div>
</div>

<div id="detailDeparture" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="mySignupModalLabel">Jadwal <strong>Keberangkatan</strong></h4>
  </div>
  <div class="modal-body" id="bodyDetailDeparture">
    
  </div>
</div>

<script>

  $(document).ready(function() {
  
   $('#calendar').fullCalendar({
      // editable: false,
      events: "<?=base_url("frontends/get_departures/$detail->id")?>",
      eventClick: function(event) {
        $.ajax({
          type: "POST",
          url: "<?=base_url('frontends/detail_departure')?>",
          data: { id: event.id }
        }).done(function(msg){
            $("#bodyDetailDeparture").html(msg);
            $('#detailDeparture').modal('show');
        });

        // $('#detailDeparture').modal('show');
      },
      loading: function(bool) {
        if (bool){
          $('#loading').show();
        }else{
          $('#loading').hide();
        }
      }
    });

  });

</script>
