<div class="top-decor"></div>

<section class="callaction">
  <div class="">
    <div class="row-fluid">
      <div class="row-fluid">
        <div class="span9">
          <?php foreach ($attribute_pages as $key => $value) {?>
            <article>
            <div class="">
              <div class="span12">
                <div class="post-image">
                  <div class="post-heading">
                    <h3><?=article_link($value,$value->title)?></h3>
                  </div>
                  <img src="<?=base_url("assets/uploads/news/".$value->image)?>" alt="<?=$value->name?>" class="align-left" width="30%">
                </div>
                <?=word_limiter($value->description,40)?>
                <div class="bottom-article">
                  <ul class="meta-post">
                    <li><i class="icon-calendar"></i><a href="#"> <?=date("d M Y",strtotime($value->updated_at))?></a></li>
                    <li><i class="icon-user"></i><a href="#"> Admin</a></li>
                    <li><i class="icon-folder-open"></i><a href="#"><?=$value->category_name?></a></li>
                    <li><i class="icon-comments"></i> <a href="#">4 Comments</a></li>
                  </ul>
                  <a href="#" class="pull-right">Continue reading <i class="icon-angle-right"></i></a>
                </div>
              </div>
            </div>
            </article>
          <?php } ?>

          <div id="pagination"> 
            <?//=$this->pagination->create_links();?>
            <span class="all">Page 1 of 3</span>
            <span class="current">1</span>
            <a href="#" class="inactive">2</a>
            <a href="#" class="inactive">3</a>
          </div>  
        </div>  
      <div class="span3"><?=generate_side_requirements()?></div>        
    </div>
  </div>
</section>
