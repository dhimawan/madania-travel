<div class="row-fluid features-content">
  <div class="container">
    <div class="span12 analyze-blue">
      <div class="analyze-strip">
        <?php foreach ($packet as $key => $value) { ?>
          <div class="span3 abso-bg" style="background: none repeat scroll 0 0 #F9F9F9; margin-bottom: 15px; ">
          <div class="box aligncenter">
            <div class="aligncenter icon">
              <?php if($value->type=="link"){?>
                <a href="<?=$value->title?>" target="_blank" alt="<?=$value->name?>">
              <?php }else{?>
                <a href="<?=base_url("paket/".underscore($value->name)."")?>">
              <?php }?>
                  <img src="<?=base_url("assets/uploads/pages/".$value->value)?>" style="width:221px;height:164px;margin-top: 10px;" alt="<?=$value->description?>"/>
                  <div class="owner-bg"></div>
                </a>
            </div>
            <div class="text">
              <h6><?=$value->name?></h6>
            </div>
          </div>
        </div>
        <?}?>
        
      </div>
    </div>
  </div>
</div>