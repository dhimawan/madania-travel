<style type="text/css">

#facebook_news > div {
    width: 475px;
}
</style>
  
  <div class="news-content row-fluid">
    <div class="container">
      <div class="span6">
        <h4 class="heading">Madania <strong>Berita</strong></h4>
        <div id="news-tab">
          <ul>
            <?php foreach ($news as $key => $value) { ?>
              <li>
                <article>
                  <div class="post-image">
                    <div class="post-heading">
                      <h5><?=article_link($value,$value->title)?></h5>           
                    </div>
                  </div>
                  <p>
                    <?=word_limiter($value->description,40)?>
                  </p>
                  <div class="bottom-article">
                    <ul class="meta-post">
                      <li><i class="icon-calendar"></i><a href="<?=news_url($value,$value->title)?>" alt="update <?=$value->title?> <?=$value->updated_at?>"> <?=$value->updated_at?></a></li>
                    </ul>
                    <!-- <a href="#" class="pull-right">Continue reading <i class="icon-angle-right"></i></a> -->
                  </div>
                
                </article>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="span6 offset">
        <h4 class="heading">Madania On <strong>Social Media</strong></h4>
        <div class="row-fluid socmed-tab">

          <ul class="nav nav-tabs">
            <li class="active"><a class="fb" href="#fb"><img src="<?=base_url("assets/images/fb.png")?>" /></a></li>
            <li><a class="twt" href="#twitter"><img src="<?=base_url("assets/images/twitter.png")?>" /></a></li>
            <li><a class="google" href="#google"><img src="<?=base_url("assets/images/google.png")?>" /></a></li>
          </ul>

          <div id='content' class="tab-content">
            <div class="tab-pane active" id="fb">
              <div id="facebook_news"></div>
              <p><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FPT-Madania-Semesta-Wisata%2F564612726938153&width=400&height=558&colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=true&amp;show_border=true&amp;appId=1424962024390837" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:400px; height:558px;background-color:#ffffff"></iframe></p>
            </div>
            <div class="tab-pane" id="twitter">
              <!-- Twitter -->
              <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/madania_travel" data-widget-id="388675789647917058">Tweet oleh @madania_travel</a>

              <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            </div>
            <div class="tab-pane" id="google">
              <!-- Google Plus -->
              <iframe src="http://widgetsplus.com:8080/13847.htm" width="300" height="481" style="padding:0; margin:0; overflow:hidden;" frameborder="0"></iframe>
            </div>
          </div>    

        </div>
      </div>
    </div>
  </div>


