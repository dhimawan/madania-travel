<section>
  <div class="currency">
    <div id="container">
      <ul id="webticker" class="large-12 columns">
        <li class="down clearfix first">
          <span>USD</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>1.3225</span>
        </li>
        <li class="down clearfix">
          <span>USD/JPY</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>97.891</span>
        </li>
        <li class="up clearfix">
          <span>GBP/USD</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>1.54198</span>
        </li>
        <li class="down clearfix">
          <span>USD/CAD</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>1.9791</span>
        </li>
        <li class="up clearfix">
          <span>EUR/USD</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>1.3225</span>
        </li>
        <li class="up clearfix">
          <span>AUD/USD</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>0.3225</span>
        </li>
        <li class="up clearfix">
          <span>EUR/IDR</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>13.225</span>
        </li>
        <li class="down clearfix">
          <span>YEN/IDR</span>
          <img class="img-up" src="<?=base_url("assets/images/up.png")?>"/>
          <img class="img-down" src="<?=base_url("assets/images/down.png")?>"/>
          <span>12.116</span>
        </li>
      </ul>
    </div>
  </div>
</section>