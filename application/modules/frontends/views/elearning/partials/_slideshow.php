<section id="featured">
  <!-- start slider -->
  <!-- Slider -->

  <!-- end slider -->	
  <div id="da-slider" class="da-slider">
    <?php foreach ($slideshow as $key => $value) { ?>
			<div class="da-slide">
				<h2><?=$value->name?></h2>
				<p><?=word_wrap($value->description,30)?></p>
				<a href="#" class="da-link">Read more</a>
				<div class="da-img"><img src="<?=base_url("assets/uploads/pages/".$value->val_image)?>" alt="<?=$value->name?>" /></div>
			</div>
		<?php } ?>
			
				<nav class="da-arrows">
					<span class="da-arrows-prev"></span>
					<span class="da-arrows-next"></span>
				</nav>
			</div>
</section>	

