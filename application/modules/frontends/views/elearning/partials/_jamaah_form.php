<script type="text/javascript">
$(document).ready(function(){
var $validator= $("#registration_form").validate({
      rules: {
        no_ktp_<?=$index?>: {
          required: true
        },
        nama_lengkap_<?=$index?>: {
          required: true,
        },
        kelamin_<?=$index?>: {
          required: true
        },
        warganegara_<?=$index?>: {
          required: true
        },
        provinsi_<?=$index?>: {
          required: true
        },
        kota_<?=$index?>: {
          required: true
        },
        kecamatan_<?=$index?>: {
          required: true
        },
        kelurahan_<?=$index?>: {
          required: true
        },
        telp_<?=$index?>: {
          required: true
        },
        pendidikan_<?=$index?>: {
          required: true
        },
        pekerjaan_<?=$index?>: {
          required: true
        },
        pernikahan_<?=$index?>: {
          required: true
        },
        golongan_darah_<?=$index?>: {
          required: true
        }

      }
    });
});
</script>
<fieldset class="span4">  
  <label>No Ktp</label>
    <input name="biodata[no_ktp][]" placeholder="* Masukan no Ktp anda" type="text" id="no_ktp_<?=$index?>" class="required" >  
    <div class="validation"></div>

  <label>Nama Lengkap*</label>
  <input name="biodata[nama_lengkap][]" placeholder="* Masukan nama lengkap anda"  type="text" id="nama_lengkap_<?=$index?>" class="required" >


  <label>Nama Ayah Kandung</label>
  <input name="biodata[nama_ayah_kandung][]" placeholder="* Masukan nama_ayah_kandung anda"  type="text"  id="nama_ayah_<?=$index?>" >
  <div class="validation"></div>
                
  <label>Tempat Lahir*</label>
  <input name="biodata[tempat_lahir][]" placeholder="* Masukan tempat_lahir anda" data-rule="email" data-msg="Please enter a valid email" type="text" id="tempat_lahir_<?=$index?>" class="required">
  <div class="validation"></div>
                
  <label>Tanggal Lahir*</label>
  <input name="biodata[tanggal_lahir][]" placeholder="* Masukan tanggal_lahir anda" type="text" id="tanggal_lahir<?=$index?>" class="required tanggal" data-date-format="dd-mm-yyyy">
  <div class="validation"></div>
               
  <label>jenis kelamin*</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[jk][]" id="kelamin_<?=$index?>" class="required"/>
    <label>Pria</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[jk][]" id="kelamin_<?=$index?>" class="required"/>
    <label>Wanita</label>
  </div>
  <div class="validation"></div>
                
  <label>Kewarganegaraan*</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[wn][]" id="warganegara_<?=$index?>" class="required"/>
    <label>Indonesia</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[wn][]" id="warganegara_<?=$index?>" class="required"/>
    <label>Asing</label>
  </div>
  <div class="validation"></div>
</fieldset>
<fieldset class="span4">
  <label>Alamat *</label>
  <textarea name="biodata[address][]" id="alamat_<?=$index?>" class="required"></textarea>
  <div class="validation"></div>
                
  <label>Provinsi*</label>
  <select name="biodata[propinsi][]" id="provinsi_<?=$index?>" class="required">
    <option value="">Pilih</option>
    <?php foreach ($propinsi as $key => $value) { ?>
      <option value="<?=$value->id?>"><?=$value->name?></option>
    <?php } ?>
  </select>
  <div class="validation"></div>
                
  <label>Kabupaten/ Kota *</label>
  <select  name="biodata[kota][]" id="kota_<?=$index?>" class="required">
    <option value="">Pilih</option>
    <?php foreach ($kota as $key => $value) { ?>
      <option value="<?=$value->id?>"><?=$value->name?></option>
    <?php } ?>
  </select>
  <div class="validation"></div>
                
  <label>Kecamatan *</label>
  <input name="biodata[kecamatan][]" placeholder="* Masukan tanggal_lahir anda"  type="text" id="kecamatan_<?=$index?>" class="required">
  <div class="validation"></div>

  <label>Desa/ Kelurahan *</label>
  <input name="biodata[kelurahan][]" placeholder="* Masukan desa/kelurahan anda" type="text" id="kelurahan_<?=$index?>" class="required">
  <div class="validation"></div>

  <label>Kode Pos</label>
  <input name="biodata[kode_pos]" placeholder="* Masukan Kode Pos anda" type="text" id="kodepos_<?=$index?>">
  <div class="validation"></div>
                
  <label>No. Telepon/ HP *</label>
  <input name="biodata[no_telp]" placeholder="* Masukan No telephone/hp anda" type="text" id="telp_<?=$index?>" class="required">
  <div class="validation"></div>
</fieldset>
<fieldset class="span3">
  <label>Pendidikan *</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>SD</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>SMP</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>SMA</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>D1/ D2/ D3/ SM</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>S1</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>S2</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pendidikan][]" id="pendidikan_<?=$index?>" class="required" class="required"/>
    <label>S3</label>
  </div>
  <div class="validation"></div>
                
  <label>Pekerjaan *</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="PNS"/>
    <label>Pegawai Negeri Sipil</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="TNI/POLRI"/>
    <label>TNI/POLRI</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Dagang"/>
    <label>Dagang</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Tani/Nelayan"/>
    <label>Tani/ Nelayan</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="pnSwastas"/>
    <label>Swasta</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Ibu Rumah Tangga"/>
    <label>Ibu Rumah Tangga</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Pelajar/Mahasiswa"/>
    <label>Pelajar/Mahasiswa</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="BUMN/BUMD"/>
    <label>BUMN/BUMD</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Pensiunan"/>
    <label>Pensiunan</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pekerjaan][]" id="pekerjaan_<?=$index?>" class="required" value="Lainnya"/>
    <label>Lainnya</label>
  </div>
  <div class="validation"></div>
</fieldset>  

<div class="clearfix"></div>  
<h4><i class="icon-plus-sign-alt"></i>Data Umroh / Haji <span id="index_jamaah_title"> </span></h4>
<hr>
<fieldset class="span4">
  <label>Pergi Haji</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pergi_haji][]" id="pergi_haji_<?=$index?>" value="Pernah"/>
    <label>Pernah</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pergi_haji][]" id="pergi_haji_<?=$index?>" value="Belum"/>
    <label>Belum</label>
  </div>
  <div class="validation"></div>
                
  <label>Nama Mahram/ Pendamping</label>
  <input name="biodata[nama_mahram]" placeholder="* Masukan tanggal_lahir anda" type="text" id="nama_mahram_<?=$index?>">
  <div class="validation"></div>

  <label>Hubungan Mahram/ Pendamping</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[hubungan_mahram][]" id="hubungan_mahram_<?=$index?>" value="Orang Tua"/>
    <label>Orang Tua</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[hubungan_mahram][]" id="hubungan_mahram_<?=$index?>" value="Anak"/>
    <label>Anak</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[hubungan_mahram][]" id="hubungan_mahram_<?=$index?>" value="Suami/Istri"/>
    <label>Suami/Istri</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[hubungan_mahram][]" id="hubungan_mahram_<?=$index?>" value="Mertua"/>
    <label>Mertua</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[hubungan_mahram][]" id="hubungan_mahram_<?=$index?>" value="Saudara Kandung"/>
    <label>Saudara Kandung</label>
  </div>
  <div class="validation"></div>
  </fieldset>      

  <fieldset class="span4">
  <label>Nomor Pendaftaran Mahram/ Pendamping</label>
  <input name="biodata[no_mahram]" placeholder="* Masukan tanggal_lahir anda" type="text" id="no_mahram_<?=$index?>">
  <div class="validation"></div>

  <label>Golongan Darah *</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[golongan_darah][]" id="golongan_darah_<?=$index?>" class="required" value="A"/>
    <label>A</label>
  </div>
  <div class="radio ">
    <input type="radio" name="biodata[golongan_darah][]" id="golongan_darah_<?=$index?>" class="required" value="B"/>
    <label>B</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[golongan_darah][]" id="golongan_darah_<?=$index?>" class="required" value="AB"/>
    <label>AB</label>
  </div>
  <div class="radio ">
    <input type="radio" name="biodata[golongan_darah][]" id="golongan_darah_<?=$index?>" class="required" value="O"/>
    <label>O</label>
  </div>
  <div class="validation"></div>

  <label>Status Perkawinan *</label>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pernikahan][]" id="pernikahan_<?=$index?>" class="required" value="Belum Menikah"/>
    <label>Belum Menikah</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pernikahan][]" id="pernikahan_<?=$index?>" class="required" value="Menikah"/>
    <label>Menikah</label>
  </div>
  <div class="radio clearfix">
    <input type="radio" name="biodata[pernikahan][]" id="pernikahan_<?=$index?>" class="required" value="Janda/Duda"/>
    <label>Janda/Duda</label>
  </div>
  <div class="validation"></div>
</fieldset>
