<div class="partner row-fluid">
  <div class="container">
    <div class="span12">
      <h4><strong> Kerjasama</strong></h4>
      <div class=" jcarousel-skin-tango">
        <div class="jcarousel-container jcarousel-container-horizontal" style="position: relative; display: block; ">
          <div class="jcarousel-clip jcarousel-clip-horizontal" style="position: relative; ">
            <ul id="mycarousel" class="recent-jcarousel clients jcarousel-list jcarousel-list-horizontal" style="overflow: hidden; position: relative; top: 0px; margin: 0px; padding: 0px; left: 0px; width: 2296px; ">
              <?php foreach ($iklan as $key => $value) {?>
                <li class="jcarousel-item jcarousel-item-horizontal jcarousel-item-1 jcarousel-item-1-horizontal" style="float: left; list-style: none; " jcarouselindex="1">
                  <a href="#">
                    <img src="<?=base_url("assets/uploads/pages/$value->value")?>" class="client-logo" alt="<?=$value->description?>" style="width:'310px';height:'120px' " >
                  </a>
                </li>
              <?php } ?>
             

            </ul>
          </div>
          <div class="jcarousel-prev jcarousel-prev-horizontal jcarousel-prev-disabled jcarousel-prev-disabled-horizontal" style="display: block; " disabled="disabled"></div>
          <div class="jcarousel-next jcarousel-next-horizontal" style="display: block; "></div>
        </div>
      </div>
    </div>
  </div>
</div>