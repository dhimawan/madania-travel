<div class="portfolio row-fluid">
  <div class="container">
    <div class="span8">
      <h4 class="heading">Foto <strong>Kegiatan</strong></h4>
      <div class="row-fluid">
        <section id="projects">
          <ul id="thumbs" class="portfolio">
            <!-- Item Project and Filter Name -->

            <?php foreach ($galleries as $key => $value) { ?>
                <!-- Item Project and Filter Name -->
                <li class="item-thumbs span4 design" data-id="id-0" data-type="web">
                  <a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<?=$value->name?>" href="<?=base_url("assets/uploads/pages/$value->value")?>">
                    <span class="overlay-img"></span>
                    <span class="overlay-img-thumb font-icon-plus"></span>
                  </a>
                  <!-- Thumb Image and Description -->
                  <img src="<?=base_url("assets/uploads/pages/$value->value")?>" alt="<?=$value->description?>">
                </li>
                <!-- End Item Project -->
              <?php } ?>
          </ul>
        </section>
      </div>
    </div>
    
    <div class="span4 offset">
      <h4 class="heading">Testimonial <strong></strong></h4>
      <div class="row-fluid">
        <div class="span12 offset">
          <?php foreach ($testimonial as $key => $value) { ?>
              <blockquote>
                <i class="icon-quote-left icon-2x"><span><?=word_limiter($value->description,10)?></span></i>
              <cite><?=$value->name?></cite>
              </blockquote>
          <?php } ?>    
        </div>
      </div>
    </div>
  </div>
</div>