<aside class="left-sidebar">
  <div class="widget">
    <form class="form-search">
      <input placeholder="Type something" type="text" class="input-medium search-query">
      <button type="submit" class="btn btn-square btn-theme">Search</button>
    </form>
  </div>
  <div class="left_box">
    <h5 class="widgetheading">Categories</h5>
    <ul class="cat">
      <?php foreach ($category as $key => $value) {?>
        <li><i class="icon-angle-right"></i><a href="<?=site_url("news/category/$value->name")?>"><?=$value->name?></a></li>
      <?php }?>
    </ul>
  </div>
  <div class="left_box">
    <h5 class="widgetheading">Latest posts</h5>
    <ul class="recent">
      <?php foreach ($latest_post as $key => $value) {?>
        <li>
          <img src="<?= ($value->is_internal==true ? base_url("assets/uploads/news/$value->image") : $value->image)?>" class="pull-left" alt="<?=$value->title?>" width="65px">
          <h6><?=article_link($value,$value->title)?></h6>
          <?=word_limiter($value->description,10)?>
        </li>
      <?php }?>
    </ul>
  </div>  
  <!-- <div class="widget">
    <h5 class="widgetheading">Popular tags</h5>
    <ul class="tags">
      <li><a href="#">Web design</a></li>
      <li><a href="#">Trends</a></li>
      <li><a href="#">Technology</a></li> 
      <li><a href="#">Internet</a></li> 
      <li><a href="#">Tutorial</a></li> 
      <li><a href="#">Development</a></li>                  
    </ul>
  </div> -->
</aside>