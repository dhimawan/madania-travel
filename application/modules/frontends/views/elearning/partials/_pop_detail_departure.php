<form class="form-horizontal">
  <div class="control-group">
    <label class="control-label" for="inputEmail">Nama</label>
    <div class="controls">
      <!-- <input type="text" id="inputEmail" placeholder="Email"> -->
      <?=$data->name?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputSignupPassword">Total Pax</label>
    <div class="controls">
    <!-- <input type="password" id="inputSignupPassword" placeholder="Password"> -->
      <?=$data->total_tax?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputSignupPassword2">Tanggal Keberangkatan</label>
    <div class="controls">
      <!-- <input type="password" id="inputSignupPassword2" placeholder="Password"> -->
      <?=date("d-m-Y",strtotime($data->startdate))?> s/d <?=date("d-m-Y",strtotime($data->enddate))?>
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputSignupPassword2">Deskripsi</label>
    <div class="controls">
      <!-- <input type="password" id="inputSignupPassword2" placeholder="Password"> -->
      <?=$data->description?>
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <!-- <button type="submit" class="btn">Daftar</button> -->
       <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Daftar </a>             
    </div>
    <!-- <p class="aligncenter margintop20">Already have an account? <a href="http://iweb-studio.com/wrapbootstrap/flattern/index.html#mySignin" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Sign in</a></p> -->
  </div>
</form>