<aside class="left-sidebar">
  <h4> Persyaratan </h4>
  <?php foreach ($requirements as $key => $value) { ?>
      <div class="left_box">
        <h5 class="widgetheading"><?=$key?></h5>
        <ul class="cat">
          <?php foreach ($value as $key2 => $value2) {?>
            <li><i class="icon-angle-right"></i><a href="#"><?=$value2["description"]?></a></li>
          <?php }?>
        </ul>
      </div>
  <?php } ?>

</aside>