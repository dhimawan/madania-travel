<div class="container">
<form>        
  <div class="row-fluid clearfix">	
    <h4 class="float-l"><i class="icon-asterisk icon-large"></i> Order Details</h4>
    <ul class="nav nav-tabs">
      <li class="active"><a href="#">General Information</a></li>
      <li><a href="#">Tab 2</a></li>
      <li><a href="#">Tab 3</a> </li>
      <li><a href="#">Tab 4</a></li>
    </ul>
  </div><!--/.row-->
  
  <hr class="hr-dark">
  
  <div class="row-fluid">
    <h4 class=""><i class="icon-plus-sign-alt"></i> General Information</h4>
    <hr>
    <div class="clearfix">
      <fieldset class="span4">
        <label>Origin</label>
        <textarea id="textarea" name="textarea">Enter Address</textarea>
        
        <select id="selectbasic" name="selectbasic" class="selectpicker">
          <option>Option select..</option>
          <option>Option one</option>
          <option>Option two</option>
        </select>
        
        <input type="text" class="input" placeholder="Add daterange picker">
        <input type="text" class="input" placeholder="Add range slider">
        
        <input class="input-small" type="text" placeholder="Pickup Date">
        <input class="input-small" type="text" placeholder="Closes at">
      </fieldset>
      
      <fieldset class="span4">
        <label>Destination</label>
        <textarea id="textarea" name="textarea">Enter Address</textarea>
        
        <select id="selectbasic" name="selectbasic" class="selectpicker">
          <option>Option select..</option>
          <option>Option one</option>
          <option>Option two</option>
        </select>
        
        <select id="selectbasic" name="selectbasic" class="selectpicker">
          <option>Option select..</option>
          <option>Option one</option>
          <option>Option two</option>
        </select>
        
        <input class="input-small" type="text" placeholder="Pickup Date">
        <input class="input-small" type="text" placeholder="Closes at">
      </fieldset>
      
      <fieldset class="span4">
        <label>Bill to location</label>
        <textarea id="textarea" name="textarea">Billing Address</textarea>
        
        <select id="selectbasic" name="selectbasic" class="selectpicker">
          <option>Option select..</option>
          <option>Option one</option>
          <option>Option two</option>
        </select>
        
        <div class="row-fluid">
          <h5>Terms</h5>
          <label class="radio">
            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
            Prepaid
          </label>
          <label class="radio">
            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
            Collect
          </label>
          <label class="radio">
            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
            Third Party
          </label>
          <label class="radio">
            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
            C.O.D.
          </label>
        </div>
      </fieldset>
    </div><!--/.clearfix-->
  </div><!--/.row-->
  
  <div class="row-fluid clearfix">
    <h4><i class="icon-plus-sign-alt"></i> Special Instructions</h4>
    <hr>
    <div class="form-inline">
      <fieldset>  
        <select class="span5">
          <option>Reference</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
        
        <input type="text" class="span6" placeholder="Enter value here">
        
        <button class="btn btn-inline btn-square"><i class="icon-plus"></i></button>
        <button class="btn btn-inline btn-square"><i class="icon-trash"></i></button>
      </fieldset>
    </div>
    <hr>
    <label>Special Instructions</label>
    <textarea id="textarea" class="span11" name="textarea" rows="2"></textarea>
  </div><!--/.row-->
  
  <div class="row-fluid">
    <h4><i class="icon-plus-sign-alt"></i> Items</h4>
    <hr>
    
    <div class="form-inline">
      <span class=" row-num">1.&nbsp;&nbsp;</span>
      <input type="text" class="span2" placeholder="Item#">
      <input type="text" class="span2" placeholder="Units">
      
      <select class="span2">
        <option>OptionOne</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
      
      <select class="span2">
        <option>OptionTwo</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
      
      <input type="text" class="span2" placeholder="Enter Item">
      
      <select class="span1">
        <option>unit</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
    </div>
    <div class="form-inline">
      <span class=" row-num">2.&nbsp;&nbsp;</span>
      <input type="text" class="span2" placeholder="Code">
      <input type="text" class="span2" placeholder="Description">

      <select class="span2">
        <option>Select3</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
      
      <select class="span2">
        <option>Select4</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
      
      <input type="text" class="span2" placeholder="Enter Item">
      
      <select class="span1">
        <option>unit</option>
        <option>Option 2</option>
        <option>Option 3</option>
        <option>Option 4</option>
        <option>Option 5</option>
      </select>
    </div>
  </div><!--/.row-->
  
  <hr>
  
  <div class="row-fluid">
    <h4><i class="icon-plus-sign-alt"></i> More Options</h4>
    <hr>
    <strong>Select all that apply</strong>
    <div class="control-group label-small">
      <div class="controls span3">
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Value</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Bootstrap</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Free</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Theme</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Bootply</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Grid</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Layout</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="123">Forms</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="345">Icon</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="567">Plugins</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="789">Container</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="012">Base CSS</label>
      </div>
      
      <div class="controls span3">
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Guaranteed Delivery AM">Twitter</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Guaranteed Delivery PM">Framework</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Hazmat">Customize</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="High Traffic Surcharge">Media Queries</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Inside delivery">Select</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Island Delivery">Dropdown</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Liftgate">Table</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Limited Access">Image Layout</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Marking and Tagging">Media Grid</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Saturday Delivery">Media Queries</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Security Charge">Lightweight</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Sorting And Segregating">Responsive</label>
      </div>
      
            <div class="controls span3">
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Facebook</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Bootstrap</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Free</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Theme</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Bootply</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Grid</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="Value">Layout</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="123">Forms</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="345">Icon</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="567">Plugins</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="789">Container</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="012">Base CSS</label>
      </div>
      
      <div class="controls span3">
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="214">Code</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="234">API</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="444">Tutorial</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="455">Layout</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="213">Stack</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="555">Frame</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="324">Align</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="123">Forms</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="345">Icon</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="567">Plugins</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="789">Container</label>
        <label class="checkbox">
          <input type="checkbox" name="checkboxes" value="012">Base CSS</label>
      </div>
    </div>
  </div><!--/.row-->
  </form>
</div><!--/.container-->