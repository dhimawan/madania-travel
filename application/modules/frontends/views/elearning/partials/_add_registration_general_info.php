<form method="post" action="<?=base_url("frontends/add_registration")?>" >
  <fieldset class="span4">
    <label>Daftar Paket</label>
    <label> </label>
    <select name="customer_packet[attribute_page_id]" id="packet_id">
      <option value="">--Pilih Paket--</option>
      <?php foreach ($packet as $key => $value) { ?>
        <option value="<?=$value->id?>"><?=$value->name?></option>
      <?php }?>
    </select>
    <select name="customer_packet[packet_id]" id="detail_packet_id" placeholder="Pilih Detail paket"> </select>
  </fieldset>
  
  <fieldset class="span4">
    <label>Jadwal Keberangkatan</label>
    <div id="date_of_departure">
      <input type="text" name="customer_packet[departure]"  class="input" placeholder="Tanggal Keberangkatan" id="departure_start">
      <input type="text " name="customer_packet[departure_end]"  class="input" placeholder="Tanggal Selesai" id="departure_end">
    </div>
    <label>Jumlah Jamaah</label>
    <select name="customer_packet[total_jamaah]" >
      <?php for ($i=1; $i < 10; $i++) { ?>
        <option value ="<?=$i?>"><?=$i?> Jammah</option>
      <?php } ?>
    </select>
  </fieldset>
  
  <fieldset class="span4">
    <label>Data pemesan</label>
    <input class="input" type="text" placeholder="Nama Pemesan"  name="customer_packet[order_name]">
    <input class="input" type="text" placeholder="No telephone" name="customer_packet[order_telp]">
    <input class="input" type="text" placeholder="No HP" name="customer_packet[order_hp]">
    <input class="input" type="text" placeholder="Email" name="customer_packet[order_email]">
    <textarea  placeholder="alamat" name="customer_packet[address]"> alamat pemesan </textarea>
    <input class="input" type="text" placeholder="Kode Kerjasama" name="customer_packet[order_mou]">
  </fieldset>
  
  <input type="submit" value="Lanjutkan" class="btn btn-large btn-rounded btn-blue">
</form>