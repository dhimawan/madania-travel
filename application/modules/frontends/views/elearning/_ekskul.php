<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="span4">
				<div class="inner-heading">
				<h2> <strong><?=$detail->intitle?></strong></h2>
				</div>
			</div>	
			<div class="span8">	
				<!-- <ul class="breadcrumb">
					<li><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#"><i class="icon-home"></i></a> <i class="icon-angle-right"></i></li>
					<li><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#">Portfolio</a> <i class="icon-angle-right"></i></li>
					<li class="active">Portfolio 4 columns</li>
				</ul> -->
			</div>
		</div>			
	</div>
</section>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				<?=$detail->incontent?>
			</div>
		</div>
		<!-- divider -->
		<div class="row">
			<div class="span12">
				<div class="solidline"></div>
			</div>
		</div>
		<!-- end divider -->
		<div class="row">
			<div class="span12">
				<div class="row">
					<section id="projects">
						<ul id="thumbs" class="portfolio">
						<?php	$attr_page = $this->db->get_where("attribute_pages",array("page_id"=> $detail->id))->result(); 
							foreach ($attr_page as $key => $value) { ?>
							<!-- Item Project and Filter Name -->
							<li class="item-thumbs span3 design" data-id="id-0" data-type="web">
								<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<?=$value->name?>" href="<?=base_url("assets/uploads/pages/$value->value")?>">
									<span class="overlay-img"></span>
									<span class="overlay-img-thumb font-icon-plus"></span>
								</a>
								<!-- Thumb Image and Description -->
								<img src="<?=base_url("assets/uploads/pages/$value->value")?>" alt="<?=$value->description?>">
							</li>
							<!-- End Item Project -->
						<?php	} ?>

						</ul>
					</section>
					
				</div>
			</div>			
		</div>
	</div>
</section>