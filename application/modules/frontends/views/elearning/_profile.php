<script>
  // (function() {
  //   var cx = '011798417585352738117:WMX-1438246666';
  //   var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
  //   gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
  //       '//www.google.com/cse/cse.js?cx=' + cx;
  //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  // })();
</script>
<!-- <gcse:search></gcse:search> -->
<div class="top-decor"></div>
<section class="callaction">
  <div class="container">
    <div class="row-fluid">
      <div class="span6">
        <h4>Sejarah <strong>Madania Semesta Wisata</strong></h4>
        <?=$detail->incontent?>
      </div>
      <div class="span6">
        <h4>Detail <strong>Perusahaan</strong></h4>
        <div class="tabbable tabs-left">
          <ul class="nav nav-tabs bold">
            <?php foreach ($child as $key => $value) {?>
              <li class="<?=($key==0 ? "active" : "")?>"><a href="<?=base_url("$detail->url"."#".$value->url)?>" data-toggle="tab"><?=$value->intitle?></a></li>
            <?php } ?>
          </ul>
          <div class="tab-content">
            <?php foreach ($child as $key => $value) {?>
              <div class="tab-pane <?=($key==0 ? "active" : "")?>" id="<?=$value->url?>">
              <?=$value->incontent?>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <h4>Galleri PT Madania Semesta Wisata</h4>
    </div>
    <div class="clearfix"></div>
    <?php foreach ($attribute_pages as $key => $value) { ?>
      <div class="span3 profile_picture">
        <img src="<?=base_url("assets/uploads/pages/$value->value")?>" alt="<?=$value->name?>" class="img-polaroid">
        <div class="roles">
          <p class="lead"><strong><?=$value->name?></strong></p>
          <p><?=word_limiter($value->description,30)?></p>
        </div>
      </div>
    <?php } ?>
    
  </div>
</div>