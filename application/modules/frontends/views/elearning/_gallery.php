<div class="top-decor"></div>

<section class="callaction">
  <div class="container">
    <div class="row-fluid">
			<div class="span12">
				<!-- <ul class="portfolio-categ filter">
					<li class="all active"><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#">All</a></li>
					<li class="web"><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#" title="">Web design</a></li>
					<li class="icon"><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#" title="">Icons</a></li>
					<li class="graphic"><a href="http://iweb-studio.com/wrapbootstrap/flattern/portfolio-4cols.html#" title="">Graphic design</a></li>
				</ul> -->

				<div class="clearfix"></div>
				<!-- <div class="row"> -->
					<section id="projects">
						<ul id="thumbs" class="portfolio">
						<?php 
							$attr_page = $this->db->get_where("attribute_pages",array("page_id"=> $detail->id))->result(); 
							foreach ($attr_page as $key => $value) { 
						?>
							<li class="item-thumbs span3 design" data-id="id-0" data-type="web">
								<a class="hover-wrap fancybox" data-fancybox-group="gallery" title="<?=$value->name?>" href="<?=base_url("assets/uploads/pages/$value->value")?>">
									<span class="overlay-img"></span>
									<span class="overlay-img-thumb font-icon-plus"></span>
								</a>
								
								<img src="<?=base_url("assets/uploads/pages/$value->value")?>" alt="<?=$value->description?>">
							</li>
						<?php } ?>
						</ul>
					</section>
					
				<!-- </div> -->
			</div>			
		</div>
  </div>
</section>
