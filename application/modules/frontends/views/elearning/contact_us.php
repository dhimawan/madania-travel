<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

<script type="text/javascript" src="<?=base_url("assets/js/mapmarker.jquery.js")?>"></script>
  
<script type="text/javascript">
  // Use below link if you want to get latitude & longitude
  // http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude.php
  $(document).ready(function(){

    var myMarkers = {"markers": [
    <?php foreach ($data as $key => $value) { ?>
       <?php 
        $name ="".strtoupper($value->status_kerjasama)."";
       if(empty($value->status_kerjasama)){
          $icon ="".base_url('assets/images/maps/pusat.png')."";
          $name = "PUSAT";
        }elseif($value->status_kerjasama=="perwakilan"){
          $icon ="".base_url('assets/images/maps/perwakilan.png')."";

        }elseif($value->status_kerjasama=="cabang"){
          $icon ="".base_url('assets/images/maps/office-building.png')."";
        }elseif($value->status_kerjasama=="subcabang"){
          $icon ="".base_url('assets/images/maps/subcabang.png')."";
        }
        
        ?>

        {"latitude": "<?=trim($value->lat)?>", 
         "longitude":"<?=trim($value->long)?>",
         "icon" : "<?=$icon?>" ,
         "baloon_text": "'<strong><?=trim($value->nama)?></strong> (<?=$name?>) <br/> <?=htmlspecialchars(preg_replace( "/\r|\n/", "",trim($value->alamat)))?> <br/> No Telp :<?=trim($value->telp)?> <br/> Email :  <?=trim($value->email)?> <br/> Kontak person : <?=trim($value->contact_person)?> <br/> '"
        } , 
    <?php } ?>
    ]
    };
    $("#map").mapmarker({
      zoom  : 5,
      center  : 'Indonesia',
      markers : myMarkers
    });

    $('.address-trigger').click(function(){
        $('.address-trigger').fadeOut();
        $(".fixed-address").animate({
        left: "+=500"});
        return false;
    });

    $('.close-trigger').click(function(){
        $('.address-trigger').fadeIn();
        $(".fixed-address").animate({
        left: "-=500"});
        return false;
    });

  });
</script>
<style type="text/css">

.map_canvas { 
  width: 95%; 
  height: 400px; 
  margin: 10px 20px 10px 0;
  max-width: none;
}

.map_canvas img { 
  max-width: none;
}

.map_canvas label { 
  width: auto; display:inline; 
} 
#map{
  height: 300px !important;
  width: 100%;
}

.gm-style img {
    max-width: none;
}

.gm-style label {
    width: auto; display:inline;
}
</style>

  <div class="top-decor"></div>

  <a class="address-trigger" href=""></a>
  <a class="close-trigger" href=""></a>

  <div class="fixed-address clearfix">
    <div class="row-fluid">
      <div class="span12">
        <div class="accordion" id="accordion2">
        <?php foreach ($data as $key => $value) { ?>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?=$value->id?>">
                <?=$value->nama?> <?php echo ($value->is_office == 1 ? "(Kantor Pusat)" : "(Agency)")?>
              </a>
            </div>
            <div id="collapse<?=$value->id?>" class="accordion-body collapse <?=($key==0 ? "in" : "")?>">
              <div class="accordion-inner">
              <ul class="unstyled">
                <li><?=$value->alamat?></li>
                <li>No telp : <?=$value->telp?></li>
                <li>Email : <?=$value->email?></li>
                <li>Kontak Person : <?=$value->contact_person?></li>
              </ul>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </div>
  </div>

  <section class="contactUs-form" id="content">
  
  <div class="container">
    <div class="cont-bg clearfix">
      <div class="row-fluid">
        <div class="span12">          
          <h4>Silahkan  <strong>Kontak Kami</strong> untuk informasi lebih lanjut  </h4>
          <form id="contactform" action="frontends/create_contact_us" method="post" class="validateform" name="send-contact">
            <div id="sendmessage">
              Your message has been sent. Thank you!
            </div>
            <div class="">
              <div class="span3 field">
                <input name="name" placeholder="* Masukan nama lengkap anda" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" type="text">  
                <div class="validation"></div>
              </div>            
              <div class="span3 field">
                <input name="email" placeholder="* Masukan Email anda" data-rule="email" data-msg="Please enter a valid email" type="text">
                <div class="validation"></div>
              </div>
              <div class="span3 field">
                <input name="title" placeholder="Masukan Judul Pesan" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" type="text">
                <div class="validation"></div>
              </div>  
              <div class="span3 field">
                <input name="telp" placeholder="* Masukan No Telphone" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" type="text">
                <div class="validation"></div>
              </div>            
              <div class="span12 margintop10 field no-margin-left">
                <textarea rows="12" name="description" class="input-block-level" placeholder="* Masukan pesan anda disini... " data-rule="required" data-msg="Please write something"></textarea>
                  <div class="validation"></div>
                <p>
                  <button class="btn-blue margintop10 pull-left" type="submit">Kirim message</button>
                  <span class="pull-right margintop20">* Silahkan isi yang memiliki tanda tersebut, Terima Kasih!</span>
                </p>
              </div>
            </div>  
          </form>
        </div>              
      </div>
    </div>
  </div>
</section>
<section class="callaction" id="map"></section>