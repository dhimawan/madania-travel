<div class="container clearfix">
  <div id="banner-wrapper">
    <div class="banner-container">
      <a href="#"><img src="<?=base_url("assets/images/banner-image.jpg")?>" alt="banner" /></a>
    </div>
    <div class="title-container">
            <h2 class="page-title"><?=$title_content?></h2>
            <p class="sub-heading"><?=$detail[0]->name?></p>
    </div>
  </div>
  <div id="content">
    <div class="breadcrumb-bar">
      <p class="breadcrumb-wrapper">
        <a href="<?=base_url()?>">Home</a> :: 
        <a class="active" href="#"><?=$title_content?></a>
      </p>
    </div>
    <div class="post-wrapper">
      <div class="property-info-block clearfix">
        <div class="property-slider-wrapper clearfix">
          <div id="nav"></div>
          <div id="slideshow">
            <?=get_attribute_products($detail[0]->id,-1,"image",array())?>
          </div>
        </div> 
        <div class="property-info">
          <h3 class="property-single-title"><?=$detail[0]->name?></h3>
            <p class="sub-heading address"><?=$detail[0]->address?> <?=($detail[0]->status !="active" ? "<h3 style='background-color:red'>SOLDOUT</h3>" : "<h3 style='background-color:GREEN'>ACTIVE</h3>" )?></p>
            <span class="date"><?=date("d-m-Y",strtotime($detail[0]->on_auction))?></span>
            <ul class="property-features-block">
              <?=get_attribute_products($detail[0]->id,-1,"text",array("open" => "<li class='home-size'>","end"=>"</li>"))?>
            </ul> 
        </div>
      </div>
      <p><?=$detail[0]->description?></p>
      <?php if(count($product_customer)== 0){?>
        <?php if($detail[0]->status == "active"){ ?>
          <div class="share-bar clearfix" style="width:100%">
            <form method="post" action="<?=base_url("members/update_product_customer")?>" style="width:100%">
              <input type="hidden" name="product_id" value="<?=$detail[0]->id?>" />
              <div ><h5>Jika Anda berminat, Tekan <input type="submit" id="minat" value="Minat" class="btn" />  agar kami bisa memprosesnya untuk anda. </h5> </div>
            </form>
          </div>
        <?php }else{ ?>
          <div class="share-bar clearfix" style="width:100%;background-color:red">
              <div ><h5> DATA PROPERTI TELAH TERLELANG </h5> </div>
            </form>
          </div>
        <?php } ?>
      <?php }else if($product_customer->status=="minat"){?>
         <div class="share-bar clearfix" style="width:100%;">
          <div ><h5>Anda Telah terdaftar sebagai peminat Properti ini.</h5> </div>
      </div>
      <?php } ?>
      
      <div class="propter-freature-list clearfix list-wrapper">
        <h3 class="title">Marketing</h3>
        <ol><?=get_attribute_product_employees($detail[0]->id,-1,"text",array("open" => "<li>","end"=>"</li>"))?></ol>
      </div>

      <div class="propter-freature-list clearfix list-wrapper">
        <h3 class="title">Detail Properti</h3>
        <ol><?=get_attribute_products($detail[0]->id,-1,"text",array("open" => "<li>","end"=>"</li>"))?></ol>
      </div>
      <div class="virtual-tour-block list-wrapper">
        <h3 class="title">Persyaratan</h3>
        <ul><?=get_requirment_products($detail[0]->id,array("open" => "<li>","end"=>"</li>"))?> </ul>
      </div>
      <hr/>


        <div class="map-container clearfix">
          <div id="map_canvas" style="width: 634px; height: 350px; float:right; border:1px solid #a3a3a3;">
            <iframe width="634" height="350" src="http://maps.google.com/?z=12&q=<?=$detail[0]->lat?>,<?=$detail[0]->long?>&output=embed&amp;hl=en&amp;geocode=&amp;" >
            </iframe>
          </div>
        </div>
    </div>
            
  </div><!-- CONTENT CLOSED --> 
  
  <div class="sidebar">
      <div class="side-widget">
        <?= $this->load->view($widget["sidepage_search"]);?>
      </div>
      <div class="side-widget">
        <?= $this->load->view($widget["sidepage_employee"]);?>
        <?= $this->load->view($widget["sidepage_populerproperti"]);?>
        <?= $this->load->view($widget["sidepage_newsletter"]);?>
      </div>
  </div><!-- SIDEBAR CLOSED -->   
</div><!-- CONTAINER CLOSED --> 
