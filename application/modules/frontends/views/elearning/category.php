<div class="container clearfix">
  <div id="banner-wrapper">
    <div class="banner-container">
        <a href="#"><img src="<?=base_url("assets/images/banner-image.jpg")?>" alt="banner" /></a>
    </div>
    <div class="title-container">
      <h2 class="page-title">Kategori Properti</h2>
      <p class="sub-heading"><?=$title_content?></p>
    </div>
  </div><!-- SLIDER WRAPPER CLOSED -->
    <div id="content">
            <ul class="listing">
                <?php foreach ($detail as $key => $value) { ?>
                <?php $image = $this->attribute_product->find(array("product_id"=>$value->id,"type" => "image"),1); ?>
                <?php $text = $this->attribute_product->find(array("product_id"=>$value->id,"type" => "text"),1); ?>
                    <li>
                            <div class="property-display-wrapper">
                                    <a href="<?=base_url("frontends/detail/".$value->id."")?>">
                                        <img src="<?=base_url("assets/uploads/products")?>/<?=(count($image) > 0 )? $image[0]->value : "" ?>" height="176" width="296" alt="house view" /></a>
                                    <div class="pricetag-listing">
                                            <small><?=$value->name?></small>
                                            <h5 class="price">Rp.<?=number_format($value->selling_price)?></h5>
                                    </div>
                            </div>
                            <div class="property-detail">
                                    <h4 class="property-title" ><?=$value->name?> <span>Rp.<?=number_format($value->selling_price)?></span></h4>
                                    <p>Rp.<?=word_limiter($value->description,40)?></p>
                                    <ul class="property-features">
                                        <?foreach ($text as $key2 => $txt) {?>
                                            <li>
                                                <p class="left-feature"><span class="city"><?=$txt->name?>:</span><?=$txt->value?></p>
                                            </li>
                                        <?php }?>
                                    </ul>
                            </div>
                     </li>
                <?php } ?>
            </ul>
            <hr />
            <div id="pagination">
                    <a href="#" class="btn current">1</a>
                    <a href="#" class="btn">2</a>
            </div>
            
    </div><!-- CONTENT CLOSED --> 
            
  <div class="sidebar">
    <div class="side-widget">
        <?= $this->load->view($widget["sidepage_search"]);?>
      </div>
      <div class="side-widget">
        <?= $this->load->view($widget["sidepage_employee"]);?>
        <?= $this->load->view($widget["sidepage_populerproperti"]);?>
        <?= $this->load->view($widget["sidepage_newsletter"]);?>
      </div>
  </div><!-- SIDEBAR CLOSED -->   