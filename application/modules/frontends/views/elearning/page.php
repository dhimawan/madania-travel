 <div class="container clearfix">
    
    <div id="banner-wrapper">
        <div class="banner-container">
            <a href="#"><img src="<?=base_url("assets/images/banner-image.jpg")?>" alt="banner" /></a>
        </div>
        <div class="title-container">
            <h2 class="page-title"> <?=$detail->intitle?></h2>
                <p class="sub-heading"></p>
        </div>
    </div><!-- SLIDER WRAPPER CLOSED -->
    <?=$detail->incontent?>
    </div><!-- CONTENT HOME CLOSED --> 
</div><!-- CONTAINER CLOSED --> 
