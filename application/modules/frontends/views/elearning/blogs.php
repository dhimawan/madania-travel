<div id="container">
<div class="double-header"><span class="first-head">Blog</span>
  <!--<span class="second-head">Check out latest news and events</span>-->
    </div>
<?php foreach ($data as $key => $value) {?>
  <div class="item-blog first">
    <h2><a href=""><?=$value->title?></a></h2>
    <div class="entry-meta">                     
        <a class="ico-link date" href=""><?=date("m Y",strtotime($value->created_at))?></a>
      <a class="ico-link author" href="">admin</a>       
        <span class="ico-link categories">
          <a href="">Featured</a>, <a href="">Gadgets</a>
        </span>          
        <a href="#" class="button" title=""><span><i class="lik"></i>Like! (124)</span></a>
        <a href="#" class="button" title=""><span><i class="commen"></i>Leave a comment</span></a>
    </div>
       <?=$value->content?>
    <a href="#" class="button" title=""><span><i class="more"></i>Details</span></a>
  </div>
<?php }?>


<div id="nav-above" class="navigation blog">
  <ul class="paginator">
      <li class="larr"><a href="#" class="button" title=""><span>First</span></a></li>
      <li class="dotes"></li>
      <li><a href="#" class="button" title=""><span>1</span></a></li>
      <li class="act"><a href="#" class="button" title=""><span>2</span></a></li>
      <li><a href="#" class="button" title=""><span>3</span></a></li>
      <li><a href="#" class="button" title=""><span>4</span></a></li>
      <li><a href="#" class="button" title=""><span>5</span></a></li>
      <li class="dotes"></li>
      <li class="rarr"><a href="#" class="button" title=""><span>Last</span></a></li>
  </ul>
  <div class="paginator-r">
      <span class="pagin-info">Page 9 of 23</span>
      <a class="prev" href=""></a>
      <a class="next" href=""></a>
  </div>
</div>
</div><!-- #container-->

<aside id="aside">
<div class="widget">
  <div class="header">Latest twitts</div>
  <div class="reviews-t">
      <div class="coda-slider-wrapper">
        <div class="coda-slider preload" id="coda-slider-1">
      
            <div class="panel">
            <div class="panel-wrapper">
                <p>Curabitur scelerisque mauris quis diam gravida eu placerat ligula scelerisque. In vitae sem nec massa imperdiet.<br />
                        <span class="blue-date">12 hours ago</span></p>
            </div>
          </div>
  
          <div class="panel">
            <div class="panel-wrapper">
                <p>Vel vitae aptent velit emagna in molestienec. torquent vehicula. Ut varius vestibulum ligula est labore semper   ornare morbi leo porttitor leo.<br />
                        <span class="blue-date">5 hours ago</span></p>
            </div>
          </div>       

          <div class="panel">
            <div class="panel-wrapper">
                <p> Nulla aliquam, lacus quis varius porttitor, dolor lorem scelerisque   lorem, vel sagittis purus nisl et lacus. Sagittis fringilla.<br />
                        <span class="blue-date">1 days ago</span></p>
            </div>
          </div>
                
                <div class="panel">
            <div class="panel-wrapper">
                <p>In penatibus donec. In venenatis vivamus. Nulla aliquam, lacus quis varius porttitor, dolor lorem scelerisque   lorem, vel sagittis purus nisl et lacus.<br />
                        <span class="blue-date">18 hours ago</span></p>
            </div>
          </div>
                
                <div class="panel">
            <div class="panel-wrapper">
                <p>Nonummy a velit. Duis dapibus pulvinar. Aliquam egestas vivamus consectetuer massa imperdiet condimentum.<br />
                        <span class="blue-date">4 days ago</span></p>
            </div>
          </div> 
      </div>           
  </div>
  <div class="reviews-b"></div>                   
</div>
     <p class="autor twit"><a href="http://twitter.com/#!/premium_theme">@premium_theme</a></p>      
</div>

<div class="widget">
    <div class="header">Flickr</div>
    <div class="flickr">
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-1.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-2.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-3.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-4.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-5.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-6.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-7.jpg" width="58" height="58" alt="" /><i></i></a>
      <a href="#" class="alignleft-f"><img src="<?=$assets_img_path?>58x58-8.jpg" width="58" height="58" alt="" /><i></i></a>
    </div>
</div>

<div class="widget">
  <div class="header">Popular posts</div>
    <div class="post first">
      <a href="#">Nonummy a velit duis dapibus pulvinar aliquam egestas vivamus.</a>
      <div class="goto-post">
          <span class="ico-link date">29 April 2011</span>
          <span class="ico-link likes">144 likes</span>
      </div>
    </div>
    <div class="post">            
      <a href="#">Vestibulum nec aliquam sonec ut mauris vel risus rutrum commodo.</a>
      <div class="goto-post">
          <span class="ico-link date">29 April 2011</span>
          <span class="ico-link likes">144 likes</span>
      </div>
    </div>
    <div class="post last">
      <a href="#">Vivamus dolor pede</a>
      <div class="goto-post">
          <span class="ico-link date">29 April 2011</span>
          <span class="ico-link likes">144 likes</span>
      </div>
    </div>   
</div>
