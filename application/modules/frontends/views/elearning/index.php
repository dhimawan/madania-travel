
<section class="index-callaction">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="big-cta">
          <div class="clearfix"></div>
          <div class="cta-text">
            <h3 itemprop="description" ><?=slogan_company()?></h3>
          </div>
          <div class="cta">
            <a class="btn btn-large btn-theme btn-rounded " href="<?//=base_url("assets/uploads/pages/".$area_download[0]->val_image."")?>" title="download brosur dari mandania travel dan madania semesta wisata" target="_blank" data-toggle="modal" data-target="#areaDownload" > Download Brosur</a>

            <a class="btn btn-large btn-primary btn-rounded" href="#" title=" Cek ketersedian porsi haji oleh madani travel pada departement agama." data-toggle="modal" data-target="#porsiHaji">Cek Prosi Haji</a>
            <!-- <a class="btn btn-large btn-rounded btn-blue" href="<?=site_url("add_registration")?>" title="PEndaftaran umroh dan haji">Pendaftaran</a> -->
            <a class="btn btn-large btn-rounded btn-blue" href="<?//=site_url("add_registration")?>" title="PEndaftaran umroh dan haji">Pendaftaran</a>
          </div>
          <div class="cta">
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="areaDownload" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4 id="mySignupModalLabel">Area <strong>Download</strong></h4>
    </div>
    <div class="modal-body" id="bodyDetailDeparture">
      <?php foreach ($area_download as $key => $value) { ?>
        <div class="media">
          <a class="pull-left" href="<?=base_url("assets/uploads/pages/".$value->val_image."")?>" target="_blank" >
            <img class="media-object" src="<?=base_url("assets/images/download.png")?>" alt="<?=$value->title?>" width="50%">
          </a>
          <div class="media-body">
            <h4 class="media-heading"><?=$value->title?></h4>
            <p><?=$value->description?> </p>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
    <div id="porsiHaji" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true" style="width:90%;left:25%;height:90%">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h4 id="mySignupModalLabel">Cek Porsi <strong>Haji</strong></h4>
    </div>
    <div class="modal-body" id="bodyDetailDeparture">
     <iframe src="http://www.haji.kemenag.go.id/" width="100%" height="500px"></iframe>
    </div>
  </div>
</section>

<?//=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_forex")?>
<section id="content">
    <div class="cont-bg clearfix" id="content_index_page">
      <?=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_features")?>
      <?=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_portofolio")?>
      <?=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_news")?>
      <?=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_extracurricular")?>
    </div>
</section>
<section id="bottom">
  <div class="container">
    <div class="row">
      <div class="span12">
        <div class="aligncenter">
          <div id="twitter-wrapper">
            <div id="twitter"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="mySignup" class="modal styled hide fade" tabindex="-1" role="dialog" aria-labelledby="mySignupModalLabel" aria-hidden="true">
  <div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
  <h4 id="mySignupModalLabel">Create an <strong>account</strong></h4>
  </div>
  <div class="modal-body">
    <form class="form-horizontal">
      <div class="control-group">
        <label class="control-label" for="inputEmail">Email</label>
        <div class="controls">
        <input type="text" id="inputEmail" placeholder="Email">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputSignupPassword">Password</label>
        <div class="controls">
        <input type="password" id="inputSignupPassword" placeholder="Password">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputSignupPassword2">Confirm Password</label>
        <div class="controls">
        <input type="password" id="inputSignupPassword2" placeholder="Password">
        </div>
      </div>
      <div class="control-group">
        <div class="controls">
          <button type="submit" class="btn">Sign up</button>
        </div>
        <p class="aligncenter margintop20">Already have an account? <a href="http://iweb-studio.com/wrapbootstrap/flattern/index.html#mySignin" data-dismiss="modal" aria-hidden="true" data-toggle="modal">Sign in</a></p>
      </div>
    </form>
  </div>
</div>