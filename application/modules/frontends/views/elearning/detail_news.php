<div class="top-decor"></div>

<section class="callaction">
  <div class="container">
    <div class="row-fluid">
      <div class="span4"><?=generate_side_news()?></div>
      <div class="span8">
        <article>
        <div class="row-fluid">
          <div class="span12 news_text" >
            <div class="post-image">
              <div class="post-heading">
                <h3 itemprop="name" ><?=article_link($value,$value->title)?></h3>           
              </div>
              <img itemprop="image"  src="<?=($value->is_internal==true ? base_url("assets/uploads/news/$value->image") : $value->image)?>" alt="<?=$value->title?>">
            </div>
            <span itemprop="description"> <?=$value->description?></span>
            <div class="bottom-article">
              <ul class="meta-post">
                <li><i class="icon-calendar"></i><a href="<?=news_url($value,$value->title)?>"> <?=date("d M Y",strtotime($value->updated_at))?></a></li>
                <li><i class="icon-user"></i><a href="<?=news_url($value,$value->title)?>"> Admin</a></li>
                <li><i class="icon-folder-open"></i><a href="<?=news_url($value,$value->title)?>"><?=$value->category_name?></a></li>
                <li><i class="icon-comments"></i> <a href="<?=news_url($value,$value->title)?>"><?=count($comments)?> Comments</a></li>

                <li><i class="icon-comments"></i> <a href="<?=news_url($value,$value->title)?>"> </li>

                <li><a href="#" data-placement="bottom" title="" data-original-title="Facebook"  onclick="shareFacebook('<?=news_url($value,$value->title)?>','<?=$value->title?>','<?=($value->is_internal==true ? base_url("assets/uploads/news/$value->image") : $value->image)?>','<?=strip_tags(word_limiter($value->description,40))?>'); return false;" ><i class="icon-facebook icon-square"></i></a></li>

                <li><a href="#" data-placement="bottom" title="" data-original-title="Twitter" onclick="shareTwitter('<?=news_url($value,$value->title)?>','<?=$value->title?>'); return false;"  ><i class="icon-twitter icon-square"></i></a></li>
                
                <li><a href="#" data-placement="bottom" title="" data-original-title="Google plus"  onclick="shareGooglePlus('<?=news_url($value,$value->title)?>'); return false;" ><i class="icon-google-plus icon-square"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        </article>
        
        <div class="comment-area">
          <h4><?=count($comments)?> Comments</h4>
          <?php foreach ($comments as $key => $value) { ?>
            <div class="media">
            <a href="#" class="thumbnail pull-left">
              <img src="<?=$this->gravatar->get_gravatar("$value->email");?>" alt="<?=$value->name?>">
            </a>
            <div class="media-body">
              <div class="media-content">
                <h6><span><?=date("d M Y",strtotime($value->updated_at))?></span> <?=$value->name?></h6>
                <p><?=$value->descriptions?></p>
                <!-- <a href="#" class="align-right">Reply comment</a> -->
              </div>
            </div>
          </div>
          <?php } ?>
          
          <h4>Tinggalkan Pesan</h4>
          
          <form id="commentform" action="<?=base_url("frontends/add_comment")?>" method="post" name="comment-form">
          <div class="">
            <div class="span6">
              <input type="text" placeholder="* Enter your full name" name="comment[name]" >  
              <input type="hidden" name="comment[news_id]" value="<?=$value->news_id?>">  
            </div>            
            <div class="span6">
              <input type="text" placeholder="* Enter your email address" name="comment[email]">
            </div>         
            <div class="span12 no-margin-left">
              <p>
                <textarea rows="12" class="input-block-level" placeholder="*Your comment here" name="comment[descriptions]"></textarea>
              </p>
              <p>
              <button class="btn btn-theme margintop10 no-margin-left" type="submit">Kirim Komentar</button>
              </p>
            </div>
          </div>  
          </form>
        </div>
      </div>
    
    </div>
  </div>
</section>