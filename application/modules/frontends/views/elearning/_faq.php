<div class="top-decor"></div>

<section class="callaction">
  <div class="container">
    <div class="row-fluid">
      <div class="row-fluid">
      	<div class="span4"><?=generate_side_requirements()?></div>  
	      <div class="span8">
	      	<div class="accordion" id="accordion2">
	      	<?=$detail->incontent?>
	        <?php foreach ($attribute_pages as $key => $value) { ?>
	          <div class="accordion-group">
	            <div class="accordion-heading">
	              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?=$value->id?>">
	                <?=$value->title?>
	              </a>
	            </div>
	            <div id="collapse<?=$value->id?>" class="accordion-body collapse <?=($key==0 ? "in" : "")?>">
	              <div class="accordion-inner">
	              	<?=$value->description?>
	              </div>
	            </div>
	          </div>
	        <?php } ?>
	        </div>
	      </div>
      </div>
    </div>
  </div>
</section>

