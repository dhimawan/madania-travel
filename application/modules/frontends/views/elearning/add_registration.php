<style type="text/css">
 .bwizard-steps {
  display: inline-block;
  margin: 0; padding: 0;
  background: #fff }
  .bwizard-steps .active {
    color: #fff;
    background: #007ACC }
  .bwizard-steps .active:after {
    border-left-color: #007ACC }
  .bwizard-steps .active a {
    color: #fff;
    cursor: default }
  .bwizard-steps .label {
    position: relative;
    top: -1px;
    margin: 0 5px 0 0; padding: 1px 5px 2px }
  .bwizard-steps .active .label {
    background-color: #333;}
  .bwizard-steps li {
    display: inline-block; position: relative;
    margin-right: 5px;
    padding: 12px 17px 10px 30px;
    *display: inline;
    *padding-left: 17px;
    background: #efefef;
    line-height: 18px;
    list-style: none;
    zoom: 1; }
  .bwizard-steps li:first-child {
    padding-left: 12px;
    -moz-border-radius: 4px 0 0 4px;
    -webkit-border-radius: 4px 0 0 4px;
    border-radius: 4px 0 0 4px; }
  .bwizard-steps li:first-child:before {
    border: none }
  .bwizard-steps li:last-child {
    margin-right: 0;
    -moz-border-radius: 0 4px 4px 0;
    -webkit-border-radius: 0 4px 4px 0;
    border-radius: 0 4px 4px 0; }
  .bwizard-steps li:last-child:after {
    border: none }
  .bwizard-steps li:before {
    position: absolute;
    left: 0; top: 0;
    height: 0; width: 0;
    border-bottom: 20px inset transparent;
    border-left: 20px solid #fff;
    border-top: 20px inset transparent;
    content: "" }
  .bwizard-steps li:after {
    position: absolute;
    right: -20px; top: 0;
    height: 0; width: 0;
    border-bottom: 20px inset transparent;
    border-left: 20px solid #efefef;
    border-top: 20px inset transparent;
    content: "";
    z-index: 2; }
  .bwizard-steps a {
    color: #333 }
  .bwizard-steps a:hover {
    text-decoration: none }
.bwizard-steps.clickable li:not(.active) {
  cursor: pointer }
.bwizard-steps.clickable li:hover:not(.active) {
  background: #ccc }
.bwizard-steps.clickable li:hover:not(.active):after {
  border-left-color: #ccc }
.bwizard-steps.clickable li:hover:not(.active) a {
  color: #08c }
@media (max-width: 480px) { 

  .bwizard-steps li:after,
  .bwizard-steps li:before {
    border: none }
  .bwizard-steps li,
  .bwizard-steps li.active,
  .bwizard-steps li:first-child,
  .bwizard-steps li:last-child {
    margin-right: 0;
    padding: 0;
    background-color: transparent }
}
.error{
  color: red;
}
</style>
<script src="<?=base_url("/assets/js/jquery.bootstrap.wizard.js")?>"></script>
<script src="<?=base_url("/assets/js/prettify.js")?>"></script>
<script src="<?=base_url("/assets/js/jquery.validate.js")?>"></script>
<!-- <script src="../prettify.js"></script> -->
<!-- // <script src="<?=base_url("/assets/js/transition.js")?>"></script> -->
<link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_js_path?>datepicker/css/datepicker.css" />
  <script type="text/javascript" src="<?=$assets_js_path?>datepicker/js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
$(document).ready(function(){

  $("#packet_id").change(function(){
    var id = $(this).val();
    if(id!=""){
      $.ajax({
        type: "POST",
        url: "<?=base_url('frontends/get_packet')?>",
        data: {id:id}
      }).done(function(msg){
        $("#detail_packet_id").html(msg);
      });
      $.ajax({
        type: "POST",
        url: "<?=base_url('frontends/get_departure_date')?>",
        data: {id:id}
      }).done(function(msg){
        $("#date_of_departure").html(msg);
      });
    }
  });

  // $('#rootwizard').bootstrapWizard({'tabClass': 'bwizard-steps'});
  $('#rootwizard').bootstrapWizard({
    'tabClass': 'bwizard-steps',
    'onTabClick': function(tab, navigation, index) {
      var $valid = $("#registration_form").valid();
      if(!$valid) {
        // $validator.focusInvalid();
        return false;
      }
    },
    'onNext': function(tab, navigation, index) {
      // alert(index);
      var $valid = $("#registration_form").valid();
      if(!$valid) {
        // $validator.focusInvalid();
        // $validator_2.focusInvalid();
        return false;
      }
    },
    onTabShow: function(tab, navigation, index) {
        var $total = navigation.find('li').length;
        var $current = index+1;
        var $percent = ($current/$total) * 100;
        $('#rootwizard').find('.bar').css({width:$percent+'%'});
        
        // If it's the last tab then hide the last button and show the finish instead
        if($current >= $total) {
            $('#rootwizard').find('.pager .next').hide();
            $('#rootwizard').find('.pager .finish').show();
            $('#rootwizard').find('.pager .finish').removeClass('disabled');
        } else {
            $('#rootwizard').find('.pager .next').show();
            $('#rootwizard').find('.pager .finish').hide();
        }
        
    }
  });
   $(".tanggal").datepicker();
  

});
function update_departure_date(id_selector){
  $.ajax({
    type: "POST",
    url: "<?=base_url('frontends/get_departure_startdate_endate')?>",
    data: {id:id_selector.value}
  }).done(function(msg){
    explode = msg.split("{{explode_this}}");
    $("#departure_start").val(explode[0]);
    $("#departure_end").val(explode[1]);
  });
}

</script>
 
 <script type="text/javascript">
  // $(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );
 </script>

<div class="top-decor"></div>

<section class="callaction">
  <div class="container">
    <div class="row-fluid clearfix">  
      <h4 class="float-l"><i class="icon-asterisk icon-large"></i>Pendaftaran Paket Umroh dan Haji</h4>
    </div><!--/.row-->
    <hr class="hr-dark">
    <div class="row-fluid">
      <h4 class=""><i class="icon-plus-sign-alt"></i> General Information</h4>
      <hr>
      <div class="clearfix">
        <?php  if($params["customer_packet"]["total_jamaah"]!=0){ ?>
          <?=$this->load->view("frontends/elearning/partials/_registration_general_info")?>
        <?php }else{ ?>
          <?=$this->load->view("frontends/elearning/partials/_add_registration_general_info")?>
        <?php }?>
        
      </div>
    </div>
    <hr class="hr-dark">
     <?php if($params["customer_packet"]["total_jamaah"]!=0){?>
     <form id="registration_form" method="post" action="<?=base_url("frontends/create_registration")?>">
      
      <input type="hidden" name="customer_packet[attribute_page_id]"  id="attribute_page_id" value="<?=$params["customer_packet"]["attribute_page_id"]?>">
      <input type="hidden" name="customer_packet[total_jamaah]"  id="total_jamaah"  value="<?=$params["customer_packet"]["total_jamaah"]?>" >
      <input type="hidden" name="customer_packet[departure_id]"  id="departure_id" value="<?=$params["customer_packet"]["departure_id"]?>" >
      <input type="hidden" name="customer_packet[packet_id]"  id="packet_id" value="<?=$params["customer_packet"]["packet_id"]?>" >

      <input type="hidden" name="customer_packet[departure_start]"  id="departure_start" value="<?=$params["customer_packet"]["departure_start"]?>">
      <input type="hidden" name="customer_packet[departure_end]"id="departure_end" value="<?=$params["customer_packet"]["departure_end"]?>">

      <input class="input" type="hidden" name="customer_packet[order_name]" value="<?=$params["customer_packet"]["order_name"]?>">
      <input class="input" type="hidden" name="customer_packet[order_telp]" value="<?=$params["customer_packet"]["order_telp"]?>">
      <input class="input" type="hidden" name="customer_packet[order_hp]" value="<?=$params["customer_packet"]["order_hp"]?>">
      <input class="input" type="hidden" name="customer_packet[order_email]" value="<?=$params["customer_packet"]["order_email"]?>">
      <input class="input" type="hidden" name="customer_packet[address]" value="<?=$params["customer_packet"]["address"]?>">
      <input class="input" type="hidden" name="customer_packet[order_mou]" value="<?=$params["customer_packet"]["order_mou"]?>">
     <div id="rootwizard"  class="row-fluid clearfix">
      <ul class="nav nav-tabs">
        <?php for ($i=0; $i < $params["customer_packet"]["total_jamaah"]; $i++) {  ?>
          <li >
              <a href="#div_jamaah_<?=$i?>" data-toggle="tab"  >
               Jamaah Ke-<span class="label"><?=$i+1?></span>
              </a>
            </li>
        <?php } ?>
      </ul>
      
        <div id="content" class="tab-content">
        <?php for ($i=0; $i < $params["customer_packet"]["total_jamaah"]; $i++) {  ?>
        <?php $data["index"] = $i ?>
          <div id="div_jamaah_<?=$i?>" class="tab-pane <?=($i==0 ? "active" : "" ) ?>">
             <h4><i class="icon-plus-sign-alt"></i>Data Jamaah <span id="index_jamaah_title"> <?=$i+1?> </span></h4>
            <?=$this->load->view("frontends/elearning/partials/_jamaah_form",$data)?>
          </div>
          
        <?php } ?>
          <ul class="pager wizard">
            <li class="previous"><a >Sebelumnya</a></li>
            <li class="next"><a>Selanjutnya</a></li>
            <!-- <li class="next finish" style="display:none;"><a href="javascript:;">Finish</a></li> -->
            <input type="submit" class="next finish" id="submit" style="display:none" value="Finish">
          </ul>
        </div>
        
      </div><!--/.row-->
    
    </form>
     <?php } ?>
      
  </div>

  <!-- <div class="container">
    <div class="row-fluid">
      <div class="row-fluid">

      
        <form id="registrationform" action="frontends/create_contact_us" method="post" class="validateform" name="send-contact">
            <div id="sendmessage">
              Your message has been sent. Thank you!
            </div>
            <div id="data_paket">
              <fieldset> 
                <legend> Data Paket</legend>
                
                <ul>
                  <li>
                    <div class="field">
                      <label>Daftar Paket</label>
                      <select name="customer_packet[attribute_page_id]" id="packet_id">
                        <option value="">--Pilih Paket--</option>
                        <?php foreach ($packet as $key => $value) { ?>
                          <option value="<?=$value->id?>"><?=$value->name?></option>
                        <?php }?>
                      </select>
                      <div class="validation"></div>
                    </div>
                    <div class="field">
                      <label>Detail Paket</label>
                      <select name="customer_packet[packet_id]" id="detail_packet_id">
                        
                      </select>
                      <div class="validation"></div>
                    </div>
                  </li>
                </ul>
              </fieldset>
            </div>
          </form>
    </div>
  </div> -->
</section>
