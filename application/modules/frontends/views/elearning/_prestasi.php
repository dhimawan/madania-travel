<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="span4">
				<div class="inner-heading">
				<h2><?=$title_content?></h2>
				</div>
			</div>	
			<div class="span8">	</div>
		</div>			
	</div>
</section>
	
<section id="content">
	<div class="container">
		<div class="row">
			<div class="span12">
				<?=$detail->incontent?>
			</div>
		</div>
		<!-- divider -->
		<div class="row">
			<div class="span12">
				<div class="solidline"></div>
			</div>
		</div>
		<!-- end divider -->
		
	<div class="row">
		<div class="span12">
			<h4>Prestasi</h4>
		</div>
		<?php 
		$attr_page = $this->db->get_where("attribute_pages",array("page_id"=> $detail->id))->result(); 
		foreach ($attr_page as $key => $value) { ?>
			<div class="span3">
				<img src="<?=base_url("assets/uploads/pages/$value->value")?>" alt="<?=$value->value?>" class="img-polaroid" width="200px" height="200px">
				<div class="roles">
					<p class="lead"><strong><?=$value->name?></strong></p>
					<?=word_limiter($value->description,5)?>
				</div>
			</div>
		<?php	} ?>
		
	</div>
</section>