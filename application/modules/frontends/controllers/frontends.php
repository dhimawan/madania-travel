<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontends extends Frontend_Controller {

  public function __construct(){
     parent::__construct();
  }

  public function index(){
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/index";
    $data["slideshow_widget"] = true;
    $data["title"] = "home";
    $data["content"]["title_content"] = " Home ";

    $data["content"]["packet"] = $this->page->get_pages("daftar_paket");
    $data["content"]["news"] = $this->db->limit("5")->order_by("updated_at","desc")->get("news")->result();
    $data["content"]["galleries"] = $this->page->get_pages_with_limit("Gallery_selama_kegiatan_umroh_dan_haji",6);
    $data["content"]["slideshow"] = $this->page->get_pages_with_limit("slideshow",5);
    $data["content"]["testimonial"] = $this->page->get_pages_with_limit("testimonial_dari_jamaah_madania_travel",3);
    $data["content"]["area_download"] = $this->page->get_pages_with_limit("area_download",25);
    $data["content"]["last_area_download"] = $this->db->limit(1)->order_by("updated_at","desc")->get_where("attribute_pages",array("page_id"=>$data["content"]["area_download"][0]->id))->row();
    $data["content"]["iklan"] = $this->page->get_pages_with_limit("kerjasama",10);
    $this->layouts->render($data);
  }

 

  public function detail($id){
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/detail";
    $data["slideshow_widget"] = true;
    $data["title"] = "detail properti ";
    $data["content"]["title_content"] = " Detail Properti ";
    $data["content"]["detail"] = $this->product->find(array('id' => $id),1);
    $data["content"]["product_customer"] = $this->db->get_where("product_customers",array("product_id" => $id,"customer_no_ktp" => $this->session->userdata("customer_no_ktp")))->row();
    $data["content"]["product_employees"] = $this->db->get_where("product_employees",array("product_id" => $id))->result();

    $this->layouts->render($data);
  }

  public function page($id){
    
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/page";
    $data["slideshow_widget"] = true;
    $data["title"] = "detail properti ";
    $data["content"]["title_content"] = " Detail Properti ";
    $data["content"]["detail"] = $this->page->find_entity_by_id($id);
    // $this->layouts->render($data);
  }

  public function pages($url){
    if($url !="" ){
      if($url == "news"){
        $this->news($url);
      }else{
        $this->load->model("pages/page");
        $seo = $this->page->get_seo($url);
        $this->layouts->layout = "pages";
        $data["content_path"] = $this->where_template($url);
        $data["slideshow_widget"] = true;
        $data["title"] = "detail properti ";
        $data["content"]["title_content"] = "$url";
        $data["content"]["detail"] = $this->db->get_where("pages",array("url" => $url))->row();
        if($url=="profile"){
          $data["content"]["attribute_pages"] = $this->db->get_where("attribute_pages",array("page_id" => $data["content"]["detail"]->id))->result();
        }else{
          $data["content"]["attribute_pages"] = $this->db->select("b.*")->join("attribute_pages b"," b.page_id = a.id")->where("a.id = ".$data["content"]["detail"]->id."")->or_where("a.parent_id = ",$data["content"]["detail"]->id)->get("pages a")->result();
        }
        
        $data["content"]["child"] = $this->page->get_child($url);
        $data["seo"] = $this->set_metatags($seo);
        $data["seo_title"] = $this->set_title_page($seo);
      
      $this->layouts->render($data);  
      }
        
    }else{
      redirect("/");
    }
  }

  public function categories(){
    $this->load->model("pages/page");
    $this->load->library('pagination');

    $url="news";
    $name = $this->uri->segment(3);
    if($name != ""){
      $category = $this->db->get_where("news_categories",array("name"=>$name))->row();
      if(count($category)>0){
        $seo = $this->page->get_seo($url);
        $this->layouts->layout = "pages";
        $config['base_url'] = base_url("$url");
        $config['per_page'] = 10;     
        $seo = $this->page->get_seo($url);
        $this->layouts->layout = "pages";
        $data["seo"] = $this->set_metatags($seo);
        $data["seo_title"] = $this->set_title_page($seo);
        $data["content_path"] ="frontends/".$this->config->item("frontend_layout")."/_news";

        $data["content"]["title_content"] = "$url";
        $data["content"]["detail"] = $this->db->get_where("pages",array("url" => $url))->row();
        $data["content"]["news"] = $this->db
            ->select("news.*,news_categories.name as category_name")
            ->order_by("updated_at","desc")
            ->join("news_categories","news_categories.id = news.news_category_id")
            ->where("news.news_category_id",$category->id)
            ->limit(10)
            ->get("news")
            ->result();
        $config['total_rows'] = 100;
        $this->pagination->initialize($config); 
        $this->layouts->render($data);
      }else{
         redirect("/");
      }
    }else{
       redirect("/");
    }
    
  }

  private function news($url){
    $this->load->model("pages/page");
    $this->load->library('pagination');
    $config['base_url'] = base_url("$url");
    $config['per_page'] = 10;     
    $seo = $this->page->get_seo($url);
    $this->layouts->layout = "pages";
    $data["seo"] = $this->set_metatags($seo);
    $data["seo_title"] = $this->set_title_page($seo);
    $data["content_path"] ="frontends/".$this->config->item("frontend_layout")."/_news";

    $data["content"]["title_content"] = "$url";
    $data["content"]["detail"] = $this->db->get_where("pages",array("url" => $url))->row();
    $data["content"]["news"] = $this->db
        ->select("news.*,news_categories.name as category_name")
        ->order_by("updated_at","desc")
        ->join("news_categories","news_categories.id = news.news_category_id")
        ->limit(10)
        ->get("news")
        ->result();
    $config['total_rows'] = 100;
    $this->pagination->initialize($config); 
    $this->layouts->render($data);
  }

  public function detail_news(){
    $this->load->library("Gravatar");
    $this->layouts->layout = "pages";
    $url = $this->uri->segment(4);
    $month = $this->uri->segment(3);
    $year = $this->uri->segment(2);
    $data["content"]["title_content"] = " ";

    if(!empty($url) && !empty($month) && !empty($year)){
      $data["content_path"] ="frontends/".$this->config->item("frontend_layout")."/detail_news";
      $data["content"]["value"] = $this->db
        ->select("news.*,news_categories.name as category_name,news.id as news_id")
        ->order_by("updated_at","desc")
        ->join("news_categories","news_categories.id = news.news_category_id")
        ->limit(10)
        ->where(array("url" => $url))
        ->get("news")
        ->row();
      $data["content"]["comments"] = $this->db->order_by("updated_at","desc")->get_where("comments",array("news_id"=>$data["content"]["value"]->id))->result();
      $seo = $this->page->get_seo_news($url);
      $data["seo"] = $this->set_metatags($seo);
      $data["seo_title"] = $this->set_title_page($seo);
      
      $this->layouts->render($data);

    }else{
      $this->news($url);
    }
  }

  public function detail_service(){
    $this->layouts->layout = "pages";
    $url = $this->uri->segment(2);
    if(!empty($url)){
      $data["content_path"] ="frontends/".$this->config->item("frontend_layout")."/detail_service";
      $params = $this->db->get_where("attribute_pages",array("name" => humanize($url)))->row();
      $data["content"]["detail"] = $params;
      $data["content"]["packets"] = $this->db->get_where("packets",array("attribute_page_id"=>$params->id))->result();
      $data["content"]["packet"] = $this->page->get_pages("daftar_paket");
      $data["content"]["title_content"] = $params->title;
      $data["seo"] = $this->set_metatags($params);
      $data["seo_title"] = $this->set_title_packet($params);
      
      $this->layouts->render($data);
    }else{
      $this->index();
    }
  }

  public function get_departures($id){
    $data = $this->db->get_where("departures",array("attribute_page_id" => $id))->result();
    $year = date('Y');
    $month = date('m');

     $event = array();
    foreach ($data as $key => $value) {
      $event[] = array(
                  'id' => $value->id,
                  'title' => "$value->name (".date("d-m-Y",strtotime($value->startdate))." S/D ". date("d-m-Y",strtotime($value->enddate)),
                  'start' => "".date("Y-m-d",strtotime($value->startdate)).""
                  // 'end' => "".date("Y-m-d",strtotime($value->enddate)).""
                  // 'url' => "http://yahoo.com/"
                );
    }
  echo json_encode($event);
  }

  public function detail_departure(){
    $params = $this->input->post();
    $data["data"] = $this->db->get_where("departures",array("id" => $params["id"]))->row();

    return $this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_pop_detail_departure",$data);
  }

  private function where_template($url){
    switch ($url) {
      case 'profiles':
        $path = "frontends/".$this->config->item("frontend_layout")."/_profile";
        break;
      case 'visi':
        $path = "frontends/".$this->config->item("frontend_layout")."/_profile";
        break;
      case 'misi':
        $path = "frontends/".$this->config->item("frontend_layout")."/_profile";
        break;
      case 'aktifitas':
        $path = "frontends/".$this->config->item("frontend_layout")."/_profile";
        break;
      case 'product_and_service_madania_travel':
        $path = "frontends/".$this->config->item("frontend_layout")."/_service";
        break;
      case 'Gallery_selama_kegiatan_umroh_dan_haji':
        $path = "frontends/".$this->config->item("frontend_layout")."/_gallery";
        break;
      case 'news':
        $path = "frontends/".$this->config->item("frontend_layout")."/_news";
        break;
      case 'contact_us':
        $path = "frontends/".$this->config->item("frontend_layout")."/_contact_us";
        break;
      case 'FAQ':
        $path = "frontends/".$this->config->item("frontend_layout")."/_faq";
        break;
        
      default:
        $path = "frontends/".$this->config->item("frontend_layout")."/page";
        break;
    }
    return $path;
  }

  public function category($id){
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/category";
    $data["slideshow_widget"] = true;
    $data["title"] = "detail properti ";
    $category = $this->category_model->find_entity_by_id($id);
    $data["content"]["title_content"] = $category->nama;
    $data["content"]["detail"] = $this->product->find(array("category_id"=>$id));
    $this->layouts->render($data);
    
  }

  public function contact_us(){
    $this->load->model("pages/page");
    $seo = $this->page->get_seo("contact_us");
    $this->layouts->layout = "pages";
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/contact_us";
    $data["slideshow_widget"] = false;
    $data["title"] = "Hubungi Kami ";
    $data["content"]["title_content"] = "Hubungi Kami";
    $data["content"]["data"] = $this->db->order_by("id","asc")->get("config")->result();
    $data["seo"] = $this->set_metatags($seo);
    $data["seo_title"] = $this->set_title_page($seo);
    $this->layouts->render($data);
    
  }

  public function create_contact_us(){
    $data = $this->input->post();
    $data["created_at"] = date("Y-m-d H:i:s");
    $data["updated_at"] = date("Y-m-d H:i:s");
    $data["status"] = "new";
    if($this->db->insert("contactus",$data)){
      $email_notif["name"]= $data["name"] ;
      $email_notif["email"]= $data["email"] ;
      $email_notif["title"]= $data["title"] ;
      $email_notif["telp"]= $data["telp"] ;
      $email_notif["description"]= $data["description"] ;
      $email_notif["date"] = $data["updated_at"] ;
      $email = array(
        "from" => 'info@madaniatravel.com' ,
        "to"   => $data["email"],
        "subject" => "Response ".$data['title']."",
        // "message" => "Terima Kasih Atas Pertanyaan anda"
        "message" => $this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_tmp_response_contact_email",$email_notif,true)
      );
      $email2 = array(
        "from" => 'info@madaniatravel.com' ,
        "to"   => "madania.marketing@gmail.com",
        "subject" => "".$data['title']."",
        "message" => $this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_tmp_contact_email",$email_notif,true)
      );
      $email3 = array(
        "from" => 'info@madaniatravel.com' ,
        "to"   => "gugunlah@gmail.com",
        "subject" => "".$data['title']."",
        "message" => $this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_tmp_contact_email",$email_notif,true)
      );
      $this->load->library("sendmail");
      $data = $this->sendmail->send($email);
      $data = $this->sendmail->send($email2);
      $data = $this->sendmail->send($email3);
      $this->contact_us();
    }
    $this->contact_us();
  }

  public function load_index_slideshow(){
    echo slideshow_view();
  }

  public function load_index_video(){
    echo generated_home_video();
  }

  public function load_index_content(){
    $data = $this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_features");
    $data +=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_portofolio");
    $data +=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_news");
    $data +=$this->load->view("frontends/".$this->config->item("frontend_layout")."/partials/_extracurricular");
    echo $data;
  }

  public function grab_kurs(){
    $url = "http://www.gmc.co.id/";
    $this->load->helper('dom_helper');
    $html = file_get_html($url);
    $find = $html->find('table[id=rate-table] tr');
    $html ='<ul id="ticker01">';
    $show = array();
    foreach ($find as $key => $value) {
      $string=array();
      if($key > 0){
        foreach ($value->find("td") as $key2 => $value2){
          if($value2->plaintext == "USD"){
            $show[] = $key;
          }
          if($value2->plaintext == "SAR"){
            $show[] = $key;
          }
        }  
      }        
    }
    foreach ($find as $key => $value) {
      $string=array();
      if($key > 0){
        if(in_array($key,$show)){
          foreach ($value->find("td") as $key2 => $value2){
            if($key2==0){
              $string[] = $value2;
            }elseif($key2==1){
              $string[] = "Rp.".$value2."(Beli)"; 
            }else{
              $string[] = "Rp".$value2."(Jual)"; 
            }
          }  
          $html .='<li><span>'.date("d/m/Y").'</span> <a href="#">'.implode($string," ").' </a></li>';
        }
      }  
    }
    $html .='</ul>';
    echo $html;
  }

  public function add_registration(){
    $this->layouts->layout = "pages";
    $seo = $this->page->get_seo("home");
    $data["seo"] = $this->set_metatags($seo);
    $data["seo_title"] = $this->set_title_page($seo);
    $this->page->get_pages("daftar_paket");
    $params = $this->input->post();
    if(count($params["customer_packet"]) > 0){
       $params["db"]["attribute_pages"] = $this->db->get_where("attribute_pages",array("id"=> $params["customer_packet"]["attribute_page_id"]))->row();
       $params["db"]["packet"] = $this->db->get_where("packets",array("id"=> $params["customer_packet"]["packet_id"]))->row();
       $params["db"]["departure"] = $this->db->get_where("departures",array("id"=> $params["customer_packet"]["departure_id"]))->row();
     
    }else{
     $params["customer_packet"]["total_jamaah"]=0;
    }
    $data["content_path"] = "frontends/".$this->config->item("frontend_layout")."/add_registration";
    $data["slideshow_widget"] = true;
    $data["title"] = "pendaftaran";
    $data["content"]["title_content"] = " pendaftaran ";
    $data["content"]["packet"] = $this->page->get_pages("daftar_paket");
    $data["content"]["params"] = $params;
    $data["content"]["propinsi"] = $this->db->get("provinces")->result();
    $data["content"]["kota"] = $this->db->get("cities")->result();

    $this->layouts->render($data);
  }

  public function get_packet(){
    $params = $this->input->post();
    $data = $this->db->get_where("packets",array("attribute_page_id"=>$params["id"]))->result();
    $html = "<option value=''>--Pilih Detail Paket--</option>";
    foreach ($data as $key => $value) {
      $html .="<option value='".$value->id."' > $value->name </option> ";
    }

    echo $html;
  }

  public function get_departure_date(){
    $params = $this->input->post();
    $data = $this->db->get_where("departures",array("attribute_page_id"=>$params["id"],"date(startdate) >=" => "".date("Y-m-d").""))->result();
    $html ="";
    if(count($data)>0){
      $html .= "<select name='customer_packet[departure_id]' onchange='update_departure_date(this)'>";
      $html .= "<option value=''>--Pilih Jadwal Keberangkatan--</option>";
        foreach ($data as $key => $value) {
          $html .="<option value='".$value->id."' > ".date("d-m-Y",strtotime($value->startdate))." s/d ".date("d-m-Y",strtotime($value->enddate))."  </option> ";
        }
      $html .="</select>";
    }
    $html .='<input type="text" class="input" placeholder="Tanggal Keberangkatan" name="customer_packet[departure_start]" id="departure_start"> ';
    $html .='<input type="text" class="input" placeholder="Tanggal Selesai"  name="customer_packet[departure_end]" id="departure_end"  >';
    echo $html;
  }

  public function get_departure_startdate_endate(){
    $params = $this->input->post();
    $data = $this->db->get_where("departures",array("id"=>$params["id"]))->row();
    echo date("d-m-Y",strtotime($data->startdate))."{{explode_this}}".date("d-m-Y",strtotime($data->enddate));
  }

  public function add_comment(){
    $params = $this->input->post();
    $params["comment"]["descriptions"] = htmlspecialchars(strip_tags($params["comment"]["descriptions"])) ;
    $params["comment"]["name"] = htmlspecialchars(strip_tags($params["comment"]["name"]));
    $params["comment"]["email"] = htmlspecialchars(strip_tags($params["comment"]["email"]));
    $params["comment"]["created_at"] = date("Y-m-d H:i:s");
    $params["comment"]["updated_at"] = date("Y-m-d H:i:s");

    $data = $this->db->get_where("news",array("id" => $params["comment"]["news_id"]))->row();
    $year = date("Y",strtotime($data->updated_at));
    $month = date("m",strtotime($data->updated_at));

    if($this->db->insert("comments",$params["comment"])){    
      redirect("news/".$year."/".$month."/".$data->url);
    }else{
      redirect("news/".$year."/".$month."/".$data->url);
    }

  }

  function create_registration(){
    $params = $this->input->post();
    // $params["customer_packet"]
    if($this->db->insert("registration",$params["customer_packet"])){
      $registration_id = $this->db->insert_id();
      // foreach ($params["biodata"]["no_ktp"] as $keyb => $valueb) {

      //   # code...
      // }
      // for ($i=0; $i < count($params["biodata"]["no_ktp"]); $i++) { 
      //   $jamaah = array(
      //       "no_ktp" => $params["biodata"]["no_ktp"][$i],
      //       "nama_lengkap" => $params["biodata"]["nama_lengkap"][$i],
      //       "nama_ayah_kandung" => $params["biodata"]["nama_ayah_kandung"][$i],
      //       "tempat_lahir" =>  $params["biodata"]["tempat_lahir"][$i],
      //       "tanggal_lahir" =>  $params["biodata"]["tanggal_lahir"][$i],
      //       "address" =>  $params["biodata"]["address"][$i],
      //       "propinsi" =>  $params["biodata"]["propinsi"][$i],
      //       "kota" =>  $params["biodata"]["kota"][$i],
      //       "kecamatan" => $params["biodata"]["kecamatan"][$i],
      //       "kelurahan" => $params["biodata"]["kelurahan"][$i],
      //       "kode_pos" => $params["biodata"]["kode_pos"][$i],
      //       "no_telp" => $params["biodata"]["no_telp"][$i],
      //       "nama_mahram" => $params["biodata"]["nama_mahram"],[$i]
      //       "no_mahram" => $params["biodata"]["no_mahram"],[$i]
      //       "pernikahan" => $params["biodata"]["pernikahan"],[$i]
      //       "jk" => $params["biodata"]["jk"],[$i]
      //       "wn" => $params["biodata"]["wn"],[$i]
      //       "pendidikan" =>$params["biodata"]["pendidikan"],[$i]
      //       "pekerjaan" => $params["biodata"]["pekerjaan"][$i],
      //       "pergi_haji" =>$params["biodata"]["pergi_haji"][$i],
      //       "hubungan_mahram" =>$params["biodata"]["hubungan_mahram"][$i],
      //       "golongan_darah" => $params["biodata"]["golongan_darah"][$i]
      //   )
      // }
    }

    echo "<pre>";
    echo count($params["biodata"]);
    echo count($params["customer_packet"]);

    // print_r($params);
  }

}