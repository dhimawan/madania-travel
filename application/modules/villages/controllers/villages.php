<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Villages extends Backend_Controller {
    function __construct(){
      parent::__construct();
  }

  function index(){
    // $crud = new grocery_CRUD();
    $crud = new ajax_grocery_CRUD();
    $data["content"]["title_content"] = "Kelurahan";
    
    $crud->set_table('villages')
        ->set_subject('kelurahan')
        ->columns('province_id','city_id','district_id','nama');
    $crud->add_fields('province_id','city_id','district_id','nama');

    $crud->display_as("city_id","Kota");
    $crud->display_as("province_id","Propinsi");
    $crud->display_as("district_id","Kecamatan");

    $crud->set_relation('city_id','cities','nama');
    $crud->set_relation('province_id','provinces','nama');
    $crud->set_relation('district_id','districts','nama');
    $crud->set_relation_dependency('city_id','province_id','province_id');
    $crud->set_relation_dependency('district_id','city_id','city_id');
    
    $crud->required_fields("city_id","nama","province_id");
    // $data["content"]["groc_view"] = $crud->render();
    // $this->layouts->render($data);
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>