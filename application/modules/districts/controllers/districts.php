<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Districts extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    // $crud = new grocery_CRUD();
    $data["content"]["title_content"] = "Kecamatan";
    $crud = new ajax_grocery_CRUD();
    $crud->set_table('districts')
        ->set_subject('Kecamatan')
        ->columns('province_id','city_id','nama');
    $crud->add_fields('province_id','city_id','nama');
    $crud->display_as("city_id","Kota");
    $crud->display_as("province_id","Propinsi");
    $crud->set_relation('city_id','cities','nama');
    $crud->set_relation('province_id','provinces','nama');
    $crud->set_relation_dependency('city_id','province_id','province_id');
    $crud->required_fields("city_id","nama","province_id");
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  function province_id()
  {
      // $this->load->helper("MY_select_area");
      $this->load->library("select_area");
      return $this->select_area->getArea('province',"province","");
  }
}
?>