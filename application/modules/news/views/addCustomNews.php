<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/swfobject.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/jquery.uploadify.v2.1.4.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/tiny_mce/tiny_mce.js")?>"></script>
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
        theme : "simple",
        editor_selector : "mceSimple"
});
</script>
<div class="span12 padding">
  <h2>
    <a href="#" role="button" class="btn btn" id="edit_page">
      <i class="glyphicon-edit" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i>Tambah Berita
    </a>
  </h2>
  <div id="content_page">  
    <form id="upload_images" action="<?=base_url("news/create")?>" method="post">
      <!-- <div class="modal-body" style="width: 90%;"> -->
      <div class="control-group">
        <label class="control-label" for="nama">Nama</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-title" name="news[name]">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="nama">Judul</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-title" name="news[title]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Gambar</label>
        <div class="span5 thumbnail" id="thumb-gambar_detail_product">  <img src="" alt=""> </div>
        <div class="controls">
          <input id="file_upload_detail_product" type="file" name="file_upload_detail_product" class="file_upload_detail_product" />
           <input id="frm-gambar_detail_product" type="hidden" name="news[image]" />
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="control-textarea">
        <label class="control-label" for="nama">Deskripsi</label>
        <div class="controls">
          <textarea id="attr_page" class="input-xlarge" style="width:100%" name="news[description]"></textarea>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="control-group">
        <label class="control-label" for="nama">Kategori Berita</label>
        <div class="controls">
          <!-- <input type="text" class="input-xlarge" id="frm-title" name="attr[title]"> -->
          <select name="news[news_category_id]">
            <?php foreach ($news_categories as $key => $value) { ?>
              <option value="<?=$value->id?>"><?=$value->name?></option>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="control-group">
        <label class="control-label" for="nama">Tagging Berita</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-tag" name="news[tag]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">SEO Keyword</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-tag" name="news[seo_keywords]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Seo Deskripsi</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-tag" name="news[seo_description]">
        </div>
      </div>
      <div class="clearfix"></div>
      <!-- </div> -->
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('.file_upload_detail_product').uploadify({
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/news',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#thumb-gambar_detail_product").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/news/"+fileObj['name']+"' width='200px'  height='80px' />");
        $("#frm-gambar_detail_product").val(fileObj['name']);
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
});


tinymce.init({
  mode : "exact",
  elements :"page_incontent",
});
</script>