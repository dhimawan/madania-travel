<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('news');
    $crud->set_subject('news');
    $crud->columns('name','title_content','url','description_content','news_category_id','tag','updated_at');

    $data["content"]["title_content"] = "Berita";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function autonews(){
    $crud = new grocery_CRUD();

    $crud->set_table('grab_news');
    $crud->set_subject('grab_news');
    $crud->where('status',"new");
    $crud->columns('name','title_content','url','description_content');
    $crud->add_action('publish', 'publish', '','',array($this,'add_publish_link'));
    $data["content"]["title_content"] = "Automation News";
    $data["content"]["groc_view"] = $crud->render();

    $this->layouts->render($data); 
  }

  public function add_publish_link($primary_key , $row){
      return site_url("news/auto_publish/$row->id");
  }

  public function auto_publish($id){
    $this->load->model("users/user");
    $user = $this->user->user_id($this->session->userdata("user_login"));
    $get = $this->db->get_where("grab_news",array("id"=>$id))->row();
    $cat = $this->db->get_where("news_categories",array("name"=>"haji"))->row();
    $data = array(
      "name"  => $get->title_content,
      "title"  => $get->title_content,
      "url"  => $get->url,
      "description " => $get->description_content,
      "news_category_id"  =>$cat->id,
      "user"  => $user,
      "status"  => "publish",
      "created_at"  => date("Y-m-d H:i:s"),
      "updated_at"  => date("Y-m-d H:i:s"),
      "image"   => $get->name,
      "seo_keywords"  => $get->name_content,
      "seo_description" => $get->description_content,
      "is_internal" => FALSE
    );
    if($this->db->insert("news",$data)){
      $this->db->where("id",$id);
      $this->db->update("grab_news",array("status" => "publish"));
      $this->session->set_flashdata('message', 'Data berhasi di tambahkan');
      redirect("news/index/");
    }
  }

  public function grab_news(){
    $data = $this->input->post();
    if($data["url"]=="http://haji.kemenag.go.id/index.php/subMenu/527"){
      $this->grab_site($data);
    }
  }

  public function depag_news(){

  }

  private function grab_site($data){
    $this->load->helper('dom_helper');
    $html = file_get_html($data["url"]);
    $find = $html->find('ul.beritalain li a');
    foreach ($find as $key => $value) {
      $this->grab_detail_news($value->href);
      echo "<hr/>";
    }
  }

  private function grab_detail_news($url){
    $this->load->helper('dom_helper');
    $this->load->helper('inflector');
    $html = file_get_html($url);
    $find = $html->find('div.pageberita');
    // echo "<ul>";
    foreach ($find as $key => $value) {
      $data['title'] = $value->find('h1',0)->plaintext;
      $data["description"] = $value->find('p',1)->plaintext;
      $gambar = $value->find('img',0)->src;
      $insert= array(  
        "name" => "$gambar" ,
        "url" => underscore($data['title']),
        "status" => "new",
        "name_content" => $data['title'],
        "title_content" => $data['title'],
        "description_content" => $data["description"],
        "from" => "$url",
        "created_at" => date("Y-m-d H:i:s"),
        "updated_at" => date("Y-m-d H:i:s")
      );
      $ins = $this->db->get_where("grab_news",array("from" => $url))->row();
      if(count($ins)==0){
        $this->db->insert("grab_news",$insert);
      }
      // echo "<li>".$data['title']."</li>";
      // echo "<li>".underscore($data['title'])."</li>";
      // echo "<li>".$data["description"]."</li>";
      // echo "<li>".$gambar."</li>";
    }
    // echo "<ul>";
  }

  public function test_grab(){
    $this->load->helper('dom_helper');
    // Grab HTML From the URL
    $html = file_get_html("http://haji.kemenag.go.id/index.php/subMenu/527");
    $find = $html->find('ul.beritalain li a');
    foreach ($find as $key => $value) {
      echo $value->href."<br/>";
    }
  }

  public function addCustomNews(){
    $data["content_path"] = "news/addCustomNews";
    $data["slideshow_widget"] = true;
    $data["title"] = "home";

    $data["content"]["news_categories"] = $this->db->get("news_categories")->result();
    $data["content"]["data"] = "";
    $this->layouts->render($data);
  }

  public function create(){
    $this->load->helper('inflector');
    $this->load->model("users/user");
    $params = $this->input->post();

    $user = $this->user->user_id($this->session->userdata("user_login"));
    
    $params["news"]["url"] = underscore($params["news"]["title"]);
    $params["news"]["user"] = $user;
    $params["news"]["created_at"] = date("Y-m-d H:i:s");
    $params["news"]["updated_at"] = date("Y-m-d H:i:s");

    if($this->db->insert("news",$params["news"])){
      $this->session->set_flashdata('message', 'Data berhasi di tambahkan');
      redirect("news/index/");
    }else{
      $this->session->set_flashdata('message', 'Terdapat kesalahan dalam Penginputan, Silahkan ulangi kembali');
      redirect("news/index/add");
    }
  }
}
?>