<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_categories extends Backend_Controller {
   function __construct(){
      parent::__construct();   
    }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('app_categories');
    $crud->set_subject('Aplikasi Kategori');
    $crud->columns('id','title','name','description' ,'controller','method');
    $data["content"]["title_content"] = "Kategori Aplikasi";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>