
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login panel</title>
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_css_path?>glyphicons.all.css" />

  <script type="text/javascript" src="<?=$assets_js_path?>jquery-1.7.2.min.js"></script>
  
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.resize.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-alert.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-button.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-carousel.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-collapse.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-dropdown.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-modal.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-tooltip.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-popover.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-scrollspy.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-tab.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-transition.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-typeahead.js"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_css_path?>bootstrap.css" />

  <script type="text/javascript" src="<?=$assets_js_path?>demo.js"></script>
</head>
<body>
  
  <div class="container login">
    <div class="row">
      <div class="span4 offset4">
        <div class="well">
        <?=$this->load->view($content_path,$content)?>
        </div> 
      </div>
    </div>
  </div>

</body>
</html>