<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Email Template - retro green</title>
<style type="text/css">
.case { background: #F4511E; }
.bottom {
  background: url("<?=base_url('assets/images/email/publisher_how.jpg')?>");
}
.box-bg {
  background: #ffffff; /* Old browsers */
    /* IE9 SVG, needs conditional override of 'filter' to 'none' */
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkZGRkZGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
    background: -moz-linear-gradient(top,  #ffffff 0%, #dddddd 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#dddddd)); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  #ffffff 0%,#dddddd 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  #ffffff 0%,#dddddd 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  #ffffff 0%,#dddddd 100%); /* IE10+ */
    background: linear-gradient(to bottom,  #ffffff 0%,#dddddd 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#dddddd',GradientType=0 ); /* IE6-8 */

    border-radius: 10px;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    padding: 15px;
}
</style>
<!--[if gte mso 9]>
<style type="text/css">
.body{background: #353732 url('images/body-bg.jpg');}	     
.case { background:none;}
</style>
<![endif]-->
</head>
<body class="body" style="margin: 0;" marginheight="0" topmargin="0" marginwidth="0" leftmargin="0">
<!--100% body table-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="case" style="">
            <!--[if gte mso 9]>
<td style="background: none;">
<![endif]-->
            <!--header text-->
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="background: none repeat scroll 0 0 #F4511E;" background="none repeat scroll 0 0 #F4511E" valign="middle" align="center" height="59" bgcolor="#c8be9a">
                        <table width="620" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <p style="font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; color: #111111;">You're receiving this newsletter because you signed up for it or bought widgets from us.
                                        Having trouble reading this email?
                                        <webversion style="color: #111111; text-decoration: underline;" href="#">View it in your browser.</webversion>
                                        Not interested anymore?
                                        <unsubscribe style="color: #111111; text-decoration: underline;" href="#">Unsubscribe Instantly.</unsubscribe>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--/header text-->
            <!--masthead-->
            <table style="background: url("<?=base_url('assets/images/email/decor-blue.png')?>") repeat scroll 0 0 rgba(0, 0, 0, 0); " background="<?=base_url('assets/images/email/decor-blue.png')?>" bgcolor="" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table id="masthead" style="padding-top: 30px" background="none" bgcolor="transparent" width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" height="179">
                        <table border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" width="197" height="177">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="153" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" width="180" height="15"> </td>
                            </tr>
                            <tr>
                                <td valign="middle" height="148">
                                    <img style="width: 100%" src="<?=base_url('assets/images/email/logo.png')?>" />
                                </td>
                            </tr>
                        </table>
                        <table width="575" border="0" align="right" cellpadding="0" cellspacing="0">
                            <tr>
                                <td valign="top" width="445" height="55">&nbsp;</td>
                                <td valign="top" width="445" height="55">&nbsp;</td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <h1 style="font-family: Georgia, 'Times New Roman', Times, serif; font-size: 48px; color: #FFF; margin:0px; padding:0px;">INFORMASI</h1>
                                </td>
                                <td valign="top">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </td>
    </tr>
</table>
            <!--/masthead-->
            <!--top intro-->
          <div class="bottom" style="width: 100%">
            <table width="605" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="35">&nbsp;</td>
                </tr>
            </table>
            <table width="605" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td valign="top" width="220">
                        <table width="205" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td valign="top" width="220">
                                    <p style="color: #dac782; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; font-weight: bold;">Berita Terkini</p>
                                    <br>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <?php foreach ($email["news"] as $key => $value) { ?>
                                        <tr>
                                          <td>
                                            <a href="<?=news_url($value,$value->title)?>">
                                              <p style="font-size: 13px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; color: #FFFFFF; float: left;"><img src="<?=base_url('assets/images/email/li.gif')?>" height="11" width="18">
                                                <?=$value->title?>
                                              </p>
                                              </a>
                                          </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                      <?php } ?>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="385" valign="top">
                        <table class="box-bg" style="" width="100%" border="0" cellspacing="0" cellpadding="20">
                            <tr>
                                <td>
                                    <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; color: #111111; font-style: italic;">Dear <?=$params["mail"]->name?>
                                    <br/><br/>
                                    <?=$params["data"]["description"]?>
                                    </p>
                                    <br>
                                    <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin: 0px; padding: 0px; color: #111111; font-style: italic;">Regards, PT Madania Semesta Wisata</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--/top intro-->
            <?php foreach ($email["packet"] as $key => $value) { ?>
              <?php if($value->type!="link"){?>
              <!--content section-->
              <table width="605" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                      <td height="25">&nbsp;</td>
                  </tr>
              </table>
              <table class="box-bg" style="" width="605" border="0" align="center" cellpadding="20" cellspacing="0">
                <tr>
                  <td valign="top">
                    <a href="<?=base_url("paket/".underscore($value->name)."")?>" target="_blank">
                      <h1 style="color: #111111; font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; font-weight: bold;">
                        <?=$value->name?>
                      </h1>
                    </a>
                    <p style="font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #6D9A83;text-align: justify;"><?=strip_tags($value->description)?></p>
                     
                  </td>
                  <td valign="top">
                    <a href="<?=base_url("paket/".underscore($value->name)."")?>" target="_blank">
                      <img style="margin: 0px; padding: 0px; display: block; border:solid 2px #989997;" src="<?=base_url("assets/uploads/pages/".$value->value)?>" width="265" height="193">
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="color: #111111; font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; font-weight: bold;">
                      Harga Mulai 
                    </h1> 
                  </td>
                  <td valign="top"> 
                    <a href="<?=base_url("paket/".underscore($value->name)."")?>" target="_blank">
                      <h1 style="color:#F4511E; font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; font-weight: bold;">
                       <?php $list =  price_list($value->id); ?> 
                       <?php foreach ($list as $key => $valueP) {?>
                        <?php sort($valueP);  ?>
                        <?=$key?> : $ <?=$valueP[0]?> s/d <?=$valueP[count($valueP)-1]?>
                        <?php echo "<br/>";?>
                       <?php } ?>
                      </h1> 
                    </a>
                  </td>
                </tr>
              </table>
              <?php } ?>
            <?php } ?>
            <!--/content section-->
            
            <!--/content section-->
            <!--footer-->
            <table width="605" border="0" align="center" cellpadding="20" cellspacing="0">
                <tr>
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #dac782; font-weight: bold; font-style:italic;">Forward this issue</p>
                        <p style="font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #6d9a83;">Do you know someone who might be interested in receiving this monthly newsletter?</p>
                        <!--button-->
                        <table width="160" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; -khtml-border-radius: 6px; font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; color: #111111; margin: 0; padding: 0; text-align: center; background: none repeat scroll 0 0 #F4511E;" height="28">
                                    <forwardtoafriend style="text-decoration: none; color: #111111;">Foward</forwardtoafriend>
                                </td>
                            </tr>
                        </table>
                        <!--/button-->
                    </td>
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #dac782; font-weight: bold; font-style:italic;">Unsubscribe</p>
                        <p style="font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #6d9a83;">You're receiving this newsletter because you signed up for the Newsletter.</p>
                        <!--button-->
                        <table width="160" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td style="border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; -khtml-border-radius: 6px; font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; color: #111111; margin: 0; padding: 0; text-align: center; background: none repeat scroll 0 0 #F4511E" height="28">
                                    <unsubscribe style="text-decoration: none; color: #fff;">Unsubscribe</unsubscribe>
                                </td>
                            </tr>
                        </table>
                        <!--/button-->
                    </td>
                    <td valign="top">
                        <p style="font-size: 16px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #dac782; font-weight: bold; font-style:italic;">Contact us</p>
                        <p style="font-size: 12px; font-family: Georgia, 'Times New Roman', Times, serif; margin-bottom: 10px; padding: 0px; color: #6d9a83;">123 Some Street<br>
                            City, State<br>
                            99999<br>
                            (147) 789 7745<br>
                            <a style="color: #111111; text-decoration: underline;" href="#">www.abcwidgets.com</a><br>
                            <a style="color: #111111; text-decoration: underline;" href="#">info@abcwidgets.com</a></p>
                    </td>
                </tr>
            </table>
            <!--footer-->
            <table width="605" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="25">&nbsp;</td>
                </tr>
            </table>
          </div>
            <!-- <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td background="images/footer.jpg" valign="top" height="59"><img style="margin: 0; padding: 0; display: block;" src="images/footer.jpg" width="1000" height="59"></td>
                </tr>
            </table> -->
        </td>
    </tr>
</table>
<!--/100% body table-->
</body>
</html>