<div class="navbar" id="navbar">
  <div class="navbar-inner">
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
      <span class="icon-user icon-white"></span>
    </a>
    <a class="brand" href="#"><img src="<?=$assets_img_path?>logo.gif"></a>
    <div class="nav-collapse collapse">
      <form class="navbar-search pull-left" action="">
        <input type="text" class="search-query span2" placeholder="Search">
      </form>
      <ul class="nav pull-right">
        <li><a href="<?=site_url("categories")?>"><i class="icon-asterisk icon-white"></i> Data Master</a></li>
        <li><a href="<?=site_url("dashboard")?>"><i class="icon-cog icon-white"></i> Dashboard</a></li>
        <li><a href="<?=site_url("login_backend/logout")?>"><i class="icon-off icon-white"></i> logout</a></li>
      </ul>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="breadcrumb">
    <div class="logged pull-right"><span>logged:</span> <a href="#"><?=$this->session->userdata("name")?></a></div>
    <ul>
      <li class="active">Dashboard</li>
    </ul>
  </div>
</div> 