<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Admin Madania travel</title>
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_css_path?>glyphicons.all.css" />

  <script type="text/javascript" src="<?=$assets_js_path?>jquery-1.7.2.min.js"></script>
  
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.pie.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>flot/jquery.flot.resize.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-alert.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-button.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-carousel.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-collapse.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-dropdown.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-modal.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-tooltip.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-popover.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-scrollspy.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-tab.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-transition.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-typeahead.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap/bootstrap-popover.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>select/select2.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap-ajax-master/spin.min.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap-ajax-master/bootstrap-ajax.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>bootstrap-dialog.js" ></script>
  <script src="<?=base_url("assets/js/jquery_ui/js/jquery-ui-1.10.0.custom.min.js")?>" type="text/javascript"></script>
  <script src="<?=base_url("assets/js/jquery_ui/js/google-code-prettify/prettify.js")?>" type="text/javascript"></script>
  <script src="<?=base_url("assets/js/jquery.chosen.min.js")?>" type="text/javascript"></script>

   <script type="text/javascript" src="<?=$assets_js_path?>highcart/highcharts.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>highcart/modules/exporting.js"></script>


  <script type="text/javascript" src="<?=$assets_js_path?>jquery.sparkline.js"></script>

  <link rel="stylesheet" type="text/css" href="<?=base_url("assets/css/chosen/chosen.css")?>" />

  <!-- Uncomment to use LESS The dynamic stylesheet language. | http://lesscss.org/ -->
  <!-- <link rel="stylesheet/less" type="text/css" href="css/main.less" /> -->
  <!-- <script type="text/javascript" src="js/less-1.3.0.min.js"></script> -->

  <!-- Uncomment to use CSS -->
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_css_path?>bootstrap.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_js_path?>select/select2.css" />
  <link href="<?=base_url("assets/js/jquery_ui/js/google-code-prettify/prettify.css")?>" rel="stylesheet">
  <link type="text/css" href="<?=base_url("assets/js/jquery_ui/css/custom-theme/jquery-ui-1.10.0.custom.css")?>" rel="stylesheet" />

  <!-- DEMO SCRIPTS -->
  <script type="text/javascript" src="<?=$assets_js_path?>demo.js"></script>
  <script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/swfobject.js")?>"></script>
  <script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/jquery.uploadify.v2.1.4.js")?>"></script>
  <script type="text/javascript" src="<?=base_url("assets/js/tiny_mce/tiny_mce.js")?>"></script>
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_css_path?>style.css" />
  <link rel="stylesheet" type="text/css" media="screen" href="<?=$assets_js_path?>datepicker/css/datepicker.css" />
  <script type="text/javascript" src="<?=$assets_js_path?>datepicker/js/bootstrap-datepicker.js"></script>
  
  <style type="text/css">
 body {
  background: url("<?=base_url()?>assets/themes/backend/images/4.png") repeat scroll 0 0 #E5D6BB !important;
}
  </style>
  <link href='<?=base_url("assets/js/fullcalendar/fullcalendar.css")?>' rel='stylesheet' />
  <link href='<?=base_url("assets/js/fullcalendar/fullcalendar.print.css")?>' rel='stylesheet' media='print' />
  <script src='<?=base_url("assets/js/fullcalendar/fullcalendar.min.js")?>'></script>
  </head>
<body>
   <input type="hidden" id="base_url" value="<?=base_url()?>"/>
  <!-- BEGIN #navbar -->
  <?= $this->load->view($widget["header_menu"]);?>
  <div class="main" id="main">
    <?= $this->load->view($widget["left_menu"]);?>
    <div class="content" id="main-content">
      <div class="row-fluid" id="main-content-row">
        <div id="message"></div>
        <?php if($this->session->flashdata('message')!=""){ ?>
          <div class="alert">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <?=$this->session->flashdata('message');?>
        </div>
        <?php } ?>
        <?=$this->load->view($content_path,$content)?>
      </div>

    </div>
  </div>
  <?= $this->load->view("themes/backend/partials/footer_widget");?>
</body>
</html>