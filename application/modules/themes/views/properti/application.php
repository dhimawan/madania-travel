<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  
  <title>Harfa Progency</title>
  <!-- Remove it if you do not need jQuery -->
  <script type="text/javascript" src="<?=$assets_js_path?>jquery-1.7.1.min.js"></script>
  <!-- Remove it if you do not need jquery.easing : http://gsgd.co.uk/sandbox/jquery/easing/ -->
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.easing.1.3.js"></script>
  <!-- jQuery selectbox for advance search -->
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.selectbox-0.5.js"></script>
  <!-- jQuery cycle plugin for sliding -->
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.cycle.all.js"></script>
  <!-- jQuery plugin for equal heights -->
  <script type="text/javascript" src="<?=$assets_js_path?>equalheights.js"></script>
  <!-- Accordion Sllider -->
  <script type="text/javascript" src="<?=$assets_js_path?>slider_accordion.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>accordionImageMenu-0.4.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.form.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.validate.js"></script>
  <!-- script file to add your own JavaScript -->
  <script src="<?=$assets_js_path?>jquery.colorbox.js"></script>
  
  <script type="text/javascript" src="<?=$assets_js_path?>script.js"></script>

  <link rel="stylesheet" type="text/css" media="all" href="<?=$assets_css_path?>style.css" />
  <link rel="stylesheet" type="text/css" media="all" id="customFont" href="#" />
  <!-- Generate Favicon Using 1.http://tools.dynamicdrive.com/favicon/ OR 2.http://www.favicon.cc/ -->
  <link rel="shortcut icon" href="assets/<?=$assets_img_path?>favicon.ico" />
<link rel="stylesheet" href="<?=base_url("assets/css/noticeui.css")?>" type="text/css" media="screen" title="no title" charset="utf-8" />
    
</head>
<body>
<input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>"/>
<div id="header-wrapper">
  <?= $this->load->view($widget["header"]);?>
</div>

<div id="wrapper">
  <?php if($this->session->flashdata('message')!=""){ ?>
   <div class="noticeui noticeui-warn">
    <h5>Pesan!</h5>
    <p> <?=$this->session->flashdata('message');?></p>
  </div>
  <?php } ?>
  <?=$this->load->view($content_path,$content)?>
</div>
<div class="footer-wrapper">
  <?= $this->load->view($widget["footer"]);?>
</div>
  <?= $this->load->view($widget["signin_content"]);?>
  <?= $this->load->view($widget["aktifasi_content"]);?>

 

</body>
</html>