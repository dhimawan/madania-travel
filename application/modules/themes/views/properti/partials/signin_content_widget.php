<div style="display: none">
  <div id="signin-content">
    <header>
        <h2>SIGN IN</h2>
    </header>
    <section >
      <form action="<?=base_url("members/member_login")?>" method="post" id="frmlogin">
        <div class="clearfix">
          <div class="six columns">
              <label>Email</label>
              <input type="text" name="email" placeholder="ex. john_doe@connectnet.com" />
          </div>
          <div class="six columns">
              <label>Password</label>
              <input type="password" name="password" placeholder="123456" />
          </div>
        </div>
        <div class="clearfix">
          <div class="six columns">
              <a href="#" id="forget_password">Forgot Password?</a>
          </div>
          <div class="six columns">
              <input type="submit" value="Sign In" />
          </div>
        </div>
      </form>

      <form action="<?=base_url("members/forget_password")?>" method="post" id="frmforgetpassword">
        <div class="clearfix">
          <div class="six columns">
              <label>Email</label>
              <input type="text" name="email" placeholder="ex. john_doe@connectnet.com" />
          </div>
          
        </div>
        <div class="clearfix">
          <div class="six columns">
              <input type="submit" value="Proses" />
          </div>
        </div>
      </form>
    </section>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $("#frmforgetpassword").hide();
  $("#forget_password").click(function(){
    $("#frmlogin").hide();
    $("#frmforgetpassword").show();
    // $("#frmforgetpassword").show();
  });
});
</script>