<div class="header">
  <a href="index.html"><img class="logo" src="<?=$assets_img_path?>logo1.png" alt="" /></a>
  <ul class="head-contact">
    <li>
      <h3>+62-22-753-9103</h3>
          <p><a href="mailto:info@harfaprogency.com">info@harfaprogency.com</a></p>
      </li>
      <?php if($this->session->userdata('customer_login')==""){ ?>
        <li class="signin">
            <a class="signin-trigger" href="#signin-content">SIGN IN</a>
        </li>
        <li class="signin">
            <a class="aktifasi-trigger" href="#aktifasi-content">AKTIFASI
              <div class="span3 login-box">
                Aktifasi NO KTP terlebih dahulu sebelum Anda login.
              </div>
            </a>
        </li>
      <?php }else{ ?>
        <li class="customer_user">
            <a class="customer_user-trigger" href="<?=base_url("members/my_account/".$this->session->userdata('customer_no_ktp')."")?>"><?=$this->session->userdata('customer_login')?></a>
        </li>
        <li class="customer_logout">
            <a class="customer_logout-trigger" href="<?=base_url("members/member_logout")?>">LOG OUT</a>
        </li>
        <li class="customer_logout">
            <a class="customer_logout-trigger" href="<?=base_url("members/myproperties")?>">Propertiku</a>
        </li>
      <?php } ?>
  </ul>
</div>
         
  <!-- Navigation -->
<div class="nav-wrapper">
  <div class="nav-container">
    <?= $this->load->view($widget["header_menu"]);?>
    <div class="seach-wrapper">
        <form method="get" id="searchform" action="#">
            <p><input type="text" class="search-field" value="Search...." name="s" id="s"/></p>
            <p><input type="submit" class="submit-search" name="submit" id="searchsubmit" value=""/></p>
        </form>
    </div>
  </div>
</div>