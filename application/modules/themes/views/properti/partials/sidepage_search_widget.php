<div class="advancesearch-wrapper">
    <div class="search-options">
    <div class="advancesearch-head">
              <h3>Find you Home</h3>
          </div>
          <form class="advance-search-form-home" action="<?=base_url("members/search")?>" method="post">
      <div class="option-bar">
        <label class="option-title">kategori</label>
        <?=f_dropdown_categories("categories","product[category_id]","chosen-select search-select","select-type","")?>
      </div><!-- end of #select1 -->
      <div class="option-bar">
        <label class="option-title">Harga</label>
        <input type="text" id="as" name="product[selling_price]" value="" />
      </div><!-- end of #select1 -->
      <div class="option-bar">
        <label class="option-title">provinsi</label>
        <?=f_select_area("province","province_id","")?> 
      </div><!-- end of #select2 -->
      <div class="option-bar">
        <label class="option-title">kota</label>
        <?=f_select_area("city","city_id","")?>
      </div><!-- end of #select3 -->
      <div class="option-bar">
        <label class="option-title">kecamatan</label>
        <?=f_select_area("district","district_id","");?>
      </div><!-- end of #select3 -->
      <div class="option-bar">
        <label class="option-title">kelurahan</label>
        <?=f_select_area("village","product[village_id]","");?>
      </div><!-- end of #select3 -->
      <?php if($this->session->userdata('customer_login') != ""){?>
      <div class="search-btn-wrapper">
            <input type="submit" value="Search" class="advance-search-btn" />
      </div>
      <?php } ?>
    </form>
  </div> 
 </div>