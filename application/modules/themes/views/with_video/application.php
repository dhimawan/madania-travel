<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Travel Free</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?=$assets_css_path?>bootstrap.css" rel="stylesheet">
    <!-- <link href="<?=$assets_css_path?>main.css" rel="stylesheet"> -->
    <link href="<?=$assets_css_path?>bootstrap-responsive.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>app.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>flexslider.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,100,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300' rel='stylesheet' type='text/css'>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>

    <section class="main-section">

      <header>
        <div class="head-top">
          <div class="container">
            <ul>
              <li>
                <a href="">
                  <img src="assets/themes/with_video/img/mail-info.png" />
                  <span>info@chicagorentfinders.com</span>
                </a>
              </li>
              <li>
                <a href="">
                  <img src="assets/themes/with_video/img/telp-info.png" />
                  <span>312 - 493 - 8446</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <nav class="main-menu">
          <div class="container">
            <ul>
                <li><a class="scrollDown" href="">Home</a></li>
                <li><a class="scrollDown" href="">FAQ</a></li>
                <li><a class="scrollDown" href="">Apartment Search</a></li>
                <li><a class="scrollDown" href="">Contact Us</a></li>
                <li><a class="scrollDown" href="">Neighborhoods</a></li>
                <li><a class="scrollDown" href="">Rent Your Condo</a></li>
                <li><a class="scrollDown" href="">Blog</a></li>
            </ul>
          </div>
        </nav>
      </header>
    
      <section id="slider" class="slider">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <img src="assets/themes/with_video/img/temp/slider-3.png" />
              <div class="absolute-text">
                <h2>We Help <strong>you Find</strong></h2>
                <h3>Aparment <strong>That Best</strong> Fit You</h3>
                <div class="clearfix">
                  <ul>
                    <li><a class="contact-us" href="">Contact Us Now</a></li>
                    <li><a class="search-site" href="">Search our site</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li>
              <img src="assets/themes/with_video/img/temp/slider-3.png" />
              <div class="absolute-text">
                <h2>We Help <strong>you Find</strong></h2>
                <h3>Aparment <strong>That Best</strong> Fit You</h3>
                <div class="clearfix">
                  <ul>
                    <li><a class="contact-us" href="">Contact Us Now</a></li>
                    <li><a class="search-site" href="">Search our site</a></li>
                  </ul>
                </div>
              </div>
            </li>
            <li>
              <img src="assets/themes/with_video/img/temp/slider-3.png" />
              <div class="absolute-text">
                <h2>We Help <strong>you Find</strong></h2>
                <h3>Aparment <strong>That Best</strong> Fit You</h3>
                <div class="clearfix">
                  <ul>
                    <li><a class="contact-us" href="">Contact Us Now</a></li>
                    <li><a class="search-site" href="">Search our site</a></li>
                  </ul>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </section>

      <section class="container">
        <div class="white-abso"></div>
        <div class="step-result">
          <div id="video">
            <img src="assets/themes/with_video/img/temp/slider-1.png" />
          </div>
        </div>
      </section>

      <section class="callaction">
        <div class="container">
          <div class="row">
            <div class="span12">
              <div class="big-cta">
                <div class="cta-text">
                  <!-- <h3>Lebih dari <span class="highlight"><strong>5000 Peserta Umroh</strong></span> yang berangkat</h3> -->
                  <h3><?=slogan_company()?></h3>
                </div>
                <div class="cta floatright">
                  <a class="btn btn-large btn-theme btn-rounded" href="#">Download Brouser</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- <section class="about-feature row-fluid">
        <div class="container">
          <div class="span12">
            <h2>Focusing on several Chicago premier <strong>luxury neighborhood locations</strong></h2>
            <p>
              River North, River West, Gold Coast, Streeterville, <strong>Loop, Printers Row, South Loop, West Loop, Lakeshore East</strong>, and Fulton River District. 
            </p>
            <p>
              In addition to those locations, <strong>we also can assist with your Chicago apartment search</strong> in finding properties in neighborhoods such as.
            </p>
          </div>
        </div>
      </section> -->

      <!-- <section id="booking-calendar">
        <div class="container">
          <h2>Availability and booking calendar</h2>
          <table>
            <thead>
              <tr>
                <th class="long"></th>
                <th class="short">Weeknight</th>
                <th class="short">Wknd</th>
                <th class="short">Weekly</th>
                <th class="short">Monthly</th>
                <th class="short">X-Night</th>
                <th class="short">Min. Stay</th>
                <th class="long availability">Availability</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="long">06/16/13 - 09/15/13 <br/>Summer 2013</td>
                <td class="short">$100</td>
                <td class="short">$500</td>
                <td class="short">$600</td>
                <td class="short">$900</td>
                <td class="short">-</td>    
                <td class="short">3 Days</td>  
                <td class="long availability">full booked</td>
              </tr>
              <tr>
                <td class="long">06/16/13 - 09/15/13 <br/>Summer 2013</td>
                <td class="short">$100</td>
                <td class="short">$500</td>
                <td class="short">$600</td>
                <td class="short">$900</td>
                <td class="short">-</td>    
                <td class="short">3 Days</td>  
                <td class="long availability">full booked</td>
              </tr>
              <tr>
                <td class="long">06/16/13 - 09/15/13 <br/>Summer 2013</td>
                <td class="short">$100</td>
                <td class="short">$500</td>
                <td class="short">$600</td>
                <td class="short">$900</td>
                <td class="short">-</td>    
                <td class="short">3 Days</td>  
                <td class="long availability available">available</td>
              </tr>
            </tbody>
          </table>
          <ul>
              <li><span></span><p>Minimum nights stay is 2 nights. (2 nights at slightly higher fee.)</p></li>
              <li><span></span><p>Minimum nights stay is 2 nights. (2 nights at slightly higher fee.)</p></li>
              <li><span></span><p>Thanksgiving is the same rate as winter daily rates.</p></li>
              <li><span></span><p>Thanksgiving is the same rate as winter daily rates.</p></li>
              <li><span></span><p>No smoking home.</p></li>
              <li><span></span><p>No smoking home.</p></li>
              <li><span></span><p>Pets accepted at an additional daily fee.</p></li>
              <li><span></span><p>Pets accepted at an additional daily fee.</p></li>
              <li><span></span><p>No charge for local calls.</p></li>
              <li><span></span><p>No charge for local calls.</p></li>
              <li><span></span><p>Linens and towels provided.</p></li>
              <li><span></span><p>Linens and towels provided.</p></li>
              <li><span></span><p>Check in time is 4:00PM and check out time is 10:00AM.</p></li>
              <li><span></span><p>Check in time is 4:00PM and check out time is 10:00AM.</p></li>
          </ul>
        </div>                
      </section> -->

<!--     <section id="map-location">
      <div class="map-location">map location</div>
      <div class="map">
        <img src="assets/themes/with_video/img/map.png" />
        <div class="map-tooltip">
          <h3>OUR LOCATION</h3>
          <p>
              We offers a free trial. Users start backing up data and most become customers within two weeks. People you get to a free trial are very likely to become paying customers. We offers a free trial. Users start backing up data and most become customers within two weeks.
          </p>
        </div>
      </div>
    </section> -->

    </section>

    <!-- <footer>
        <div class="container row-fluid clearfix">
            <div class="span4">
                <ul class="clearfix">
                    <li><a href="">About</a></li>
                    <li><a href="">Booking Calendar</a></li>
                    <li><a href="">Gallery</a></li>
                    <li><a href="">Location</a></li>
                    <li><a href="">Features</a></li>
                </ul>
            </div>
            <div class="center span4">
                <p>Share this rental with friends:</p>
                <ul>
                    <li><a class="facebook" href=""></a></li>
                    <li><a class="twitter" href=""></a></li>
                    <li><a class="googleplus" href=""></a></li>
                    <li><a class="email" href=""></a></li>
                </ul>
            </div>
            <div class="span4 no-border-right">
                <a href="">Terms of Service</a>
                <a href="">Privacy Policy</a>
                <p>Powered by KMB property rental</p>
            </div>
        </div>
    </footer> -->


    <script src="<?=$assets_js_path?>jquery.js"></script>
    <script src="<?=$assets_js_path?>jquery.flexslider.js"></script>

    <script type="text/javascript">

      $(window).load(function(){
          $('.flexslider').flexslider({
              animation: "slide",
              start: function(slider){
                  $('body').removeClass('loading');
              }
          });
      });

      $('.main-menu a.scrollDown').click(function(){

          var el = $(this).attr('href');
          var elWrapped = $(el);
          
          scrollToDiv(elWrapped,40);
          
          return false;
      
      });

      function scrollToDiv(element,navheight){        

          var offset = element.offset();
          var offsetTop = offset.top;
          var totalScroll = offsetTop-navheight;
          
          $('body,html').animate({
              scrollTop: totalScroll
          }, 500);
      
      }

    </script>
  </body>
</html>