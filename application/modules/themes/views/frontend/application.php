<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  
    <!--[if IE]><script src="<?=$assets_js_path?>html5.js"></script><![endif]-->
    <title>Maya's Blog|| <?=$slideshow_widget?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <link rel="stylesheet" href="<?=$assets_css_path?>style.css"/>
    <link rel="stylesheet" href="<?=$assets_css_path?>customize-dark.css"  />
    <link rel="stylesheet" href="<?=$assets_css_path?>slider.css" />
    <link rel="stylesheet" href="<?=$assets_css_path?>nivo-slider.css" type="text/css" />
    <!--[if lte IE 7]><link rel="stylesheet" type="text/css" media="all" href="css/old_ie.css" /><![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="../css/grunge-ie8.css" />
    <![endif]-->
    <script src="<?=$assets_js_path?>cufon-yui.js"></script>
    <script src="<?=$assets_js_path?>Proclamate_Heavy_900.font.js"></script>
    <script src="<?=$assets_js_path?>cufon-colors-dark.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>jquery.easing.1.3.js"></script>
   <script type="text/javascript" src="<?=$assets_js_path?>jquery.coda-slider-2.0.js"></script>     
    <script type="text/javascript" src="<?=$assets_js_path?>jquery.jfancytile.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>sliderman.1.3.6.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>plugins/highslide/highslide-full.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>plugins/highslide/highslide.config.js" charset="utf-8"></script>
  <link rel="stylesheet" type="text/css" href="<?=$assets_js_path?>plugins/highslide/highslide.css" />
    <!--[if lt IE 7]>
  <link rel="stylesheet" type="text/css" href="<?=$assets_js_path?>plugins/highslide/highslide-ie6.css" />
  <![endif]-->
    <script type="text/javascript" src="<?=$assets_js_path?>PIE.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>scripts.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>custom.js"></script>
    <script type="text/javascript" src="<?=$assets_js_path?>jquery.nivo.ext.js"></script>
  <script type="text/javascript" src="<?=$assets_js_path?>jquery.carouFredSel-4.5.1.js"></script>
  <script>
    // DO NOT REMOVE!
    // b21add52a799de0d40073fd36f7d1f89
    hs.graphicsDir = '<?=$assets_js_path?>plugins/highslide/graphics/';
  </script>

<meta charset="UTF-8"></head>

<body>
  <div id="home-bg">
      <?= $this->load->view("themes/frontend/partials/header_widget");?>
      <div class="navig-nivo"></div>
      <?php if(isset($slideshow_widget)){
        echo $this->load->view("themes/frontend/partials/slideshow_widget");
      }
      ?>
      <?//=$this->load->view("layouts/frontend/partials/slideshow_widget");?>
      <div id="container" class="full-width">
        <?=$this->load->view($content_path,$content)?>
      </div>
      <?= $this->load->view("themes/frontend/partials/client_widget");?>
      <div class="line-footer"></div>
      <?= $this->load->view("themes/frontend/partials/footer_widget");?>
    </div>
  </body>
</html>