<!DOCTYPE html>
<html lang="en" class=" js no-touch no-csstransforms3d csstransitions"  xmlns:fb="http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/LocalBusiness">
  <head>
    <?=set_title_page()?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?=set_metatags()?>
    <!-- css -->
    <link href="<?=$assets_css_path?>bootstrap.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>bootstrap-responsive.css" rel="stylesheet">


    <link href="<?=$assets_css_path?>fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>jcarousel.css" rel="stylesheet">
    <!-- <link href="<?=$assets_css_path?>flexslider.css" rel="stylesheet"> -->
    <link href="<?=$assets_css_path?>with_video/css/flexslider.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>style.css" rel="stylesheet">
    <!-- Theme skin -->
    <link id="t-colors" href="<?=$assets_css_path?>default.css" rel="stylesheet">
    <link id="bodybg" href="<?=$assets_css_path?>bg1.css" rel="stylesheet" type="text/css">
    <link  href="<?=$assets_css_path?>mobile.css" rel="stylesheet" type="text/css">
	 <!-- <link href="<?=$assets_css_path?>cslider.css" rel="stylesheet"> -->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
   <script type="text/javascript">
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-43573378-2', 'madaniatravel.com');
    ga('send', 'pageview');
   </script>
</head>
<body>
  <div id="fb-root"></div>


  <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>"/>
  <?=$this->load->view("themes/".$this->config->item("frontend_themes")."/partials/_header_with_video")?>
    <div id="wrapper">
      <?php if($this->session->flashdata('message')!=""){ ?>
        <div class="noticeui noticeui-warn">
          <h5>Pesan!</h5>
          <p> <?=$this->session->flashdata('message');?></p>
        </div>
      <?php } ?>
      <?=$this->load->view($content_path,$content)?>
      <?= $this->load->view($widget["footer"]);?>
    </div>

    <a href="#" class="scrollup" style="display: none; "><i class="icon-chevron-up icon-square icon-32 active"></i></a>	
    <!-- javascript ================================================== -->
    <script src="<?=$assets_js_path?>jquery.js"></script>
    <script src="<?=$assets_js_path?>jquery.easing.1.3.js"></script>
    <script src="<?=$assets_js_path?>bootstrap.js"></script>
    <script src="<?=$assets_js_path?>jquery.jcarousel.min.js"></script>   
    <script src="<?=$assets_js_path?>jquery.fancybox.pack.js"></script> 
    <script src="<?=$assets_js_path?>jquery.fancybox-media.js"></script> 
    <script src="<?=$assets_js_path?>prettify.js"></script>   
    <script src="<?=$assets_js_path?>quicksand/jquery.quicksand.js"></script> 
    <script src="<?=$assets_js_path?>setting.js"></script>  
    <script src="<?=$assets_js_path?>with_video/js/jquery.flexslider.js"></script>

    <script src="<?=$assets_js_path?>modernizr.custom.28468.js"></script>
    <script src="<?=$assets_js_path?>jquery.ba-cond.min.js"></script>
    <script src="<?=$assets_js_path?>jquery.slitslider.js"></script>
    <script src="<?=$assets_js_path?>animate.js"></script>
    <link href="<?=base_url("assets/js/social-feed-master/css/jquery.socialfeed.css")?>" rel="stylesheet" />
    <script src="<?=base_url("assets/js/social-feed-master/js/jquery.socialfeed.utility.js")?>"></script>
    <script src="<?=base_url("assets/js/social-feed-master/js/jquery.socialfeed.js")?>"></script>
    <script src="<?=$assets_js_path?>jquery.fitvids.js"></script>
    <script src="<?=$assets_js_path?>jquery.li-scroller.1.0.js"></script>
    <link href="<?=$assets_css_path?>li-scroller.css" rel="stylesheet" />
    
    <script src="<?=$assets_js_path?>jquery.vticker.js"></script>
    <script src="<?=$assets_js_path?>jquery.vticker.js"></script>

    
    <script src="<?=$assets_js_path?>custom.js"></script>

    <!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript">
$(document).ready(function(){
  $.get("<?=base_url("frontends/load_index_video")?>", function( data ) {
    $("#video").html( data );
  });
});
</script>
  <script type="text/javascript">
  $(window).load(function(){
    $("#loading_slider").remove();
   $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        start: function(slider){
          $("#loading_slider").remove();
        },
         slideshow: true
    });

   $.get("<?=base_url("frontends/grab_kurs")?>", function( data ) {
     $(".conversi_kurs").html(data);
     // $("ul#ticker01").liScroll();
   });
  });

  $('.main-menu a.scrollDown').click(function(){

      var el = $(this).attr('href');
      var elWrapped = $(el);
      
      scrollToDiv(elWrapped,40);
      
      return false;

  });

  function scrollToDiv(element,navheight){        

      var offset = element.offset();
      var offsetTop = offset.top;
      var totalScroll = offsetTop-navheight;
      
      $('body,html').animate({
          scrollTop: totalScroll
      }, 500);

  }
  </script>
  <script src="<?=$assets_js_path?>jquery.lazyload.js"></script>
  </body>
</html>