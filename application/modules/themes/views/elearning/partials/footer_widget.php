<footer>
  <div class="container">
    <div class="row">
      <div class="span3">
        <div class="widget">
          <h5 class="widgetheading">Halaman</h5>
          <ul class="link-list">
            <?=generated_frontend_footer_without_child()?>
          </ul>
        </div>
      </div>
      
      <div class="span3">
        <div class="widget" itemscope itemtype="http://schema.org/LocalBusiness">
          <h5 class="widgetheading">Kontak Kami</h5>
          <strong itemprop="name"> <?= strtoupper(get_company_name())?> </strong>
          <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="streetAddress" ><?=get_company_address()?></span>
          </address>
          <p>
            <i class="icon-phone" ></i><span itemprop="telephone" ><?=get_company_telp()?></span> <br>
            <i class="icon-envelope-alt"></i><span itemprop="email" > <?=get_company_email()?></span>
          </p>
        </div>
      </div>
      <div class="span4">
        <div class="widget">
          <h5 class="widgetheading">Jam Kerja</h5>
          <ul class="post-list">
            <li><meta itemprop="openingHours" content="Senin-Kamis 08:30 am - 17:00 pm"> Senin-Kamis 08:30 am - 17:00 pm</li>
            <li><meta itemprop="openingHours" content="Jumat 08:30 am - 16:00 pm"> Jumat 08:30 am - 16:00 pm</li>
            <li><meta itemprop="openingHours" content="Sabtu 8:30 pm - 12:00 pm"> Sabtu 8:30 pm - 12:00 pm</li>
            <li><meta itemprop="openingHours" content="Minggu Tutup"> Minggu Tutup</li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="footer-inner">
    <div class="container">
      <div class="clearfix">
        <?=generated_our_advertasing()?>
      </div>
    </div>
  </div>

  <div id="sub-footer">

    <div class="container">
      <div class="row">
        <div class="span6">
          <div class="copyright">
            <p><span>© 7foundation 2013 All right reserved. Designed by</span> <a href="www.sevenfoundation.web.id" target="_blank"> SevenFoundation </a></p>
          </div>
        </div>
        <div class="span6">
          <div class="right">
            <div class="subpage-open" id="footer-open">
              <span id="footer-open-icon"></span>
            </div>
            <ul class="social-network">
              <li><a href="#" data-placement="bottom" title="" data-original-title="Facebook"  onclick="shareFacebook('<?=base_url()?>','PT Madania Semesta Wisata','<?=base_url("assets/images/logo-madania_landscape.png")?>','<?=strip_tags(slogan_company());?>'); return false;" ><i class="icon-facebook icon-square"></i></a></li>
              <li><a href="#" data-placement="bottom" title="" data-original-title="Twitter" onclick="shareTwitter('<?=base_url()?>','PT Madania Semesta Wisata'); return false;"  ><i class="icon-twitter icon-square"></i></a></li>
              <!-- <li><a href="#" data-placement="bottom" title="" data-original-title="Linkedin"><i class="icon-linkedin icon-square"></i></a></li> -->
              <!-- <li><a href="#" data-placement="bottom" title="" data-original-title="Pinterest"><i class="icon-pinterest icon-square"></i></a></li> -->
              <li><a href="#" data-placement="bottom" title="" data-original-title="Google plus"  onclick="shareGooglePlus('<?=base_url()?>'); return false;" ><i class="icon-google-plus icon-square"></i></a></li>
            </ul>
            <span class="callus">  <i class="icon-phone" ></i> Hubungi Kami 24Jam (+6282 221 331 441) </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
