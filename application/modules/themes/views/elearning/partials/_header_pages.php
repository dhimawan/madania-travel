<section class="main-section">
  <header>
    <div class="head-top">
      <div class="container">
        <ul>
          <li>
            <a href="">
              <img src="<?=base_url("assets/themes/elearning/img/with_video/img/mail-info.png")?>" alt="mail_info"/>
              <span>info@madaniatravel.com</span>
            </a>
          </li>
          <li>
            <a href="">
              <img src="<?=base_url("assets/themes/elearning/img/with_video/img/telp-info.png")?>" alt="telp_info" />
              <span>(022) 730-7888</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <nav class="main-menu">
      <div class="container">
        <div class="logo_header">
          <img src="<?=base_url("assets/images/logo-madania_landscape.png")?>" alt="logo madani travel "/>
        </div>
        <ul><?=generated_frontend_menu_without_child()?></ul>
        <?=generated_frontend_mobile_menu()?>
      </div>
    </nav>
  </header>

  <section id="slider_pages" class="slider">
    <!-- <div class="flexslider">
      <ul class="slides">
        <?//=slideshow_view()?>
      </ul>
    </div> -->
  </section>

  <section class="container">
    <div class="white-abso"><h2><?=$content["title_content"]?><h2></div>
  </section>

  
</section>
