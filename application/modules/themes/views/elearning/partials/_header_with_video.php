
<section class="main-section">
  <header>
    <div class="head-top">
      <div class="container">
        <ul>
          <li>
            <a href="">
              <img src="<?=base_url("assets/themes/elearning/img/with_video/img/mail-info.png")?>" />
              <span> <a href="mailto:info@madaniatravel.com">info@madaniatravel.com</a> </span>
            </a>
          </li>
          <li>
            <a href="">
              <img src="<?=base_url("assets/themes/elearning/img/with_video/img/telp-info.png")?>" />
              <span>(022) 730-7888</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <nav class="main-menu">
      <div class="container">
        <div class="logo_header">
          <img itemprop="image"  src="<?=base_url("assets/images/logo-madania_landscape.png")?>" alt="madania semesta wisata"/> <h1 style="display:none" itemprop="name"> PT Madania Semesta Wisata</h1>
        </div>
        <ul><?=generated_frontend_menu_without_child()?></ul>
        <?=generated_frontend_mobile_menu()?>
      </div>
    </nav>
  </header>

  <section id="slider" class="slider">
    <div class="flexslider">
      <ul class="slides" id="slider_view">
        <?=slideshow_view()?>
      </ul>
    </div>
  </section>

  <section class="container">
    <div class="white-abso">
      <div class="conversi_kurs"> </div>
     <!--  <ul>
        <li><span>27/12/2013</span> <a href="#">USD Rp.10,920(Beli) Rp10,960(Jual) </a></li>
        <li><span>27/12/2013</span> <a href="#">REAL Rp.10,920(Beli) Rp10,960(Jual) </a></li>
      </ul> -->
      <span class="currency-logo"><img alt="konversi mata uang" src="<?=base_url("assets/images/gmc-logo.png")?>" /></span>
    </div>
    <div class="step-result">
      <div id="video">
        <?//=generated_home_video()?>
        <!-- <iframe width="465" height="262" src="//www.youtube.com/embed/D3dV1CrpnDA?autoplay=1" frameborder="0" allowfullscreen ></iframe> -->
      </div>
    </div>
  </section>

  
</section>
