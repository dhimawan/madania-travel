<?=doctype()?>
<html lang="en" class=" js no-touch no-csstransforms3d csstransitions" itemscope itemtype="http://schema.org/LocalBusiness" >
  <head>
    <?=$seo_title?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?=$seo?>
    <script src="<?=$assets_js_path?>jquery.js"></script>
    <!-- css -->
    <link href="<?=$assets_css_path?>bootstrap.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>bootstrap-responsive.css" rel="stylesheet">


    <link href="<?=$assets_css_path?>fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>jcarousel.css" rel="stylesheet">
    <!-- <link href="<?=$assets_css_path?>flexslider.css" rel="stylesheet"> -->
    <link href="<?=$assets_css_path?>with_video/css/flexslider.css" rel="stylesheet">
    <link href="<?=$assets_css_path?>style.css" rel="stylesheet">
    <!-- Theme skin -->
    <link id="t-colors" href="<?=$assets_css_path?>default.css" rel="stylesheet">
    <!-- color picker -->
    <!-- <link rel="stylesheet" href="<?=$assets_css_path?>colorpicker.css" type="text/css"> -->
    <!-- boxed bg -->
    <link id="bodybg" href="<?=$assets_css_path?>bg1.css" rel="stylesheet" type="text/css">
    <link  href="<?=$assets_css_path?>mobile.css" rel="stylesheet" type="text/css">
    
	 <!-- <link href="<?=$assets_css_path?>cslider.css" rel="stylesheet"> -->

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- <link rel="shortcut icon" href="ico/favicon.png"> -->
    <!-- // <script src="<?=$assets_js_path?>twitterlib.js"></script> -->
   <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-43573378-2', 'madaniatravel.com');
    ga('send', 'pageview');

  </script>
</head>
<body>
  <input type="hidden" name="base_url" id="base_url" value="<?=base_url()?>"/>
  <?=$this->load->view("themes/".$this->config->item("frontend_themes")."/partials/_header_pages")?>
    <div id="wrapper">
      <?php if($this->session->flashdata('message')!=""){ ?>
        <div class="noticeui noticeui-warn">
          <h5>Pesan!</h5>
          <p> <?=$this->session->flashdata('message');?></p>
        </div>
      <?php } ?>
      <?=$this->load->view($content_path,$content)?>
      <?= $this->load->view($widget["footer"]);?>
    </div>
    <a href="#" class="scrollup" style="display: none; "><i class="icon-chevron-up icon-square icon-32 active"></i></a>	
    <!-- javascript ================================================== -->
    
    <script src="<?=$assets_js_path?>jquery.easing.1.3.js"></script>
    <script src="<?=$assets_js_path?>bootstrap.js"></script>
    <script src="<?=$assets_js_path?>jquery.jcarousel.min.js"></script>   
    <script src="<?=$assets_js_path?>jquery.fancybox.pack.js"></script> 
    <script src="<?=$assets_js_path?>jquery.fancybox-media.js"></script> 
    <script src="<?=$assets_js_path?>prettify.js"></script>   
    <script src="<?=$assets_js_path?>quicksand/jquery.quicksand.js"></script> 
    <script src="<?=$assets_js_path?>setting.js"></script>  
    <!-- // <script src="<?=$assets_js_path?>with_video/js/jquery.js"></script> -->
    <script src="<?=$assets_js_path?>with_video/js/jquery.flexslider.js"></script>

    <!-- //<script src="<?=$assets_js_path?>with_video/js/jquery.flexslider.js"></script> -->
    <!--// <script src="<?=$assets_js_path?>jquery.tweet.js"></script>  -->
   <!-- // <script src="<?=$assets_js_path?>jquery.flexslider.js"></script>  
   <!-- // <script src="<?=$assets_js_path?>jquery.nivo.slider.js"></script> -->
    <script src="<?=$assets_js_path?>modernizr.custom.28468.js"></script>
    <script src="<?=$assets_js_path?>jquery.ba-cond.min.js"></script>
    <script src="<?=$assets_js_path?>jquery.slitslider.js"></script>
    <script src="<?=$assets_js_path?>animate.js"></script>
    <link href='<?=$assets_js_path?>fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='<?=$assets_js_path?>fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />

    <script src='<?=$assets_js_path?>fullcalendar/fullcalendar.min.js'></script>
    <script src="<?=$assets_js_path?>custom.js"></script>
   <!-- // <script src="<?=$assets_js_path?>jquery.cslider.js"></script> -->
    <!-- // <script src="<?=$assets_js_path?>jquery.cookie.js"></script> -->
    <!-- // <script src="<?=$assets_js_path?>colorpicker.js"></script> -->
    <!-- // <script src="<?=$assets_js_path?>optionspanel.js"></script> -->
    <!-- Placed at the end of the document so the pages load faster -->

  <script type="text/javascript">

  $(window).load(function(){
      $('.flexslider').flexslider({
          animation: "slide",
          start: function(slider){
              $('body').removeClass('loading');
          }
      });
  });

  $('.main-menu a.scrollDown').click(function(){

      var el = $(this).attr('href');
      var elWrapped = $(el);
      
      scrollToDiv(elWrapped,40);
      
      return false;

  });

  function scrollToDiv(element,navheight){        

      var offset = element.offset();
      var offsetTop = offset.top;
      var totalScroll = offsetTop-navheight;
      
      $('body,html').animate({
          scrollTop: totalScroll
      }, 500);

  }

  function shareFacebook(link,title,media,text) {
    window.open('//www.facebook.com/share.php?s=100&p[url]=' + encodeURIComponent(link) + '&p[images][0]=' + encodeURIComponent(media) + '&p[title]=' + encodeURIComponent(title)+'&p[summary]='+encodeURIComponent(text),'Facebook','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  }
  function shareTwitter(link,title) {
    window.open('//twitter.com/home?status=' + encodeURIComponent(title) + '+' + encodeURIComponent(link),'Twitter','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  }
  function sharePinterest(link,title,media,text) {
    window.open('//pinterest.com/pin/create/button/?url=' + encodeURIComponent(shareLink) + '&media=' + encodeURIComponent(shareMedia) + '&description=' + encodeURIComponent(shareTitle),'Pinterest','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  }
  function shareGooglePlus(link,title,media,text) {
    window.open('//plus.google.com/share?url=' + encodeURIComponent(link),'GooglePlus','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  }
  function shareLinkedIn() {
    window.open('//www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(shareLink) + '&title=' + encodeURIComponent(shareTitle) + '&source=' + encodeURIComponent(shareLink),'LinkedIn','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
  }

  </script>
  
  </body>
</html>