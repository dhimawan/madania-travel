<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academic_years extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('academic_year');
    $crud->set_subject('tahun akademik');
    $data["content"]["title_content"] = "Tahun Akademik";
    $crud->set_theme('datatables');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>