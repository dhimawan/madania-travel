<style type="text/css">

.control-group {
    float: left;
    /*margin-bottom: 9px;*/
    margin-right: 10px;
}


.control-left-form > .control-image {
    margin-right: 10px;
}

.control-left-form {
    float: left;
}
</style>
<div class="span12 padding">
	<h2>
    <a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal">
      <i class="glyphicon-circle-plus" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i>Tambah <?=$title_content?>
    </a>
  </h2>
	<div id="content_page"> 
		<?=get_packet_list($data->id)?>
	</div>
</div>
<div class="span12 padding">
	<h2>
    <a href="#" role="button" class="btn btn" id="edit_page">
      <i class="glyphicon-edit" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i>Detail Keberangkatan
    </a>
  </h2>
	<div id="content_page" class="detail_keberangkatan"> 
		
	</div>
</div>
<?=$this->load->view("pages/partials/_pop_add_packet")?>
<script type="text/javascript">
$(document).ready(function(){
  $('.file_upload_detail_product').uploadify({
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/pages',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#thumb-gambar_detail_product").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/pages/"+fileObj['name']+"' width='200px'  height='80px' />");
        $("#frm-gambar_detail_product").val(fileObj['name']);
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
    tinymce.init({
    mode : "exact",
    elements :"attr_page",
  });
  
});

function detailKeberangkatan(id){
	// console.log(id);
	$(".detail_keberangkatan").html("Loading ......");
	$.ajax({
    type: "POST",
    url: "<?=base_url('pages/detail_packet_list')?>",
    data: { id: id}
  }).done(function(msg){
      $(".detail_keberangkatan").html(msg);
  });


}
</script>
