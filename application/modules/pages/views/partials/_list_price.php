<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Biaya</th>
      <th>Tanggal</th>
      <th>User</th>
      <th>Status</th>
      <th>Deskripsi</th>
    </tr>
  </thead>
  <tbody class="tbody_depature">
    <?php foreach ($history_packets as $key => $value) { ?>
      <tr id="tr_<?=$value->id?>">
        <td><?=($key+1)?></td>
        <td><?=$value->name?></td>
        <td>
          <ul class="table_input">
            <li> <a href="javascript:void(0);" id="link_single" onclick="show_ul('form_single_<?=$key?>')" >Single</a>
              <ul id="form_single_<?=$key?>" class="form_price_active"> 
                <li>  Min : <?=$value->min_single_price?> </li> 
                <li>  max : <?=$value->max_single_price?> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_double" onclick="show_ul('form_double_<?=$key?>')">Double </a>
              <ul id="form_double_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_double_price?></li> 
                <li>  max : <?=$value->max_double_price?> </li> 
              </ul>
            </li>
            <li > <a href="javascript:void(0);" id="link_triple" onclick="show_ul('form_triple_<?=$key?>')">triple </a>
              <ul id="form_triple_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_triple_price?> </li> 
                <li>  max : <?=$value->max_triple_price?> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_quad" onclick="show_ul('form_quad_<?=$key?>')"> quad </a>
              <ul id="form_quad_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_quad_price?></li> 
                <li>  max : <?=$value->max_quad_price?> </li> 
              </ul>
            </li>
          </ul>
        </td>
        <td><?=$value->updated_at?></td>
        <td><?=$value->username?></td>
        <td><?=$value->status?></td>
        <td><?=$value->description?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>