<form method="POST" action="<?=base_url("pages/update_packet_list")?>" >
  <div id="myModalEdit">
    <div class="" style="width: 100%;">
      <div class="control-left-form">
        <div class="control-image"> 
          <img src="<?=base_url("assets/uploads/pages/$page->value")?>">
        </div>
        <label class="control-label" for="nama">Image</label>
        <div class="controls">
          <input id="file_upload_detail_product" type="file" name="file_upload_detail_product" class="file_upload_detail_product" />

          <input type="hidden" class="input-xlarge" id="frm-value" name="attr[value]" value="<?=$page->value?> ">
          <input type="hidden" class="input-xlarge" id="frm-value" name="attr[id]" value="<?=$page->id?> ">
        </div>
      </div>
      <div class="control-seo">
        <label class="control-label" for="nama">Nama</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-name" name="attr[name]" value="<?=$page->name?> ">
        </div>
      </div>
      <div class="control-seo">
        <label class="control-label" for="frm-title">Jadikan Link</label>
        <div class="controls">
          <!-- <input type="text" class="input-xlarge" id="frm-seo_description" name="attr[seo_description]" value="<?=$page->seo_description?> "> -->
          <select name="attr[type]" >
            <option value="text"> -- Tidak -- </option>
            <option value="link"> -- Iya -- </option>
          </select>
        </div>
      </div>
      <div class="control-seo">
        <label class="control-label" for="frm-title">Judul (masukan url bila type dijadikan link)</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-title" name="attr[title]" value="<?=$page->title?> ">
        </div>
      </div>
      <div class="control-seo">
        <label class="control-label" for="frm-title">Seo Keyword</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-seo_keywords" name="attr[seo_keywords]" value="<?=$page->seo_keywords?> ">
        </div>
      </div>
      <div class="control-seo">
        <label class="control-label" for="frm-title">Seo description</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-seo_description" name="attr[seo_description]" value="<?=$page->seo_description?> ">
        </div>
      </div>
      
      <div class="control-textarea">
        <label class="control-label" for="frm-description">Deskripsi</label>
        <div class="controls">
          <textarea class="input-xlarge" id="frm-description" name="attr[description]"  ><?=$page->description?> </textarea>
        </div>
      </div>

      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
      </div>
    </div>
    
  </div>
</form>