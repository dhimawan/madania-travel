<script type="text/javascript">
$(document).ready(function(){
  $('#btn-confirm-callback').live("click",function(){
    var link  = $(this).attr("title");
    BootstrapDialog.confirm('Anda Yakin untuk menghapus data?', function(result){
      if(result == true){
        window.location.href = link ;
      }
    });
  });
  
});
function edit_form_departure(id){
  $(".text_"+id).hide();
  $(".form_"+id).show(); 
}
function hide_form_departure(id){
  $(".text_"+id).show();
  $(".form_"+id).hide();
}

function update_departure(id){
  $.ajax({
      type: "POST",
      url: "<?=base_url('pages/update_departure')?>",
      data: $(".form_edit_departure_"+id).serialize()
    }).done(function(msg){
      // $("#tr_"+id).html(msg);
      window.location.href = "<?=base_url("pages/packet_list")?>" ;
    });
}
</script>
<form method="post" action="<?=base_url("pages/add_departure")?>">
<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th>Tanggal</th>
      <th>Total pax</th>
      <th>Deskripsi</th>
      <th class="actions" width="110">Actions</th>
    </tr>
  </thead>
  <tbody class="tbody_depature">
    <?php foreach ($departures as $key => $value) { ?>
      <tr>
        <td>
          <span class="text_<?=$value->id?>">
            <?=($key+1)?>
          </span>
        </td>
        <td>
          <span class="text_<?=$value->id?>">
            <?=$value->name?>
          </span>
          <div class="form_<?=$value->id?>" style="display:none">
            <input type="hidden" class="input-xlarge form_edit_departure_<?=$value->id?>" id="frm-value" name="departure[id]" value="<?=$value->id?>">
            <input type="text" name="departure[name]" class="input form_edit_departure_<?=$value->id?>" value="<?=$value->name?>"/>
          </div>
        </td>
        <td>
          <span class="text_<?=$value->id?>">
            <?=date("d/m/Y",strtotime($value->startdate))?> - <?=date("d/m/Y",strtotime($value->enddate))?> 
          </span>
          <div class="form_<?=$value->id?>" style="display:none">
            <input type="text" name="departure[startdate]" class="input-mini tanggal form_edit_departure_<?=$value->id?>" value="<?=date("m/d/Y",strtotime($value->startdate))?>"/> - <input type="text" name="departure[enddate]" class="input-mini tanggal form_edit_departure_<?=$value->id?>" value="<?=date("m/d/Y",strtotime($value->enddate))?>"/> </td>
          </div>
        </td>
        <td>
          <span class="text_<?=$value->id?>"><?=$value->total_tax?></span>
          <div class="form_<?=$value->id?>" style="display:none">
            <input type="text" name="departure[total_tax]" class="input form_edit_departure_<?=$value->id?>" value="<?=$value->total_tax?>"/>
          </div>
        </td>
        <td>
          <span class="text_<?=$value->id?>"><?=$value->description?></span>
          <div class="form_<?=$value->id?>" style="display:none">
            <input type="text" name="departure[description]" class="input form_edit_departure_<?=$value->id?>" value="<?=$value->description?>"/>
          </div>
        </td>
        <td class="actions">
          <span class="text_<?=$value->id?>">
            <a href="javascript:void(0);" class="btn" id="<?=$value->id?>" onclick="edit_form_departure(<?=$value->id?>);return false;"><i class="glyphicon-pencil"></i></a>
            <a class="btn" id="btn-confirm-callback" title="<?=base_url("pages/delete_departure/$value->id")?>"><i class="glyphicon-remove-2"></i></a>
          </span>
          <div class="form_<?=$value->id?>" style="display:none">
            <input type="button" class="btn btn-primary" id="addNewImage" value=" simpan" onclick="update_departure(<?=$value->id?>);" >
            <a href="javascript:void(0);" class="btn"><i class="glyphicon-remove-2" onclick="hide_form_departure(<?=$value->id?>);return false;"></i></a>
          </div>
          <!-- <a href="#" class="btn"><i class="glyphicon-right-arrow"></i> -->
        </td>
      </tr>
    <?php } ?>
    
     <tr>
        <td>
          <input type="hidden" class="input-xlarge" id="frm-value" name="departure[attribute_page_id]" value="<?=$page->id?> ">
        </td>
        <td><input type="text" name="departure[name]" class="input" /></td>
        <td><input type="text" name="departure[startdate]" class="input-mini tanggal" /> - <input type="text" name="departure[enddate]" class="input-mini tanggal" /> </td>
        <td><input type="text" name="departure[total_tax]" class="input" /></td>
        <td><input type="text" name="departure[description]" class="input" /></td>
        <td class="actions">
          <input type="submit" class="btn btn-primary" id="addNewImage" value=" simpan" >
          <!-- <input type="reset" class="glyphicon-right-arrow " id="addNewImage" value=" simpan" > -->
          <!-- <a href="#" class="btn"><i class="glyphicon-right-arrow"></i></a> -->
          <a href="#" class="btn"><i class="glyphicon-remove-2"></i></a>
        </td>
        
      </tr>
    
  </tbody>
</table>
</form>
