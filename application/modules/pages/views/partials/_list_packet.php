<style type="text/css">
.table_input > li{

}
</style>
<script type="text/javascript">
$(document).ready(function(){
  $(".form_price").hide();

});
 function show_ul(vari){
    $("#"+vari).toggle();
  }
  function edit_packet_price(id){
    $("#tr_"+id).html("<td colspan='5' align='center'><b>Loading form......</b></td>");
    $.ajax({
      type: "POST",
      url: "<?=base_url('pages/edit_packet_price')?>",
      data: { id: id}
    }).done(function(msg){
      $("#tr_"+id).html(msg);
    });
  }
  function update_form(classname){
    // $("#tr_"+id).html("<td colspan='5' align='center'><b> Procces Loading ......</b></td>");
    console.log($("."+classname).serialize());
    $.ajax({
      type: "POST",
      url: "<?=base_url('pages/update_packet_price')?>",
      data: $("."+classname).serialize()
    }).done(function(msg){
      detailKeberangkatan(<?=$page->id?>);
    });
  }
</script>
<form method="post" action="<?=base_url("pages/add_packet")?>">
<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Nama</th>
      <th width="300px">Biaya</th>
      <th>Deskripsi</th>
      <th class="actions">Actions</th>
    </tr>
  </thead>
  <tbody class="tbody_depature">
    <?php foreach ($packets as $key => $value) { ?>
      <tr id="tr_<?=$value->id?>">
        <td><?=($key+1)?></td>
        <td><?=$value->name?></td>
        <td>
          <ul class="table_input">
            <li> <a href="javascript:void(0);" id="link_single" onclick="show_ul('form_single_<?=$key?>')" >Single</a>
              <ul id="form_single_<?=$key?>" class="form_price_active"> 
                <li>  Min : <?=$value->min_single_price?> </li> 
                <li>  max : <?=$value->max_single_price?> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_double" onclick="show_ul('form_double_<?=$key?>')">Double </a>
              <ul id="form_double_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_double_price?></li> 
                <li>  max : <?=$value->max_double_price?> </li> 
              </ul>
            </li>
            <li > <a href="javascript:void(0);" id="link_triple" onclick="show_ul('form_triple_<?=$key?>')">triple </a>
              <ul id="form_triple_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_triple_price?> </li> 
                <li>  max : <?=$value->max_triple_price?> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_quad" onclick="show_ul('form_quad_<?=$key?>')"> quad </a>
              <ul id="form_quad_<?=$key?>" class="form_price"> 
                <li>  Min : <?=$value->min_quad_price?></li> 
                <li>  max : <?=$value->max_quad_price?> </li> 
              </ul>
            </li>
          </ul>
        </td>
        <td><?=$value->description?></td>
        <td class="actions">
          <a href="javascript:void(0);" onclick="edit_packet_price(<?=$value->id?>)" class="btn"><i class="glyphicon-conversation"></i>
          <a href="#" class="btn"><i class="glyphicon-remove-2"></i>
          <a href="#" class="btn"><i class="glyphicon-right-arrow"></i>
        </td>
      </tr>
    <?php } ?>
     <tr>
        <td>
          <input type="hidden" class="input-xlarge" id="frm-value" name="packet[attribute_page_id]" value="<?=$page->id?>">
        </td>
        <td><input type="text" name="packet[name]" class="input" /></td>
        <td>
          <ul class="table_input">
            <li> <a href="javascript:void(0);" id="link_single" onclick="show_ul('form_single')" >Single</a>
              <ul id="form_single" class="form_price_active"> 
                <li>  Min : <input type="text" name="price[min_single_price]" class="input-mini"  value="0"/> </li> 
                <li>  max : <input type="text" name="price[max_single_price]" class="input-mini"  value="0"/> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_double" onclick="show_ul('form_double')">Double </a>
              <ul id="form_double" class="form_price"> 
                <li>  Min : <input type="text" name="price[min_double_price]" class="input-mini"  value="0"/> </li> 
                <li>  max : <input type="text" name="price[max_double_price]" class="input-mini"  value="0"/> </li> 
              </ul>
            </li>
            <li > <a href="javascript:void(0);" id="link_triple" onclick="show_ul('form_triple')">triple </a>
              <ul id="form_triple" class="form_price"> 
                <li>  Min : <input type="text" name="price[min_triple_price]" class="input-mini"  value="0"/> </li> 
                <li>  max : <input type="text" name="price[max_triple_price]" class="input-mini" value="0" /> </li> 
              </ul>
            </li>
            <li> <a href="javascript:void(0);" id="link_quad" onclick="show_ul('form_quad')"> quad </a>
              <ul id="form_quad" class="form_price"> 
                <li>  Min : <input type="text" name="price[min_quad_price]" class="input-mini"  value="0"/> </li> 
                <li>  max : <input type="text" name="price[max_quad_price]" class="input-mini"  value="0"/> </li> 
              </ul>
            </li>
          </ul>
         
        </td>
        <td><textarea name="packet[description]" class="input" style="width: 150px; height: 204px;" id="p-description"> </textarea></td>
        <td class="actions">
          <input type="submit" class="btn btn-primary" id="addNewImage" value=" simpan" >
          <a href="#" class="btn"><i class="glyphicon-remove-2"></i></a>
        </td>
        
      </tr>
    
  </tbody>
</table>
</form>