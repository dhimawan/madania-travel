
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Tambah Packet</h3>
  </div>
  <form id="upload_images" action="<?=base_url("pages/create_attribute_packet")?>" method="post">
    <div class="modal-body" style="width: 90%;">
      <div class="control-group">

        <label class="control-label" for="nama"> 
          <input type="checkbox" class="input-xlarge" id="frm-title" name="attr[type]" value="link"> 
          Jadikan link 
       </label>
        <div class="controls">
          
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="nama">Gambar</label>
        <div class="span5 thumbnail" id="thumb-gambar_detail_product">  <img src="" alt=""> </div>
        <div class="controls">
          <input id="file_upload_detail_product" type="file" name="file_upload_detail_product" class="file_upload_detail_product" />
           <input id="frm-gambar_detail_product" type="hidden" name="attr[value]" />
           <input type="hidden" name="attr[page_id]" value="<?=$data->id?>" />
           <!-- <input type="hidden" name="attr[type]" value="text" /> -->
          </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Title</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-title" name="attr[title]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Nama</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="attr[name]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Seo Keyword</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="attr[seo_keywords]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Seo Deskripsi</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="attr[seo_description]">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-textarea">
        <label class="control-label" for="nama">Deskripsi</label>
        <div class="controls">
          <textarea id="attr_page" class="input-xlarge" style="width:100%" name="attr[description]"></textarea>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
    </div>
  </form>
</div>