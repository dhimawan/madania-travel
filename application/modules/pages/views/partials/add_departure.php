<tr>
  <td>#</td>
  <td><input type="text" name="departure[name]" class="input-xlarge" /></td>
  <td><?=date("d/m/Y",strtotime($value->startdate))?> - <?=date("d/m/Y",strtotime($value->enddate))?> </td>
  <td><?=$page->name?></td>
  <td><?=$value->description?></td>
  <td class="actions">
    <a href="#" class="btn"><i class="glyphicon-conversation"></i>
    <a href="#" class="btn"><i class="glyphicon-remove-2"></i>
    <a href="#" class="btn"><i class="glyphicon-right-arrow"></i>
  </td>
</tr>