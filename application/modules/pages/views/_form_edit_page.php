<div id="myModalEdit" style="display:none">
    <form id="upload_images" action="<?=base_url("pages/update_pages")?>" method="post">
    <div class="modal-body" style="width: 100%;">
      <div class="control-group">
        <div class="controls">
           <input type="hidden" name="pages[id]" value="<?=$data->id?>" />
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Title</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="pages[intitle]" value="<?=$data->intitle?> ">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="nama">Nama</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="pages[name]" value="<?=$data->name?> ">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="nama">Url</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="pages[url]" value="<?=$data->url?> ">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Seo Keyword</label>
        <div class="controls">
          <textarea class="input-xlarge" name="pages[seo_keywords]"> <?=$data->seo_keywords?> </textarea>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="nama">Seo Deskripsi</label>
        <div class="controls">
          <textarea class="input-xlarge" name="pages[seo_description]"> <?=$data->seo_description?> </textarea>
        </div>
      </div>
      <div class="clearfix"></div>

      <div class="control">
        <label class="control-label" for="nama">Deskripsi</label>
        <div class="controls">
          <textarea id="page_incontent" class="input-xlarge tinyMCE_text" style="width:100%;height:500px" id="frm-image_description" name="pages[incontent]"> <?=$data->incontent?> </textarea>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
    </div>
    </form>
  </div>