<div id="myEditModal_<?=$value->id?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Edit Attribute Pages</h3>
  </div>
  <form id="upload_images" action="<?=base_url("pages/update_attribute_pages")?>" method="post">
  <div class="modal-body" style="width: 90%;">
    <div class="control-group">
      <label class="control-label" for="nama">Gambar</label>
      <div class="span5 thumbnail" id="thumb-gambar_detail_product_<?=$value->id?>">  
        <img src="<?=$this->config->item("path_upload_pages");?>assets/uploads/pages/<?=$value->value?>"width='200px'  height='80px' />
      </div>
      <div class="controls">
        <input id="file_upload_detail_product_<?=$value->id?>" type="file" name="file_upload_detail_product" class="file_upload_detail_product_<?=$value->id?>" />
         <input id="frm-gambar_detail_product_<?=$value->id?>" type="hidden" name="attr[value]" value="<?=$value->value?>" />
         <input type="hidden" name="attr[page_id]" value="<?=$value->page_id?>" />
         <input type="hidden" name="attr[id]" value="<?=$value->id?>" />
         <input type="hidden" name="attr[type]" value="text" />
         <!-- <a href="javascript:$('#file_upload_detail_product').uploadifyUpload();">Upload Files</a> -->
        <!-- <p class="help-block">Masukan Gambar</p> -->
        </div>
    </div>
    <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Title</label>
        <div class="controls">
          <input type="text" class="input-xlarge" id="frm-image_name" name="attr[title]" value="<?=$value->title?> ">
        </div>
      </div>
    <div class="clearfix"></div>
    <div class="control-group">
      <label class="control-label" for="nama">Nama</label>
      <div class="controls">
        <input type="text" class="input-xlarge" id="frm-image_name" name="attr[name]" value="<?=$value->name?>">
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="control-group">
      <label class="control-label" for="nama">Deskripsi</label>
      <div class="controls">
        <textarea id="attr_page_<?=$value->id?>" class="input-xlarge" style="width:200px;height:200px" name="attr[description]"><?=$value->description?></textarea>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
  </div>
  </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $("#file_upload_detail_product_<?=$value->id?>").uploadify({
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/pages',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#thumb-gambar_detail_product_<?=$value->id?>").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/pages/"+fileObj['name']+"' width='200px'  height='80px' />");
        $("#frm-gambar_detail_product_<?=$value->id?>").val(fileObj['name']);
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
});
</script>