<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Overview</a></li>
    <li><a href="#tab2" data-toggle="tab">Jadwal Keberangkatan</a></li>
    <li><a href="#tab3" data-toggle="tab">Detail Paket</a></li>
    <li><a href="#tab4" data-toggle="tab">History Biaya</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">
      <?=$this->load->view("pages/partials/_form_packet")?>
    </div>
    <div class="tab-pane" id="tab2">
      <?=$this->load->view("pages/partials/_form_departure")?>
    </div>
    <div class="tab-pane" id="tab3">
      <?=$this->load->view("pages/partials/_list_packet")?>
    </div>
    <div class="tab-pane" id="tab4">
      <?=$this->load->view("pages/partials/_list_price")?>
    </div>
  </div>
</div>


<div class="clearfix"></div>
<br/>

<!-- <button class="btn" data-dismiss="modal" aria-hidden="true" id="add_departure">Tambah Jadwal</button> -->
<script type="text/javascript">
$(document).ready(function(){

  $('.file_upload_detail_product').uploadify({
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/pages',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $(".control-image").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/pages/"+fileObj['name']+"' width='200px'  height='80px' />");
        $("#frm-value").val(fileObj['name']);
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });

  tinymce.init({
    mode : "exact",
    elements :"frm-description,p-description,Ep-description",
  });
  $(".tanggal").datepicker();
});
</script>