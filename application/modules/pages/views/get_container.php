<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/swfobject.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/jquery.uploadify.v2.1.4.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/tiny_mce/tiny_mce.js")?>"></script>
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
        theme : "simple",
        editor_selector : "mceSimple"
});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('#btn-confirm-callback').live("click",function(){
    var link  = $(this).attr("title");
    BootstrapDialog.confirm('Anda Yakin untuk menghapus data?', function(result){
      if(result == true){
        window.location.href = link ;
      }
    });
  });
  $("#edit_page").click(function(){
    $("#content_page").hide();
    $("#myModalEdit").show();
  });
});
</script>

<div class="span12 padding">
  <h2>
    <a href="#" role="button" class="btn btn" id="edit_page">
      <i class="glyphicon-edit" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i><?=$title_content?>
    </a>
  </h2>
  <div id="content_page"> <?=$data->incontent?> </div>
  <?=$this->load->view("pages/_form_edit_page",$data)?>
</div>


<div class="span12">
  <h2>
    <a href="#myModal" role="button" class="btn btn-primary" data-toggle="modal">
      <i class="glyphicon-circle-plus" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i>Tambah
    </a>
    Attributes  
  </h2>
  <div class="well">
    <ul class="thumbnails">
      <?=get_attribute_pages($data->id)?>
    </ul>

</div>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Tambah Attribute Pages</h3>
  </div>
  <form id="upload_images" action="<?=base_url("pages/create_attribute_pages")?>" method="post">
  <div class="modal-body" style="width: 90%;">
    <div class="control-group">
      <label class="control-label" for="nama">Gambar</label>
      <div class="span5 thumbnail" id="thumb-gambar_detail_product">  <img src="" alt=""> </div>
      <div class="controls">
        <input id="file_upload_detail_product" type="file" name="file_upload_detail_product" class="file_upload_detail_product" />
         <input id="frm-gambar_detail_product" type="hidden" name="attr[value]" />
         <input type="hidden" name="attr[page_id]" value="<?=$data->id?>" />
         <input type="hidden" name="attr[type]" value="text" />
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="control-group">
      <label class="control-label" for="nama">Title</label>
      <div class="controls">
        <input type="text" class="input-xlarge" id="frm-title" name="attr[title]">
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="control-group">
      <label class="control-label" for="nama">Nama</label>
      <div class="controls">
        <input type="text" class="input-xlarge" id="frm-image_name" name="attr[name]">
      </div>
    </div>
    <div class="clearfix"></div>

    <div class="control-textarea">
      <label class="control-label" for="nama">Deskripsi</label>
      <div class="controls">
        <textarea id="attr_page" class="input-xlarge" style="width:100%" name="attr[description]"></textarea>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
  </div>
  </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
  $('.file_upload_detail_product').uploadify({
      'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
      'script'    : '<?=base_url("uploads/do_upload")?>',
      'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
      'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/pages',
      'auto'      : true,
      'onComplete'  : function(event, ID, fileObj, response, data) {
        $("#thumb-gambar_detail_product").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/pages/"+fileObj['name']+"' width='200px'  height='80px' />");
        $("#frm-gambar_detail_product").val(fileObj['name']);
      },
      'onUploadError' : function(file, errorCode, errorMsg, errorString) {
            alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
        }
  });
});


tinymce.init({
  mode : "exact",
  elements :"page_incontent",
});
</script>