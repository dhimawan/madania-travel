<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends Backend_Controller {
  function __construct()
    {
      parent::__construct();
      $this->load->helper("pages");
    }

  public function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('pages');
    $crud->set_subject('pages');
    $data["content"]["title_content"] = "Pages";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function get_container($id){
    $pages = $this->db->get_where("pages",array("id" => $id))->row();
    $data["content_path"] = "pages/get_container";
    $data["slideshow_widget"] = true;
    $data["title"] = "home";
    $data["content"]["title_content"] = $pages->intitle;
    $data["content"]["data"] = $pages;
    $this->layouts->render($data);
  }

  public function create_attribute_pages(){
    $data = $this->input->post();
    $data["attr"]["created_at"] = date("Y-m-d H:i:s");
    $data["attr"]["updated_at"] = date("Y-m-d H:i:s");
    $data["attr"]["user_created"] = $this->session->userdata("name");
    if($this->db->insert("attribute_pages",$data["attr"])){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    redirect("pages/get_container/".$data["attr"]["page_id"]);
  }

  public function delete_attribute_pages($id,$page_id){
    if($id !=""){
      $this->db->where("id",$id);
      if($this->db->delete("attribute_pages")){
        $this->session->set_flashdata('message', 'Data berhasil Diproses'); 
        redirect("pages/get_container/".$page_id); 
      }
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
        redirect("pages/get_container/".$page_id); 
    }
  }

  public function update_attribute_pages(){
    $data = $this->input->post();
    $this->db->where("id",$data["attr"]["id"]);
    if($this->db->update("attribute_pages",$data["attr"])){
      $this->session->set_flashdata('message', 'Data berhasil Diproses'); 
      redirect("pages/get_container/".$data["attr"]["page_id"]); 
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
        redirect("pages/get_container/".$data["attr"]["page_id"]); 
    }
  }

  public function update_pages(){
    $data = $this->input->post();
    $this->db->where("id",$data["pages"]["id"]);
    if($this->db->update("pages",$data["pages"])){
      $this->session->set_flashdata('message', 'Data berhasil Diproses'); 
      redirect("pages/get_container/".$data["pages"]["id"]); 
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
        redirect("pages/get_container/".$data["pages"]["id"]); 
    }
  }

  public function packet_list(){
    $pages = $this->db->get_where("pages",array("url" => "daftar_paket"))->row();
    $data["content_path"] = "pages/packet_list";
    $data["slideshow_widget"] = true;
    $data["title"] = "home";
    $data["content"]["title_content"] = $pages->intitle;
    $data["content"]["data"] = $pages;
    $data["content"]["attribute_pages"] = $this->db->get_where("attribute_pages",array("page_id"=> $pages->id))->result();
    $this->layouts->render($data);
  }

  public function detail_packet_list(){
    $post = $this->input->post();
    $data["page"] = $this->db->get_where("attribute_pages",array("id"=>$post["id"]))->row();
    $data["departures"] = $this->db
                            ->where(array("attribute_page_id" => $post["id"],"status" => "active"))
                            ->order_by("startdate","asc")
                            ->get("departures")
                            ->result(); 
   $data["packets"] = $this->db
                            ->select("a.id , a.name , a.attribute_page_id , a.description , b.min_single_price , b.max_single_price , b.min_double_price , b.max_double_price , b.min_triple_price , b.max_triple_price , b.min_quad_price , b.max_quad_price,b.id as price_id")
                            ->where(array("a.attribute_page_id" => $post["id"]))
                            ->order_by("a.updated_at","asc")
                            ->join("packet_prices b","b.packet_id = a.id and b.status='active'")
                            ->get("packets a")
                            ->result();
    
    $data["history_packets"] = $this->db
                            ->select("a.id , a.name , a.attribute_page_id , a.description , b.min_single_price , b.max_single_price , b.min_double_price , b.max_double_price , b.min_triple_price , b.max_triple_price , b.min_quad_price , b.max_quad_price,b.id as price_id, c.username,a.updated_at, b.status")
                            ->where(array("a.attribute_page_id" => $post["id"]))
                            ->order_by("b.updated_at","desc")
                            ->join("packet_prices b","b.packet_id = a.id ")
                            ->join("users c","c.id = b.user_id ")
                            ->get("packets a")
                            ->result();
    $this->load->view("pages/detail_packet_list",$data);
  }

  public function update_packet_list(){
    $data = $this->input->post();
    $this->db->where("id",$data["attr"]["id"]);
    if($this->db->update("attribute_pages",$data["attr"])){
      $this->session->set_flashdata('message', 'Data berhasil Diproses'); 
      redirect("pages/packet_list"); 
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
        redirect("pages/packet_list"); 
    }
  }

  public function add_departure(){
    $data = $this->input->post();
    $data["departure"]["id"] = "";
    $data["departure"]["startdate"] = date("Y-m-d H:i:s", strtotime($data["departure"]["startdate"]));
    $data["departure"]["enddate"] = date("Y-m-d H:i:s", strtotime($data["departure"]["enddate"]));
    if($this->db->insert("departures",$data["departure"])){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    
    redirect("pages/packet_list");
  }

  public function delete_departure($id){
    if($id !=""){
      $this->db->where("id",$id);
      if($this->db->update("departures",array("status" => "nonactive") )){
        $this->session->set_flashdata('message', 'Data berhasil Diproses'); 
        redirect("pages/packet_list/"); 
      }
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
        redirect("pages/packet_list/"); 
    }
  }

  public function add_packet(){
    $this->load->model("packets/packet");
    $this->load->model("users/user");
    $data = $this->input->post();
    $email = $this->session->userdata("user_login");
    $this->user->user_id($email);
    $data["packet"]["created_at"] = date("Y-m-d H:i:s");
    $data["packet"]["updated_at"] = date("Y-m-d H:i:s");
    $data["price"]["user_id"] = $this->user->user_id($email);
    if($this->packet->save($data)){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    redirect("pages/packet_list");
  }

  public function edit_packet_price(){
    $post = $this->input->post();
    $data["packet"] = $this->db
                            ->select("a.id , a.name , a.attribute_page_id , a.description , b.min_single_price , b.max_single_price , b.min_double_price , b.max_double_price , b.min_triple_price , b.max_triple_price , b.min_quad_price , b.max_quad_price,b.id as price_id")
                            ->where(array("a.id" => $post["id"]))
                            ->order_by("a.updated_at","asc")
                            ->join("packet_prices b","b.packet_id = a.id and b.status='active'")
                            ->get("packets a")
                            ->row();

    $this->load->view("pages/partials/_form_packet_price",$data);
  }

  public function update_packet_price(){
    $this->load->model("packets/packet");
    $this->load->model("users/user");
    $data = $this->input->post();
    $email = $this->session->userdata("user_login");
    $this->user->user_id($email);
    $data["Epacket"]["created_at"] = date("Y-m-d H:i:s");
    $data["Epacket"]["updated_at"] = date("Y-m-d H:i:s");
    $data["Eprice"]["user_id"] = $this->user->user_id($email);
    if($this->packet->update_packet_price($data)){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
  }

  public function create_attribute_packet(){
    $data = $this->input->post();
    $data["attr"]["created_at"] = date("Y-m-d H:i:s");
    $data["attr"]["updated_at"] = date("Y-m-d H:i:s");
    $data["attr"]["user_created"] = $this->session->userdata("name");
    if($this->db->insert("attribute_pages",$data["attr"])){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    redirect("pages/packet_list");
  }

  public function update_departure(){
    $params = $this->input->post();
    // print_r($params);
    // die;
    $params["departure"]["startdate"] = date("Y-m-d H:i:s", strtotime($params["departure"]["startdate"]));
    $params["departure"]["enddate"] = date("Y-m-d H:i:s", strtotime($params["departure"]["enddate"]));
    $this->db->where("id",$params["departure"]["id"]);
    if($this->db->update("departures",$params["departure"])){
      $status = TRUE;
    }else{
      $status = FALSE;
    }
    return $status;
  }
}
?>