<?php
	
	function get_attribute_pages($page_id){
		$this_ci =& get_instance();
		$html = "";
		if($page_id !=""){
			$data = $this_ci->db->get_where("attribute_pages",array("page_id" => $page_id))->result();
			if(count($data)>0){
				foreach ($data as $key => $value) {
					$html .= '<li class="span3">';
					$html .= '<div class="thumbnail">';
					$html .= '<img src="'.base_url("assets/uploads/pages/".$value->value).'" alt="'.$value->name.'" width="200px"  height="200px" >';
					$html .= '<div class="caption">';
					$html .= '<h5>'.$value->name.'</h5>';
					$html .= '<p>'. word_limiter($value->description,30).'</p>';
					$html .= '<p>';
					$html .= '<a href="#myEditModal_'.$value->id.'" role="button" class="btn btn-primary" data-toggle="modal" >Edit</a> ';
					$html .= '<button class="btn btn-danger" id="btn-confirm-callback" title="'.base_url('pages/delete_attribute_pages/'.$value->id.'/'.$value->page_id).'">Delete </button>';
					$html .= '</p>';
					$html .= '</div>';
					$html .= '</div>';
					$html .= '</li>';
					$html .= update_form($value);
					
				}
			} 
		}
		return $html;
	}

	function get_packet_list($page_id){
		$this_ci =& get_instance();
		$html = "";
		if($page_id !=""){
			$data = $this_ci->db->get_where("attribute_pages",array("page_id" => $page_id))->result();
			if(count($data)>0){
				foreach ($data as $key => $value) {
					$html .= '<a href="#myDetailModal_'.$value->id.'" onclick="detailKeberangkatan('.$value->id.');return false;">';

					$html .= '<li class="span2">';
					$html .= '<div class="thumbnail">';
					$html .= '<img src="'.base_url("assets/uploads/pages/".$value->value).'" alt="'.$value->name.'" width="100px"  height="100px" >';
					$html .= '<div class="caption">';
					$html .= '<h5>'.$value->name.'</h5>';
					// $html .= '<p>'. word_limiter($value->description,30).'</p>';
					$html .= '<p>';
					// $html .= '<a href="#myEditModal_'.$value->id.'" role="button" class="btn btn-primary" data-toggle="modal" >Edit</a> ';
					// $html .= '<button class="btn btn-danger" id="btn-confirm-callback" title="'.base_url('pages/delete_attribute_pages/'.$value->id.'/'.$value->page_id).'">Delete </button>';
					$html .= '</p>';
					$html .= '</div>';
					$html .= '</div>';
					$html .= '</li>';
					$html .= '</a>';
				}
			} 
		}
		return $html;
	}

	function update_form($value){
		$this_ci =& get_instance();
		$data["value"] = $value;
		return $this_ci->load->view("pages/_form_edit_attribute",$data);
	}
	
?>