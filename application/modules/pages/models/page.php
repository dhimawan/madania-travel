<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page extends Common_Model{
  function __construct(){
    parent::__construct("pages");
  }

  function get_pages($url){
  	return $this->db->select("*")
  	->from("pages a")
  	->join("attribute_pages b","b.page_id = a.id")
  	->where("a.url",$url)
  	->get()
  	->result();
  }

  function get_pages_with_limit($url,$limit){
  	return $this->db->select("a.*,b.*,b.value as val_image")
  	->from("pages a")
  	->join("attribute_pages b","b.page_id = a.id")
  	->where("a.url",$url)
  	->limit($limit)
  	->order_by("b.updated_at desc")
  	->get()
  	->result();
  }

  function get_seo($url){
    return $this->db->get_where("pages",array("url" => "$url"))->row();
  }

  function get_child($url){
    $parent = $this->db->get_where("pages",array("url" => "$url"))->row();
    return $this->db->get_where("pages",array("parent_id" => $parent->id))->result();
  }

  function get_seo_news($url){
    return $this->db->select("title as intitle,seo_description,seo_keywords")->get_where("news",array("url" => "$url"))->row();
  }
  
  function get_seo_detail_packet($url){
    return $this->db->select("title as intitle,seo_description,seo_keywords")->get_where("news",array("url" => "$url"))->row();
  }
  

  public function get_packet_graph($url,$year){
    $current_year = date("Y");
    $start_month = 1;

    $end_month = 12;
    $pages = $this->db->get_where("pages",array("url" => "daftar_paket"))->row();
    
    $year_sql = $this->db
        ->select("distinct(YEAR(registrations.departure_start)) as year_all")
        ->join("jamaah_registrations","jamaah_registrations.registration_id = registrations.id")
        ->order_by("registrations.departure_start asc")
        ->get("registrations")
        ->result();
    $data_year = array();
    foreach ($year_sql as $key => $value_year) {
      $data_year[] = $value_year->year_all;
    }
    $attr_page = $this->db->get_where("attribute_pages",array("page_id"=> $pages->id,"type" => "text"))->result();
    $data = array();
    $data_jamaah = array();
    $data_packet = array();
    foreach ($attr_page as $key => $value) {
      for($i=$start_month; $i <= $end_month ; $i++) { 
        $result = $this->db
        ->select("count(jamaah_registrations.id) as total")
        ->where(array("YEAR(registrations.departure_start)" => $year,"MONTH(registrations.departure_start)" => $i,"registrations.attribute_page_id" => $value->id) )
        ->join("jamaah_registrations","jamaah_registrations.registration_id = registrations.id")
        ->group_by("registrations.attribute_page_id")
        ->get("registrations")
        ->row();

        $data[$value->id][] = (empty($result) ? 0 : $result->total);
      }
       $result_donut = $this->db
        ->select("count(jamaah_registrations.id) as total")
        ->where(array("YEAR(registrations.departure_start)" => $year,"registrations.attribute_page_id" => $value->id) )
        ->join("jamaah_registrations","jamaah_registrations.registration_id = registrations.id")
        ->group_by("registrations.attribute_page_id")
        ->get("registrations")
        ->row();
      $data_packet["$value->name"] = (empty($result_donut) ? 0 : $result_donut->total);
    }

    for($i=$start_month; $i <= $end_month ; $i++) { 
        $result = $this->db
          ->select("count(jamaah_registrations.id) as total")
          ->where(array("YEAR(registrations.departure_start)" => $year,"MONTH(registrations.departure_start)" => $i) )
          ->join("jamaah_registrations","jamaah_registrations.registration_id = registrations.id")
          ->group_by("registrations.attribute_page_id")
          ->get("registrations")
          ->row();
        
        $data_jamaah[] = (empty($result) ? 0 : $result->total);

    }
    $data_cabang = array();
    $cabang = $this->db->get_where("config",array("no_kerjasama !=" => ""))->result();
    foreach ($cabang as $key => $cvalue) {
      for($i=$start_month; $i <= $end_month ; $i++) { 
        $result = $this->db
          ->select("count(jamaah_registrations.id) as total")
          ->where(array("YEAR(registrations.departure_start)" => $year,"MONTH(registrations.departure_start)" => $i,"jamaah_registrations.no_kerjasama" => "$cvalue->no_kerjasama") )
          ->join("jamaah_registrations","jamaah_registrations.registration_id = registrations.id")
          ->group_by("jamaah_registrations.no_kerjasama")
          ->get("registrations")
          ->row();
        $data_cabang["".$cvalue->nama."-".$cvalue->no_kerjasama][] = (empty($result) ? 0 : $result->total);
      }
    }
    return array("line_perpaket"=>$data, "per_jamaah" => $data_jamaah,"per_packet" => $data_packet,"per_cabang" => $data_cabang,"data_year" => $year_sql);
  }

}
?>