<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends Backend_Controller {
  function __construct()
  {
      parent::__construct();
      $widget = array(
        "left_menu" => "themes/backend/partials/sidebar_left_widget",
        "header_menu" => "themes/backend/partials/header_widget"
        );
      $this->layouts->get_widget($widget);
  }

public function index($year = null){
  // echo $year;
  $params = $this->input->get();
  // print_r($params);
  $this->load->model("pages/page","pages_model");
  $data["content"]["title_content"] = "Grafik ";
  $data["content_path"] = "reports/index";
  $year = (empty($year) ? date("Y") : $year );
  $data["content"]["year"] = $year;
  $data["content"]["packet_line_graph"] = $this->pages_model->get_packet_graph("daftar_paket",$year);
  $pages = $this->db->get_where("pages",array("url" => "daftar_paket"))->row();
  $data["content"]["attribute_pages"] = $this->db->get_where("attribute_pages",array("page_id"=> $pages->id,"type" => "text"))->result();

  $this->layouts->render($data);
}
  // DIBAWAH INI NANTI DIHAPUS SAJA

  public function report_customers(){
    $data["content"]["title_content"] = "Laporan  Pelanggan ";
    $crud = new ajax_grocery_CRUD();
    $crud->set_table('customers');
    $crud->set_subject('pelanggan');
    $crud->columns('no_ktp','name','sex','religion_id','village_id','address','email','telp');
    $crud->add_fields('no_ktp','name','sex','religion_id','province_id','city_id','district_id','village_id','address','email','telp');
    $crud->edit_fields('no_ktp','name','sex','religion_id','province_id','city_id','district_id','village_id','address','email','telp');
    
    $crud->set_rules('no_ktp','no_ktp','required');
    $crud->display_as("name","Nama Lengkap");
    $crud->display_as("religion_id","Agama");
    $crud->display_as("village_id","Kelurahan");
    $crud->display_as("address","Alamat");
    $crud->display_as("sex","Jenis Kelamin");
    $crud->set_relation('religion_id','religions','name');
    $crud->set_relation('village_id','villages','nama');    
    $crud->set_relation_dependency('city_id','province_id','province_id');
    
    $crud->callback_add_field('province_id',array($this,'select_province'));
    $crud->callback_add_field('city_id',array($this,'select_cities'));
    $crud->callback_add_field('district_id',array($this,'select_districts'));


    $crud->callback_edit_field('province_id',array($this,'select_province'));
    $crud->callback_edit_field('city_id',array($this,'select_cities'));
    $crud->callback_edit_field('district_id',array($this,'select_districts'));

    $crud->field_type('city_id', 'Kota');
    $crud->field_type('province_id', 'Propinsi');
    $crud->field_type('district_id', 'Kecamatan');
    $crud->unset_fields('name','aktivasi_key','status_sso','status');
    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    $crud->callback_before_update(array($this,'notused_data'));
    $crud->callback_before_insert(array($this,'notused_data'));
 
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_assets(){
    $data["content"]["title_content"] = "Laporan Assets Properti";
    $crud = new grocery_CRUD();
    $crud->set_table('products');
    $crud->set_subject('Properti');
    $crud->columns('name','category_id','description' ,'no_shm','on_auction','bank_id','selling_price','address','status' );
    $crud->display_as("name","Nama");
    $crud->display_as("description","Deskripsi");
    $crud->display_as("on_auction","Tanggal Lelang");
    $crud->display_as("address","alamat");
    $crud->display_as("bank_id","Bank");
    $crud->display_as("selling_price","Harga");
    $crud->display_as("category_id","Kategori");
    $crud->set_relation('bank_id','banks','name');
    $crud->set_relation('category_id','categories','nama');
    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    $data["content"]["title_content"] = "Laporan Asset";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_minat(){
    $data["content"]["title_content"] = "laporan Pelanggan Minat";
    $crud = new grocery_CRUD();
    $where = "product_customers.status IN ('minat','dp','lelang','menang','gagal','lunas')";
    $crud->where($where);
    $crud->set_table('product_customers');
    $crud->set_subject('product_customers');
    $crud->set_relation('product_id','products','name');
    $crud->set_relation('customer_no_ktp','customers','name');
    $crud->columns('customer_no_ktp','telp_pelanggan','product_id','alamat_properti','selling_price_properti','tangal_lelang','created_at','status');

    $crud->callback_column('telp_pelanggan',array($this,'customer_telp_callback'));
    $crud->callback_column('alamat_properti',array($this,'product_properti_callback'));
    $crud->callback_column('selling_price_properti',array($this,'product_sellingprice_callback'));
    $crud->callback_column('tangal_lelang',array($this,'product_onauction_callback'));
    $crud->callback_column('created_at',array($this,'product_created_at_callback'));

    $crud->display_as("customer_no_ktp","nama pelanggan");
    $crud->display_as("product_id","nama properti");
    $crud->display_as("created_at","Tgl Pengajuan");
    $crud->display_as("selling_price_properti","Harga");


    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    // $crud->unset_print();
    // $crud->unset_export();
    // $crud->add_action('update', 'update', '','ui-icon-image',array($this,'update_link'));
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_dp(){
    $data["content"]["title_content"] = "Laporan Down payment";
    $crud = new grocery_CRUD();
    $where = "product_customers.status IN ('dp','lelang','menang','gagal','lunas')";
    $crud->where($where);
    $crud->set_table('product_customers');
    $crud->set_subject('product_customers');
    $crud->set_relation('product_id','products','name');
    $crud->set_relation('customer_no_ktp','customers','name');
    $crud->columns('customer_no_ktp','telp_pelanggan','product_id','alamat_properti','selling_price_properti','tangal_lelang','total_dp','date_dp','created_at');

    $crud->callback_column('telp_pelanggan',array($this,'customer_telp_callback'));
    $crud->callback_column('alamat_properti',array($this,'product_properti_callback'));
    $crud->callback_column('selling_price_properti',array($this,'product_sellingprice_callback'));
    $crud->callback_column('tangal_lelang',array($this,'product_onauction_callback'));
    $crud->callback_column('date_dp',array($this,'convert_date'));
    $crud->callback_column('total_dp',array($this,'convert_currency'));

    $crud->display_as("customer_no_ktp","nama pelanggan");
    $crud->display_as("product_id","nama properti");
    $crud->display_as("created_at","tanggal pengajuan");
    $crud->display_as("date_dp","Tgl Dp");
    $crud->callback_column('created_at',array($this,'product_created_at_callback'));
    $crud->display_as("selling_price_properti","Harga");


    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    // $crud->unset_print();
    // $crud->unset_export();
    // $crud->add_action('update', 'update', '','ui-icon-image',array($this,'update_dp_link'));
    // $crud->add_action('perjanjian minat', 'perjanjian minat', '','ui-icon-image',array($this,'pdf_dp_link'));
    // $crud->add_action('perjanjian kerjasama', 'perjanjian kerjasama', '','ui-icon-image',array($this,'pdf_aggrement_link'));

    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_lelang(){
    $data["content"]["title_content"] = "Laporan Lelang";
    $crud = new grocery_CRUD();
    $where = "product_customers.status IN ('lelang','menang','gagal','lunas')";
    $crud->where($where);
    $crud->set_table('product_customers');
    $crud->set_subject('product_customers');
    $crud->set_relation('product_id','products','name');
    $crud->set_relation('customer_no_ktp','customers','name');
    $crud->columns('customer_no_ktp','telp_pelanggan','product_id','alamat_properti','selling_price_properti','no_lelang','penyetuju_lelang','tgl_lelang','created_at','status');

    $crud->callback_column('telp_pelanggan',array($this,'customer_telp_callback'));
    $crud->callback_column('alamat_properti',array($this,'product_properti_callback'));
    $crud->callback_column('selling_price_properti',array($this,'product_sellingprice_callback'));
    $crud->callback_column('tangal_lelang',array($this,'product_onauction_callback'));
    $crud->callback_column('date_dp',array($this,'convert_date'));
    $crud->callback_column('total_dp',array($this,'convert_currency'));

    $crud->display_as("customer_no_ktp","nama pelanggan");
    $crud->display_as("product_id","nama properti");
    $crud->display_as("created_at","tanggal pengajuan");
    // $crud->display_as("date_dp","Tgl Dp");
    // $crud->callback_column('created_at',array($this,'product_created_at_callback'));
    $crud->display_as("selling_price_properti","Harga");
    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    // $crud->unset_print();
    // $crud->unset_export();
    // $crud->add_action('update', 'update', '','ui-icon-image',array($this,'update_lelang_link'));
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_pelunasan(){
    $data["content"]["title_content"] = "Laporan Pelunasan";
    $crud = new grocery_CRUD();
    $where = "product_customers.status IN ('menang','lunas')";
    $crud->where($where);
    $crud->set_table('product_customers');
    $crud->set_subject('product_customers');
    $crud->set_relation('product_id','products','name');
    $crud->set_relation('customer_no_ktp','customers','name');
    $crud->columns('customer_no_ktp','telp_pelanggan','product_id','alamat_properti','selling_price_properti','total_dp','total_pelunasan','tgl_pelunasan','status');

    $crud->callback_column('telp_pelanggan',array($this,'customer_telp_callback'));
    $crud->callback_column('alamat_properti',array($this,'product_properti_callback'));
    $crud->callback_column('selling_price_properti',array($this,'product_sellingprice_callback'));
    $crud->callback_column('tangal_lelang',array($this,'product_onauction_callback'));
    $crud->callback_column('date_dp',array($this,'convert_date'));
    $crud->callback_column('total_dp',array($this,'convert_currency'));
    $crud->callback_column('total_pelunasan',array($this,'convert_currency_pelunasan'));

    $crud->display_as("customer_no_ktp","nama pelanggan");
    $crud->display_as("product_id","nama properti");
    $crud->display_as("selling_price_properti","Harga");


    $crud->unset_delete();
    $crud->unset_add();
    $crud->unset_edit();
    // $crud->unset_print();
    // $crud->unset_export();
    // $crud->add_action('update', 'update', '','ui-icon-image',array($this,'update_lelang_link'));
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function report_collaboration(){
    $this->load->model("products/product");
    $data["content_path"] = "reports/report_collaboration";
    $data["content"]["products"] = $this->product->report_collaboration();
    $data["content"]["title_content"] = "Laporan Konsolidasi";

    $this->layouts->render($data);
  }

   public function customer_telp_callback($value,$row){
    $data = $this->db->get_where("customers",array("no_ktp" => $row->customer_no_ktp))->row();
    return $data->telp;
  }

  public function product_properti_callback($value,$row){
    $data = $this->db->get_where("products",array("id" => $row->product_id))->row();
    return $data->address;
  }

  public function product_sellingprice_callback($value,$row){
    $data = $this->db->get_where("products",array("id" => $row->product_id))->row();
    return "Rp.".number_format($data->selling_price);
  }

  public function product_onauction_callback($value,$row){
    $data = $this->db->get_where("products",array("id" => $row->product_id))->row();
    return date("d-m-Y",strtotime($data->on_auction));
  }

  public function product_created_at_callback($value,$row){
    $data = $this->db->get_where("products",array("id" => $row->product_id))->row();
    return date("d-m-Y",strtotime($data->created_at));
  }

  public function update_link($primary_key , $row)
  {
      return site_url('product_customers/edit_minat').'?product_id='.$row->product_id.'&customer_no_ktp='.$row->customer_no_ktp;
  }

  public function update_dp_link($primary_key , $row){
      return site_url('product_customers/edit_dp').'?id='.$row->id.'&product_id='.$row->product_id.'&customer_no_ktp='.$row->customer_no_ktp;
  }

  public function update_lelang_link($primary_key , $row){
      return site_url('product_customers/edit_lelang').'?id='.$row->id.'&product_id='.$row->product_id.'&customer_no_ktp='.$row->customer_no_ktp;
  }

  public function update_pelunasan_link($primary_key , $row){
    return site_url('product_customers/edit_pelunasan').'?id='.$row->id.'&product_id='.$row->product_id.'&customer_no_ktp='.$row->customer_no_ktp;
  }

  public function convert_date($value,$row){
    return date("d-m-Y",strtotime($row->date_dp));
  }

  public function convert_currency($value,$row){
    return "Rp.".number_format($row->total_dp);
  }

  public function convert_currency_pelunasan($value,$row){
    return "Rp.".number_format($row->total_pelunasan);
  }

}