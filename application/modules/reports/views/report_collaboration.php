<div class="row-fluid" id="main-content-row">
  <div class="span12">
    <h2><i class="glyphicon-more-windows"></i><?=$content["title_content"]?></h2>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Properti</th>
          <th>Harga Jual</th>
          <th>Dp</th>
          <th>Total Pelunasan</th>
          <th>Total( (total_pelunasan + Dp) - Hargajual )</th>
          <!-- <th class="actions" width="110">Actions</th> -->
        </tr>
      </thead>
      <tbody>
        <?php $ttl_selling_price=0; ?>
        <?php $ttl_total_dp=0; ?>
        <?php $ttl_pelunasan=0; ?>
        <?php $ttl_total_keseluruhan=0; ?>
        <?php foreach ($content["products"] as $key => $value) { ?>
        <?php $ttl_selling_price += $value->selling_price; ?>
        <?php $ttl_total_dp += $value->total_dp; ?>
        <?php $ttl_pelunasan += $value->total_pelunasan ; ?>
        <?php $ttl_total_keseluruhan += $value->total_keseluruhan ; ?>
        
        <tr>
          <td><?=($key + 1)?></td>
          <td><?=$value->name?></td>
          <td style="text-align: right;">Rp. <?=number_format($value->selling_price)?></td>
          <td style="text-align: right;">Rp. <?=number_format($value->total_dp)?></td>
          <td style="text-align: right;">Rp. <?=number_format($value->total_pelunasan)?></td>
          <td style="text-align: right;">Rp. <?=number_format($value->total_keseluruhan)?></td>
        </tr> 
        <?php } ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2"> Total </td>
          <td style="text-align: right;">Rp.<?=number_format($ttl_selling_price)?></td>
          <td style="text-align: right;">Rp.<?=number_format($ttl_total_dp)?></td>
          <td style="text-align: right;">Rp.<?=number_format($ttl_pelunasan)?></td>
          <td style="text-align: right;">Rp.<?=number_format($ttl_total_keseluruhan)?></td>
        </tr> 
      </tfoot>
    </table>
  </div>
</div>