
<!-- BEGIN #main-content-row -->
<div class="row-fluid" id="main-content-row">
  <div class="span12" id="main-content-span">
    <div class="span12" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>
        Grafik Per Paket <?//=$year?>  &nbsp;&nbsp;&nbsp;&nbsp; 
        <?php foreach ($packet_line_graph["data_year"]  as $key => $value) { ?>
          <a href="<?=base_url("reports/index/$value->year_all")?>" class="btn <?=($value->year_all == $year ? "btn-success" : "btn-info")?>" >
              <?=$value->year_all?>
            </a>
        <?php } ?>
      </h2>
      <?php foreach ($attribute_pages as $key => $value) { ?>
        <?=$this->load->view("reports/partials/_packet_cart",array("value"=> $value))?> 
      <?php } ?>
    </div>
  </div>


  <div class="clearfix"></div>
  <div class="span12" id="main-content-span">
    <div class="span8" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>Grafik Total Jamaah Perbulan <?=$year?></h2>
      <div id="cart_total"></div>
    </div>

    <div class="span4" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>Grafik Total Per Paket <?=$year?></h2>
        <?=$this->load->view("reports/partials/_packet_angular_cart")?> 
    </div>
  </div>
  <div class="clearfix"></div>
  
  <!-- <div class="span12" id="main-content-span">
    <div class="span12" id="detail_cart">
      <h2><i class="glyphicon-message-flag"></i>Master Detail {{}}</h2>
       
    </div>
  </div> -->
  <div class="clearfix"></div>

  <div class="span12" id="main-content-span">
    <div class="span12" id="main-content-span">
      <h2><i class="glyphicon-message-flag"></i>Grafik Per Cabang <?=$year?></h2>
      <?php foreach ($packet_line_graph["per_cabang"] as $key => $value) { ?>
        <?=$this->load->view("reports/partials/_cabang_cart",array("value"=> $value ,"name" => $key))?> 
      <?php } ?>
    </div>
  </div>

</div>


<script type="text/javascript">
$(function () {
  $('#cart_total').highcharts({
    chart: {
        type: 'area'
    },
    title: {
        text: 'Grafik Total Jamaah Perbulan <?=$year?>'
    },
    subtitle: {
        text: 'Source:www.madaniatravel.com'
    },
    xAxis: {
        labels: {
            formatter: function() {
                return this.value; // clean, unformatted number for year
            }
        }
    },
    yAxis: {
        title: {
            text: 'Total Jamaah'
        },
        labels: {
            formatter: function() {
                return this.value ;
            }
        }
    },
    tooltip: {
        pointFormat: '<b>{point.y:,.0f}</b> {series.name} '
    },
    xAxis: {
      categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei','Jun','Jul','Agustus','Sept','Okt','Nov','Des']
    },
     plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
               series: {
                  cursor: 'pointer',
                  point: {
                      events: {
                          click: function() {
                        
                              get_detail_cart(this.x,<?=$year?>);
                              
                          }
                      }
                  },
                  marker: {
                      lineWidth: 1
                  }
              }
            },
    series: [{
      name: 'Jamaah',
      data: [<?=implode(",",$packet_line_graph["per_jamaah"])?>]
    }]
  });
});

function get_detail_cart(id,year){
  
  // e.preventDefault(); 
          // Call the scroll function
  goToByScroll();  
  // alert(id); 
}

function goToByScroll(id){
      // Reove "link" from the ID
    // id = id.replace("link", "");
      // Scroll
    $('html,body').animate({
        scrollTop: $("#detail_cart").offset().top},
        'slow');
}

    // $("#sidebar > ul > li > a").click(function(e) { 
    //       // Prevent a page reload when a link is pressed
    //     e.preventDefault(); 
    //       // Call the scroll function
    //     goToByScroll($(this).attr("id"));           
    // });
</script>

<style type="text/css">
.bs-callout-info {
    background-color: #F4F8FA;
    border-color: #BCE8F1;
}
.bs-callout {
    border-left: 3px solid #EEEEEE;
    margin: 20px 0;
    padding: 20px;
}

.bs-callout-danger {
    background-color: #FDF7F7;
    border-color: #EED3D7;
}
</style>