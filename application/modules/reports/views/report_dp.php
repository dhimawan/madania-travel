<div class="row-fluid" id="main-content-row">
  <h2><?=$title_content?></h2>
  <div class="span12" id="main-content-span">
    <?php 
      echo $groc_view->output;
    foreach($groc_view->css_files as $file): 
      echo "<link type='text/css' rel='stylesheet' href='".$file."' />";
    endforeach;
    foreach($groc_view->js_files as $file_js):
      echo "<script src='".$file_js."' ></script>";
    endforeach;
    ?>
  </div>
</div>