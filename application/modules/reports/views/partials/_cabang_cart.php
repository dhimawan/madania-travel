<div class="bs-callout <?=(array_sum($value) > 0 ? 'bs-callout-info' : 'bs-callout-danger' )?>  span2" style="margin-left:10px;margin-top:10px">
  <p style="color:#000000;height: 36px;"><b>( <?=array_sum($value)?> Jamaah )</b></p>
  <div id="container_<?=underscore($name)?>" > </div>
  <p style="color:#000000;height: 36px;"><?=$name?> </p>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$.fn.sparkline.defaults.common.height = '50px';
	$.fn.sparkline.defaults.common.width = '100%';
	 var myvalues = [<?=implode(",",$value)?>];
   $('#container_<?=underscore($name)?>').sparkline(myvalues,{
   	 type :'line',
   	 lineWidth:2,
   	 spotRadius:2,
   	 tooltipFormat: '{{offset:names}} : {{y}}',
   	 tooltipValueLookups: {
         names: {
                0: 'Jan',
                1: 'Feb',
                2: 'Mar',
                3: 'Apr',
                4: 'Mei',
                5: 'Jun',
                6: 'Jul',
                7: 'Agu',
                8: 'Sep',
                9: 'Okt',
                10: 'Nov',
                11: 'Des'
            }
    }
   });

});
</script>