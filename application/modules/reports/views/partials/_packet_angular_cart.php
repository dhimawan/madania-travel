<div class="span12 bs-callout bs-callout-info">
  <h5 style="color:#000000"></h5>
  <div id="angular" > </div>
</div>

<div class="clearfix"></div>
<script type="text/javascript">
$(function () {
  $('#angular').highcharts({
		chart: {
		      plotBackgroundColor: null,
		      plotBorderWidth: null,
		      plotShadow: false
		  },
		  title: {
		      text: 'Grafik per paket <?=$year?>'
		  },
		  tooltip: {
		    pointFormat: '<b>{point.y:.0f} </b> Jamaah'
		  },
		  plotOptions: {
				pie: {
				    allowPointSelect: true,
				    cursor: 'pointer',
				    dataLabels: {
				        enabled: false
				    },
				    showInLegend: true
				}
			},
		  series: [{
		      type: 'pie',
		      name: 'Paket',
		      data: [<?php foreach ($packet_line_graph["per_packet"] as $key => $value) {
		      	echo "['".$key."', ".$value."],";
		      }?>]
		  }]
		});

});
</script>