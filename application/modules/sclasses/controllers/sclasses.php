<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sclasses extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('classes');
    $crud->set_subject('classes');
    $data["content"]["title_content"] = "Kelas";
    $crud->set_theme('datatables');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>