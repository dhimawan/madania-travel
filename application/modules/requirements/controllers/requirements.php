<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requirements extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('requirements');
    $crud->set_subject('requirements');
    $data["content"]["title_content"] = "Kota";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }


}
?>