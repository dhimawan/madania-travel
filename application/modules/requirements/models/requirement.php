<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requirement extends Common_Model{
  function __construct(){
    parent::__construct("requirements");
  }

  function get_grouptag_array(){
  	$tag = $this->db
  	->group_by("tag")
  	->get("requirements")
  	->result();
  	$data = array();
  	foreach ($tag as $key => $value) {
  		$req = $this->db->get_where("requirements",array("tag" => $value->tag))->result();
  		$requ = array() ;
  		foreach ($req as $key2 => $val2) {
  			$requ[] = array("name" => $val2->name , "description" => $val2->description, "id" => $val2->id);
  		}
  		$data[$value->tag] = $requ;
  	}
  	return $data;
  }
}
?>