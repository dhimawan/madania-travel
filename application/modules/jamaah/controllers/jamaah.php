<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('jamaah');
    $crud->set_subject('jamaah');
    $data["content"]["title_content"] = "Data Jamaah";
    // $crud->set_relation('province_id','provinces','nama');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>