<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends Common_Model{
  function __construct(){
    parent::__construct("categories");
  }

  function find_name($id){
    return $this->category_model->find_entity_by_id($id)->nama;
  }
}
?>