<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('categories');
    $crud->set_subject('Kategori ');
    $data["content"]["title_content"] = "Kategori ";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }
}