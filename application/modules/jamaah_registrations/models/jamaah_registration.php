<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//require_once ('application/modules/commons/models/common_model.php');

class jamaah_registration extends Common_Model{
  function __construct(){
    parent::__construct("jamaah_registrations");
  }

  public function getAllJamaahRegistration($is_admin,$mou){

  	$this->db->select('
  		jamaah_registrations.id,
  		jamaah.nama_lengkap,
  		jamaah.tempat_lahir,
  		jamaah.tanggal_lahir,
  		jamaah.jk,
  		attribute_pages.name,
  		departures.startdate,
  		departures.enddate
  	');
  	if(!$is_admin){
  		$this->db->where("no_kerjasama",$mou); 
  	}
  	$this->db->join("jamaah","jamaah.id = jamaah_registrations.jamaah_id");
  	$this->db->join("registrations","registrations.id = jamaah_registrations.registration_id");
  	$this->db->join("attribute_pages","attribute_pages.id = registrations.attribute_page_id");
  	$this->db->join("departures","departures.id = registrations.departure_id");

  	
  	$this->flexigrid->build_query();
  	$return['records'] = $this->db->get("jamaah_registrations");
  	$this->db->select('count(jamaah_registrations.id) as record_count');
  	$this->db->join("jamaah","jamaah.id = jamaah_registrations.jamaah_id");
  	$this->db->join("registrations","registrations.id = jamaah_registrations.registration_id");
  	$this->db->join("attribute_pages","attribute_pages.id = registrations.attribute_page_id");
  	$this->db->join("departures","departures.id = registrations.departure_id");

  	$this->db->from("jamaah_registrations");

		$this->flexigrid->build_query(FALSE);
		$record_count = $this->db->get();
		$row = $record_count->row();
		$return['record_count'] = $row->record_count;
  	return $return;
  }
}
?>