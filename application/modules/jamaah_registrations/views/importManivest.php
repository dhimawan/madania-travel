<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/swfobject.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/jquery.uploadify.v2.1.4.js")?>"></script>
<!-- // <script src="<?=base_url("assets/js/uploadify_new/jquery.uploadify.min.js")?>" type="text/javascript"></script> -->
<!-- <link rel="stylesheet" type="text/css" href="<?=base_url("assets/js/uploadify_new/uploadify.css")?>" > -->

<script type="text/javascript" src="<?=base_url("assets/js/tiny_mce/tiny_mce.js")?>"></script>
<script type="text/javascript">
tinyMCE.init({
        mode : "textareas",
        theme : "simple",
        editor_selector : "mceSimple"
});
$(document).ready(function(){
  $("#packet_id").change(function(){
    var id = $(this).val();
    if(id!=""){
      $.ajax({
        type: "POST",
        url: "<?=base_url('frontends/get_packet')?>",
        data: {id:id}
      }).done(function(msg){
        $("#detail_packet_id").html(msg);
      });
      $.ajax({
        type: "POST",
        url: "<?=base_url('jamaah_registrations/get_departure_date')?>",
        data: {id:id}
      }).done(function(msg){
        $("#date_of_departure").html(msg);
      });
    }
  });
  $(".tanggal").datepicker();
});
function update_departure_date(id_selector){
  $.ajax({
    type: "POST",
    url: "<?=base_url('frontends/get_departure_startdate_endate')?>",
    data: {id:id_selector.value}
  }).done(function(msg){
    explode = msg.split("{{explode_this}}");
    $("#departure_start").val(explode[0]);
    $("#departure_end").val(explode[1]);
  });
}
</script>
<div class="span12 padding">
  <h2>
    <a href="#" role="button" class="btn btn" id="edit_page">
      <i class="glyphicon-edit" rel="tooltip" data-original-title=".glyphicon-circle-plus"></i>Import data manivest
    </a>
  </h2>
  <div id="content_page">  
    <form id="upload_images" action="<?=base_url("jamaah_registrations/do_import_manivest")?>" method="post" enctype="multipart/form-data">
      <div class="control-group">
        <label class="control-label" for="nama">Daftar Paket</label>
        <div class="controls">
          <select name="customer_packet[attribute_page_id]" id="packet_id">
            <option value="">--Pilih Paket--</option>
            <?php foreach ($packet as $key => $value) { ?>
              <option value="<?=$value->id?>"><?=$value->name?></option>
            <?php }?>
          </select>
        </div>
      </div>
<!--       <div class="control-group">
        <label class="control-label" for="nama">Detail Paket</label>
        <div class="controls">
          <select name="customer_packet[packet_id]" id="detail_packet_id" placeholder="Pilih Detail paket"> </select>
        </div>
      </div> -->
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">Jadwal Keberangkatan</label>
        <div class="controls" id="date_of_departure">
          <input type="text" name="customer_packet[departure]"  class="input" placeholder="Tanggal Keberangkatan" id="departure_start">
          <input type="text " name="customer_packet[departure_end]"  class="input" placeholder="Tanggal Selesai" id="departure_end">
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="control-group">
        <label class="control-label" for="nama">File(.xls)</label>
        <!-- <div class="span5 thumbnail" id="thumb-gambar_detail_product">   <img src="" alt=""> </div> -->
        <span id="info_upload"> </span>
        <div class="controls">
          <input id="file_upload_detail_product" type="file" name="userfile" class="file_upload_detail_product" accept=".xls" />
           <input id="frm-gambar_detail_product" type="hidden" name="news[image]" />
          
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <!-- <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> -->
        <input type="submit" class="btn btn-primary" id="addNewImage" value="Simpan">
        <input type="reset" class="btn" id="addNewImage" value="close">
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  // $('.file_upload_detail_product').uploadify({
  //     'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
  //     'script'    : '<?=base_url("uploads/do_upload_manivest")?>',
  //     'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
  //     'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/manivest',
  //     'auto'      : true,
  //     'onComplete'  : function(event, ID, fileObj, response, data) {
  //       alert(response);
  //       // $("#thumb-gambar_detail_product").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/manivest/"+fileObj['name']+"' width='200px'  height='80px' />");
  //       $("#frm-gambar_detail_product").val(fileObj['name']);
  //       $("#info_upload").html(fileObj['name']);
  //     },
  //     'onUploadError' : function(file, errorCode, errorMsg, errorString) {
  //           alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
  //       }
  // });
// $('.file_upload_detail_product').uploadify({
  <?php $timestamp = time();?>
    // $('.file_upload_detail_product').uploadify({
    //    'requeueErrors' : true,
    //     // 'formData'     : {
    //     //   'timestamp' : "<?php echo $timestamp;?>",
    //     //   'token'     : "<?php echo md5('madaniatravel'.$timestamp);?>"
    //     // },
    //     'auto'      : true,
    //     'swf'      : "<?=base_url("assets/js/uploadify_new/uploadify.swf")?>",
    //     'uploader' : "<?=base_url("uploads/do_upload_manivest")?>",
    //     'onUploadError' : function(file, errorCode, errorMsg, errorString) {
    //         alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
    //     }
    //   });
});


tinymce.init({
  mode : "exact",
  elements :"page_incontent",
});
</script>