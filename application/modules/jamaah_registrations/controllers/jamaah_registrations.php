<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jamaah_Registrations extends Backend_Controller {
  function __construct()
    {
        parent::__construct();
        $this->load->model("jamaah_registrations/jamaah_registration","jamaah_registration_model");
        $this->load->library('plugins/flexigrid');
        $this->load->helper('plugins/flexigrid');
        $in = (serialize ($_POST));
        log_message('debug', "Ajax module Post -".$in);
    }

  public function index(){
    $colModel['no'] = array('NO',40,TRUE,'center',2);
    $colModel['nama_lengkap'] = array('Nama Lengkap',180,TRUE,'left',1);
    $colModel['tempat_lahir'] = array('Tempat Lahir',180,TRUE,'left',1);
    $colModel['tanggal_lahir'] = array('Tanggal Lahir',120,TRUE,'left',1);
    $colModel['jenis_kelamin'] = array('Jenis Kelamin',70, TRUE,'left',1);
    $colModel['paket'] = array('Paket',130, TRUE,'left',1);
    $colModel['tanggal_berangkat'] = array('Tanggal Berangkat',130, TRUE,'left',1);
    $colModel['tanggal_berakhir'] = array('Tanggal berakhir',130, TRUE,'left',1);

    /*
     * Aditional Parameters
     */
    $gridParams = array(
      'width' => "100%",
      'height' => 400,
      'rp' => 15,
      'rpOptions' => '[10,15,20,25,40]',
      'pagestat' => 'Displaying: {from} to {to} of {total} items.',
      'blockOpacity' => 0.5,
      'title' => 'Data Manivest',
      'usepager'=> true,
      'useRp' => true,
      'showTableToggleBtn' => true,
    );
    
    /*
     * 0 - display name
     * 1 - bclass
     * 2 - onpress
     */
    $buttons[] = array('Download Template Manivest','download','jamaah_registration');
    $buttons[] = array('separator');
    $buttons[] = array('Import Manivest','add','jamaah_registration');
    $buttons[] = array('separator');
    
    //Build js
    //View helpers/flexigrid_helper.php for more information about the params on this function
    $grid_js = build_grid_js('flex1',site_url("/jamaah_registrations/ajax"),$colModel,'id','asc',$gridParams,$buttons);
    $data["content"]['js_grid'] = $grid_js;
    $data["content_path"] = "jamaah_registrations/index";
    $this->layouts->render($data);
  }

  public function ajax(){
    $valid_fields = array('No','nama_lengkap','tempat_lahir','tanggal_lahir','jenis_kelamin','paket','tanggal_berangkat','tanggal_berakhir');
    $this->flexigrid->validate_post('jamaah_registrations.id','asc',$valid_fields);

    $is_admin = $this->user_model->is_admin($this->session->userdata("user_login"));
    $mou = $this->user_model->no_kerjasama($this->session->userdata("user_login"));
    $records = $this->jamaah_registration_model->getAllJamaahRegistration($is_admin,$mou);
    
    $this->output->set_header($this->config->item('json_header'));
    $record_items = array();
    foreach ($records['records']->result() as $index => $row)
    {
      $record_items[] = array($row->id,
      $row->id,
      $row->nama_lengkap,
      $row->tempat_lahir,
      date("d-m-Y",strtotime($row->tanggal_lahir)),
      $row->jk,
      $row->name,
      date("d-m-Y",strtotime($row->startdate)),
      date("d-m-Y",strtotime($row->enddate))
      );
    }
    $this->flexigrid->json_build($records['record_count'],$record_items);
    $this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items));
  }

  public function download_manivest(){
    $this->load->helper('download');
    // $data = 'template manivest xls';
    $data = file_get_contents(FCPATH."assets/template/template_manivest.xls"); // Read the file's contents

    $name = 'template_manivest.xls';
    force_download($name, $data);
  }

  function iiindex2(){
    $crud = new grocery_CRUD();
    $crud->set_table('jamaah_registrations');
    $crud->fields('nama_lengkap', 'tempat_lahir', 'tanggal_lahir' ,'jk' ,'nama_mahram', 'age');

    $crud->callback_column('nama_lengkap',array($this,'get_jamaah'));

    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function get_jamaah($value,$row){
   
    return $value."-".$row->jamaah_id;

  }

  public function importManivest(){
    // importManivest
    $data["content"]["packet"] = $this->page->get_pages("daftar_paket");
    $data["content_path"] = "jamaah_registrations/importManivest";
    $data["content"]["data"] = "";
    $this->layouts->render($data);
  }

  public function do_import_manivest(){
    $this->load->library('excel');
    $config['upload_path'] = './assets/uploads/manivest';
    $config['allowed_types'] = '*';
    $config['encrypt_name'] = TRUE;

    $this->load->library('upload', $config);
    $params = $this->input->post();
    $errors = array();
    if(empty($params["customer_packet"]["attribute_page_id"])){
      $errors[] = "Daftar Paket wajib di isi";
    }
    if(empty($params["customer_packet"]["departure_id"])){
      if(empty($params["customer_packet"]["departure_start"]) && empty($params["customer_packet"]["departure_end"])){
        $errors[] = "Daftar Tanggal Berangkat dan berakhir wajib diisi";
      }else{
        $params["customer_packet"]["departure_id"] = $this->create_departure($params);
      }
    }
    
    if(count($errors) > 0 ){
      print_r($errors);
    }else{
      if($_FILES["userfile"]["type"]=="application/vnd.ms-excel" || $_FILES["userfile"]["type"]=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"){
        if (!$this->upload->do_upload()){
          $data["content"]["error"] = $this->upload->display_errors();
          echo $data["content"]["error"];
        }else{
          $data = array('upload_data' => $this->upload->data());
          // print_r($data);
          $objReader = PHPExcel_IOFactory::createReader('Excel5');
          $objPHPExcel = $objReader->load($config['upload_path']."/".$data["upload_data"]["file_name"]);
          $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
          $total_jamaah = count($sheetData) - 1;

          $registration = array(
            "departure_id"      => $params["customer_packet"]["departure_id"],
            "attribute_page_id" => $params["customer_packet"]["attribute_page_id"],
            "departure_start" => date("Y-m-d H:i:s",strtotime($params["customer_packet"]["departure_start"])),
            "departure_end" => date("Y-m-d H:i:s",strtotime($params["customer_packet"]["departure_end"])),
            "total_jamaah "     => $total_jamaah ,
            "created_at"        => date("Y-m-d H:i:s"),
            "updated_at"        => date("Y-m-d H:i:s")
          );
          // print_r($registration);
          // die;
          $this->db->insert("registrations",$registration);
          $registration_id = $this->db->insert_id();

          foreach ($sheetData as $index => $data) {
            if($index != 1){
              //***************** Insert DATA JAMAAH ************************ //
              $jamaah_arr = array(
                "nama_lengkap"  => "".$data["B"]."",
                "jk"            => ($data["C"]=="MR" ? "pria" : "wanita"),
                "tanggal_lahir" => "".date("Y-m-d",strtotime($data["E"]))."",
                "tempat_lahir"  => "'".$data["F"]."",
                "nama_mahram"   => "".$data["I"]."",
                "created_at"    => "".date("Y-m-d  H:i:s")."", 
                "updated_at"    => "".date("Y-m-d  H:i:s").""
              );
              $jamaah = $this->db->get_where("jamaah",array("nama_lengkap" => $jamaah_arr["nama_lengkap"], "jk"=>$jamaah_arr["jk"],"tanggal_lahir" => $jamaah_arr["tanggal_lahir"],"tempat_lahir" =>$jamaah_arr["tempat_lahir"] ))->row();
              if(empty($jamaah)){
                $this->db->insert("jamaah",$jamaah_arr);
                $jamaah_id = $this->db->insert_id();
              }else{
                $jamaah_id = $jamaah->id;
              }
              //***************** END Insert DATA JAMAAH ************************//

              $passdate = explode("-",$data["G"]);
              $start = date("Y-m-d",strtotime($passdate[0]));
              $day_month = date("m-d",strtotime($passdate[0]));

              $passport = array(
                "jamaah_id"   =>  $jamaah_id,
                "no_passport" => "".$data["D"]."",
                "berlaku"     => "".$start."",
                "berakhir"    => "".$passdate[1]."-".$day_month."",
                "status"      => "active",
                "created_at"  => "".date("Y-m-d H:i:s").""
              );
              $pass = $this->db->get_where("passports",array("jamaah_id" => $passport["jamaah_id"] , "no_passport" => $passport["no_passport"]))->row();
              if(empty($pass)){
                $this->db->insert("passports",$passport);
                $pass_id =  $this->db->insert_id();
              }else{
                $pass_id =  $pass->id;
              }
                
              $config = $this->db->get_where("config",array("no_kerjasama" => $data["K"]))->row();
              $user = $this->db->get_where("users",array("email" => $this->session->userdata("user_login")))->row();
              // print_r($this->session->userdata("user_login"));die;
              $jamaah_registration = array(
                "registration_id" => $registration_id,
                "jamaah_id"       => $jamaah_id,
                "user_id"         => $user->id,
                "no_kerjasama"    => (empty($config)? "" : "$config->no_kerjasama"),
                "age"             => (empty($data["J"])? 0 : $data["J"]),
                "created_at"      => "".date("Y-m-d H:i:s")."",
                "updated_at"      => "".date("Y-m-d H:i:s").""
              );
              $this->db->insert("jamaah_registrations",$jamaah_registration);
              // echo "<pre>";
              // print_r($jamaah);
              // print_r($passport);
              // foreach ($data as $key => $value) {

              // }
            }
          }
          redirect("jamaah_registrations");
          $this->deleteFiles($config['upload_path']);
          
        }
      }else{
        echo "invalid data";
      }
    }
  } 

  private function create_departure($params){
    $id = "";
    if(!empty($params["customer_packet"]["attribute_page_id"])){
      $data = array(
        "name" => "Request User ".date("dmY",strtotime($params["customer_packet"]["departure_start"]))."S/D".date("dmY",strtotime($params["customer_packet"]["departure_end"]))."",
        "created_at" => "".date("Y-m-d H:i:s")."",
        "updated_at" => "".date("Y-m-d H:i:s")."",
        "startdate" => "".date("Y-m-d H:i:s",strtotime($params["customer_packet"]["departure_start"]))."",
        "enddate" => "".date("Y-m-d H:i:s",strtotime($params["customer_packet"]["departure_end"]))."",
        "attribute_page_id"  => $params["customer_packet"]["attribute_page_id"],
        "status"  => "active" ,
        "type"  => "request_user",
      );
      $this->db->insert("departures",$data);
      $id = $this->db->insert_id();
    }
    return $id;
  }

  private function deleteFiles($path){
    $files = glob($path.'*'); // get all file names
    foreach($files as $file){ // iterate files
      if(is_file($file))
        unlink($file); // delete file
        //echo $file.'file deleted';
    }   
  }

  public function get_departure_date(){
    $params = $this->input->post();
    $data = $this->db->get_where("departures",array("attribute_page_id"=>$params["id"]))->result();
    $html ="";
    if(count($data)>0){
      $html .= "<select name='customer_packet[departure_id]' onchange='update_departure_date(this)'>";
      $html .= "<option value=''>--Pilih Jadwal Keberangkatan--</option>";
        foreach ($data as $key => $value) {
          $html .="<option value='".$value->id."' > ".date("d-m-Y",strtotime($value->startdate))." s/d ".date("d-m-Y",strtotime($value->enddate))."  </option> ";
        }
      $html .="</select>";
    }
    $html .='<input type="text" class="input" placeholder="Tanggal Keberangkatan" name="customer_packet[departure_start]" id="departure_start"> ';
    $html .='<input type="text" class="input" placeholder="Tanggal Selesai"  name="customer_packet[departure_end]" id="departure_end"  >';
    echo $html;
  }

  public function get_departure_startdate_endate(){
    $params = $this->input->post();
    $data = $this->db->get_where("departures",array("id"=>$params["id"]))->result();
    $html = array();
    foreach ($variable as $key => $value) {
      $html[] = date("d-m-Y",strtotime($value->startdate))."{{explode_this}}".date("d-m-Y",strtotime($value->enddate)); 
    }
    echo implode(" ", $html);
    // echo date("d-m-Y",strtotime($data->startdate))."{{explode_this}}".date("d-m-Y",strtotime($data->enddate));
  }

}
?>