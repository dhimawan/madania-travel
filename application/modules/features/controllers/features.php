<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Features extends Backend_Controller {
  function __construct(){
      parent::__construct();
    }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('features');
    $crud->set_subject('feature');
    $crud->columns('id','name','key','description' ,'controller_name','method_name','second_method_name','created_at');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>