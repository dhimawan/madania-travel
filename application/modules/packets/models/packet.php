<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packet extends Common_Model{
  function __construct(){
    parent::__construct("packets");
  }

  public function save($params){
  	$status = FALSE ;
  	if($this->db->insert("packets",$params["packet"])){
  		$params["price"]["packet_id"] = $this->db->insert_id();
  		$params["price"]["created_at"] = date("Y-m-d H:i:s");
    	$params["price"]["updated_at"] = date("Y-m-d H:i:s");
    	$params["price"]["status"] = "active";
    	$this->db->insert("packet_prices",$params["price"]);
    	$status = TRUE;
  	}
  	return $status;
  }

  public function update_packet_price($params){
  	$status = FALSE ;
  	$this->db->where("id",$params["Epacket"]["id"]);
  	if($this->db->update("packets",$params["Epacket"])){
  		$this->nonactive_status_price($params["Epacket"]["id"]);
  		$params["Eprice"]["packet_id"] = $params["Epacket"]["id"];
  		$params["Eprice"]["created_at"] = date("Y-m-d H:i:s");
    	$params["Eprice"]["updated_at"] = date("Y-m-d H:i:s");
    	$params["Eprice"]["status"] = "active";
    	$this->db->insert("packet_prices",$params["Eprice"]);
    	$status = TRUE;
  	}
  	return $status;
  }

  

  public function nonactive_status_price($packet_id){
  	$this->db->where("packet_id",$packet_id);
  	$this->db->update("packet_prices",array("status" => "nonactive"));
  }

  public function packet_prices_array(){
    $packet = $this->db
    ->join("packet_prices","packet_prices.packet_id = packets.id and packet_prices.status = 'active'")
    ->order_by("packets.attribute_page_id asc ")
    ->group_by("packets.id")
    ->get("packets")
    ->result();

    $data=array();
    foreach ($packet as $key => $value){
      // if(!empty($data[$value->attribute_page_id])){
      //   $data[$value->attribute_page_id][] =array( 
      //     "packet_name" => $value->name ,
      //     "packet_description" => $value->description,
      //     "min_single_price"  => $value->min_single_price,
      //     "max_single_price" => $value->max_single_price,
      //     "min_double_price" => $value->min_double_price,
      //     "max_double_price" => $value->max_double_price,
      //     "min_triple_price" => $value->min_triple_price,
      //     "max_triple_price" => $value->max_triple_price,
      //     "min_quad_price" => $value->min_quad_price,
      //     "max_quad_price" => $value->max_quad_price
      //    );
      // }else{
        $data[$value->attribute_page_id][] =array(
          "attribute_page_id" => $value->attribute_page_id,
          "packet_name" => $value->name ,
          "packet_description" => $value->description,
          "min_single_price"  => $value->min_single_price,
          "max_single_price" => $value->max_single_price,
          "min_double_price" => $value->min_double_price,
          "max_double_price" => $value->max_double_price,
          "min_triple_price" => $value->min_triple_price,
          "max_triple_price" => $value->max_triple_price,
          "min_quad_price" => $value->min_quad_price,
          "max_quad_price" => $value->max_quad_price
         );
      // }
    }
    return $data;
  }
}
?>