<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Packets extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('packets');
    $crud->set_subject('packets');
    $data["content"]["title_content"] = "packets";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }
}
?>