<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('videos');
    $crud->set_subject('videos');
    $data["content"]["title_content"] = "Daftar Video";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }
}
?>