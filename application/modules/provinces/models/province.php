<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Province extends Common_Model{
  function __construct(){
    parent::__construct("provinces");
  }

  function selectOption(){
    $option = Array();
    $query = $this->db->get("provinces");
    foreach($query->result() as $value){
      $option[$value->id] = $value->nama ;
    }
    return $option;
  }
}
?>