<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Provinces extends Backend_Controller {
   function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('provinces');
    $crud->set_subject('Propinsi');
    $data["content"]["title_content"] = "Propinsi";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>