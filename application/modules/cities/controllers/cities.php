<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cities extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('cities');
    $crud->set_subject('Kota');
    $data["content"]["title_content"] = "Kota";
    $crud->set_relation('province_id','provinces','nama');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  function getCities($province_id){
   $data["cities"] = $this->city->findall_entity_by("province_id",$province_id);
   $html = $this->load->view("cities/getCities",$data);
    return $html;
  }
}
?>