<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Login_backend extends MX_Controller
{

  function __construct()
  {
    $this->load->model('users/user','user_model');
    $this->layouts->themes = $this->config->item("backend_layout");
    $this->layouts->folder = $this->config->item("folder");
    $this->layouts->assets = $this->config->item("backend_assets");
    $this->layouts->layout = "login";
  }
  public function index(){
    $data["content_path"] = "login_backend/index";
    $data["content"] = "";
    $this->layouts->render($data);
    

  }

  public function login(){
   if((!$this->input->post('email') =='') && (!$this->input->post('password')=='') ){
      $data['email']=$this->input->post('email');
      $data['password']=$this->encode($this->input->post('password'));
      $rows=$this->user_model->find($data);
      if(count($rows) > 0){
        $features = $this->user_model->get_features($rows[0]->email);
        $this->session->set_flashdata('success', ' Anda berhasil login');
        $this->session->set_userdata('user_login',$rows[0]->email);
        $this->session->set_userdata('name',$rows[0]->username);
        $this->session->set_userdata('group',$rows[0]->group_id);
        $this->session->set_userdata('features',$features);
        redirect('dashboard');
      }else{
        $this->session->set_flashdata('error', 'Email / Password anda salah silakan ulangi kembali ');
        $this->index();
      }
    }else{
        $this->session->set_flashdata('error', 'Email / Password anda salah silakan ulangi kembali ');
        $this->index();
    }
  }
  
  public function logout(){
    $this->session->sess_destroy();
    redirect('login_backend/');
  }

  private function encode($pass){
    $salt=$this->config->item('encryption_key');
     if (!empty($salt) ||!empty($pass) ){
          $data = md5($pass."".$salt);
      }
      return $data;
  }

}