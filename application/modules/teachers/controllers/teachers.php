<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teachers extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('teachers');
    $crud->set_subject('teachers');
    $data["content"]["title_content"] = "Guru";
    $crud->set_theme('datatables');
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>