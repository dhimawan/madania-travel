<?php 

function menu_left($type){
  $this_ci =& get_instance();
  $this_ci->load->database();
  $this_ci->load->model("app_menus/app_menu");
  $this_ci->load->model("app_categories/app_category");
  $categori = $this_ci->app_category->find_entity_by_name("$type");
  $data = $this_ci->app_menu->find(array("app_category_id"=> $categori->id));
  // if($type == "setting"){
  //   $categori = $this_ci->app_category->find_entity_by_name("setting_backend");
  //   $data = $this_ci->app_menu->find(array("app_category_id"=> $categori->id));
  // }elseif($type == "dasboard"){
  //   $categori = $this_ci->app_category->find_entity_by_name("setting_backend");
  //   $data = $this_ci->app_menu->find(array("name"=>"dashboard_backend"));
  // }elseif($type == "master"){
  //   $data = $this_ci->app_menu->find(array("name"=>"data_master_backend"));
  // }
  
  $menu = generated_menu($data,"");
  return $menu;
}

function generated_menu($data,$current_menu = ""){
  $this_ci =& get_instance();
  $this_ci->load->database();
  $this_ci->load->model("features/feature");
  $this_ci->load->model("group_features/group_feature");

  $html = '<ul class="nav">';
  $html .='<li class="active"><a href="#"><span class="label label-inverse"><i class="icon-home icon-white"></i></span> Settings </a></li>';
  $active_menu = "";
  foreach ($data as $key => $value) {
    $feature = $this_ci->feature->find_entity_by_id($value->feature_id);
    $group_feature = $this_ci->group_feature->find(array('feature_id' => $value->feature_id,'group_id' => $this_ci->session->userdata("group")));
    if($this_ci->session->userdata("group")=="99"){
      $html .='<li class="'.$active_menu.'">';
        $html .= '<a href="'.site_url($feature->controller_name."/".$feature->method_name).'"><span class="label label-inverse"><i class="'.random_image_menu().' icon-white"></i></span> '.$value->name.' </a>';
        $html .="</li>";
    }else{
      if(!empty($group_feature)){
        $html .='<li class="'.$active_menu.'">';
        $html .= '<a href="'.site_url($feature->controller_name."/".$feature->method_name).'"><span class="label label-inverse"><i class="'.random_image_menu().' icon-white"></i></span> '.$value->name.' </a>';
        $html .="</li>";
      }
    }
    
  }
  $html .="</ul>";

  return $html;
}


function random_image_menu(){
  $icon = array("icon-home","icon-list","icon-th-large","icon-list","icon-exclamation-sign");
  return $icon[array_rand($icon)];
}

function header_menu(){
  $this_ci =& get_instance();
  $this_ci->load->database();
  $this_ci->load->model("pages/page");
  $this_ci->load->model("categories/category_model");

  $html = '<ul class="main-nav">';
  $html .='<li class="active"><a href="'.base_url().'">Home</a></li>';
  if($this_ci->session->userdata('customer_login')!=""){
    $html .= '<li><a class="dd" href="#">Kategori properti</a>';
    $html .="<ul>";
    $category =  $this_ci->category_model->find(array());
    foreach ($category as $key => $cat) {
      $html .='<li><a href="'.base_url("frontends/category/".$cat->id."").'">'.$cat->nama.'</a></li>';
    }
    $html .="</ul>";
    $html .="</li>";
  }

  $about =  $this_ci->page->find_entity_by("url","about-us");
  $html .='<li><a href="'.base_url("frontends/page/".$about->id."").'">'.$about->intitle.'</a></li>';
  $caralelang =  $this_ci->page->find_entity_by("url","cara_lelang");
  $html .='<li><a href="'.base_url("frontends/page/".$caralelang->id."").'">'.$caralelang->intitle.'</a></li>';
  $carapendaftaran =  $this_ci->page->find_entity_by("url","cara_pendaftaran");
  $html .='<li><a href="'.base_url("frontends/page/".$carapendaftaran->id."").'">'.$carapendaftaran->intitle.'</a></li>';
  $html .='<li><a href="'.base_url("frontends/contactus").'">Hubungi kami</a></li>';
 
  $html .= '</ul>';

  return $html;
}