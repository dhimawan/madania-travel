<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_menu extends Common_Model{
  function __construct(){
    parent::__construct("app_menus");
  }

  function get_menu_categories($category_id){
  	$menu = $this->db->get_where("app_menus",array("app_category_id" => $category_id))->result();
  	$array_menu = array();
  	foreach ($menu as $key => $value) {
  		$array_menu[]=$value->feature_id; 
  	}
  	return $array_menu;
  }

}
?>