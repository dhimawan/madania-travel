<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_menus extends Backend_Controller {
   function __construct(){
      parent::__construct();
    }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('app_menus');
    $crud->set_subject('Application Menus');
    $crud->set_relation("feature_id",'features',"key");
    $crud->set_relation("app_category_id",'app_categories',"title");
    $data["content"]["title_content"] = "Menu Application";
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

}
?>