<style>
  .ev-form {
    border-top: 1px solid #CCCCCC !important;
    margin: 50px 10px 10px;
    padding: 10px !important;
  }
  .ev-form fieldset {}
  .ev-form legend {
    background: none repeat scroll 0 0 #FFFFFF;
    border: medium none;
    float: left;
    font-family: Arial;
    margin: 0;
    padding: 0 20px;
    position: absolute;
    top: -20px;
    width: auto;
  }
  .ev-form #name_field_box {
    margin-top: 20px;
  }
  .ev-form #name_display_as_box {}
  .ev-form #name_input_box {
    margin-bottom: 10px;
    width: 100%;
  }
  .ev-form #name_input_box span {
    float: left;
    margin-right: 2%;
    width: 23%;
  }
  .ev-form select {
    padding: 2px 5px !important;
  }
  .ev-form .pDiv {
    border: medium none !important;
  }
  .ev-map {
    background: none repeat scroll 0 0 #FAFAFA;
    border: 1px solid #EEEEEE;
    height: 300px;
    margin-left: 10px;
    margin-top: 20px;
    text-align: center;
    width: 95%;
  }
  .ev-longlate {
    margin-left: 10px;
    margin-top: 20px;
    width: 95%;
  }
  .ev-longlate label {}
  .ev-longlate span {
    float: left;
    margin-right: 3%;
    width: 45%;
  }
  .ev-longlate input {
    width: 100% !important;
  }
  .ev-address {
    margin-left: 10px;
    margin-top: 20px;
    width: 95%;
  }
  .ev-address label {}
  .ev-address span {}
  .ev-address textarea {
    margin-bottom: 20px !important;
  }
  #geocomplete { width: 95%}

.map_canvas { 
  width: 95%; 
  height: 400px; 
  margin: 10px 20px 10px 0;
  max-width: none;
}

.map_canvas img { 
  max-width: none;
}

.map_canvas label { 
  width: auto; display:inline; 
} 

/*############*/

.input-mini {
    width: 203px !important;
}
/*
.right_tumbnail {
    float: right;
    width: 225px !important;
    margin-left: 0 !important;
    margin-right: 0 !important;
}


.left_tumbnail {
    float: left;
    width: 260px;
}

.thumbnail {
    border: 1px solid #DDDDDD;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
    display: block;
    float: left;
    line-height: 1;
    padding: 4px;
    width: 500px !important;
}*/
</style>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
<script type="text/javascript" src="<?=base_url("assets/js/jquery.geocomplete.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/swfobject.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/uploadify-v2.1.4/jquery.uploadify.v2.1.4.js")?>"></script>
<script type="text/javascript" src="<?=base_url("assets/js/bootstrap-datepicker.js")?>"></script>
<script type="text/javascript" src="<?=base_url()?>/assets/js/app_area.js"></script>

<link href="<?=base_url("assets/js/uploadify-v2.1.4/uploadify.css")?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url("assets/css/datepicker.css")?>" rel="stylesheet" type="text/css" />
<script>
      $(function(){

        $("#ev-map").geocomplete({
          map: ".map_canvas",
          details: "form ",
          markerOptions: {
            draggable: true
          }
        });
        
        $("#ev-map").bind("geocode:dragged", function(event, latLng){
          $("#lat").val(latLng.lat());
          $("#long").val(latLng.lng());
          $("#reset").show();
        });
        
        
        $("#reset").click(function(){
          $("#ev-map").geocomplete("resetMarker");
          $("#reset").hide();
          return false;
        });
        
        $("#find").click(function(){
          $("#ev-map").trigger("geocode");
        }).click();
        $("#ev-map").trigger("geocode");
        // $('.currency').autoNumeric('init');
        // $('.currency_t').autoNumeric('init');
        // $('.currency').blur(function(){
        //   var number = $('.currency').autoNumeric('get'); 
        //   $("#product_harga_beli").val(number);
        // });
        // $('.currency_t').blur(function(){
        //   var number = $(this).autoNumeric('get'); 
        //   var new_id = $(this).attr("id").split("-");
        //   $("#"+new_id[1]).val(number);
        // });
        // expenses_value_new_id[1]

        $("#status_kerjasama").change(function(){
          var status = $(this).val();
          $.ajax({
            type: "POST",
            url: "<?=base_url('app_config/get_status_kerjasama')?>",
            data: {status:status}
          }).done(function(msg){
            $("#parent_id").html(msg);
          });
        });
      });
    </script>
<div style="width: 100%;" class="flexigrid crud-form">  
  <div class="mDiv">
    <div class="ftitle">
      <div class="ftitle-left">
        
      </div>
      <div class="clear"></div>
    </div>
    <div class="ptogtitle" title="Minimize/Maximize">
      <span></span>
    </div>
  </div>
<div id="main-table-box">
  <form accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off" id="" method="post" action="<?=base_url('app_config/update_custom_config')?>">   <div class="ev-form form-div">
      <fieldset>
        <legend>Daftar Cabang</legend>
        <div id="name_input_box" class="form-input-box">
            <span>
              <label>Nama :</label>
              <input type="text" class="input-mini" id="product_name" name="config[nama]" value="<?=$data->nama?>">
              <input type="hidden" class="input-mini" id="product_name" name="config[id]" value="<?=$data->id?>">
            </span>
            <span>
              <label>Slogan:</label>
              <input type="text" class="input-mini" id="product_name" name="config[slogan]" value="<?=$data->slogan?>">
            </span>
            <span>
              <label>Email :</label>
              <input type="text" class="input-mini" id="product_name" name="config[email]" value="<?=$data->email?>">
            </span>
            <span>
              <label>Facebook :</label>
              <input type="text" class="input-mini" id="product_name" name="config[facebook]" value="<?=$data->facebook?>">
            </span>
            <span>
              <label>Twiter :</label>
              <input type="text" class="input-mini" id="product_name" name="config[twiter]" value="<?=$data->twiter?>">
            </span>
            <span>
              <label>Google + :</label>
              <input type="text" class="input-mini" id="product_name" name="config[google_plus]" value="<?=$data->google_plus?>">
            </span>

            <span>
              <label>No Telphone :</label>
              <input type="text" class="input-mini" id="product_name" name="config[telp]" value="<?=$data->telp?>">
            </span>
            <span>
              <label>Contact Person :</label>
              <input type="text" class="input-mini" id="product_name" name="config[contact_person]" value="<?=$data->contact_person?>">
            </span>
            <span>
              <label>No Kerjasama :</label>
              <input type="text" class="input-mini" id="product_name" name="config[no_kerjasama]" value="<?=$data->no_kerjasama?>">
            </span>
            <span>
              <label>Kantor Pusat :</label>
              <!-- <input type="text" class="input-mini" id="product_name" name="product[name]"> -->
              <select name="config[is_office]"> 
                <option value="false" <?=($data->is_office==true ? "selected":"")?>> Tidak </option>
                <option value="true" <?=($data->is_office==true ? "selected":"")?>> Iya </option>
              </select>
            </span>
            <span>
              <label>Status Kerjasama :</label>
              <!-- <input type="text" class="input-mini" id="product_name" name="product[name]"> -->
              <select name="config[status_kerjasama]" id="status_kerjasama"> 
                <option value="">--Pilih--</option>
                <option value="cabang" <?=($data->status_kerjasama=="cabang" ? "selected":"")?>> cabang </option>
                <option value="subcabang" <?=($data->status_kerjasama=="subcabang" ? "selected":"")?>> sub cabang </option>
                <option value="perwakilan" <?=($data->status_kerjasama=="perwakilan" ? "selected":"")?>> perwakilan </option>
                <option value="pusat" <?=($data->status_kerjasama=="pusat" ? "selected":"")?>> pusat </option>
              </select>
            </span>
            <span>
              <label>Cabang/subcabang/Perwakilan Dari :</label>
              <select name="config[parent_id]" id="parent_id"> 
                <option value="">-Tidak Tersedia-</option>
              </select>
              <!-- <input type="text" class="input-mini" id="product_name" name="product[name]"> -->
            </span>
        </div>
       
      </fieldset>
    </div>
    <div class="ev-form form-div">
      <fieldset>
        <legend>LOKASI</legend>
        <div id="name_field_box" class="form-field-box odd">
          <div id="name_input_box" class="form-input-box">
            
          </div>
          <div class="ev-address">
            <span><label>Address</label><textarea cols="" rows="" id="ev-map" name="config[alamat]"><?=$data->alamat?></textarea></span>
          </div>

          <div class="clear"></div> 
        </div>
        <div class="ev-map map_canvas"></div>
        <!-- <div class="map_canvas"> disini</div> -->
        <div class="ev-longlate">
          <span ><label>Long</label><input type="text" id="long" name="config[long]" value="<?=$data->long?>"/></span>
          <span><label>Late</label><input type="text" id="lat" name="config[lat]" value="<?=$data->lat?>"/></span>
          <div class="clear"></div>
        </div>
      </fieldset>
    </div>
    
    <div class="pDiv">
      <div class="form-button-box">
        <input type="submit" class="btn btn-large" value="Save" id="save_products">
      </div> 
      <div class="form-button-box">
        <input type="button" class="btn btn-large" onclick="javascript: goToList()" value="Cancel">
      </div>
            
      <div class="form-button-box">
        <div id="FormLoading" class="small-loading">Loading, saving data...</div>
      </div>
      <div class="clear"></div> 
    </div>
  </form>
</div>
</div>



<script type="text/javascript">
$(document).ready(function(){
  
  // $('.currency_t').autoNumeric('set','value');
  // $(".currency_t").each(function(){
  //   var valu = $(this).val();
  //   $(this).autoNumeric('set',valu);
  // });
  // $("#crudForm").submit(function(){
  //   var data = $(this).serialize();
  //   var url = $("#base_url").val();
  //   jQuery.ajax({
  //     dataType:'script',
  //     type:'post',
  //     data: data,
  //     success: function(request){
  //       // console.log(request);
  //       ShowNotifyMessage(request);
  //     },
  //     loading: function(request){
  //       $('#spinner').show();
  //       $(".FormLoading").show();
  //       $("#save_products").hide();

  //     },
  //     error:function(request){
  //       // ShowNotifyMessage(request.responseText);$('#spinner').hide()
  //       // window.location 
  //     },
  //     url: "<?=base_url('products/create')?>"
  //   });
  //   return false;
  // });
  // $('.datepicker').datepicker();
  // $('.file_upload_detail_product').uploadify({
  //     'uploader'  : '<?=base_url("assets/js/uploadify-v2.1.4/uploadify.swf")?>',
  //     'script'    : '<?=base_url("uploads/do_upload")?>',
  //     'cancelImg' : '<?=base_url("assets/js/uploadify-v2.1.4/cancel.png")?>',
  //     'folder'    : '<?=$this->config->item("path_upload_pages");?>assets/uploads/products',
  //     // 'folder'    : '<?=base_url("assets/uploads/products");?>',
  //     'auto'      : true,
  //     'onComplete'  : function(event, ID, fileObj, response, data) {
  //       // alert($(this).attr("id"));
  //       $("#thumb-gambar_detail_product").html("<img src='<?=$this->config->item("path_upload_pages");?>assets/uploads/products/"+fileObj['name']+"' width='200px'  height='80px' />");
  //       $("#frm-gambar_detail_product").val(fileObj['name']);
  //       // $("#tr_form_detail_produk-hide").clone(true).appendTo(".body_detail_produk");

  //       // $('#myModal').modal('hide');
  //     },
  //     'onUploadError' : function(file, errorCode, errorMsg, errorString) {
  //           alert('The file ' + file.name + ' could not be uploaded: ' + errorString);
  //       }
  // });

  $("#addNewImage").click(function(){
    var data = $("#upload_images").serialize();
    $(".close").trigger("click");
    var name = $("#frm-image_name").val();
    var description = $("#frm-image_description").val();
    var gambar = $("#frm-gambar_detail_product").val();
    var index = $("#index_image").val();
    var new_index = parseInt($("#tbody_images>tr").length);
    $("#index_image").val(new_index);
    var tr = '<tr id="tr_image'+new_index+'">';
        tr +='<td> <a href="#" class="thumbnail"><img src="<?=$this->config->item("path_upload_pages");?>assets/uploads/products/'+gambar+'" alt="" width="160px"></a></td>';
        tr +='<td>'+name+'</td>';
        tr +='<td>'+description+'</td>';
        tr +='<td class="actions">';
        tr +='<a href="#" class="btn"><i class="glyphicon-conversation"></i>';
        tr +='</a><a href="#" class="btn"><i class="glyphicon-remove-2"></i>';
        tr +='</a><a href="#" class="btn"><i class="glyphicon-right-arrow"></i></a>';
        tr +='<input type="hidden" name="product_attribute[][value]" id="product_attribute_value " value="'+gambar+'"/>';
        tr +='<input type="hidden" name="product_attribute['+new_index+'][name]" id="product_attribute_'+new_index+'_name" value="'+name+'"/>';
        tr +='<input type="hidden" name="product_attribute['+new_index+'][description]" id="product_attribute_'+new_index+'_description" value="'+description+'"/>';
        tr +='<input type="hidden" name="product_attribute['+new_index+'][type]" id="product_attribute_'+new_index+'_type" value="image"/>';
        tr +='</td>';
      tr +='</tr>';
    $("#tbody_images").append(tr);
  });

  $("#addNewDetail").click(function(){
    // var data = $("#upload_images").serialize();
    $(".close").trigger("click");
    var name = $("#frm-detail_name").val();
    var description = $("#frm-detail_description").val();
    var value = $("#frm-detail_value").val();
    var new_index = parseInt($("#tbody_detail>tr").length);
    $("#index_image").val(new_index);

    var tr = '<tr id="tr_detail'+new_index+'">';
        tr +='<td>'+name+'</td>';
        tr +='<td>'+value+'</td>';
        tr +='<td>'+description+'</td>';
        tr +='<td class="actions">';
        tr +='<a href="#" class="btn"><i class="glyphicon-conversation"></i>';
        tr +='</a><a href="#" class="btn"><i class="glyphicon-remove-2"></i>';
        tr +='</a><a href="#" class="btn"><i class="glyphicon-right-arrow"></i></a>';
        
        tr +='<input type="hidden" name="product_attribute_text['+new_index+'][name]" id="product_attribute_text_'+new_index+'_name" value="'+name+'"/>';
        tr +='<input type="hidden" name="product_attribute_text['+new_index+'][value]" id="product_attribute_text_'+new_index+'_value" value="'+value+'"/>';
        tr +='<input type="hidden" name="product_attribute_text['+new_index+'][description]" id="product_attribute_text_'+new_index+'_description" value="'+description+'"/>';
        tr +='<input type="hidden" name="product_attribute_text['+new_index+'][type]" id="product_attribute_text_'+new_index+'_type " value="text"/>';
        tr +='</td>';
      tr +='</tr>';
    $("#tbody_detail").append(tr);
  });

});

function ShowNotifyMessage(messages){
  html = '<div class="alert"><button type="button" class="close" data-dismiss="alert">&times;</button>';
  html += messages;
  html +='</div>';
  $('#messages').html(html);
}

function cek_hpp(){
  var hpp = $("#product_harga_beli").val();
  $(".expenses_checkbox").each(function(d){
    var check = $(this).is(":checked");
    if(check){
      hpp = parseInt($("#expenses_value_"+$(this).val()).val()) + parseInt(hpp);
    }
  });
  persentase = (parseInt($("#persentase").val()) / 100) * hpp ;
  hpp = hpp + persentase;
  $("#product_harga").val(hpp);
  // $("#harga_jual").html(hpp);
  $("#harga_jual").autoNumeric("init");
  $("#harga_jual").autoNumeric('set',hpp); 
  $("#save_products").show();
}
</script>
<link href="http://localhost/7foundation/project/properti/assets/grocery_crud/themes/flexigrid/css/flexigrid.css" rel="stylesheet" type="text/css">

