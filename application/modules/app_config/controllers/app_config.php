<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_config extends Backend_Controller {
  function __construct(){
      parent::__construct();
  }

  function index(){
    $crud = new grocery_CRUD();
    $crud->set_table('config');
    $crud->set_subject('config');
    $data["content"]["title_content"] = "Konfiguration";
    // $crud->set_relation('province_id','provinces','nama');
    // $crud->callback_add_field('long',array($this,'get_map'));
    $data["content"]["groc_view"] = $crud->render();
    $this->layouts->render($data);
  }

  public function addCustomConfig(){
    $data["content"]["title_content"] = "Properti";

    $data["content_path"] = "app_config/addCustomConfig";
    $data["content"] = "Properti";
    $this->layouts->render($data);
  }

  public function get_status_kerjasama(){
    $status = $this->input->post("status");
    $html = '<option value="">-Tidak Tersedia-</option>';
    if(($status != "cabang") || ($status!="pusat") || ($status!="")){
      if($status =="subcabang"){
        $this->db->where(array("status_kerjasama"=> "cabang"));
        
      }elseif($status =="perwakilan"){
        $this->db->where(array("status_kerjasama"=> "cabang"));
        $this->db->or_where(array("status_kerjasama"=> "subcabang"));

      }else{
        $this->db->where(array("status_kerjasama"=> "pusat"));
        
      }
      $data = $this->db->get("config")->result();  
      foreach ($data as $key => $value) {
        $html .='<option value="'.$value->id.'">'.$value->nama.'('.$value->alamat.')</option>';
      }
    }
    echo $html;
  }

  public function create_custom_config(){
    $params = $this->input->post();
    if($this->db->insert("config",$params["config"])){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    redirect("app_config");
  }

  public function editCustomConfig(){
    $id=$this->uri->segment(4);
    $data["content"]["title_content"] = "Properti";
    $data["content_path"] = "app_config/editCustomConfig";
    $data["content"]["data"] = $this->db->get_where("config",array("id" => $id))->row();
    $this->layouts->render($data);
  }

  public function update_custom_config(){
    $params = $this->input->post();
    $this->db->where("id",$params["config"]["id"]);
    if($this->db->update("config",$params["config"])){
      $this->session->set_flashdata('message', 'Data berhasil disimpan');  
    }else{
      $this->session->set_flashdata('message', 'Terdapat Kesalahan dalam penginputan, Silahkan Periksa Kembali!');
    }
    redirect("app_config");
  }

  public function get_map(){
    return $this->load->view("app_config/get_map");
  }
}
?>