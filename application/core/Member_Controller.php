<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Member_Controller extends MX_Controller{
  
  function __construct()
  {
     parent::__construct();
     $this->layouts->themes = $this->config->item("frontend_layout");
     $this->layouts->folder = $this->config->item("folder");
     $this->layouts->assets = $this->config->item("frontend_assets");
     $this->layouts->layout = $this->config->item("layout");
     $this->layouts->folder_themes = $this->config->item("frontend_themes");
     $widget = array(
        "footer" => "themes"."/".$this->config->item("frontend_layout")."/partials/footer_widget",
        "header" => "themes"."/".$this->config->item("frontend_layout")."/partials/header_widget",
        "header_menu" => "themes"."/".$this->config->item("frontend_layout")."/partials/header_menu_widget",
        "sidepage_employee" => "themes"."/".$this->config->item("frontend_layout")."/partials/sidepage_employee_widget",
        "sidepage_populerproperti" => "themes"."/".$this->config->item("frontend_layout")."/partials/sidepage_populerproperti_widget",
        "sidepage_newsletter" => "themes"."/".$this->config->item("frontend_layout")."/partials/sidepage_newsletter_widget",
        "sidepage_search" => "themes"."/".$this->config->item("frontend_layout")."/partials/sidepage_search_widget",
        "signin_content" => "themes"."/".$this->config->item("frontend_layout")."/partials/signin_content_widget",
        "aktifasi_content" => "themes"."/".$this->config->item("frontend_layout")."/partials/aktifasi_content_widget"
        );
     $this->layouts->get_widget($widget);
     $this->load->helper("products/products");
     $this->load->helper("employees/employees");
     $this->load->helper("app_menus/menu");
     $this->load->model("pages/page");
     $this->load->model("categories/category_model");
     $this->load->model("products/attribute_product");
     $this->load->model("customers/customer");

  }

  public function encode($pass){
    $salt=$this->config->item('encryption_key');
     if (!empty($salt) || !empty($pass) ){
          $data = md5($pass."".$salt);
      }
      return $data;
  }

}


