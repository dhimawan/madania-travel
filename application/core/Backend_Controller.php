<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Backend_Controller extends MX_Controller{
  
  function __construct()
  {
    parent::__construct();
    $this->layouts->themes = $this->config->item("backend_layout");
    $this->layouts->folder = $this->config->item("folder");
    $this->layouts->assets = $this->config->item("backend_assets");
    $this->layouts->layout = $this->config->item("layout");
    $this->load->library('grocery_CRUD');
    $this->load->library('ajax_grocery_CRUD');
    $this->load->helper("app_menus/menu");
    $widget = array(
      "left_menu" => "themes/backend/partials/sidebar_left_widget",
      "header_menu" => "themes/backend/partials/header_widget",
      "footer_menu" => "themes/backend/partials/footer_widget"
      );
    $this->layouts->get_widget($widget);
    $this->load->library('grocery_CRUD'); 
    $this->load->library('session');
    $this->load->model("app_categories/app_category");
    $this->load->model("app_menus/app_menu");
    $this->load->model("features/feature");
    $this->load->model('groups/group','group_model');
    
    $this->load->model('users/user','user_model');
    $this->load->helper("backend");

     $this->check_login();
     // Jangan dihapus by Danzztakezo@gmail.com
     // $this->get_assosiation_class();
     // $this->get_assosiation_menu();
     $this->run_migrate();

  }

  private function get_assosiation_class(){
    $router =& load_class('Router', 'core');
    $this_ci =& get_instance();
    $method_name = $router->fetch_method();
    $class_name = $router->fetch_class();
    $second_method_name = $this->uri->segment(3);
    $data[] = $router->fetch_class();
    $data[] = $router->fetch_method();
    if(!empty($second_method_name)){
      $data[] = $second_method_name;
    }
    $key = implode("_", $data);
    $name = implode(" ", $data);
    $feature = $this_ci->db->get_where("features",array("controller_name"=>$class_name,"method_name" => $method_name,"second_method_name"=> $second_method_name))->row();
    if(count($feature) == 0){
      $insert = array("name" => $name , "key" => $key , "description" => $name ,"controller_name" => $class_name , "method_name" => $method_name,"second_method_name" => $second_method_name,"created_at" => date("Y-m-d H:i:s"));
      $this->db->insert("features",$insert);
    }else{

    }
    return true;
  }

  private function get_assosiation_menu(){
    $router =& load_class('Router', 'core');
    // $this_ci =& get_instance();
    $method_name = $router->fetch_method();
    $class_name = $router->fetch_class();
    $second_method_name = $this->uri->segment(3);
    $data[] = $router->fetch_class();
    $data[] = $router->fetch_method();
    if(!empty($second_method_name)){
      $data[] = $second_method_name;
    }
    $key = implode("_", $data);
    $name = implode(" ", $data);
    $menus = $this->db->get_where("app_menus",array("name"=>$key))->row();
    if(count($menus)==0){
      $feature = $this->db->get_where("features",array("key"=>$key))->row();
      $insert = array("name" => $key,"description" => $class_name." ".$key,"feature_id" => $feature->id,"app_category_id" => 3);
      $this->db->insert("app_menus",$insert);

    }

  }

  private function authorizer(){
    $logged = $this->session->userdata('user_login');
    $data = $this->user_model->find_entity_by("email",$logged);
    $roles = $this->config->item('roles');
    $controller_active = $this->uri->segment(1);
    $group = $this->cek_group($data->group_id);
    if(empty($group)){
        $this->session->set_flashdata('error', 'anda tidak memiliki akses Kesistem');
        redirect("login_backend");
    }else{
      // if(array_key_exists($group,$roles)){
      //   if(in_array($controller_active,array_values($roles[$group]))){

      //   }else{
      //     $this->session->set_flashdata('error', 'Anda Tidak memiliki akses ke dalam sistem');
      //     redirect("login_backend");
      //   }
      // }elseif($group=="superadmin"){

      // }else{
      //   $this->session->set_flashdata('error', 'acount group anda belum terdaftar disistem');
      //   redirect("login_backend");
      // }
    }
  }

  // public function authority_menu($controller_name){
  //   echo "$controller_name";
  // }

  private function cek_group($group_id){
    
    return $this->group_model->find_entity_by_id($group_id)->nama;
  }

  private function cek_email($email){
    if(!empty($email)){
      $where['email']=$this->encode($email);
      $rows=$this->user_model->find($where);
      if(count($rows)>0){
        $data=true;
      }else{
        $data=false;
      }
    }else{
      $data=false;
    }
    return $data;
  }

  private function get_group($email){
    // if(!empty($email)){
      $where['email']=$email;
      $rows=$this->user_model->find($where);
      // if(count($rows)>0){
      //   $data=true;
      // }else{
      //   $data=false;
      // }
    // }else{
    //   $data=false;
    // }
    return $rows[0]->group_id;
  }

  private function check_login(){
    $logged=$this->session->userdata('user_login');
     $segment=$this->uri->segment(2);
     if(empty($logged) && ($this->cek_email($logged))==false){
        $this->session->set_flashdata('error', 'Anda Tidak Memiliki Akses / Silakan Login Terlebih Dahulu');
      redirect("login_backend");
     }else{
       // $this->template
       //      ->set_partial("top_menu","backend/partials/_top_menu")
       //      ->set_partial("left_menu","backend/partials/_left_menu");
       // $this->menu();
       $this->authorizer();
       // $this->set_menu();
     }
  }

  private function set_menu(){
    // echo $this->session->userdata('menus');
    // die;
    if(count($this->session->userdata('menus')) == 1){
      if($this->get_group($this->session->userdata('user_login')) == 99){
      $data = $this->app_menu->find(array());
      foreach ($data as $key => $value) {
        $feature = $this->feature->find_entity_by_id($value->feature_id);
        $menus["menus"][] =array("title"=>$value->name,"controller_name" => $feature->controller_name,"method_name" => $feature->method_name,"app_category_id" =>$value->app_category_id );
      }
      $features = $this->feature->find(array());
      foreach ($features as $val){
        # code...
      }
    }else{
      
    }
    // echo "<pre>";
    // $_session["menus"] = $menus;
    // // print_r($menus);die;
    // $this->session->set_userdata($menus);
    // print_r($this->session->userdata('menus'));
    }

  }

  public function encode($pass){
    $salt=$this->config->item('encryption_key');
     if (!empty($salt) ||!empty($pass) ){
          $data = md5($pass."".$salt);
      }
      return $data;
  }

  private function run_migrate(){
    if(!$this->migration->current())
    {
      show_error($this->migration->error_string());
    }else{
      // $this->session->set_flashdata('message', 'Migration Berhasil dilakukan'); 
      // redirect("dashboard");
    }
  }

  // public function encode_active($email){
  //   $salt=$this->config->item('encryption_key');
  //    if (!empty($salt) ||!empty($email) ){
  //         $data = base64_encode($email."|pisah|".$salt);
  //     }
  //     return $data;
  // }

  // private function restrict(){
  //   redirect("backend/login_admin");
  // }


  // public function menu(){
  //  $rows["partials_data"] = $this->check_notification();
  //  //echo "<pre>"; print_r($rows);
  //  $this->template
  //           ->set_partial("top_menu","backend/partials/_top_menu",$rows)
  //           ->set_partial("left_menu","backend/partials/_left_menu",$rows)
  //           ->set_partial("error_message","backend/partials/_error_message")
  //       ;

  // }

  // public function check_notification(){
  //   //echo"<h1> masuk sini </h1>";
  //   $data["nav_val"] = $this->uri->segment(1);
  //   $data["nav_val3"] = $this->uri->segment(3);
  //   $status_pembayaran = $this->sell_model->find(array("status_pembayaran" => "blom"));
  //   $data["count_sp"]=count($status_pembayaran);
  //   $status_retur = $this->db->query("SELECT * FROM `detail_retur` WHERE `status` IN ('baru', 'cofirm') GROUP BY `retur_pembelian_id` ");
  //   $data["count_retur"]=count($status_retur);
  //   $data["logged"] = $this->session->userdata('user_login');
  //   $data["user"] = $this->user->find_entity_by("email",$data["logged"]);
  //   $data["group"] = $this->group_model->find_entity_by_id($data["user"]->group_id)->nama;
  //   $data["roles"] = $this->config->item("roles");
  //  return $data;
  // }
}
