<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layouts{
  
  public $folder_layout='themes';
  public $themes='default';
  public $assets='default';
  private $controller_name = '';
  private $method_name = '';
  public $layout = 'login';
  public $widget = array();
  // public $widget_name = "";

  public function __construct(){
    $CI =& get_instance();
    $CI->load->helper("url");
    // echo $this->layout;
  }

  
  /* ############# PARAMATER DATA TO SEND ON THE LAYOUT 
  $data or $parameter= array(
      "content_path" => "folder/view_name",
      "title" => "title page content",
      "content"  => array(
                    "title_content" => "add product number",
                    "data" => "aaaa",

                  )
              some array data from database or else
  )
  */
  public function render($data){
    $CI =& get_instance();
    $CI->config->load('layout'); 
    $parameter =  $CI->layouts->configuration_layout($data);
    
    $themes = $this->folder_layout."/".$this->themes."/".$this->layout;
    $CI->load->view($themes,$parameter);
  }

  public function configuration_layout($parameter){
    $CI =& get_instance();
    $parameter["assets_path"] = $CI->config->item("base_url")."assets/".$this->folder_layout."/".$this->themes;
    // $parameter["assets_path"] =  $CI->config->item("folder_assets_layout");
    if(!isset($parameter["content_path"])){
      $this->get_controller_name($CI->router->class);
      $this->get_method_name($CI->router->method);
      $parameter["content_path"] = $this->controller_name."/".$this->method_name;
    }
    // if(!empty($this->widget)){
      $parameter["widget"] = $this->widget;
      // print_r($this->widget);
    // }
    $parameter["assets_css_path"] = $parameter["assets_path"]."/css/";
    $parameter["assets_js_path"] = $parameter["assets_path"]."/js/";
    $parameter["assets_img_path"] = $parameter["assets_path"]."/images/";
    // $parameter["widget"] = 
    return $parameter;
  }


  public function themes($themes_layout){
    $CI->themes = $themes_layout;
  }

  public function folder($folder){
    $CI->folder_layout = $folder;
  }

  public function assets($assets_folder){
    $CI->assets = $assets_folder;
  }

  public function get_controller_name($controller_name){
    $this->controller_name = $controller_name;
  }

  public function get_method_name($method_name){
    $this->method_name = $method_name;
  }

  public function get_layout($layout){
    $CI->layout = $layout;
  }

  public function get_widget($widget){
    $this->widget =$widget; 
  }

}