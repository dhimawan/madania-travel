<?php

Class Select_area{

  public function getArea($name ,$select ,$selected){
    $CI =& get_instance();
    $attribute = 'class="chosen-select" ';
    if($select =="province"){
      $options = $CI->province->selectOption();
      $attribute .= ' onChange="getCities($(this).val());"';
    }

    return form_dropdown($name, $options, $selected,$attribute);
  }
}