<?php

function select_area($tipe,$select_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $options = array();
  $attributes = "class='chosen-select' ";
  $html = "";
  if($tipe == "province"){
    $query = $ci->db->get("provinces")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    $attributes .= ' id="sel_'.$tipe.'" onChange="getCities($(this).val(),\'sel_city\')"';
  }elseif($tipe == "city"){
    $query = $ci->db->get("cities")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" onChange="getDistricts($(this).val(),\'sel_district\')"';

  }elseif($tipe == "district"){
    $query = $ci->db->get("districts")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" onChange="getVillages($(this).val(),\'sel_village\')"';
  }elseif($tipe == "village"){
    $query = $ci->db->get("villages")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" "';
  }
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function f_select_area($tipe,$select_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $options = array("00" => "--PILIH--");
  $attributes = "class='chosen-select' ";
  $html = "";
  if($tipe == "province"){
    $query = $ci->db->get("provinces")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    $attributes .= ' id="sel_'.$tipe.'" onChange="getCities($(this).val(),\'sel_city\')"';
  }elseif($tipe == "city"){
    $query = $ci->db->get("cities")->result();
    foreach($query as $key => $val){
      // $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" onChange="getDistricts($(this).val(),\'sel_district\')"';

  }elseif($tipe == "district"){
    $query = $ci->db->get("districts")->result();
    foreach($query as $key => $val){
      // $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" onChange="getVillages($(this).val(),\'sel_village\')"';
  }elseif($tipe == "village"){
    $query = $ci->db->get("villages")->result();
    foreach($query as $key => $val){
      // $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" "';
  }
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function dropdown_bank($tipe,$select_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $attributes = "class='chosen-select' ";
  $query = $ci->db->get("banks")->result();
    $options[] = "";
    foreach($query as $key => $val){
      $options[$val->id] = $val->name;
    }
    $attributes .= ' id="sel_'.$tipe.'" "';
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function dropdown_religions($tipe,$select_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $attributes = "class='chosen-select' ";
  $query = $ci->db->get("religions")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->name;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" "';
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function dropdown_categories($tipe,$select_name,$class_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $attributes = " class='".$class_name."'  ";
  $query = $ci->db->get("categories")->result();
    foreach($query as $key => $val){
      $options[$val->id] = $val->nama;
    }
    // $attributes .= ' id="sel_'.$tipe.'"';
    $attributes .= ' id="sel_'.$tipe.'" ';
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function f_dropdown_categories($tipe,$select_name,$class_name,$id_name,$selected){
  $ci =& get_instance();
  $ci->load->database();
  $attributes = " class='".$class_name."'  ";
  $options = array("00" => "--PILIH--");
  
  $query = $ci->db->get("categories")->result();
  foreach($query as $key => $val){
    $options[$val->id] = $val->nama;
  }
  $attributes .= " id='$id_name' ";
  return form_dropdown($select_name, $options, $selected,$attributes);
}

function table_requirement($class_table,$id_name,$name,$product_id = ""){
  $ci =& get_instance();
  $ci->load->database();
  $ci->load->library('table');
  $tmpl = array ( 'table_open'  => '<table class="'.$class_table.'">' );

  $ci->table->set_template($tmpl); 
  $query = $ci->db->query("SELECT * FROM requirements");
  $result = $query->result();
  if($query->num_rows() > 0){
    foreach($result as $row){
      $checked = FALSE;
      if(!empty($product_id)){
        $rp = $ci->db->query("SELECT * FROM product_requirements where product_id ='".$product_id."' and requirement_id = $row->id ")->row();
        $checked = (!empty($rp) ? TRUE : FALSE ) ;
      }
      

      $checkbox = array(
        'name'        => $name,
        'id'          => $id_name,
        'value'       =>  $row->id,
        'checked'     => $checked,
        'style'       => 'margin:10px',
        );
      $ci->table->add_row(form_checkbox($checkbox), $row->name, $row->description );
    }
  }else{
     $ci->table->add_row('No results found','','','');
  }
  $ci->table->set_heading('#'. form_checkbox('checkall', 'All', TRUE), 'Nama', 'Deskripsi');
// $ci->table->add_row('Fred', '<strong>Blue</strong>', 'Small');
  return  $ci->table->generate(); 
}

function table_requirement_product($class_table,$id_name,$name,$product_id ){
  $ci =& get_instance();
  $ci->load->database();
  $ci->load->library('table');
  $tmpl = array ( 'table_open'  => '<table class="'.$class_table.'">' );

  $ci->table->set_template($tmpl); 
  $query = $ci->db->query("SELECT product_requirements.id as product_requirement_id,requirements.name,requirements.description FROM product_requirements 
    join requirements on requirements.id = product_requirements.requirement_id 
    where product_id ='".$product_id."'");
  $result = $query->result();
  if($query->num_rows() > 0){
    foreach($result as $row){
      $checked = FALSE;
      if(!empty($product_id)){
        $rp = $ci->db->query("SELECT * FROM  product_customer_requirments where product_requirment_id ='".$row->product_requirement_id."'")->row();
        $checked = (!empty($rp) ? TRUE : FALSE ) ;
      }

      $checkbox = array(
        'name'        => $name,
        'id'          => $id_name,
        'value'       =>  $row->product_requirement_id,
        'checked'     => $checked,
        'style'       => 'margin:10px',
        );
      $form = ($checked==true ? "<center>sudah</center>" : form_checkbox($checkbox) );
      $ci->table->add_row( $form  , $row->name, $row->description );
    }
  }else{
     $ci->table->add_row('No results found','','','');
  }
  $ci->table->set_heading('#'. form_checkbox('checkall', 'All', TRUE), 'Nama', 'Deskripsi');
// $ci->table->add_row('Fred', '<strong>Blue</strong>', 'Small');
  return  $ci->table->generate(); 
}