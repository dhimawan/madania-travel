<?php 

  function generated_frontend_menu(){
    $this_ci =& get_instance();
    $data = $this_ci->db->get_where("pages",array("parent_id" => "0"))->result();
    $html = "";
    foreach ($data as $key => $value) {
      $html .= '<li class="dropdown">';
      $html .= '<a href="'.base_url("frontends/pages/$value->url").'">'.$value->intitle.'</a>';
      $html .= genereted_frontend_childmenu($value->id);
      $html .= '</li>';
    }
    return $html;
  }

  function generated_frontend_menu_without_child(){
    $this_ci =& get_instance();
    $data = $this_ci->db->get_where("pages",array("parent_id" => "0","is_menu" => true))->result();
    $html = "";
    $html .= '<li class="dropdown">';
    $html .= '<a href="'.base_url("/").'">Home</a>';
    $html .= '</li>'; 
    foreach ($data as $key => $value) {
      $html .= '<li class="dropdown">';
      $html .= '<a href="'. site_url("$value->url").'">'.$value->intitle.'</a>';
      $html .= '</li>';
    }
    $html .= '<li class="dropdown">';
    $html .= '<a href="'.site_url("kontak_kami").'">Kontak Kami</a>';
    $html .= '</li>'; 
    return $html;
  }

  function generated_frontend_mobile_menu(){
    $this_ci =& get_instance();
    $data = $this_ci->db->get_where("pages",array("parent_id" => "0","is_menu" => true))->result();
    $html = "";
    $html .= '<select class="selectmenu">'; 
    $html .= '<option value="#">--Pilih Menu --</option>';
    $html .= '<option value="'.site_url("/").'">Home</option>';

    foreach ($data as $key => $value) {
      $html .= '<option value="'.site_url("$value->url").'">'.$value->intitle.'</option>';

    }
    $html .= '<li class="dropdown">';
    $html .= '<option value="'.site_url("kontak_kami").'">Kontak Kami</option>';
    $html .= '</select>'; 
    return $html;
  }

  function generated_frontend_footer_without_child(){
    $this_ci =& get_instance();
    $data = $this_ci->db->get_where("pages",array("parent_id" => "0","is_menu" => true))->result();
    $html = "";
    $html .= '<li class="dropdown">';
    $html .= '<a href="'.base_url("/").'">Home</a>';
    $html .= '</li>'; 
    foreach ($data as $key => $value) {
      $html .= '<li class="dropdown">';
      $html .= '<a href="'.base_url("$value->url").'">'.$value->intitle.'</a>';
      $html .= '</li>';
    }
    $html .= '<li class="dropdown">';
    $html .= '<a href="'.base_url("kontak_kami").'">Kontak Kami</a>';
    $html .= '</li>'; 
    return $html;
  }

  function genereted_frontend_childmenu($id){
    $this_ci =& get_instance();
    $html = "";
    if($id > 0){
      $data = $this_ci->db->get_where("pages",array("parent_id" => $id))->result();
      if(count($data)> 0 ){
        $html .= '<ul class="dropdown-menu bold" >';
        foreach ($data as $key => $value) {
          $html .= '<li><a href="'.base_url("frontends/pages/$value->url").'">'.$value->intitle.'</a></li>';
        }
        $html .= '</ul>';
      }
    }
    return $html;
  }

  function slogan_company(){
    $this_ci =& get_instance();
    $data = $this_ci->db->get("config")->row();
    return $data->slogan;
  }

  function slideshow_view(){
    $this_ci =& get_instance();
    $this_ci->load->helper("text");
    $slide = $this_ci->page->get_pages_with_limit("slideshow",5);
    $html = "";
    foreach ($slide as $key => $value) {
      $html .='<li>
          <img src="'.base_url("assets/uploads/pages/".$value->val_image).'" data-original="'.base_url("assets/uploads/pages/".$value->val_image).'" alt="'.$value->name.'" class="lazy"/>
          <div class="absolute-text"> 
            <h3>'.$value->name.'</h3>
            '.word_limiter($value->description,30).'
            <div class="clearfix">
              <ul>
                <li><a class="contact-us" href="'.base_url("kontak_kami").'">Hubungi Kami </a></li>
                <li><a class="search-site" href="#">Detail</a></li>
              </ul>
            </div>
          </div>
        </li>';
    }
    return $html;
  }

  function set_metatags(){
    $router =& load_class('Router', 'core');
    $this_ci =& get_instance();
    $class_name = $router->fetch_class();
    $method_name = $router->fetch_method();
    if($class_name == "frontends"){
     $meta = get_metadata($method_name);
    }
    echo $meta; 
}

  function get_metadata($method){
    $this_ci =& get_instance();
    $this_ci->load->model("pages/page");
    if($method=="contact_us"){
      $data = $this_ci->page->get_seo("contact_us");
    }elseif($method == "index"){
      $data = $this_ci->page->get_seo("home");
    }elseif($method == "add_registration"){
      $data = $this_ci->page->get_seo("home");
    }else{
      $data = $this_ci->page->get_seo($method);
    }
    $meta = array(
        array('name' => 'robots', 'content' => 'no-cache'),
        array('name' => 'description', 'content' => $data->seo_description),
        array('name' => 'keywords', 'content' => $data->seo_keywords),
        array('name' => 'robots', 'content' => 'index'),
        array('name' => 'rating', 'content' => 'general'),
        array('name' => 'copyright', 'content' => date("Y")." madaniatravel"),
        array('name' => 'distribution', 'content' => "global"),
        array('name' => 'author', 'content' => "sevenfoundation"),
        array('name' => 'expires', 'content' => "never"),
        array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
      );

    return meta($meta);
  }

  function set_title_page(){
    $router =& load_class('Router', 'core');
    $this_ci =& get_instance();
    $this_ci->load->model("pages/page");
    $class_name = $router->fetch_class();
    $method_name = $router->fetch_method();
    if($class_name == "frontends"){
      if($method_name=="contact_us"){
        $pages = $this_ci->page->get_seo("contact_us");
      }elseif($method_name == "index"){
        $pages = $this_ci->page->get_seo("home");
      }elseif($method == "add_registration"){
        $data = $this_ci->page->get_seo("home");
      }else{
        $data = $this_ci->page->get_seo($method);
      }
    }
    echo "<title>".$pages->intitle."</title>"; 
}

  function _generate_xhtml_meta_tags($tags,$keywords,$content,$robots){
      $output = "\n";
      if(!empty($tags))
      {
          foreach($tags as $name=>$content)
          {
              $output .= '< meta name="'.$name.'" content="'.$content.'" /&gt;'."\n";
          }
      }
      
      if(!empty($this->keywords))
          $output .= '< meta name="keywords" content="'.implode(',', $keywords).'" />'."\n";
      
      if(!empty($this->robots))
          $output .= '< meta name="robots" content="'.implode(',', $robots).'" />'."\n";
      
      return $output;
  }

  function article_link($data,$title){
    $year = date("Y",strtotime($data->updated_at));
    $month = date("m",strtotime($data->updated_at));
    return '<a href="'.site_url("news/".$year."/".$month."/".$data->url).'">'.$title.'</a>';
  }

  function news_url($data,$title){
    $year = date("Y",strtotime($data->updated_at));
    $month = date("m",strtotime($data->updated_at));
    return "".site_url("news/".$year."/".$month."/".$data->url)."";
  }

  function generate_side_news(){
    $this_ci =& get_instance();
    $data["category"] = $this_ci->db->get("news_categories")->result();
    $data["latest_post"] = $this_ci->db->order_by("updated_at","desc")->limit(5)->get("news")->result();

    $this_ci->load->view("frontends/".$this_ci->config->item("frontend_layout")."/partials/_generate_side_news",$data);
  }

  function generate_side_requirements(){
    $this_ci =& get_instance();
    $this_ci->load->model("requirements/requirement");
    $data["requirements"] = $this_ci->requirement->get_grouptag_array();
    $this_ci->load->view("frontends/".$this_ci->config->item("frontend_layout")."/partials/_generate_side_requirements",$data);
  }
  
  function generated_home_video(){
    $this_ci =& get_instance();
    $data= $this_ci->db->where("status","active")->order_by("updated_at","desc")->limit(1)->get("videos")->row();
    return '<iframe width="465" height="262" src="'.$data->url.'?autoplay='.$data->autoplay.'" frameborder="0" allowfullscreen ></iframe>';
  }

  function get_price_packet($id){
    $this_ci =& get_instance();
    $data= $this_ci->db->where(array("status"=>"active","packet_id"=>$id))->order_by("updated_at","desc")->limit(1)->get("packet_prices")->row();
    $price = array( $data->min_single_price, $data->max_single_price, $data->min_double_price, $data->max_double_price, $data->min_triple_price, $data->max_triple_price, $data->min_quad_price, $data->max_quad_price );
    $length = count($price);
    asort($price);
    echo $price[0]." s/d ".$price[$length-1]." USD";
  }

  function generated_our_advertasing(){
    $this_ci =& get_instance();
    $page= $this_ci->db->where(array("name"=>"iklan"))->limit("1")->get("pages")->row();
    $data = $this_ci->db->get_where("attribute_pages",array("page_id" => $page->id))->result();
    $html = "";
    foreach ($data as $key => $value) {
      $html .='<div class="no-border-left footer-inline">
                <a class="clearfix" href="">
                  <img alt="'.$value->name.'" src="'.base_url("assets/uploads/pages/".$value->value).'" class="">
                  <span>
                    <span class="orange">'.$value->name.'</span>
                  </span>
                </a>
              </div>';
    }
    return $html;

  }

  function current_company(){
    $this_ci =& get_instance();
    $data = $this_ci->db->where(array("status_kerjasama"=>""))->limit("1")->get("config")->row();
    return $data;
  }

  function get_company_name(){
    $this_ci =& get_instance();
    $data = current_company();
    return $data->nama;
  }

  function get_company_address(){
    $this_ci =& get_instance();
    $data = current_company();
    return $data->alamat;
  }

  function get_company_telp(){
    $this_ci =& get_instance();
    $data = current_company();
    return $data->telp;
  }

  function get_company_email(){
    $this_ci =& get_instance();
    $data = current_company();
    return $data->email;
  }

  function price_list($attribute_id){
    $this_ci =& get_instance();
    $data = $this_ci->db
    ->join("packet_prices","packet_prices.packet_id = packets.id and packet_prices.status='active'")
    ->where("packets.attribute_page_id",$attribute_id)
    ->get("packets")
    ->result();
    $prices = array();
    foreach ($data as $key => $value) {
      $prices[$value->name] =array($value->min_single_price , $value->max_single_price, $value->min_double_price , $value->min_triple_price , $value->max_triple_price , $value->max_quad_price);
    }
    // print_r($prices);die;
   return $prices;
  }

?>
