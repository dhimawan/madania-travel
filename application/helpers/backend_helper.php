<?php 
  function headermenu_backend(){
    $ci =& get_instance();
    $ci->load->database();
    $data = $ci->db->get("app_categories")->result();
    $html = "";

    // $ci->user_model->access_top_header_menu();
    // $features = $ci->user_model->get_features($ci->session->userdata("email"));
    $ses_features = $ci->session->userdata("features");
    if(count($data) > 0){
      foreach ($data as $key => $value) {
        if($ci->session->userdata("group")==99){
          $access = TRUE;
        }else{
          $child = $ci->app_menu->get_menu_categories($value->id);
          // echo "<br/>";
          $caccses = array();
          if(count($child)>0)
          foreach ($child as $cvalue) {
            if(count($ses_features)>0){
              if(in_array($cvalue, $ses_features, true)){
                $caccses = TRUE;
              }
            } 
          }
          $access = (count($caccses)>0 ? TRUE : FALSE);
        }

        if($access){
          $html .="<li>";
          $html .="<a href='".site_url($value->controller_name."/".$value->method_name)."'> <i class='icon-asterisk icon-white'></i> ".$value->title." </a>";
          $html .="</li>";
        }
      }
    }
    return $html;
  }

  function leftmenu_backend(){
    $router =& load_class('Router', 'core');
    $this_ci =& get_instance();
    $method_name = $router->fetch_method();
    $class_name = $router->fetch_class();
    $html = "";
    $f = $this_ci->db->get_where("features",array("key" => $class_name."_".$method_name))->row();
    $ses_features = $this_ci->session->userdata("features");
    if(count($f) > 0){
      $cat = $this_ci->db->get_where("app_menus",array("feature_id" => $f->id))->row();
      if(empty($cat)){
        $f = $this_ci->db->get_where("features",array("controller_name" => $class_name))->row();
        $cat = $this_ci->db->get_where("app_menus",array("feature_id" => $f->id))->row();

      }
      $menus = $this_ci->db->get_where("app_menus",array("app_category_id" => $cat->app_category_id))->result();

      if(count($menus) > 0){
        foreach ($menus as $key => $value) {
          $feature = $this_ci->db->get_where("features",array("id" => $value->feature_id))->row();
          if(in_array($feature->id, $ses_features, true)){
            $html .= '<li class="'.($value->feature_id == $f->id ? 'active' : '' ).'">';
            $html .= '<a href="'.base_url($feature->controller_name."/".$feature->method_name).'"><span class="label label-inverse"><i class="icon-home icon-white"></i></span> '.$value->name.' </a>';
            $html .= '</li>';
          }
        }
      }
    }
    if($class_name == "pages" and $method_name == "packet_list"){

    }else if($class_name == "pages" and $method_name != "packet_list"){
      $pages = $this_ci->db->get_where("pages",array("parent_id" => 0,"is_admin" => true))->result();
      $html .= '<li class="">';
      $html .= '<a href="'.base_url("pages/index/").'"><span class="label label-inverse"><i class="icon-home icon-white"></i></span> Pages  </a>';
      $html .= '</li>';
      $active = " ";
      foreach ($pages as $key => $value){
        if($method_name == "get_container"){
          if($this_ci->uri->segment(3)== $value->id){
            $active = "active";
          }else{
            $active = "";
          }
        }
        $html .= '<li class="'.$active.'">';
        $html .= '<a href="'.base_url("pages/get_container/".$value->id).'" data-target="#area_'.$value->id.'"  data-toggle="collapse"><span class="label label-inverse"><i class="icon-home icon-white"></i></span> '.$value->name.' </a>';
        $html .= get_child_leftmenu($value->id);
        $html .= '</li>';
      }
    } 

    return $html;
  }

  function get_child_leftmenu($val){
    $this_ci =& get_instance();
    $data = $this_ci->db->get_where("pages",array("parent_id" => $val))->result();
    $html = "";
    if(count($data) > 0 ){
      $html .= '<ul class="collapse in" id="area_'.$val.'">';
      foreach ($data as $key => $value) {
        $html .= '<li><a href="'.base_url("pages/get_container/".$value->id).'"><span class="label label-inverse"><i class="icon-home icon-white"></i></span> '.$value->name.' </a></li>';
      }
      $html .= '</ul>';
    }
    return $html;
  }
?>
