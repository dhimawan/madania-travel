<?php 

  function get_form($data,$wrapper_outset,$wrapper_component,$wrapper_label,$wrapper_input,$view_num){
    $ci =& get_instance();
    $ci->load->helper("form");
    $html = $wrapper_outset['open_tag'];
    foreach($data as $key => $value) {
      if(in_array($key,range($view_num["start"],$view_num["end"]))){
        $html .= $wrapper_component['open_tag'];
        $html .= $wrapper_label['open_tag']." ".$data[$key]["label_name"]." ".$wrapper_label['close_tag'];
        $html .= prepared_form($data[$key]["input_type"],$value,$wrapper_label);
        $html .= $wrapper_component['close_tag'];
      }
      
    }
    $html .= $wrapper_outset['close_tag'];
    
    return $html;
  }

  function prepared_form($input_type,$value,$wrapper_label){
    $ci =& get_instance();
    switch ($input_type) {
      case 'text':
        $html = text_input($value);
        break;
      
      default:
        # code...
        break;
    }
    return $html;
  }

   function text_input($value){
    $ci =& get_instance();
    $ci->load->helper("form");
    $data = array(
              'name'        => $value["name_input"],
              'id'          => $value["id_input"],
              'class'       => $value["class_input"],
              'value'       => $value["value_input"]
            );

  return form_input($data);
  }

  function product_requirment($product_id){
    $ci =& get_instance();
    $ci->load->database();
    $data = $ci->db
    ->join("requirements","requirements.id = product_requirements.requirement_id")
    ->where(array("product_id" => $product_id ,"status" => "active"))
    ->get("product_requirements")
    ->result();
    $html = "<dl>";
    if(count($data) > 0 ){
      foreach ($data as $key => $value){
        $html .= "<dd> - ".strip_tags($value->name)."</dd>"; 
      }
    }
    $html .= "</dl>"; 
    return $html;
  }

?>