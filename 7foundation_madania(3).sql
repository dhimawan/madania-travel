-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 22, 2013 at 03:54 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `7foundation_madania`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_categories`
--

CREATE TABLE IF NOT EXISTS `app_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `controller_name` varchar(50) NOT NULL,
  `method_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `app_categories`
--

INSERT INTO `app_categories` (`id`, `title`, `name`, `description`, `created_at`, `controller_name`, `method_name`) VALUES
(1, 'Dashboard', 'dashboard_backend', 'dashboard_backend', '2013-06-17 00:00:00', 'dashboard', 'index'),
(2, 'data master', 'data_master_backend', 'data_master_backend', '2013-06-17 00:00:00', 'religions', 'index'),
(3, 'Setting', 'setting_backend', 'setting Backend', '2013-06-17 00:00:00', 'groups', 'index'),
(5, 'Pages', 'page_backend', 'page_backend', '2013-08-06 00:00:00', 'pages', 'index'),
(6, 'berita', 'news', '', '0000-00-00 00:00:00', 'news', 'index'),
(7, 'paket', 'paket', 'daftar paket', '2013-10-01 10:12:01', 'pages', 'packet_list');

-- --------------------------------------------------------

--
-- Table structure for table `app_menus`
--

CREATE TABLE IF NOT EXISTS `app_menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `feature_id` int(11) NOT NULL,
  `app_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `app_menus`
--

INSERT INTO `app_menus` (`id`, `name`, `description`, `feature_id`, `app_category_id`) VALUES
(1, 'dashboard', 'dashboard dashboard_index', 42, 1),
(5, 'Kategori', '<p>\n	categories categories_index</p>\n', 34, 2),
(8, 'Agama', '<p>\n	religions religions_index</p>\n', 45, 2),
(11, 'Propinsi', '<p>\n	provinces provinces_index</p>\n', 53, 2),
(14, 'Kota', '<p>\n	cities cities_index</p>\n', 61, 2),
(17, 'Kecamatan', '<p>\n	districts districts_index</p>\n', 70, 2),
(18, 'Kelurahan', '<p>\n	villages villages_index</p>\n', 71, 2),
(24, 'Users', '<p>\n	users users_index</p>\n', 1, 3),
(27, 'Group', '<p>\n	groups groups_index</p>\n', 7, 3),
(30, 'Fitur', '<p>\n	features features_index</p>\n', 15, 3),
(33, 'Group Features', '<p>\n	group_features group_features_index</p>\n', 21, 3),
(36, 'Pages', '<p>\n	pages pages_index</p>\n', 28, 5),
(39, 'Menu', '<p>\n	app_menus app_menus_index</p>\n', 80, 3),
(40, 'Kategori', '<p>\n	app_categories app_categories_index</p>\n', 79, 3),
(41, 'Siswa', '<p>\n halaman Siswa</p>\n', 191, 4),
(42, 'curriculums', '<p>\n curriculums</p>\n', 195, 4),
(43, 'Tahun Ajaran', '<p>\n data tahun ajaran</p>\n', 196, 4),
(44, 'Kelas', '<p>\n classes</p>\n', 197, 4),
(45, 'Guru', '<p>\n Guru</p>\n', 198, 4),
(46, 'Daftar Cabang', '<p>app config</p>\n', 203, 3),
(47, 'berita', '<p>\n s</p>\n', 221, 6),
(48, 'daftar paket', '<p>\n daftar paket</p>\n', 243, 7),
(49, 'Persyaratan', '<p>\n persyaratan umroh dan haji</p>\n', 76, 7),
(50, 'Kategori berita', '<p>\n kategori berita</p>\n', 255, 6),
(51, 'Video', '<p>\n Video</p>\n', 257, 6),
(52, 'Automation News', '<p>\n page_autonews</p>\n', 275, 6);

-- --------------------------------------------------------

--
-- Table structure for table `attribute_pages`
--

CREATE TABLE IF NOT EXISTS `attribute_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `type` enum('image','text','link') NOT NULL,
  `page_id` int(11) NOT NULL,
  `tag` text NOT NULL,
  `user_created` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `seo_keywords` varchar(200) NOT NULL,
  `seo_description` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `attribute_pages`
--

INSERT INTO `attribute_pages` (`id`, `name`, `value`, `description`, `type`, `page_id`, `tag`, `user_created`, `created_at`, `updated_at`, `seo_keywords`, `seo_description`, `title`) VALUES
(3, 'sufatina', 'delp.jpg', '<p>asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd asdasdasdasdasd</p>', 'text', 1, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(4, 'GARUDA ', '6.png', '<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>', 'text', 1, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(6, 'FEDRAMP', 'fedramp.jpg', '<p>asdalksdjlaskhdkasjhdkasjhd aksd;alskjdl ashdjlaslasjdl asdjsadsd</p>', 'text', 1, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(11, 'jamaah keberangkatan 1', 'jamaah umroh.JPG', '<p>lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum</p>', 'text', 12, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(20, 'Umroh Reguler   ', 'umroh-reguler.jpg   ', '<p>Paket Umrah yang dilaksanakan pada bulan Ramadhan. Lain halnya dengan Umrah Reguler, Paket Ramadhan berdurasi 12 hari (pertengahan akhir) atau 24 hari (awal ramadhan). Paket 12 hari Ramadhan terbagi dalam beberapa tempat tujuan, yaitu 3 hari di Medinah, 7 hari di Mekkah, dan 2 hari perjalanan keberangkatan kepulangan. Paket 24 hari Ramadhan terbagi dalam beberapa tempat tujuan, yaitu 8 hari di Medinah, 14 hari di Mekkah, 2 hari perjalanan keberangkatan kepulangan.</p>', 'link', 15, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ' Madania Travel - Umroh Reguler   ', ' Madania Travel - Umroh Reguler , Madania semesta wisata - Umroh Reguler ,umroh reguler murah bandung,   ', 'Madania Travel - Umroh Reguler   '),
(21, 'Umroh Ramadhan', 'umrohramadan.jpg', 'Daftar Paket Umroh Ramadhan', 'text', 15, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(22, 'Islamic Tour', 'islamictour.jpg', 'Daftar Paket Islamic Tour', 'text', 15, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', ''),
(23, 'Islamic Request Tour  ', 'demoRequestHeader.png  ', 'Daftar Paket Islamic Request Tour  ', 'text', 15, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ' Daftar Paket Islamic Request Tour  ', ' Daftar Paket Islamic Request Tour  ', ' Daftar Paket Islamic Request Tour  '),
(24, 'haji khusus ', 'indo-tour.jpg  ', '<p>haji khusus</p>', 'text', 15, '', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'haji khusus ', 'haji khusus ', 'haji khusus '),
(28, 'Kisah Jamaah Non Kuota', 'artikel-kisah-jamaah-non-kuota61_a.jpg', '<p><strong>Makkah</strong>, Menjadi jamaah haji ibadah khusus yang dulu disebut dengan ONH Plus tidak selamanya nyaman dan enak. Apalagi mereka naik haji tidak berdasarkan kuota yang telah ditetapkan oleh Kementerian Agama RI.<br /><br />Mereka yang disebut dengan jamaah non kuota itu harus kucing-kucingan dengan berbagai pihak. Mereka telah membayar ongkos yang lebih mahal dari haji reguler ataupun haji khusus yang telah ditetapkan biayanya oleh pemerintah.<br /><br />Mereka diiming-imingi oleh beberapa perusahaan travel bisa berangkat ke tanah suci dengan biaya sama seperti ONH Plus. Padahal perusahaan travel tersebut tidak terdaftar masuk dalam 244 perusahaan travel Penyelenggara Ibadah Haji Khusus (PIHK) yang terdaftar di Kemenag.<br /><br />Mereka mencari calon jamaah haji bukan di Pulau Jawa, namun di daerah-daerah seperti Sumatera Utara, Jambi, Riau, Madura, Kalimantan, Sulawesi, Nusa Tenggara Barat hingga Kepulauan Maluku. Biaya yang dikeluarkan juga tidak sedikit berkisar antara Rp 70 juta - 100 juta per orang.<br /><br />Berdasarkan penulusuran detikcom, sebagian besar jamaah non kuota ini berangkat dari tanah air tidak langsung terbang menuju Jeddah, Arab Saudi. Namun ada pula rombongan yang transit lebih dulu di Singapura baru meneruskan perjalanan menuju Abu Dhabi terus ke Jeddah.<br /><br />Dari Jeddah rombongan sudah ditunggu oleh rekanan travel atau penghubung setempat. Mereka masuk Jeddah dan Mekkah biasanya satu minggu sebelum batas waktu penutupan atau closing death tanggal 20 Oktober pukul 24.00 WAS. Bahkan ada yang masuk beberapa jam menjelang batas waktu penutupan.<br /><br />Tidak banyak rombongan jamaah non kuota ini, sekitar 20-50 orang setiap rombongan. Mereka menggunakan kendaraan langsung di bawa menuju Mekkah. Bila beruntung ada yang langsung di tempatkan di hotel-hotel yang berdekatan dengan Masjid Haram. Namun ada pula yang ditempatkan di hotel yang jauh di luar Kota Mekkah di daerah perbatasan dengan Jeddah. Mereka sering menyebutnya dengan hotel transit..<br />Sumber : Onlinenews Dtk.<br /><br />Selama di Mekkah sudah ada seorang mukimin Mekkah asal Indonesia yang bertugas menjadi guide atau pemandu. Biasanya seorang guide di sesuaikan dengan asal daerah jamaah. Kalau jamaah berasal dari Makassar, yang menjadi guide kadangkala juga berasal dari Sulawesi Selatan atau Indonesia Timur.<br /><br />Bila jamaah dari NTB yang jadi guide juga orang Lombok. Bila jamaah dari Madura yang menjadi guide juga seorang mukimin dari Madura.Di tempat transit, mereka seringkali tidak mendapatkan jatah makan seperti jamaah ibadah haji khusus (ONH Plus) dengan makan ala prasmanan. Mereka ada yang harus mencari makan sendiri. Namun ada pula yang mendapatkan jatah nasi boks.<br /><br />Jamaah haji reguler yang terdaftar resmi di Kemenag mengenakan gelang tangan logam dengan tanda ada nama pemakai gelang, nomer kloter dan nomer paspor. Sedangkan ibadah khusus dan non kuota hanya mengenakan gelang karet warna biru yang diberikan oleh Muasasah di Mekkah, Arab Saudi.<br /><br />Seperti yang dialami Amir Fahrudin (67) jamaah asal NTB ketika tersesat di dekat Masjidil Haram oleh seseorang diantarkan ke petugas sektor di daerah Bakhutmah Namun setelah di cek dari gelang tangan yang dikenakan ternyata tidak sama.<br /><br />"Saya datang baru tanggal 20 Oktober, saat salat di Masjidil Haram terpisah dari rombongan yang berjumlah 15 orang. Saya tidak tahu pintu masuknya setelah tersesat lama baru diantarkan ke petugas," ungkap Amir.<br /><br />Hal serupa juga dialami oleh Bading (70) asal Makasar yang sempat tersesat selama dua hari satu malam di Masjidil Haram. Saat salat di Masjidil Haram dia juga terpisah dari rombongan. Karena tidak tahu jalan dia terpaksa tidur di sekitar masjid. Dia pun sudah berkeliling masjid beberapa kali untuk menemukan teman satu rombongan.<br /><br />Saat ditemukan dan diantar ke Balai Pengobatan Haji Indonesia (BPHI) Mekkah, kondisinya masih lemah karena tidak makan dan hanya minum air zam-zam yang ada di masjid. Di BPHI dia langsung diberikan segelas air teh hangat dan mie instan.Setelah pulih kesehatannya, dia baru bisa bercerita dengan bahasa campuran, Bahasa Indonesia dan bahasa daerah Sulawesi Selatan.<br /><br />Saat ada petugas petugas dari muasasah setempat yang mengurusi jamaah, dia tidak mau naik mobil yang disiapkan. Dia mengatakan kemarin sudah naik mobil itu ternyata tidak diantar ke pemondokan, hanya diantar ke masjidil haram.<br /><br />Setelah dilakukan pembicaraan agak lama, datanglah seorang mukiman Mekkah yang menjadi pemandu. Setelah melihat Bading, si pemandu membenarkan bila dia adalah jamaah tersebut satu rombongan.<br /><br />"Benar itu anggota kami. Bapak sebentar kita antar pulang ke rumah. Ibu sudah menunggu di penginapan. Ibu khawatir bapak tidak ketemu," katanya.<br /><br />Oleh temus yang bertugas menjadi pengemudi, Lukmanul Hakim menanyakan di mana tempat menginap. Dia mengatakan penginapan berada di daerah Murjud. Lukman pun terkaget karena daerah itu di luar kota Mekkah sekitar 15 km atau di daerah perbatasan Mekkah-Jeddah.<br /><br />"Kok di situ," tanya Lukman lagi.<br /><br />Sambil berkata pelan, orang yang menjadi pemandu tersebut mengatakan hotel transit saja. Lukman pun hanya melongo dengan sedikit keheranan karena ada jamaah haji yang ditempatkan di daerah itu. Karena wilayah itu sudah lama tidak ditempati jamaah dari Indonesia.<br /><br />"Itu jauh sekali dan hanya dikatakan tempat transit," kata Lukman seorang mukimin yang tinggal di Mekkah lebih kurang 10 tahun.<br /><br />Menteri Agama RI yang juga Amirul Haj, Suryadharma Ali menyatakan kasus penipuan dan penelantaran jamaah haji oleh Penyelenggara Ibadah Haji Khusus (PIHK) yang tidak terdaftar di Kementerian Agama masih terjadi sampai saat ini.<br /><br />Menurutnya sesuai dengan UU No 13 tahun 2008 tentang Haji, penyelenggara haji adalah kementerian agama. Oleh karena itu jika ada PIHK yang tidak terdaftar dan menyelenggarakan haji, itu ilegal.<br /><br />"Perjalanan keluar negeri untuk jalan-jalan atau urusan bisnis berbeda dengan perjalanan naik haji ke tanah suci. Sesuai Undang-Undang Nomor 13 tahun 2008, penyelenggaraan haji diatur oleh Kementerian Agama," katanya.<br /><br />Menurut Suryadharma, jamaah haji yang diberangkatkan oleh PIHK yang tidak resmi biasanya menggunakan visa non kuota. Namun dia juga heran, jamaah non kuota bisa sampai Jeddah hingga Makkah.<br /><br />"Bahkan ada yang tidak bisa pulang ke Tanah karena tidak ada tiket. Atau begitu sampai mereka ditelantarkan tanpa ada yang mengurus. Pada akhirnya karena mereka warga negara Indonesia juga menjadi tanggungjawab pemerintah untuk mengurus mereka," katanya.<br /><br />Karena banyak PIHK yang tidak bertanggungjawab menurut dia saat di Armina juga mereka tidak mendapatkan hak makanan mereka. Akibatnya mereka terlantar dan masuk ke tenda jamaah resmi. Selain itu jika mereka meninggal dunia, pengurusannya pun memakan waktu.<br /><br />"Akhirnya mereka bergabung ke tenda jamaah resmi dan makan makanan yang disediakan untuk jamaah resmi," tegasnya.<br /><br />Dia mengingatkan agar muslim Indonesia tidak memaksakan diri berangkat ke tanah suci, jika belum mendapatkan porsi. Akibat terlalu memaksakan diri, akhirnya banyak masyarakat yang tertipu oleh biro travel nakal. "Sudah ada pimpinan travel yang berurusan dengan kepolisian," pungkas dia.<br /><br />Seemntara itu Direktur Jenderal Penyelenggara Haji dan Umrah (PHU) Kementerian Agama, Anggito Abimanyu menegaskan sekitar 15 Penyelenggara Ibadah Haji Khusus (PIHK) yang terindikasi menyelenggarakan pelayanan ibadah haji dibawah standar. Kalau kelimabelas PIHK tersebut terbukti melanggar,<br /><br />Kementerian Agama akan melakukan teguran hingga penutupan operasional. Dia mengatakan ada sekitar 244 PIHK yang resmi terdaftar di Kemenag. "Kalau sifatnya penipuan, merugikan jemaah, mengambil uang hak-hak jemaah, maka akan kami tindak," tuturnya.<br /><br />Menurut dia, banyak jamaah yang membayar setoran awalnya melalui PIHK/travel. Travel tidak menyetor ke rekening Menteri Agama di perbankan yang ditunjuk sehingga tidak mendapatkan porsi.<br /><br />"Kami persilahkan calon jemaah yang tertipu, yang tidak jadi berangkat mengadu ke pihak polisi supaya ada delik pengaduan, delik penipuan. Calon jemaah tidak berangkat karena tidak memperoleh visa dari Kedutaan Arab Saudi,&rdquo; katanya.<br /><br />Anggito menambahkan sudah ada Memorandum of Understanding (MoU) dengan Kedutaan Arab Saudi, setelah Ramadhan lalu. Isinya, semua pemberian visa, disampaikan kepada Kementerian Agama RI.<br /><br />"Disisi lain, Kedutaan Arab Saudi berhak memberikan visa kepada tamu-tamunya, kepada ormas, kepada Kementerian-Kementerian tertentu. Namun kami tetap meminta untuk diinformasikan kepada kami," pungkas dia..</p>', 'text', 8, '', 'super_administrator', '2013-09-02 20:43:41', '2013-09-02 20:43:41', '', '', ''),
(30, 'Paket Ramadhan', 'circling_the_kabah_te.jpg', '<p>Paket Umrah yang dilaksanakan pada bulan Ramadhan. Lain halnya dengan Umrah Reguler, Paket Ramadhan berdurasi 12 hari (pertengahan akhir) atau 24 hari (awal ramadhan). Paket 12 hari Ramadhan terbagi dalam beberapa tempat tujuan, yaitu 3 hari di Medinah, 7 hari di Mekkah, dan 2 hari perjalanan keberangkatan kepulangan. Paket 24 hari Ramadhan terbagi dalam beberapa tempat tujuan, yaitu 8 hari di Medinah, 14 hari di Mekkah, 2 hari perjalanan keberangkatan kepulangan.</p>', 'text', 16, '', 'super_administrator', '2013-09-02 21:31:57', '2013-09-02 21:31:57', '', '', ''),
(35, 'Paket Nurul Iman', 'EI-BC019_Mecca_G_20100514113734.jpg', '<p>Paket Nurul Iman: Paket Umrah Reguler 9 hari, terbagi dalam beberapa tempat tujuan, yaitu 2 hari di Medinah, 4 hari di Mekkah, 1 hari di Jeddah, dan 2 hari perjalanan keberangkatan kepulangan. </p>', 'text', 16, '', 'super_administrator', '2013-09-21 06:20:39', '2013-09-21 06:20:39', '', '', ''),
(36, 'Paket Haji Khusus', 'slider-3.png', '<p>Kami menyediakan paket Haji Khusus (plus) untuk memfasilitasi dan melayani calon jemaah Haji yang ingin maksimal dalam beribadah guna menggapai Haji Mabrur, melalui serangkaian program perjalanan haji yang efektif, aman, dan nyaman dengan muatan pembinaan keislaman yang berkualitas. Quota maksimal pemberangkatan tiap tahunnya adalah 400 jemaah, dengan asusmsi pendaftaran dan kelengkapan dokumen tidak ada keterlambatan. Dengan izin Haji D/527 Th. 2011 untuk PT. Madania Semesta Wisata dan D/224 th 2010 untuk anak perusahaan PT.Kharisma Muzdalifah.</p>', 'text', 16, '', 'super_administrator', '2013-09-21 06:23:15', '2013-09-21 06:23:15', '', '', ''),
(37, 'Paket Nurul Islam', 'mekkah.jpg', '<p><br />Paket Nurul Islam: Paket Umrah Reguler 9 hari, terbagi dalam beberapa tempat tujuan, yaitu 2 hari di Medinah, 4 hari di Mekkah, 1 hari di Jeddah, dan 2 hari perjalanan keberangkatan kepulangan. Perbedaannya terletak pada harga dan fasilitas hotel yang ditawarkan.</p>', 'text', 16, '', 'super_administrator', '2013-09-26 07:01:39', '2013-09-26 07:01:39', '', '', ''),
(38, 'Achmad hariaynto , CEO nutrifood', '', 'Alhamdulillah saya sangat menikmati perjalanan ini, saya merasakan enak dan nyaman mengikuti umrah bersama Percikan Iman. Alhamdulillah pula semua berjalan sesuai dengan yang Saya inginkan dan saya harapkan. Buat saya bimbingan Umrah Percikan Iman lebih dari memadai. Saya pernah beberapa kali berangkat Umrah, tapi bimbingan di Percikan Iman sangat berbeda. Banyak hal yang selama ini saya tidak dapatkan di bimbingan lain tetapi di bimbingan Umrah Percikan Iman ini justru saya mendapatkannya. Memang sejak awal saya dan suami saya memutuskan untuk berangkat Umrah bersama Percikan Iman tujuannya adalah ingin mengetahui ibadah umrah yang sebenarnya itu seperti apa. Karena dari pengalaman-pengalaman umrah yang lalu di travel lain terkadang ada beberapa hal yang membuat saya ragu, benar nggak yang saya laksanakan ini dan ternyata Alhamdulillah semua keraguan itu bisa di jawab di Travel Percikan Iman ini. Jadi, saya bisa lebih nyaman dan tenteram dalam melaksanakan seluruh rangkaian ibadah umrah.', 'text', 13, '', 'super_administrator', '2013-09-30 10:27:40', '2013-09-30 10:27:40', '', '', ''),
(39, 'Ibu Dewi Sodja Laela – Dosen', '', 'Fasilitas yang diberikan juga sangat nyaman dan sesuai dengan yang dijanjikan. Alhamdulillah fasilitas yang diberikan tersebut seperti hotel, kendaraan, makanan, kepanitiaan bisa memberikan kemudahan dan menambah kekhusyuan kami para jama&rsquo;ah dalam beribadah. Dan ketika ada kendala di lapangan saya melihat pembimbing dan team dari Percikan Iman begitu cepat dan tanggap dalam menanganinya. Meskipun, saya sempat berdialog dengan jama&rsquo;ah lain terutama yang jama&rsquo;ah yang sudah berumur yang sepuh-sepuh ketika ada kendala dilapangan dan ternyata perubahan itu harusnya lebih ditekankan lagi informasinya, bahkan kalau bisa harus terus diulang-ulang sehingga mereka pun bisa mengikutinya secara tepat dan cepat. Terus, saran juga buat ke depannya sebaiknya informasi dan bimbingan itu juga harus dilakukan secara personal ke jama&rsquo;ah yang sepuh jadi tidak hanya mengandalkan pengarahan yang bersifat masal dalam group/kelompok.', 'text', 13, '', 'super_administrator', '2013-09-30 10:28:19', '2013-09-30 10:28:19', '', '', ''),
(40, 'Apakah program ini bisa diikuti oleh setiap orang ', '', 'Ya, setiap orang dapat mengikuti program ini', 'text', 19, '', 'super_administrator', '2013-10-04 09:33:26', '2013-10-04 09:33:26', '', '', 'xxx'),
(41, 'Apakah pendaftarannya bisa diikuti juga dari selur', '', 'Ya, bisa. Bahkan dari luar negeri, seperti Dubai, Hongkong pun sudah banyak mendaftar.', 'text', 19, '', 'super_administrator', '2013-10-04 09:36:35', '2013-10-04 09:36:35', '', '', 'Apa itu Cloud Computing?'),
(42, 'Bagaimana legalitas perusahaan PT. Madania Semesta', '', '-', 'text', 19, '', 'super_administrator', '2013-10-04 14:55:36', '2013-10-04 14:55:36', '', '', 'Bagaimana legalitas perusahaan PT. Madania Semesta Wisata ? '),
(43, 'emirates airway', 'emirates.png', 'emirates airway', 'text', 26, '', 'super_administrator', '2013-10-05 16:19:58', '2013-10-05 16:19:58', '', '', 'emirates airway'),
(44, 'Garuda airway', 'l79958-garuda-indonesia-air-logo-51257.png', 'Garuda airway', 'text', 26, '', 'super_administrator', '2013-10-05 16:20:22', '2013-10-05 16:20:22', '', '', 'Garuda airway'),
(45, 'syarat haji', 'pedomanPendaftaranHajiKhusus.pdf', 'syarat haji', 'text', 25, '', 'super_administrator', '2013-10-05 17:08:58', '2013-10-05 17:08:58', '', '', 'syarat haji'),
(46, 'Travel Boutique ', '50231_232144593489098_1192890526_n.jpg ', '<p>Travel Boutique by Madania dibawah naungan PT. MADANIA SEMESTA WISATA, yang telah lama bergerak di bidang jasa pelayanan wisata khususnya di bidang Umrah dan Haji khusus, ingin memberikan satu solusi dalam kebutuhan masyarakat umum, khususnya yang menginginkan terwujudnya satu impian perjalanan wisata yang diharapkan dapat memberikan segala kenangan indah. Telah bekerjasama baik dengan segala komponenya, Airlines domestic ataupun International, Hotel domestic international, ataupun kebutuhan lainnya yang terlibat sehinga mampu memberikan paket pilihan wisata menarik dengan harga kompetitif yang dikerjakan secara professional dan komitmen penuh.</p> ', 'link', 15, '', 'super_administrator', '2013-10-10 19:21:13', '2013-10-10 19:21:13', ' ', ' ', 'http://travel-boutique.co.id/  '),
(47, 'xxx', '1044225_10200165059597260_37124957_n.jpg', 'xxx', 'text', 24, '', 'super_administrator', '2013-10-17 18:04:17', '2013-10-17 18:04:17', '', '', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_products`
--

CREATE TABLE IF NOT EXISTS `attribute_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `type` enum('image','text') NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `attribute_products`
--

INSERT INTO `attribute_products` (`id`, `name`, `value`, `description`, `type`, `product_id`) VALUES
(1, 'Bandung', 'listing-photo-residential-paris-village-1-001.jpg', 'qwwww', 'image', 4),
(2, 'Bandung', 'listing-photo-residential-paris-village-1-001.jpg', 'qwwww', 'image', 4),
(3, 'Bandung', 'lantai dasar', 'lantai dasar', 'text', 4),
(4, 'Bandung', 'lantai dasar', 'lantai dasar', 'text', 4),
(5, 'Bandung', 'listing-photo-residential-paris-village-1-001.jpg', 'qwwww', 'image', 5),
(6, 'Bandung', 'listing-photo-residential-paris-village-1-001.jpg', 'qwwww', 'image', 5),
(7, 'Bandung', 'lantai dasar', 'lantai dasar', 'text', 5),
(8, 'Bandung', 'lantai dasar', 'lantai dasar', 'text', 5),
(9, 'sddd', 'listing-photo-residential-paris-village-1-001.jpg', 'asdasdasdsad', 'image', 6),
(10, 'sddd', 'listing-photo-residential-paris-village-1-001.jpg', 'asdasdasdsad', 'image', 6),
(11, 'dasdasd', 'asdasd', 'asdasdasd', 'text', 6),
(12, 'dasdasd', 'asdasd', 'asdasdasd', 'text', 6),
(13, 'rumah impian ', 'rumah-impianku-32.jpg', 'gambar dari depan ', 'image', 8),
(14, 'rumah minimalis samping', 'desain rumah minimalis 1.jpg', 'gambar dari samping mempunya luas tanah cukup besar', 'image', 8),
(15, 'Ruang', '6 kamar', 'terdiri dari 6 kamar ', 'text', 8),
(16, 'Lantai', '2 lt', 'terdiri dari 2 lantai yang memiliki luas lantai 100m2 ', 'text', 8),
(17, 'dvxcvxcv', 'landscape_photography_04.jpg', 'xcvxcvxcvxcv', 'image', 9),
(18, 'lantai', '2 lt ', 'lantai luas ak', 'text', 9),
(19, 'dvxcvxcv', 'landscape_photography_04.jpg', 'xcvxcvxcvxcv', 'image', 10),
(20, 'lantai', '2 lt ', 'lantai luas ak', 'text', 10),
(21, 'dvxcvxcv', 'landscape_photography_04.jpg', 'xcvxcvxcvxcv', 'image', 11),
(22, 'lantai', '2 lt ', 'lantai luas ak', 'text', 11),
(23, 'Sukasenang', 'promo1.jpg', 'ssss', 'image', 12),
(24, 'Sukasenang', '2951652-section-of-the-cold-war-wall.jpg', 'ssss', 'image', 12),
(25, 'lantai', '3 lt', 'luas lantai 300 m2', 'text', 12),
(26, 'lantai', '3 lt', 'luas lantai 300 m2', 'text', 12),
(27, 'lantai', '2 lt', 'luas lantai 150 m2', 'text', 12),
(28, 'halaman', '2 lt', 'luas lantai 150 m2', 'text', 12),
(29, 'ssss', 'band-348x320.jpg', 'sss', 'image', 13),
(30, 'ssss', 'DSC_0022.JPG', 'sss', 'image', 13),
(31, 'ssss', 'upacara 2 mei 2012b.jpg', 'sss', 'image', 13),
(32, 'ssss', '603652_655878061104832_66509981_n.jpg', 'sss', 'image', 13),
(33, '-', '3', 'luas lantai 150 m2', 'text', 13),
(34, 'w', '3', 'luas lantai 150 m2', 'text', 13),
(35, 's', '3', 'luas lantai 150 m2', 'text', 13),
(36, 'sss', '3', 'luas lantai 150 m2', 'text', 13),
(42, 'kamar mandi', '5 km', 'luas kesuluhan', 'text', 14),
(44, 'kamar mandi', '4 kamar', 'rumah mewah', 'text', 15),
(45, 'ssss', 'desain eksterior rumah tropis modern.jpg', 'ssss', 'image', 16),
(46, 'luas ', '809m3', 'jjjhhgajhb ', 'text', 16),
(47, 'ssss', 'desain eksterior rumah tropis modern.jpg', 'ssss', 'image', 17),
(48, 'luas ', '809m3', 'jjjhhgajhb ', 'text', 17),
(49, 'aaa', 'check.png', 'aaa', 'image', 18),
(50, 'aaa', '809m3', 'jjjhhgajhb ', 'text', 18),
(51, 'aaa', 'check.png', 'aaa', 'image', 19),
(52, 'aaa', '809m3', 'jjjhhgajhb ', 'text', 19),
(53, 'ddd', 'check.png', 'ddd', 'image', 20),
(54, 'dddd', '809m3', 'jjjhhgajhb ', 'text', 20),
(55, '22www', 'check.png', 'wwww', 'image', 21),
(56, 'r4ee', '809m3', 'jjjhhgajhb ', 'text', 21);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `nama`, `deskripsi`, `date_created`, `date_updated`) VALUES
(1, 'Rumah', '<p>\n	rumah xxx</p>\n', '2013-05-23 00:00:00', '2013-05-23 00:00:00'),
(2, 'rumah mewah', '<p>\n	kategori rumah mewah dengan nilai asset &gt; 1 m</p>\n', '2013-06-19 20:26:09', '2013-06-19 20:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_province_cities` (`province_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `nama`, `province_id`) VALUES
(1, 'bandung', 1),
(2, 'Sunter', 2),
(3, 'surabaya', 4),
(4, 'tuban', 4),
(5, 'jogja', 3),
(6, 'cianjur', 1);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(11) NOT NULL,
  `descriptions` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `news_id`, `descriptions`, `created_at`, `updated_at`, `name`, `email`) VALUES
(1, 1, 'tindakan yang berina ', '2013-10-02 00:00:00', '2013-10-02 00:00:00', 'wati', 'meeeva.me@gmail.com'),
(2, 1, 'mantap', '2013-10-02 00:00:00', '2013-10-02 00:00:00', 'feri', 'feripriyanto@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `comment_news`
--

CREATE TABLE IF NOT EXISTS `comment_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `news_id` int(11) NOT NULL,
  `updated_at` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `slogan` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `facebook` varchar(200) NOT NULL,
  `twiter` varchar(200) NOT NULL,
  `telp` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `long` varchar(200) NOT NULL,
  `lat` varchar(200) NOT NULL,
  `google_plus` varchar(200) NOT NULL,
  `youtube` varchar(200) NOT NULL,
  `flicker` varchar(200) NOT NULL,
  `is_office` tinyint(1) NOT NULL,
  `contact_person` varchar(100) NOT NULL DEFAULT 'N/A',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `nama`, `slogan`, `email`, `facebook`, `twiter`, `telp`, `alamat`, `long`, `lat`, `google_plus`, `youtube`, `flicker`, `is_office`, `contact_person`) VALUES
(1, 'Madania Travel', 'Lebih dari <span class="highlight"> <strong>5000 Peserta Umroh</strong> </span> telah yang diberangkat', 'info@madaniatravel.com', 'N/A', 'N/A', 22123, 'GRAHA MADANIA Jl. Cijagra No 22B, Buah Batu, Bandung', '107.62493238623051', '-6.941576938744924', '', '', '', 1, 'N/A'),
(2, 'Cabang Bandung ', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Jl. Cibiru Hilir No. 20 Cileunyi Bandung', '107.76968426127928', '-6.933448800581996', 'N/A', 'N/A', 'N/A', 0, 'Yogi Yoswara'),
(3, 'Cabang Lampung', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Jl. Perwira No. 21, Raja Basa-Bandar Lampung', '105.26697594603274', '-5.441461031758887', 'N/A', 'N/A', 'N/A', 0, 'Shinta Desia'),
(4, 'Bekasi ', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Komp. TVRI Blok E3 No. 14 RT 010 RW 012 Jatirahayu-Bekasi', '106.99313354492188', '-6.233333299999986', 'N/A', 'N/A', 'N/A', 0, 'Ricky Ade Putra'),
(5, 'Cabang Jakarta', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Jl. H. Samali No. 31B Kalibata - Jaksel', '106.83495866137696', '-6.2668226588980795', 'N/A', 'N/A', 'N/A', 0, 'M. Agus Salim '),
(6, 'Cabang Kalimantan', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Jl. Pesantren Hidayatullah RT / RW 002/001 Selisun Nunukan Kaltim', '117.68040131376961', '4.102844978370742', 'N/A', 'N/A', 'N/A', 0, 'Achmad Amir'),
(7, 'Cabang Semarang', 'N/A', 'N/A', 'N/A', 'N/A', 0, 'Jl. Letjen Sutoyo No. 25 Lowokbaru', '110.4194132820312', '-6.970074567236799', 'N/A', 'N/A', 'N/A', 0, 'M. Galih Awantoro');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `telp` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` enum('new','read','reply') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `title`, `telp`, `email`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'dani taufik himawan', 'server error ', '098776', 'danzztakezo@gmail.com', 'server error  Can''t execute API function Account::Shared::MySQL list_dbs: 500 - Can''t connect to mysqld: Too many connections', 'new', '2013-10-11 20:27:24', '2013-10-11 20:27:24'),
(2, 'test', 'test', 'test', 'danzztakezo@gmail.com', 'test2', 'new', '2013-10-11 20:29:30', '2013-10-11 20:29:30'),
(3, 'test', 'test', 'test', 'danzztakezo@gmail.com', 'test2', 'new', '2013-10-11 20:31:03', '2013-10-11 20:31:03'),
(4, 'test', 'test', 'test', 'danzztakezo@gmail.com', 'test2', 'new', '2013-10-11 20:31:55', '2013-10-11 20:31:55'),
(5, 'test', 'test', 'test', 'danzztakezo@gmail.com', 'server error asem', 'new', '2013-10-11 20:33:11', '2013-10-11 20:33:11'),
(6, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:53:25', '2013-10-11 20:53:25'),
(7, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:53:40', '2013-10-11 20:53:40'),
(8, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:54:00', '2013-10-11 20:54:00'),
(9, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:54:23', '2013-10-11 20:54:23'),
(10, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:54:39', '2013-10-11 20:54:39'),
(11, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:56:55', '2013-10-11 20:56:55'),
(12, 'dani ', 'Test halaman kontak', '098776', 'danzztakezo@gmail.com', 'ini pesan dikirimkan via website madaniatravel.com.', 'new', '2013-10-11 20:57:38', '2013-10-11 20:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `curriculums`
--

CREATE TABLE IF NOT EXISTS `curriculums` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('active','nonactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pasport` varchar(40) NOT NULL,
  `no_ktp` varchar(40) NOT NULL,
  `no_registrasi` varchar(50) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_ayah_kandung` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `umur` int(11) NOT NULL,
  `kelamin` enum('pria','wanita') NOT NULL,
  `kecamatan_id` int(11) NOT NULL,
  `kota_id` int(11) NOT NULL,
  `propinsi_id` int(11) NOT NULL,
  `kodepos` int(11) NOT NULL,
  `pendidikan_id` int(11) NOT NULL,
  `pekerjaan_id` int(11) NOT NULL,
  `pergi_haji` enum('pernah','belum') NOT NULL,
  `nama_mahram` varchar(100) NOT NULL,
  `hubungan_mahram` enum('Orang tua','Anak','Suami','Mertua','saudara kandung') NOT NULL,
  `no_pendaftaran_mahram_pendamping` varchar(50) NOT NULL,
  `golongan_darah` enum('A','B','AB','O') NOT NULL,
  `status_perkawinan` enum('menikah','belum menikah','janda/duda') NOT NULL,
  `kode_diagnosa` text NOT NULL,
  `ciri_rambut` varchar(100) NOT NULL,
  `ciri_alis` varchar(100) NOT NULL,
  `ciri_hidung` varchar(100) NOT NULL,
  `ciri_muka` varchar(100) NOT NULL,
  `ciri_tinggi` varchar(100) NOT NULL,
  `ciri_berat` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `departures`
--

CREATE TABLE IF NOT EXISTS `departures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `total_tax` int(11) NOT NULL,
  `attribute_page_id` int(11) NOT NULL,
  `status` enum('active','nonactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `departures`
--

INSERT INTO `departures` (`id`, `name`, `description`, `created_at`, `updated_at`, `startdate`, `enddate`, `total_tax`, `attribute_page_id`, `status`) VALUES
(1, '09 hari', '09 hari', '2013-10-01 00:00:00', '2013-10-01 00:00:00', '2013-10-06 00:00:00', '2013-10-15 00:00:00', 80, 20, 'active'),
(2, '09 hari ', '09 hari', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-10-03 00:00:00', '2013-10-12 00:00:00', 0, 20, 'nonactive'),
(3, '0877', 'www', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-10-18 00:00:00', '2013-10-19 00:00:00', 80, 20, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE IF NOT EXISTS `districts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `province_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_district_city` (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `nama`, `city_id`, `province_id`) VALUES
(1, 'padasuka', 1, 1),
(2, 'bogorejo', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE IF NOT EXISTS `features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `key` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `controller_name` varchar(50) NOT NULL,
  `method_name` varchar(50) NOT NULL,
  `second_method_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=287 ;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `key`, `description`, `controller_name`, `method_name`, `second_method_name`, `created_at`) VALUES
(1, 'users index', 'users_index', 'users index', 'users', 'index', '0', '2013-06-16 11:47:55'),
(2, 'users index add', 'users_index_add', 'users index add', 'users', 'index', 'add', '2013-06-16 11:48:04'),
(3, 'users index edit', 'users_index_edit', 'users index edit', 'users', 'index', 'edit', '2013-06-16 11:48:19'),
(4, 'users index delete', 'users_index_delete', 'users index delete', 'users', 'index', 'delete', '2013-06-16 11:48:34'),
(7, 'groups index', 'groups_index', 'groups index', 'groups', 'index', '0', '2013-06-16 11:48:55'),
(8, 'groups index add', 'groups_index_add', 'groups index add', 'groups', 'index', 'add', '2013-06-16 11:48:57'),
(9, 'groups index edit', 'groups_index_edit', 'groups index edit', 'groups', 'index', 'edit', '2013-06-16 11:49:02'),
(11, 'groups index insert', 'groups_index_insert', 'groups index insert', 'groups', 'index', 'insert', '2013-06-16 11:49:36'),
(12, 'groups index delete', 'groups_index_delete', 'groups index delete', 'groups', 'index', 'delete', '2013-06-16 11:50:03'),
(13, 'groups index ajax_list_info', 'groups_index_ajax_list_info', 'groups index ajax_list_info', 'groups', 'index', 'ajax_list_info', '2013-06-16 11:50:03'),
(14, 'groups index ajax_list', 'groups_index_ajax_list', 'groups index ajax_list', 'groups', 'index', 'ajax_list', '2013-06-16 11:50:03'),
(15, 'features index', 'features_index', 'features index', 'features', 'index', '0', '2013-06-16 11:50:17'),
(16, 'features index add', 'features_index_add', 'features index add', 'features', 'index', 'add', '2013-06-16 11:50:28'),
(17, 'features index edit', 'features_index_edit', 'features index edit', 'features', 'index', 'edit', '2013-06-16 11:50:48'),
(18, 'features index delete', 'features_index_delete', 'features index delete', 'features', 'index', 'delete', '2013-06-16 11:51:04'),
(19, 'features index ajax_list_info', 'features_index_ajax_list_info', 'features index ajax_list_info', 'features', 'index', 'ajax_list_info', '2013-06-16 11:51:05'),
(20, 'features index ajax_list', 'features_index_ajax_list', 'features index ajax_list', 'features', 'index', 'ajax_list', '2013-06-16 11:51:05'),
(21, 'group_features index', 'group_features_index', 'group_features index', 'group_features', 'index', '0', '2013-06-16 11:51:29'),
(22, 'group_features index add', 'group_features_index_add', 'group_features index add', 'group_features', 'index', 'add', '2013-06-16 11:51:31'),
(23, 'group_features index insert_validation', 'group_features_index_insert_validation', 'group_features index insert_validation', 'group_features', 'index', 'insert_validation', '2013-06-16 11:51:59'),
(24, 'group_features index insert', 'group_features_index_insert', 'group_features index insert', 'group_features', 'index', 'insert', '2013-06-16 11:51:59'),
(25, 'group_features index delete', 'group_features_index_delete', 'group_features index delete', 'group_features', 'index', 'delete', '2013-06-16 11:52:29'),
(26, 'group_features index ajax_list_info', 'group_features_index_ajax_list_info', 'group_features index ajax_list_info', 'group_features', 'index', 'ajax_list_info', '2013-06-16 11:52:29'),
(27, 'group_features index ajax_list', 'group_features_index_ajax_list', 'group_features index ajax_list', 'group_features', 'index', 'ajax_list', '2013-06-16 11:52:29'),
(28, 'pages index', 'pages_index', 'pages index', 'pages', 'index', '0', '2013-06-16 11:52:31'),
(29, 'pages index add', 'pages_index_add', 'pages index add', 'pages', 'index', 'add', '2013-06-16 11:52:33'),
(30, 'pages index edit', 'pages_index_edit', 'pages index edit', 'pages', 'index', 'edit', '2013-06-16 11:52:44'),
(31, 'pages index delete', 'pages_index_delete', 'pages index delete', 'pages', 'index', 'delete', '2013-06-16 11:52:59'),
(32, 'pages index ajax_list_info', 'pages_index_ajax_list_info', 'pages index ajax_list_info', 'pages', 'index', 'ajax_list_info', '2013-06-16 11:52:59'),
(33, 'pages index ajax_list', 'pages_index_ajax_list', 'pages index ajax_list', 'pages', 'index', 'ajax_list', '2013-06-16 11:52:59'),
(34, 'categories index', 'categories_index', 'categories index', 'categories', 'index', '0', '2013-06-16 11:53:19'),
(35, 'categories index add', 'categories_index_add', 'categories index add', 'categories', 'index', 'add', '2013-06-16 11:53:25'),
(36, 'categories index insert_validation', 'categories_index_insert_validation', 'categories index insert_validation', 'categories', 'index', 'insert_validation', '2013-06-16 11:53:47'),
(37, 'categories index insert', 'categories_index_insert', 'categories index insert', 'categories', 'index', 'insert', '2013-06-16 11:53:47'),
(38, 'categories index edit', 'categories_index_edit', 'categories index edit', 'categories', 'index', 'edit', '2013-06-16 11:53:55'),
(39, 'categories index delete', 'categories_index_delete', 'categories index delete', 'categories', 'index', 'delete', '2013-06-16 11:54:06'),
(40, 'categories index ajax_list_info', 'categories_index_ajax_list_info', 'categories index ajax_list_info', 'categories', 'index', 'ajax_list_info', '2013-06-16 11:54:06'),
(41, 'categories index ajax_list', 'categories_index_ajax_list', 'categories index ajax_list', 'categories', 'index', 'ajax_list', '2013-06-16 11:54:06'),
(42, 'dashboard index', 'dashboard_index', 'dashboard index', 'dashboard', 'index', '0', '2013-06-16 11:54:12'),
(43, 'users index ajax_list_info', 'users_index_ajax_list_info', 'users index ajax_list_info', 'users', 'index', 'ajax_list_info', '2013-06-16 11:54:18'),
(44, 'users index ajax_list', 'users_index_ajax_list', 'users index ajax_list', 'users', 'index', 'ajax_list', '2013-06-16 11:54:19'),
(45, 'religions index', 'religions_index', 'religions index', 'religions', 'index', '0', '2013-06-16 11:54:54'),
(46, 'religions index add', 'religions_index_add', 'religions index add', 'religions', 'index', 'add', '2013-06-16 11:54:56'),
(47, 'religions index insert_validation', 'religions_index_insert_validation', 'religions index insert_validation', 'religions', 'index', 'insert_validation', '2013-06-16 11:55:11'),
(48, 'religions index insert', 'religions_index_insert', 'religions index insert', 'religions', 'index', 'insert', '2013-06-16 11:55:11'),
(49, 'religions index edit', 'religions_index_edit', 'religions index edit', 'religions', 'index', 'edit', '2013-06-16 11:55:16'),
(50, 'religions index delete', 'religions_index_delete', 'religions index delete', 'religions', 'index', 'delete', '2013-06-16 11:55:26'),
(51, 'religions index ajax_list_info', 'religions_index_ajax_list_info', 'religions index ajax_list_info', 'religions', 'index', 'ajax_list_info', '2013-06-16 11:55:27'),
(52, 'religions index ajax_list', 'religions_index_ajax_list', 'religions index ajax_list', 'religions', 'index', 'ajax_list', '2013-06-16 11:55:27'),
(53, 'provinces index', 'provinces_index', 'provinces index', 'provinces', 'index', '0', '2013-06-16 11:55:53'),
(54, 'provinces index add', 'provinces_index_add', 'provinces index add', 'provinces', 'index', 'add', '2013-06-16 11:55:54'),
(55, 'provinces index edit', 'provinces_index_edit', 'provinces index edit', 'provinces', 'index', 'edit', '2013-06-16 11:56:00'),
(56, 'provinces index insert_validation', 'provinces_index_insert_validation', 'provinces index insert_validation', 'provinces', 'index', 'insert_validation', '2013-06-16 11:56:07'),
(57, 'provinces index insert', 'provinces_index_insert', 'provinces index insert', 'provinces', 'index', 'insert', '2013-06-16 11:56:07'),
(58, 'provinces index delete', 'provinces_index_delete', 'provinces index delete', 'provinces', 'index', 'delete', '2013-06-16 11:56:19'),
(59, 'provinces index ajax_list_info', 'provinces_index_ajax_list_info', 'provinces index ajax_list_info', 'provinces', 'index', 'ajax_list_info', '2013-06-16 11:56:20'),
(60, 'provinces index ajax_list', 'provinces_index_ajax_list', 'provinces index ajax_list', 'provinces', 'index', 'ajax_list', '2013-06-16 11:56:20'),
(61, 'cities index', 'cities_index', 'cities index', 'cities', 'index', '0', '2013-06-16 11:56:23'),
(62, 'cities index add', 'cities_index_add', 'cities index add', 'cities', 'index', 'add', '2013-06-16 11:56:26'),
(63, 'cities index insert_validation', 'cities_index_insert_validation', 'cities index insert_validation', 'cities', 'index', 'insert_validation', '2013-06-16 11:56:34'),
(64, 'cities index insert', 'cities_index_insert', 'cities index insert', 'cities', 'index', 'insert', '2013-06-16 11:56:35'),
(65, 'cities index success', 'cities_index_success', 'cities index success', 'cities', 'index', 'success', '2013-06-16 11:56:35'),
(66, 'cities index edit', 'cities_index_edit', 'cities index edit', 'cities', 'index', 'edit', '2013-06-16 11:56:40'),
(67, 'cities index delete', 'cities_index_delete', 'cities index delete', 'cities', 'index', 'delete', '2013-06-16 11:56:46'),
(68, 'cities index ajax_list_info', 'cities_index_ajax_list_info', 'cities index ajax_list_info', 'cities', 'index', 'ajax_list_info', '2013-06-16 11:56:46'),
(69, 'cities index ajax_list', 'cities_index_ajax_list', 'cities index ajax_list', 'cities', 'index', 'ajax_list', '2013-06-16 11:56:47'),
(70, 'districts index', 'districts_index', 'districts index', 'districts', 'index', '0', '2013-06-16 11:56:52'),
(71, 'villages index', 'villages_index', 'villages index', 'villages', 'index', '0', '2013-06-16 11:57:03'),
(74, 'products index', 'products_index', 'products index', 'products', 'index', '0', '2013-06-16 12:06:05'),
(75, 'employees index', 'employees_index', 'employees index', 'employees', 'index', '0', '2013-06-16 12:06:08'),
(76, 'requirements index', 'requirements_index', 'requirements index', 'requirements', 'index', '0', '2013-06-16 12:06:12'),
(77, 'customers index', 'customers_index', 'customers index', 'customers', 'index', '0', '2013-06-16 12:06:13'),
(79, 'app_categories index', 'app_categories_index', 'app_categories index', 'app_categories', 'index', '0', '2013-06-16 12:34:26'),
(80, 'app_menus index', 'app_menus_index', 'app_menus index', 'app_menus', 'index', '0', '2013-06-16 12:35:08'),
(81, 'app_menus index delete', 'app_menus_index_delete', 'app_menus index delete', 'app_menus', 'index', 'delete', '2013-06-16 12:38:40'),
(82, 'app_menus index ajax_list_info', 'app_menus_index_ajax_list_info', 'app_menus index ajax_list_info', 'app_menus', 'index', 'ajax_list_info', '2013-06-16 12:38:40'),
(83, 'app_menus index ajax_list', 'app_menus_index_ajax_list', 'app_menus index ajax_list', 'app_menus', 'index', 'ajax_list', '2013-06-16 12:38:40'),
(84, 'app_menus index add', 'app_menus_index_add', 'app_menus index add', 'app_menus', 'index', 'add', '2013-06-16 12:40:41'),
(85, 'app_menus index edit', 'app_menus_index_edit', 'app_menus index edit', 'app_menus', 'index', 'edit', '2013-06-16 12:41:43'),
(86, 'app_menus index update_validation', 'app_menus_index_update_validation', 'app_menus index update_validation', 'app_menus', 'index', 'update_validation', '2013-06-16 12:42:20'),
(87, 'app_menus index update', 'app_menus_index_update', 'app_menus index update', 'app_menus', 'index', 'update', '2013-06-16 12:42:20'),
(88, 'app_menus index success', 'app_menus_index_success', 'app_menus index success', 'app_menus', 'index', 'success', '2013-06-16 12:43:10'),
(89, 'groups index insert_validation', 'groups_index_insert_validation', 'groups index insert_validation', 'groups', 'index', 'insert_validation', '2013-06-17 19:01:55'),
(90, 'groups index success', 'groups_index_success', 'groups index success', 'groups', 'index', 'success', '2013-06-17 19:01:56'),
(91, 'group_features create', 'group_features_create', 'group_features create', 'group_features', 'create', '0', '2013-06-17 19:15:23'),
(92, 'users index insert_validation', 'users_index_insert_validation', 'users index insert_validation', 'users', 'index', 'insert_validation', '2013-06-19 06:06:02'),
(93, 'users index insert', 'users_index_insert', 'users index insert', 'users', 'index', 'insert', '2013-06-19 06:06:03'),
(94, 'users index success', 'users_index_success', 'users index success', 'users', 'index', 'success', '2013-06-19 06:06:03'),
(95, 'products editCustomProducts edit', 'products_editCustomProducts_edit', 'products editCustomProducts edit', 'products', 'editCustomProducts', 'edit', '2013-06-19 07:10:02'),
(96, 'products addCustomProducts add', 'products_addCustomProducts_add', 'products addCustomProducts add', 'products', 'addCustomProducts', 'add', '2013-06-19 07:10:14'),
(97, 'products add_product_employees add', 'products_add_product_employees_add', 'products add_product_employees add', 'products', 'add_product_employees', 'add', '2013-06-19 07:10:50'),
(98, 'products get_calendar_json', 'products_get_calendar_json', 'products get_calendar_json', 'products', 'get_calendar_json', '0', '2013-06-19 07:32:34'),
(99, 'customers index add', 'customers_index_add', 'customers index add', 'customers', 'index', 'add', '2013-06-19 08:11:09'),
(100, 'employees index add', 'employees_index_add', 'employees index add', 'employees', 'index', 'add', '2013-06-19 08:13:24'),
(101, 'requirements index add', 'requirements_index_add', 'requirements index add', 'requirements', 'index', 'add', '2013-06-19 08:20:50'),
(102, 'requirements index insert_validation', 'requirements_index_insert_validation', 'requirements index insert_validation', 'requirements', 'index', 'insert_validation', '2013-06-19 08:21:41'),
(103, 'requirements index insert', 'requirements_index_insert', 'requirements index insert', 'requirements', 'index', 'insert', '2013-06-19 08:21:41'),
(104, 'requirements index success', 'requirements_index_success', 'requirements index success', 'requirements', 'index', 'success', '2013-06-19 08:21:41'),
(105, 'categories index success', 'categories_index_success', 'categories index success', 'categories', 'index', 'success', '2013-06-19 08:26:11'),
(106, 'cities getCities 3', 'cities_getCities_3', 'cities getCities 3', 'cities', 'getCities', '3', '2013-06-19 08:27:03'),
(107, 'cities getCities 1', 'cities_getCities_1', 'cities getCities 1', 'cities', 'getCities', '1', '2013-06-19 08:27:08'),
(108, 'cities getCities 4', 'cities_getCities_4', 'cities getCities 4', 'cities', 'getCities', '4', '2013-06-19 08:27:18'),
(109, 'products create', 'products_create', 'products create', 'products', 'create', '0', '2013-06-19 08:30:10'),
(110, 'products add_product_customers add', 'products_add_product_customers_add', 'products add_product_customers add', 'products', 'add_product_customers', 'add', '2013-06-19 09:03:26'),
(111, 'groups index update_validation', 'groups_index_update_validation', 'groups index update_validation', 'groups', 'index', 'update_validation', '2013-06-19 09:31:50'),
(112, 'groups index update', 'groups_index_update', 'groups index update', 'groups', 'index', 'update', '2013-06-19 09:31:51'),
(113, 'categories index update_validation', 'categories_index_update_validation', 'categories index update_validation', 'categories', 'index', 'update_validation', '2013-06-19 09:42:19'),
(114, 'categories index update', 'categories_index_update', 'categories index update', 'categories', 'index', 'update', '2013-06-19 09:42:19'),
(122, 'requirements index edit', 'requirements_index_edit', 'requirements index edit', 'requirements', 'index', 'edit', '2013-06-19 09:44:59'),
(123, 'requirements index update_validation', 'requirements_index_update_validation', 'requirements index update_validation', 'requirements', 'index', 'update_validation', '2013-06-19 09:45:11'),
(124, 'requirements index update', 'requirements_index_update', 'requirements index update', 'requirements', 'index', 'update', '2013-06-19 09:45:11'),
(125, 'requirements index delete', 'requirements_index_delete', 'requirements index delete', 'requirements', 'index', 'delete', '2013-06-19 09:45:19'),
(126, 'requirements index ajax_list_info', 'requirements_index_ajax_list_info', 'requirements index ajax_list_info', 'requirements', 'index', 'ajax_list_info', '2013-06-19 09:45:20'),
(127, 'requirements index ajax_list', 'requirements_index_ajax_list', 'requirements index ajax_list', 'requirements', 'index', 'ajax_list', '2013-06-19 09:45:20'),
(128, 'employees index insert_validation', 'employees_index_insert_validation', 'employees index insert_validation', 'employees', 'index', 'insert_validation', '2013-06-19 09:49:14'),
(129, 'employees index insert', 'employees_index_insert', 'employees index insert', 'employees', 'index', 'insert', '2013-06-19 09:49:14'),
(130, 'employees index success', 'employees_index_success', 'employees index success', 'employees', 'index', 'success', '2013-06-19 09:49:14'),
(131, 'employees index edit', 'employees_index_edit', 'employees index edit', 'employees', 'index', 'edit', '2013-06-19 09:49:32'),
(132, 'employees index update_validation', 'employees_index_update_validation', 'employees index update_validation', 'employees', 'index', 'update_validation', '2013-06-19 09:50:56'),
(133, 'employees index update', 'employees_index_update', 'employees index update', 'employees', 'index', 'update', '2013-06-19 09:50:56'),
(134, 'products create_product_employee', 'products_create_product_employee', 'products create_product_employee', 'products', 'create_product_employee', '0', '2013-06-19 09:58:21'),
(135, 'pages index update_validation', 'pages_index_update_validation', 'pages index update_validation', 'pages', 'index', 'update_validation', '2013-06-21 09:58:35'),
(136, 'pages index update', 'pages_index_update', 'pages index update', 'pages', 'index', 'update', '2013-06-21 09:58:35'),
(137, 'pages index success', 'pages_index_success', 'pages index success', 'pages', 'index', 'success', '2013-06-21 09:58:35'),
(138, 'shareds getdistricts 5', 'shareds_getdistricts_5', 'shareds getdistricts 5', 'shareds', 'getdistricts', '5', '2013-06-22 10:39:42'),
(139, 'shareds getdistricts 4', 'shareds_getdistricts_4', 'shareds getdistricts 4', 'shareds', 'getdistricts', '4', '2013-06-22 10:39:49'),
(140, 'shareds getVillages 2', 'shareds_getVillages_2', 'shareds getVillages 2', 'shareds', 'getVillages', '2', '2013-06-22 10:39:51'),
(141, 'shareds getdistricts 3', 'shareds_getdistricts_3', 'shareds getdistricts 3', 'shareds', 'getdistricts', '3', '2013-06-22 11:00:00'),
(142, 'products index ajax_list_info', 'products_index_ajax_list_info', 'products index ajax_list_info', 'products', 'index', 'ajax_list_info', '2013-06-22 11:01:30'),
(143, 'products index ajax_list', 'products_index_ajax_list', 'products index ajax_list', 'products', 'index', 'ajax_list', '2013-06-22 11:01:55'),
(144, 'products index edit', 'products_index_edit', 'products index edit', 'products', 'index', 'edit', '2013-06-22 12:07:41'),
(145, 'shareds get_all_area_option 2', 'shareds_get_all_area_option_2', 'shareds get_all_area_option 2', 'shareds', 'get_all_area_option', '2', '2013-06-22 12:11:40'),
(146, 'products update', 'products_update', 'products update', 'products', 'update', '0', '2013-06-22 14:27:56'),
(147, 'products index edit%5D', 'products_index_edit%5D', 'products index edit%5D', 'products', 'index', 'edit%5D', '2013-06-22 14:30:25'),
(148, 'shareds get_all_area_option 1', 'shareds_get_all_area_option_1', 'shareds get_all_area_option 1', 'shareds', 'get_all_area_option', '1', '2013-06-22 15:15:23'),
(149, 'users index update_validation', 'users_index_update_validation', 'users index update_validation', 'users', 'index', 'update_validation', '2013-06-23 04:18:08'),
(150, 'users index update', 'users_index_update', 'users index update', 'users', 'index', 'update', '2013-06-23 04:18:08'),
(151, 'products index delete', 'products_index_delete', 'products index delete', 'products', 'index', 'delete', '2013-06-23 04:29:37'),
(152, 'employees index upload_file', 'employees_index_upload_file', 'employees index upload_file', 'employees', 'index', 'upload_file', '2013-06-24 05:01:12'),
(153, 'customers index edit', 'customers_index_edit', 'customers index edit', 'customers', 'index', 'edit', '2013-06-24 15:40:25'),
(154, 'customers index update_validation', 'customers_index_update_validation', 'customers index update_validation', 'customers', 'index', 'update_validation', '2013-06-24 15:40:49'),
(155, 'customers index update', 'customers_index_update', 'customers index update', 'customers', 'index', 'update', '2013-06-24 15:40:50'),
(156, 'customers index success', 'customers_index_success', 'customers index success', 'customers', 'index', 'success', '2013-06-24 15:40:50'),
(157, 'customers index insert_validation', 'customers_index_insert_validation', 'customers index insert_validation', 'customers', 'index', 'insert_validation', '2013-06-24 15:55:46'),
(158, 'customers index insert', 'customers_index_insert', 'customers index insert', 'customers', 'index', 'insert', '2013-06-24 15:55:47'),
(159, 'products add_product_customer', 'products_add_product_customer', 'products add_product_customer', 'products', 'add_product_customer', '0', '2013-06-24 18:54:32'),
(160, 'customers index delete', 'customers_index_delete', 'customers index delete', 'customers', 'index', 'delete', '2013-06-24 19:40:02'),
(161, 'cities getCities 2', 'cities_getCities_2', 'cities getCities 2', 'cities', 'getCities', '2', '2013-06-27 08:39:15'),
(162, 'shareds getdistricts 2', 'shareds_getdistricts_2', 'shareds getdistricts 2', 'shareds', 'getdistricts', '2', '2013-06-27 08:39:17'),
(163, 'features index insert_validation', 'features_index_insert_validation', 'features index insert_validation', 'features', 'index', 'insert_validation', '2013-08-06 10:33:23'),
(164, 'features index insert', 'features_index_insert', 'features index insert', 'features', 'index', 'insert', '2013-08-06 10:33:23'),
(165, 'slideshow', 'frontend_slideshow', '<p>\n	halaman frontend slide show</p>\n', 'frontends', 'slideshows', '', '2013-08-05 17:00:00'),
(166, 'features index success', 'features_index_success', 'features index success', 'features', 'index', 'success', '2013-08-06 10:33:23'),
(167, 'pages index insert_validation', 'pages_index_insert_validation', 'pages index insert_validation', 'pages', 'index', 'insert_validation', '2013-08-06 11:06:29'),
(168, 'pages index insert', 'pages_index_insert', 'pages index insert', 'pages', 'index', 'insert', '2013-08-06 11:06:30'),
(169, 'app_categories index add', 'app_categories_index_add', 'app_categories index add', 'app_categories', 'index', 'add', '2013-08-06 11:11:08'),
(170, 'pages get_container profiles', 'pages_get_container_profiles', 'pages get_container profiles', 'pages', 'get_container', 'profiles', '2013-08-06 11:31:50'),
(171, 'pages get_container home', 'pages_get_container_home', 'pages get_container home', 'pages', 'get_container', 'home', '2013-08-06 11:37:07'),
(172, 'pages get_container 1', 'pages_get_container_1', 'pages get_container 1', 'pages', 'get_container', '1', '2013-08-06 11:37:43'),
(173, 'pages get_container 6', 'pages_get_container_6', 'pages get_container 6', 'pages', 'get_container', '6', '2013-08-06 11:57:47'),
(174, 'pages get_container 7', 'pages_get_container_7', 'pages get_container 7', 'pages', 'get_container', '7', '2013-08-06 11:57:50'),
(175, 'pages get_container 8', 'pages_get_container_8', 'pages get_container 8', 'pages', 'get_container', '8', '2013-08-06 11:57:52'),
(176, 'pages get_container 9', 'pages_get_container_9', 'pages get_container 9', 'pages', 'get_container', '9', '2013-08-06 11:57:54'),
(177, 'pages create_attribute_pages', 'pages_create_attribute_pages', 'pages create_attribute_pages', 'pages', 'create_attribute_pages', '0', '2013-08-06 15:12:08'),
(178, 'pages delete_atttribute_pages 2', 'pages_delete_atttribute_pages_2', 'pages delete_atttribute_pages 2', 'pages', 'delete_atttribute_pages', '2', '2013-08-07 04:32:19'),
(179, 'pages delete_atttribute_pages 3', 'pages_delete_atttribute_pages_3', 'pages delete_atttribute_pages 3', 'pages', 'delete_atttribute_pages', '3', '2013-08-07 04:32:48'),
(180, 'pages delete_attribute_pages 2', 'pages_delete_attribute_pages_2', 'pages delete_attribute_pages 2', 'pages', 'delete_attribute_pages', '2', '2013-08-07 04:43:10'),
(181, 'pages delete_attribute_pages 1', 'pages_delete_attribute_pages_1', 'pages delete_attribute_pages 1', 'pages', 'delete_attribute_pages', '1', '2013-08-07 04:43:36'),
(182, 'pages delete_attribute_pages 5', 'pages_delete_attribute_pages_5', 'pages delete_attribute_pages 5', 'pages', 'delete_attribute_pages', '5', '2013-08-07 04:47:23'),
(183, 'pages update_attribute_pages', 'pages_update_attribute_pages', 'pages update_attribute_pages', 'pages', 'update_attribute_pages', '0', '2013-08-08 03:12:44'),
(184, 'pages get_container 10', 'pages_get_container_10', 'pages get_container 10', 'pages', 'get_container', '10', '2013-08-08 07:06:50'),
(185, 'pages update_pages', 'pages_update_pages', 'pages update_pages', 'pages', 'update_pages', '0', '2013-08-08 09:59:54'),
(186, 'pages get_container 11', 'pages_get_container_11', 'pages get_container 11', 'pages', 'get_container', '11', '2013-08-08 10:12:45'),
(187, 'pages get_container 12', 'pages_get_container_12', 'pages get_container 12', 'pages', 'get_container', '12', '2013-08-08 10:12:47'),
(188, 'pages get_container 13', 'pages_get_container_13', 'pages get_container 13', 'pages', 'get_container', '13', '2013-08-08 10:12:48'),
(189, 'pages get_container 14', 'pages_get_container_14', 'pages get_container 14', 'pages', 'get_container', '14', '2013-08-08 10:12:50'),
(190, 'pages maps', 'pages_maps', 'pages maps', 'pages', 'maps', '0', '2013-08-08 12:03:28'),
(191, 'students index', 'students_index', 'students index', 'students', 'index', '0', '2013-06-16 11:52:31'),
(192, 'app_menus index insert_validation', 'app_menus_index_insert_validation', 'app_menus index insert_validation', 'app_menus', 'index', 'insert_validation', '2013-08-11 11:20:03'),
(193, 'app_menus index insert', 'app_menus_index_insert', 'app_menus index insert', 'app_menus', 'index', 'insert', '2013-08-11 11:20:04'),
(194, 'students index add', 'students_index_add', 'students index add', 'students', 'index', 'add', '2013-08-11 11:21:06'),
(195, 'curriculums index', 'curriculums_index', 'curriculums index', 'curriculums', 'index', '0', '2013-08-11 11:21:06'),
(196, 'Academic years', 'academic_years_index', 'Academic_years', 'academic_years', 'index', '0', '2013-08-11 11:21:06'),
(197, 'sclasses', 'sclasses_index', 'classes', 'sclasses', 'index', '0', '2013-08-11 11:21:06'),
(198, 'teachers', 'teachers_index', 'teachers', 'teachers', 'index', '0', '2013-08-11 11:21:06'),
(199, 'teachers index add', 'teachers_index_add', 'teachers index add', 'teachers', 'index', 'add', '2013-08-23 15:28:45'),
(200, 'app_categories index edit', 'app_categories_index_edit', 'app_categories index edit', 'app_categories', 'index', 'edit', '2013-09-02 10:59:12'),
(201, 'app_categories index update_validation', 'app_categories_index_update_validation', 'app_categories index update_validation', 'app_categories', 'index', 'update_validation', '2013-09-02 10:59:22'),
(202, 'app_categories index update', 'app_categories_index_update', 'app_categories index update', 'app_categories', 'index', 'update', '2013-09-02 10:59:22'),
(203, 'application configuration', 'app_config_index', '<p>\n application configuration Backend</p>\n', 'app_config', 'index', '', '2013-09-01 17:00:00'),
(204, 'app_config index edit', 'app_config_index_edit', 'app_config index edit', 'app_config', 'index', 'edit', '2013-09-02 11:31:12'),
(205, 'app_config index update_validation', 'app_config_index_update_validation', 'app_config index update_validation', 'app_config', 'index', 'update_validation', '2013-09-02 11:32:33'),
(206, 'app_config index update', 'app_config_index_update', 'app_config index update', 'app_config', 'index', 'update', '2013-09-02 11:32:34'),
(207, 'app_config index success', 'app_config_index_success', 'app_config index success', 'app_config', 'index', 'success', '2013-09-02 11:32:34'),
(208, 'features index update_validation', 'features_index_update_validation', 'features index update_validation', 'features', 'index', 'update_validation', '2013-09-02 11:38:51'),
(209, 'features index update', 'features_index_update', 'features index update', 'features', 'index', 'update', '2013-09-02 11:38:51'),
(210, 'pages get_container 15', 'pages_get_container_15', 'pages get_container 15', 'pages', 'get_container', '15', '2013-09-02 11:58:14'),
(211, 'pages add_dinamic_pages', 'pages_add_dinamic_pages', 'pages add_dinamic_pages', 'pages', 'add_dinamic_pages', '0', '2013-09-02 12:50:35'),
(212, 'pages delete_attribute_pages 18', 'pages_delete_attribute_pages_18', 'pages delete_attribute_pages 18', 'pages', 'delete_attribute_pages', '18', '2013-09-02 14:02:51'),
(213, 'pages delete_attribute_pages 17', 'pages_delete_attribute_pages_17', 'pages delete_attribute_pages 17', 'pages', 'delete_attribute_pages', '17', '2013-09-02 14:02:57'),
(214, 'pages delete_attribute_pages 16', 'pages_delete_attribute_pages_16', 'pages delete_attribute_pages 16', 'pages', 'delete_attribute_pages', '16', '2013-09-02 14:03:03'),
(215, 'pages get_container 16', 'pages_get_container_16', 'pages get_container 16', 'pages', 'get_container', '16', '2013-09-02 14:24:56'),
(216, 'pages delete_attribute_pages 34', 'pages_delete_attribute_pages_34', 'pages delete_attribute_pages 34', 'pages', 'delete_attribute_pages', '34', '2013-09-05 03:07:42'),
(217, 'pages delete_attribute_pages 31', 'pages_delete_attribute_pages_31', 'pages delete_attribute_pages 31', 'pages', 'delete_attribute_pages', '31', '2013-09-20 23:17:50'),
(218, 'pages delete_attribute_pages 25', 'pages_delete_attribute_pages_25', 'pages delete_attribute_pages 25', 'pages', 'delete_attribute_pages', '25', '2013-09-21 00:30:08'),
(219, 'pages delete_attribute_pages 26', 'pages_delete_attribute_pages_26', 'pages delete_attribute_pages 26', 'pages', 'delete_attribute_pages', '26', '2013-09-21 00:30:14'),
(220, 'pages delete_attribute_pages 27', 'pages_delete_attribute_pages_27', 'pages delete_attribute_pages 27', 'pages', 'delete_attribute_pages', '27', '2013-09-21 00:30:20'),
(221, 'news', 'news_index', '<p>\n halaman berita</p>\n', 'news', 'index', '', '2013-09-21 18:34:00'),
(222, 'app_categories index insert_validation', 'app_categories_index_insert_validation', 'app_categories index insert_validation', 'app_categories', 'index', 'insert_validation', '2013-09-21 13:35:46'),
(223, 'app_categories index insert', 'app_categories_index_insert', 'app_categories index insert', 'app_categories', 'index', 'insert', '2013-09-21 13:35:47'),
(224, 'app_categories index success', 'app_categories_index_success', 'app_categories index success', 'app_categories', 'index', 'success', '2013-09-21 13:35:47'),
(225, 'dashboard make_base', 'dashboard_make_base', 'dashboard make_base', 'dashboard', 'make_base', '0', '2013-09-21 14:20:04'),
(226, 'dashboard migration', 'dashboard_migration', 'dashboard migration', 'dashboard', 'migration', '0', '2013-09-21 14:41:58'),
(227, 'pages get_container 17', 'pages_get_container_17', 'pages get_container 17', 'pages', 'get_container', '17', '2013-09-25 23:24:02'),
(228, 'pages delete_attribute_pages 32', 'pages_delete_attribute_pages_32', 'pages delete_attribute_pages 32', 'pages', 'delete_attribute_pages', '32', '2013-09-26 00:58:26'),
(229, 'pages delete_attribute_pages 19', 'pages_delete_attribute_pages_19', 'pages delete_attribute_pages 19', 'pages', 'delete_attribute_pages', '19', '2013-09-26 01:14:37'),
(230, 'pages delete_attribute_pages 15', 'pages_delete_attribute_pages_15', 'pages delete_attribute_pages 15', 'pages', 'delete_attribute_pages', '15', '2013-09-26 01:14:42'),
(231, 'pages delete_attribute_pages 10', 'pages_delete_attribute_pages_10', 'pages delete_attribute_pages 10', 'pages', 'delete_attribute_pages', '10', '2013-09-26 01:14:46'),
(232, 'pages delete_attribute_pages 9', 'pages_delete_attribute_pages_9', 'pages delete_attribute_pages 9', 'pages', 'delete_attribute_pages', '9', '2013-09-26 01:14:50'),
(233, 'pages delete_attribute_pages 8', 'pages_delete_attribute_pages_8', 'pages delete_attribute_pages 8', 'pages', 'delete_attribute_pages', '8', '2013-09-26 01:14:54'),
(234, 'pages delete_attribute_pages 7', 'pages_delete_attribute_pages_7', 'pages delete_attribute_pages 7', 'pages', 'delete_attribute_pages', '7', '2013-09-26 01:14:59'),
(235, 'app_config index add', 'app_config_index_add', 'app_config index add', 'app_config', 'index', 'add', '2013-09-30 05:05:43'),
(236, 'pages get_container 18', 'pages_get_container_18', 'pages get_container 18', 'pages', 'get_container', '18', '2013-09-30 06:31:14'),
(237, 'pages profiles', 'pages_profiles', 'pages profiles', 'pages', 'profiles', '0', '2013-09-30 06:52:35'),
(238, 'pages get_container 20', 'pages_get_container_20', 'pages get_container 20', 'pages', 'get_container', '20', '2013-09-30 11:20:15'),
(239, 'pages get_container 21', 'pages_get_container_21', 'pages get_container 21', 'pages', 'get_container', '21', '2013-09-30 11:22:29'),
(240, 'pages get_container 22', 'pages_get_container_22', 'pages get_container 22', 'pages', 'get_container', '22', '2013-09-30 11:23:50'),
(241, 'pages get_container 23', 'pages_get_container_23', 'pages get_container 23', 'pages', 'get_container', '23', '2013-09-30 11:25:48'),
(242, 'news index', 'news_index', '', 'news', 'index', '', '2013-09-30 17:06:34'),
(243, 'pages packet_list', 'pages_packet_list', 'pages packet_list', 'pages', 'packet_list', '0', '2013-09-30 22:12:14'),
(244, 'daftar paket', 'pages_packet_list', '<p>\n daftar paket</p>\n', 'pages', 'packet_list', '', '2013-10-01 03:17:22'),
(245, 'pages detail_packet_list', 'pages_detail_packet_list', 'pages detail_packet_list', 'pages', 'detail_packet_list', '0', '2013-10-01 02:44:01'),
(246, 'app_config index edit.html', 'app_config_index_edit.html', 'app_config index edit.html', 'app_config', 'index', 'edit.html', '2013-10-02 11:59:57'),
(247, 'pages update_packet_list', 'pages_update_packet_list', 'pages update_packet_list', 'pages', 'update_packet_list', '0', '2013-10-03 03:04:37'),
(248, 'pages add_departure', 'pages_add_departure', 'pages add_departure', 'pages', 'add_departure', '0', '2013-10-03 06:07:07'),
(249, 'pages get_container 19', 'pages_get_container_19', 'pages get_container 19', 'pages', 'get_container', '19', '2013-10-04 01:27:37'),
(250, 'pages delete_attribute_pages 33', 'pages_delete_attribute_pages_33', 'pages delete_attribute_pages 33', 'pages', 'delete_attribute_pages', '33', '2013-10-04 09:45:48'),
(251, 'pages delete_attribute_pages 29', 'pages_delete_attribute_pages_29', 'pages delete_attribute_pages 29', 'pages', 'delete_attribute_pages', '29', '2013-10-04 09:54:19'),
(252, 'pages delete_attribute_pages 14', 'pages_delete_attribute_pages_14', 'pages delete_attribute_pages 14', 'pages', 'delete_attribute_pages', '14', '2013-10-04 09:58:23'),
(253, 'pages delete_attribute_pages 12', 'pages_delete_attribute_pages_12', 'pages delete_attribute_pages 12', 'pages', 'delete_attribute_pages', '12', '2013-10-04 09:58:45'),
(254, 'pages delete_attribute_pages 13', 'pages_delete_attribute_pages_13', 'pages delete_attribute_pages 13', 'pages', 'delete_attribute_pages', '13', '2013-10-04 10:01:36'),
(255, 'kategori berita', 'news_categories_index', '<p>\n news_categories_index</p>\n', 'news_categories', 'index', '', '2013-10-05 13:03:40'),
(256, 'pages get_container 24', 'pages_get_container_24', 'pages get_container 24', 'pages', 'get_container', '24', '2013-10-05 08:12:14'),
(257, 'videos', 'videos_index', '<p>\n videos_index</p>\n', 'videos', 'index', '', '2013-10-05 13:18:33'),
(258, 'videos index add', 'videos_index_add', 'videos index add', 'videos', 'index', 'add', '2013-10-05 08:18:58'),
(259, 'pages get_container 25', 'pages_get_container_25', 'pages get_container 25', 'pages', 'get_container', '25', '2013-10-05 08:20:37'),
(260, 'pages get_container 26', 'pages_get_container_26', 'pages get_container 26', 'pages', 'get_container', '26', '2013-10-05 08:45:20'),
(261, 'pages get_container aaa.com', 'pages_get_container_aaa.com', 'pages get_container aaa.com', 'pages', 'get_container', 'aaa.com', '2013-10-05 10:07:49'),
(262, 'news index add', 'news_index_add', 'news index add', 'news', 'index', 'add', '2013-10-05 11:19:56'),
(263, 'pages add_packet', 'pages_add_packet', 'pages add_packet', 'pages', 'add_packet', '0', '2013-10-09 11:57:44'),
(264, 'pages edit_packet_price', 'pages_edit_packet_price', 'pages edit_packet_price', 'pages', 'edit_packet_price', '0', '2013-10-09 13:01:18'),
(265, 'pages update_packet_price', 'pages_update_packet_price', 'pages update_packet_price', 'pages', 'update_packet_price', '0', '2013-10-09 13:26:04'),
(266, 'pages create_attribute_packet', 'pages_create_attribute_packet', 'pages create_attribute_packet', 'pages', 'create_attribute_packet', '0', '2013-10-10 12:21:13'),
(267, 'videos index edit', 'videos_index_edit', 'videos index edit', 'videos', 'index', 'edit', '2013-10-11 07:26:03'),
(268, 'videos index insert_validation', 'videos_index_insert_validation', 'videos index insert_validation', 'videos', 'index', 'insert_validation', '2013-10-11 07:27:38'),
(269, 'videos index insert', 'videos_index_insert', 'videos index insert', 'videos', 'index', 'insert', '2013-10-11 07:27:38'),
(270, 'videos index success', 'videos_index_success', 'videos index success', 'videos', 'index', 'success', '2013-10-11 07:27:38'),
(271, 'videos index update_validation', 'videos_index_update_validation', 'videos index update_validation', 'videos', 'index', 'update_validation', '2013-10-11 07:27:51'),
(272, 'videos index update', 'videos_index_update', 'videos index update', 'videos', 'index', 'update', '2013-10-11 07:27:51'),
(273, 'news test_grab', 'news_test_grab', 'news test_grab', 'news', 'test_grab', '0', '2013-10-12 10:53:20'),
(274, 'news assets images', 'news_assets_images', 'news assets images', 'news', 'assets', 'images', '2013-10-12 10:53:25'),
(275, 'new_autonews', 'news_autonews', '<p>\n page_autonews</p>\n', 'news', 'autonews', '', '2013-10-14 09:23:35'),
(276, 'news grab_news', 'news_grab_news', 'news grab_news', 'news', 'grab_news', '0', '2013-10-14 04:32:36'),
(277, 'pages delete_departure 2', 'pages_delete_departure_2', 'pages delete_departure 2', 'pages', 'delete_departure', '2', '2013-10-16 09:22:58'),
(278, 'news autonews ajax_list_info', 'news_autonews_ajax_list_info', 'news autonews ajax_list_info', 'news', 'autonews', 'ajax_list_info', '2013-10-16 10:31:53'),
(279, 'news autonews ajax_list', 'news_autonews_ajax_list', 'news autonews ajax_list', 'news', 'autonews', 'ajax_list', '2013-10-16 10:31:53'),
(280, 'news autonews delete', 'news_autonews_delete', 'news autonews delete', 'news', 'autonews', 'delete', '2013-10-16 10:32:03'),
(281, 'news autonews edit', 'news_autonews_edit', 'news autonews edit', 'news', 'autonews', 'edit', '2013-10-17 11:26:27'),
(282, 'news aaa', 'news_aaa', 'news aaa', 'news', 'aaa', '0', '2013-10-17 11:27:09'),
(283, 'news publish', 'news_publish', 'news publish', 'news', 'publish', '0', '2013-10-17 11:27:32'),
(284, 'news auto_publish 8', 'news_auto_publish_8', 'news auto_publish 8', 'news', 'auto_publish', '8', '2013-10-17 11:27:46'),
(285, 'news auto_publish 9', 'news_auto_publish_9', 'news auto_publish 9', 'news', 'auto_publish', '9', '2013-10-17 11:57:08'),
(286, 'news auto_publish 10', 'news_auto_publish_10', 'news auto_publish 10', 'news', 'auto_publish', '10', '2013-10-18 03:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `grab_news`
--

CREATE TABLE IF NOT EXISTS `grab_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `status` enum('new','find_link','publish') NOT NULL,
  `name_content` text NOT NULL,
  `title_content` text NOT NULL,
  `description_content` text NOT NULL,
  `from` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `grab_news`
--

INSERT INTO `grab_news` (`id`, `name`, `url`, `status`, `name_content`, `title_content`, `description_content`, `from`, `created_at`, `updated_at`) VALUES
(9, 'http://haji.kemenag.go.id/assets/images/berita/gatot1.jpg', 'jamaah_haji_nonkuota_menurun_drastis', 'publish', 'Jamaah Haji Nonkuota Menurun Drastis', 'Jamaah Haji Nonkuota Menurun Drastis', '\n Makkah (Sinhat)--Jumlah haji Indonesia nonkuota pada musim haji 1434 hijriah menurun drastis karena aturan Pemerintah Saudi sangat ketat termasuk memberikan sanksi bagi penduduk setempat yang memfasilitasi mereka.\n \n Dubes Indonesia untuk Arab Saudi Gatot Abdullah Mansur kepada wartawan di Media Center Haji Daker Mekkah, Rabu, mengatakan jumlahnya sekarang sangat sedikit sekitar puluhan orang sementara biasanya mencapai ribuan orang.\n \n &ldquo;Orang Indonesia yang terbukti melanggar aturan menggunakan visa haji maka akan segera dideportasi dan mereka tidak boleh masuk ke Arab Saudi selama 10 tahun,&rdquo; katanya.\n \n Selain itu, ia menjelaskan, bagi penduduk setempat yang kedapatan memberikan fasilitas untuk haji bagi para mukiman (istilah untuk pelanggar visa haji) maka akan dibawa ke meja hijau dengan tuntutan satu tahun penjara serta fasilitas haji lainnya disita termasuk kendaraan yang mengangkut para mukimin.\n \n Ia mengungkapkan, saat ini Staf di Konsul Jenderal Republik Indonesia (KJRI) yang akan umroh saja tetap harus menggunakan jasa travel dan tidak bisa seperti dulu dengan hanya dikoordinir KJRI Jeddah. &ldquo;Ada seorang diplomat dari negara lain yang tidak tahu aturan umroh, tetap saja dihukum,&rdquo; katanya.\n \n Tidak hanya untuk negara lain, Pemerintah Arab Saudi juga mulai membatasi haji bagi warganya sendiri sampai 50 persen dari kuota sebelumnya.\n \n Pantauan di lapangan, masih ada jamaah haji yang tidak menggunakan visa haji sehingga otomatis mereka tidak mendapat fasilitas kendaraan antar jemput serta tidak mendapat maktab di Arafah dan Mina. Mereka biasanya tidur di masjid-masjid bahkan saat mabit mereka berdiam di sembarang tempat mulai dari trotoar jalan sampai kawasan perbukitan.(MCH/Rep)\n \n &nbsp;', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/910', '2013-10-17 18:56:55', '2013-10-17 18:56:55'),
(10, 'http://haji.kemenag.go.id/assets/images/berita/kbih.jpg', 'amirul_haj_wacanakan_perbanyak_visa_untuk_pembimbing_ibadah_kbih', 'new', ' Amirul Haj Wacanakan Perbanyak Visa untuk Pembimbing Ibadah KBIH', ' Amirul Haj Wacanakan Perbanyak Visa untuk Pembimbing Ibadah KBIH', '\n Makkah (Sinhat)--Proses pembimbingan ibadah terhadap jemaah selama di Mekah dinilai belum maksimal. Oleh karena itu, Amirul Haj yang juga Menteri Agama Suryadharma Ali mewacanakan untuk memperbanyak pemberian visa bagi pembimbing ibadah dari KBIH.\n \n &quot;Banyak kasus yang terjadi terkait dengan ibadah. Ada jemaah yang tak mau tawaf dan sa&#39;i karena sudah mencium Hajar Aswad. Ada yang tidak tahu apa itu sa&#39;i dan tahallul. Banyak sekali yang begitu,&quot; katanya ketika ditanya Tim MCH, di Kantor Daerah Kerja (Daker) Mekah, Rabu (16/10).\n \n Menurut dia, selama ini, ada dua pihak yang bertugas dalam pembimbingan ibadah, yakni pembimbing KBIH dan petugas yang direkrut oleh Kementerian Agama. &quot;Selama ini, kuota pembimbing KBIH masih sedikit. Oleh karena itu, selama berada di Arab Saudi, tugas mereka dibantu oleh para petugas yang direkrut oleh Kementerian Agama. Sekarang, persoalannya, apakah jemaah mau &#39;nurut&#39; sama petugas kita ataukah kepada pembimbing KBIH. Mana yang lebih efektif?&quot; ujarnya.\n \n Atas dasar itulah, ia mengaku akan mewacanakan penambahan visa untuk pembimbing KBIH. Soalnya ia memandang bahwa hal itu akan lebih efektif untuk memaksimalkan ibadah jemaah. (MCH/Pr)\n &nbsp;', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/911', '2013-10-17 18:56:58', '2013-10-17 18:56:58'),
(11, 'http://haji.kemenag.go.id/assets/images/berita/jam-02.jpg', 'banyak_jamaah_mabit_di_sekitar_jamarat', 'new', 'Banyak Jamaah Mabit di Sekitar Jamarat', 'Banyak Jamaah Mabit di Sekitar Jamarat', '\n Mekkah (Sinhat)--Banyak jamaah memilih melakukan &quot;mabit&quot; atau bermalam di sekitar Jamarat di Mina dan tidak di maktab yang sudah ditentukan, sebagian merupakan warga lokal Arab Saudi dan jamaah mandiri yang tidak mempunyai maktab.\n \n Pantauan Selasa malam sampai Rabu pagi, ribuan orang terus berdatangan memenuhi halaman jamarat, bahkan meluber sampai trotoar jalan dan median jalan mulai depan Al Malik sampai bagian bawah lorong menuju Jamarat.\n \n Petugas kepolisian setempat dibuat kelabakan karena begitu mengusir satu keluarga akan muncul belasan keluarga lain yang menggelar tikar. Mereka bilang &quot;maktab-maktab&quot; artinya diminta bermalam di tenda masing-masing.\n \n Satu keluarga dari lokal Arab Saudi sampai bersitegang karena dipaksa pindah, padahal mereka sudah menjelaskan tidak punya maktab dan sudah berkali-kali diusir, sehingga hanya trotoar jalan yang menjadi pilihannya.\n \n Tidak hanya troroar yang menjadi incaran, tetapi juga lantai atas sebuah toilet di Jamarat yang hanya dihubungkan dengan tangga kecil juga diburu para jamaah. Mereka memilih dekat dengan jamarat karena setelah bermalam maka pagi hari mereka langsung melempar jumroh yang ada di depan mereka.\n \n Polisi kesulitan membubarkan mereka, sehingga sampai selepas tengah malam, akhirnya polisi membiarkan mereka menguasai halaman Jamarat dan berbagai sudut lain di sekitar Jamarat.\n \n Baru sekitar pukul 03.00 dini hari tiga mobil patroli masuk ke halaman Jamarat dan terus membunyikan sirine untuk membubarkan kerumunan jamaah yang tengah tertidur. Suara sirine terus meraung sampai menjelang subuh, sehingga halaman Jamarat mulai terbebas dari kerumunan jamaah.\n \n Bagi mereka yang mendapat maktab di Mina Jadid maka bermalam di sekitar Jamarat menjadi pilihan tepat karena jarak tempuh dari Mina Jadid sampai Jamarat sekitar sembilan kilometer karena harus memutar melalui Terowongan Muaisim.\n \n Mereka bisa bergerak sehabis magrib ke Jamarat, lalu bermalam dan pagi subuh mereka melempar jumroh. Setelah Shalat Subuh mereka bisa kembali ke Maktab saat udara masih segar.\n \n Sebagian kecil jamaah haji Indonesia juga melakukan pola itu, namun tidak semua tertarik karena melempar jumroh saat menjelag subuh dianggap tidak afdol.\n \n Sayangnya, keterbatasan toilet di areal Jamarat membuat jamaah harus antre sampai setengah jam untuk mendapat giliran.\n \n Bagi jamaah yang ingin bergerak ke Jamarat lebih baik membuang air seni lebih dulu, dan jangan terlalu banyak minum untuk menghindari keinginan buang air seni.(MCH/Ant)', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/912', '2013-10-17 18:56:59', '2013-10-17 18:56:59'),
(12, 'http://haji.kemenag.go.id/assets/images/berita/min-001.jpg', 'tak_paham_bahasa_arab,_ratusan_jamaah_haji_tersasar', 'new', 'Tak Paham Bahasa Arab, Ratusan Jamaah Haji  Tersasar', 'Tak Paham Bahasa Arab, Ratusan Jamaah Haji  Tersasar', '\n Makkah (Sinhat)--Sekitar 500 jamaah haji Indonesia tersesat dan tidak mengetahui arah pulang ke maktab mereka karena tak paham papan petunjuk arah yang memakai bahasa Arab dan Melayu.\n \n Data itu didapat dari sejumlah pos petugas. Jumlah yang tersesat dipastikan lebih banyak lagi karena banyak juga yang diselesaikan antarjamaah sendiri.\n \n &ldquo;Petugas yang sudah pernah mukim di Mekkah saja ada yang tersesat, apalagi jamaah haji terutama yang berusia lanjut,&rdquo; kata Amirul Haj/Menteri Agama, Suryadharma Ali, saat mengunjungi Media Center Haji di Daerah Kerja (Daker) Makkah, Rabu (16/10).\n \n Menag yang juga Amirul Haj Amirul Haj itu mengakui lokasi Jamarot sekarang memang cukup membingungkan, karena ada jembatan layang dan persimpangan. Sehingga mereka yang sudah berkali-kali datang juga sering harus menghafal lebih dulu.\n \n &ldquo;Lokasinya sekarang banyak perbedaan, terutama adanya beberapa terowongan baru,&rdquo; katanya.\n \n Menurut Suryadharma, jika izin untuk membuat petunjuk berbahasa Indonesia itu dikabulkan Pemerintah Arab Saudi, maka pihaknya akan memasang papan arah, terutama di lokasi-lokasi strategis seperti Jamarot, Masjidil Haram, Mina dan Arafah. &ldquo;Kami akan terus melobi, agar upaya ini bisa dipenuhi,&rdquo; katanya.\n \n Terkait dengan sebelas kendaraan roda dua yang membantu mengantarkan jamaah tersesat, Menag mengakui jumlah kendaraan itu tidak cukup sehingga tahun depan kalau memungkinkan akan ditambah sampai 50 kendaraan.\n \n &ldquo;Nanti akan ditempatkan di pos-pos tertentu agar jamaah yang tersesat segera diantarkan ke pondokan atau maktabnya,&rdquo; tutup Menag.(MCH/Rep)\n \n &nbsp;', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/913', '2013-10-17 18:57:01', '2013-10-17 18:57:01'),
(13, 'http://haji.kemenag.go.id/assets/images/berita/saudiu_emb.jpg', 'jamarat_padat,_polisi_pagar_betis', 'new', 'Jamarat Padat, Polisi Pagar Betis', 'Jamarat Padat, Polisi Pagar Betis', '\n Makkah(Sinhat)--Puncak kepadatan di Jamarat, &nbsp;tempat melempar jumroh di Mina terjadi Kamis &nbsp;siang atau setelah Zawal, posisi matahari tergelincir ke arah Timur, sehingga kepolisian setempat membuat pagar betis ketat agar jamaah tidak saling bertabrakan.', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/915', '2013-10-18 10:40:45', '2013-10-18 10:40:45'),
(14, 'http://haji.kemenag.go.id/assets/images/berita/sakit_mina1.JPG', 'ruangan_bphi_mina_tak_akan_pernah_representatif', 'new', 'Ruangan BPHI Mina Tak Akan Pernah Representatif', 'Ruangan BPHI Mina Tak Akan Pernah Representatif', '\n Makkah (Sinhat)--Fasilitas ruangan Balai Pengobatan Haji Indonesia (BPHI) di Mina tidak akan pernah representatif. Hal itu diakui oleh Menteri Agama selaku Amirulhaj Suryadharma Ali saat meninjau BPHI di Mina, Rabu (16/10) malam WAS.\n \n &quot;Tidak akan pernah representatif karena memang serbadarurat. Yang kita butuhkan adalah ruangan yang lebih luas sehingga bisa menampung lebih banyak pasien,&quot; ujarnya.\n \n Dalam kesempatan itu, ia mengaku bersyukur karena puncak musim haji tahun ini, hingga hari tasyrik pertama, tingkat kunjungan pasien menurun. &quot;Itu pun sebagian besar lantaran kelelahan dan penyakit bawaan dari tanah air, seperti darah tinggi, diabetes, dan vertigo. Selain itu, sebagian besar dari mereka mengalami dehidrasi karena kurang minum. Akibatnya penyakit yang selama ini tersembunyi jadi muncul,&quot; tuturnya.\n \n Hal senada diungkapkan oleh Kepala Bidang Kesehatan Panitia Penyelenggara Ibadah Haji (PPIH) Arab Saudi dr Fidiansjah. Ia pun mengaku bersyukur lantaran jumlah jemaah wafat, hingga Rabu (16/10/2013) pukul 18.00 WAS, &quot;hanya&quot; 89 orang. &quot;Sementara tahun 2012, pada periode yang sama, jumlah jemaah wafat mencapai 141 orang dan tahun 2011 mencapai 145 orang. Alhamdulillah, perbaikan sistem yang kami lakukan berjalan dengan baik, yakni pendekatan koordinasi,&quot; ucapnya. (MCH/Pr)\n &nbsp;', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/916', '2013-10-18 10:40:46', '2013-10-18 10:40:46'),
(15, 'http://haji.kemenag.go.id/assets/images/berita/wafat_di_mina.jpg', 'enam_orang_jamaah_haji_wafat_di_mina', 'new', 'Enam Orang Jamaah Haji Wafat di Mina', 'Enam Orang Jamaah Haji Wafat di Mina', '\n Makkah (Sinhat)--Hingga malam kedua mabit di&nbsp; Mina, Rabu malam waktu setempat, sudah enam jemaah haji Indonesia wafat.\n \n Menurut data dari Sistem Komputerisasi dan Informasi Haji Terpadu (Siskohat) Kesehatan, Kamis dinihari, selain enam jemaah yang wafat,terdapat tiga jemaah yang harus menjalani rawat inap.\n \n Sedangkan jemaah yang harus dirujuk ke Rumah Sakit Arab Saudi sebanyak 16 orang.&nbsp; Adapun yang menjalani rawat jalan di BalaiPengobatan Haji Indonesia (BPHI) Mina sebanyak 147 jemaah.\n \n Hingga saat ini total jemaah haji Indonesia yang wafat sebanyak 89 orang. Sementara itu, jemaah di Jamarat yang akan melontar jumroh semakin padat. Ribuan jemaah yang hendak melempar jumroh bergabung dengan yang mereka yang mabit dengan mengelar tikar disekeliling Jamarat membuat lokasi tersebut&nbsp; semakin padat. (MCH/Ant)&nbsp;', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/917', '2013-10-18 10:40:51', '2013-10-18 10:40:51'),
(16, 'http://haji.kemenag.go.id/assets/images/berita/bus_kemenag.jpg', 'jamaah_haji_mulai_tinggalkan_mina', 'new', 'Jamaah Haji  Mulai Tinggalkan Mina', 'Jamaah Haji  Mulai Tinggalkan Mina', '\n Mina (Sinhat)--Setelah dua malam masa mabit (bermalam) di Mina untuk melontar jumrah, gelombang kepulangan jemaah haji kembali ke Mekah mulai berlangsung, Kamis (17/10).', 'http://haji.kemenag.go.id/index.php/subMenu/informasi/berita/detailberita/918', '2013-10-18 10:40:52', '2013-10-18 10:40:52');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `status` enum('aktif','nonaktif') DEFAULT NULL,
  `deskripsi` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `nama`, `status`, `deskripsi`, `date_created`, `date_updated`) VALUES
(2, 'marketing', 'aktif', '<p>\n	pengelola gudang</p>\n', '2013-04-02 00:00:00', '2013-04-02 00:00:00'),
(99, 'superadmin', 'aktif', 'super admin == super man == super herp but that not god', '2012-04-28 01:04:21', '2012-04-28 01:04:21'),
(100, 'Operator', 'aktif', '<p>\n	group operator</p>\n', '2013-06-18 00:00:00', NULL),
(101, 'manager', 'aktif', '<p>\n	halaman pengolahan manager</p>\n', '2013-06-18 00:00:00', NULL),
(102, 'sekretaris', 'nonaktif', '<p>\n	operator data</p>\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `group_features`
--

CREATE TABLE IF NOT EXISTS `group_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`,`feature_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1481 ;

--
-- Dumping data for table `group_features`
--

INSERT INTO `group_features` (`id`, `group_id`, `feature_id`, `created_at`) VALUES
(1293, 100, 1, '2013-06-19 06:19:24'),
(1296, 100, 2, '2013-06-19 06:19:24'),
(1295, 2, 2, '2013-06-19 06:19:24'),
(1329, 100, 16, '2013-06-19 06:19:24'),
(1299, 100, 3, '2013-06-19 06:19:24'),
(1326, 100, 15, '2013-06-19 06:19:24'),
(1302, 100, 4, '2013-06-19 06:19:24'),
(1323, 100, 14, '2013-06-19 06:19:24'),
(1305, 100, 7, '2013-06-19 06:19:24'),
(1320, 100, 13, '2013-06-19 06:19:24'),
(1317, 100, 12, '2013-06-19 06:19:24'),
(1314, 100, 11, '2013-06-19 06:19:24'),
(1292, 2, 1, '2013-06-19 06:19:24'),
(1294, 101, 1, '2013-06-19 06:19:24'),
(1297, 101, 2, '2013-06-19 06:19:24'),
(1298, 2, 3, '2013-06-19 06:19:24'),
(1300, 101, 3, '2013-06-19 06:19:24'),
(1301, 2, 4, '2013-06-19 06:19:24'),
(1303, 101, 4, '2013-06-19 06:19:24'),
(1304, 2, 7, '2013-06-19 06:19:24'),
(1306, 101, 7, '2013-06-19 06:19:24'),
(1308, 100, 8, '2013-06-19 06:19:24'),
(1311, 100, 9, '2013-06-19 06:19:24'),
(1307, 2, 8, '2013-06-19 06:19:24'),
(1309, 101, 8, '2013-06-19 06:19:24'),
(1310, 2, 9, '2013-06-19 06:19:24'),
(1312, 101, 9, '2013-06-19 06:19:24'),
(1313, 2, 11, '2013-06-19 06:19:24'),
(1315, 101, 11, '2013-06-19 06:19:24'),
(1316, 2, 12, '2013-06-19 06:19:24'),
(1318, 101, 12, '2013-06-19 06:19:24'),
(1319, 2, 13, '2013-06-19 06:19:24'),
(1321, 101, 13, '2013-06-19 06:19:24'),
(1322, 2, 14, '2013-06-19 06:19:24'),
(1324, 101, 14, '2013-06-19 06:19:24'),
(1325, 2, 15, '2013-06-19 06:19:24'),
(1327, 101, 15, '2013-06-19 06:19:24'),
(1328, 2, 16, '2013-06-19 06:19:24'),
(1330, 101, 16, '2013-06-19 06:19:24'),
(1331, 2, 17, '2013-06-19 06:19:24'),
(1333, 101, 17, '2013-06-19 06:19:24'),
(1334, 2, 18, '2013-06-19 06:19:24'),
(1335, 101, 18, '2013-06-19 06:19:24'),
(1336, 2, 19, '2013-06-19 06:19:24'),
(1337, 101, 19, '2013-06-19 06:19:24'),
(1338, 2, 20, '2013-06-19 06:19:24'),
(1339, 101, 20, '2013-06-19 06:19:24'),
(1340, 2, 21, '2013-06-19 06:19:24'),
(1341, 101, 21, '2013-06-19 06:19:24'),
(1342, 2, 22, '2013-06-19 06:19:24'),
(1343, 101, 22, '2013-06-19 06:19:24'),
(1344, 2, 23, '2013-06-19 06:19:24'),
(1345, 101, 23, '2013-06-19 06:19:24'),
(1346, 2, 24, '2013-06-19 06:19:24'),
(1347, 101, 24, '2013-06-19 06:19:24'),
(1348, 2, 25, '2013-06-19 06:19:24'),
(1349, 101, 25, '2013-06-19 06:19:24'),
(1350, 2, 26, '2013-06-19 06:19:24'),
(1351, 101, 26, '2013-06-19 06:19:24'),
(1352, 2, 27, '2013-06-19 06:19:24'),
(1353, 101, 27, '2013-06-19 06:19:24'),
(1354, 2, 28, '2013-06-19 06:19:24'),
(1355, 101, 28, '2013-06-19 06:19:24'),
(1356, 2, 29, '2013-06-19 06:19:24'),
(1357, 101, 29, '2013-06-19 06:19:24'),
(1358, 2, 30, '2013-06-19 06:19:24'),
(1359, 101, 30, '2013-06-19 06:19:24'),
(1360, 2, 31, '2013-06-19 06:19:24'),
(1362, 101, 31, '2013-06-19 06:19:24'),
(1363, 2, 32, '2013-06-19 06:19:24'),
(1364, 101, 32, '2013-06-19 06:19:24'),
(1365, 2, 33, '2013-06-19 06:19:24'),
(1366, 101, 33, '2013-06-19 06:19:24'),
(1367, 2, 34, '2013-06-19 06:19:24'),
(1368, 101, 34, '2013-06-19 06:19:24'),
(1369, 2, 35, '2013-06-19 06:19:24'),
(1370, 101, 35, '2013-06-19 06:19:24'),
(1371, 2, 36, '2013-06-19 06:19:24'),
(1372, 101, 36, '2013-06-19 06:19:24'),
(1373, 2, 37, '2013-06-19 06:19:24'),
(1374, 101, 37, '2013-06-19 06:19:24'),
(1375, 2, 38, '2013-06-19 06:19:24'),
(1376, 101, 38, '2013-06-19 06:19:24'),
(1377, 2, 39, '2013-06-19 06:19:24'),
(1378, 101, 39, '2013-06-19 06:19:24'),
(1379, 2, 40, '2013-06-19 06:19:24'),
(1380, 101, 40, '2013-06-19 06:19:24'),
(1381, 2, 41, '2013-06-19 06:19:24'),
(1382, 101, 41, '2013-06-19 06:19:24'),
(1383, 2, 42, '2013-06-19 06:19:24'),
(1384, 101, 42, '2013-06-19 06:19:24'),
(1385, 2, 43, '2013-06-19 06:19:24'),
(1386, 101, 43, '2013-06-19 06:19:24'),
(1387, 2, 44, '2013-06-19 06:19:24'),
(1388, 101, 44, '2013-06-19 06:19:24'),
(1389, 2, 45, '2013-06-19 06:19:24'),
(1390, 101, 45, '2013-06-19 06:19:24'),
(1391, 2, 46, '2013-06-19 06:19:24'),
(1392, 101, 46, '2013-06-19 06:19:24'),
(1393, 2, 47, '2013-06-19 06:19:24'),
(1394, 101, 47, '2013-06-19 06:19:24'),
(1395, 2, 48, '2013-06-19 06:19:24'),
(1396, 101, 48, '2013-06-19 06:19:24'),
(1397, 2, 49, '2013-06-19 06:19:24'),
(1398, 101, 49, '2013-06-19 06:19:24'),
(1399, 2, 50, '2013-06-19 06:19:24'),
(1400, 101, 50, '2013-06-19 06:19:24'),
(1401, 2, 51, '2013-06-19 06:19:24'),
(1402, 101, 51, '2013-06-19 06:19:24'),
(1403, 2, 52, '2013-06-19 06:19:24'),
(1404, 101, 52, '2013-06-19 06:19:24'),
(1405, 2, 53, '2013-06-19 06:19:24'),
(1406, 101, 53, '2013-06-19 06:19:24'),
(1407, 2, 54, '2013-06-19 06:19:24'),
(1408, 101, 54, '2013-06-19 06:19:24'),
(1409, 2, 55, '2013-06-19 06:19:24'),
(1410, 101, 55, '2013-06-19 06:19:24'),
(1411, 2, 56, '2013-06-19 06:19:24'),
(1412, 101, 56, '2013-06-19 06:19:24'),
(1413, 2, 57, '2013-06-19 06:19:24'),
(1414, 101, 57, '2013-06-19 06:19:24'),
(1415, 2, 58, '2013-06-19 06:19:24'),
(1416, 101, 58, '2013-06-19 06:19:24'),
(1417, 2, 59, '2013-06-19 06:19:24'),
(1418, 101, 59, '2013-06-19 06:19:24'),
(1419, 2, 60, '2013-06-19 06:19:24'),
(1420, 101, 60, '2013-06-19 06:19:24'),
(1421, 2, 61, '2013-06-19 06:19:24'),
(1422, 101, 61, '2013-06-19 06:19:24'),
(1423, 2, 62, '2013-06-19 06:19:24'),
(1424, 101, 62, '2013-06-19 06:19:24'),
(1425, 2, 63, '2013-06-19 06:19:24'),
(1426, 101, 63, '2013-06-19 06:19:24'),
(1427, 2, 64, '2013-06-19 06:19:24'),
(1428, 101, 64, '2013-06-19 06:19:24'),
(1429, 2, 65, '2013-06-19 06:19:24'),
(1430, 101, 65, '2013-06-19 06:19:24'),
(1431, 2, 66, '2013-06-19 06:19:24'),
(1432, 101, 66, '2013-06-19 06:19:24'),
(1433, 2, 67, '2013-06-19 06:19:24'),
(1434, 101, 67, '2013-06-19 06:19:24'),
(1435, 2, 68, '2013-06-19 06:19:24'),
(1436, 101, 68, '2013-06-19 06:19:24'),
(1437, 2, 69, '2013-06-19 06:19:24'),
(1438, 101, 69, '2013-06-19 06:19:24'),
(1439, 2, 70, '2013-06-19 06:19:24'),
(1440, 101, 70, '2013-06-19 06:19:24'),
(1441, 2, 71, '2013-06-19 06:19:24'),
(1442, 101, 71, '2013-06-19 06:19:24'),
(1443, 2, 72, '2013-06-19 06:19:24'),
(1444, 101, 72, '2013-06-19 06:19:24'),
(1445, 2, 73, '2013-06-19 06:19:24'),
(1446, 101, 73, '2013-06-19 06:19:24'),
(1447, 2, 74, '2013-06-19 06:19:24'),
(1448, 101, 74, '2013-06-19 06:19:24'),
(1449, 2, 75, '2013-06-19 06:19:24'),
(1450, 101, 75, '2013-06-19 06:19:24'),
(1451, 2, 76, '2013-06-19 06:19:24'),
(1452, 101, 76, '2013-06-19 06:19:24'),
(1453, 2, 77, '2013-06-19 06:19:24'),
(1454, 101, 77, '2013-06-19 06:19:24'),
(1455, 2, 78, '2013-06-19 06:19:24'),
(1456, 101, 78, '2013-06-19 06:19:24'),
(1457, 2, 79, '2013-06-19 06:19:24'),
(1458, 101, 79, '2013-06-19 06:19:24'),
(1459, 2, 80, '2013-06-19 06:19:24'),
(1460, 101, 80, '2013-06-19 06:19:24'),
(1461, 2, 81, '2013-06-19 06:19:24'),
(1462, 101, 81, '2013-06-19 06:19:24'),
(1463, 2, 82, '2013-06-19 06:19:24'),
(1464, 101, 82, '2013-06-19 06:19:24'),
(1465, 2, 83, '2013-06-19 06:19:24'),
(1466, 101, 83, '2013-06-19 06:19:24'),
(1467, 2, 84, '2013-06-19 06:19:24'),
(1468, 101, 84, '2013-06-19 06:19:24'),
(1469, 2, 85, '2013-06-19 06:19:24'),
(1470, 101, 85, '2013-06-19 06:19:24'),
(1471, 2, 86, '2013-06-19 06:19:24'),
(1472, 101, 86, '2013-06-19 06:19:24'),
(1473, 2, 87, '2013-06-19 06:19:24'),
(1474, 101, 87, '2013-06-19 06:19:24'),
(1475, 2, 88, '2013-06-19 06:19:24'),
(1476, 101, 88, '2013-06-19 06:19:24'),
(1477, 2, 89, '2013-06-19 06:19:24'),
(1478, 101, 89, '2013-06-19 06:19:24'),
(1286, 2, 90, '2013-06-17 19:30:25'),
(1361, 100, 31, '2013-06-19 06:19:24'),
(1479, 101, 90, '2013-06-19 06:19:24'),
(1289, 2, 91, '2013-06-17 19:30:25'),
(1332, 100, 17, '2013-06-19 06:19:24'),
(1480, 101, 91, '2013-06-19 06:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(13);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `description` text,
  `user` int(11) DEFAULT NULL,
  `news_category_id` int(11) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `status` enum('publish','unpublish') NOT NULL,
  `comment_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `image` varchar(100) NOT NULL,
  `seo_keywords` varchar(200) NOT NULL,
  `seo_description` varchar(200) NOT NULL,
  `is_internal` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `name`, `title`, `url`, `description`, `user`, `news_category_id`, `tag`, `status`, `comment_id`, `created_at`, `updated_at`, `image`, `seo_keywords`, `seo_description`, `is_internal`) VALUES
(1, 'Minta Kuota Haji, Pemprov Aceh Janji Lobi Kerajaan Arab Saudi', 'Minta Kuota Haji, Pemprov Aceh Janji Lobi Kerajaan Arab Saudi', 'Minta_Kuota_Haji_Pemprov_Aceh_Janji_Lobi_Kerajaan_Arab_Saudi', '<p>BANDA ACEH - Pemerintah Aceh berjanji melobi Kerajaan Arab Saudi, meminta penambahan kuota haji provinsi. Rencana tersebut menyusul banyaknya calon jamaah haji (CJH) yang sudah masuk daftar tunggu.\n \nDaftar tunggu haji Aceh sudah mencapai 55.329 orang, artinya jika dengan jatah kuota sekarang yaitu 3.147 orang, maka butuh 17 tahun baru selesai memberangkatkan mereka ke tanah suci.\n\n"Kami akan bersama-sama dengan Kementerian Agama akan melobi kerajaan Arab Saudi agar dapat menyetujui penambahan kuota haji di masa mendatang," kata Gubernur Aceh, Zaini Abdullah saat melepas pemberangkatan CJH kelompok terbang pertama di Asrama Embarkasi Haji Banda Aceh, Minggu (29/9/2013).\n\nMenurutnya, lobi kuota itu untuk mengurangi pengurangan antrian panjang daftar tunggu calon haji Aceh. Diakuinya, minat warga Serambi Mekkah untuk naik haji terus meningkat dari tahun ke tahun.\n\nSelain pada Kerajaan Arab Saudi, Zaini juga memohon agar Pemerintah Pusat bisa menambah kuota haji Aceh.\n\nTahun ini, Aceh memberangkatkan 3.147 JCH dalam delapan kelompok terbang. Kuota ini lebih kecil dibandingkan jumlah tahun lalu, menyusul adanya pemotongan kuota haji 20 persen akibat adanya rehabilitasi Masjidil Haram.\n\nKloter pertama sebanyak 433 JCH akan berangkat melalui Bandara International Sultan Iskandar Muda Blang Bintang, Aceh Besar sore tadi. Tujuh jamaah yang sedianya berangkat dalam kloter satu batal berangkat hari ini karena sakit. Mereka akan disisipkan ke kloter lainnya.</p>', 1, 1, 'haji,umroh', 'publish', 0, '2013-10-01 00:00:00', '2013-10-01 00:00:00', 'circling_the_kabah_te.jpg', 'Minta Kuota Haji, Pemprov Aceh Janji Lobi Kerajaan Arab Saudi', 'Minta Kuota Haji, Pemprov Aceh Janji Lobi Kerajaan Arab Saudi', 1),
(2, 'Ibadah Haji Tahun Ini Lebih Baik', 'Ibadah Haji Tahun Ini Lebih Baik', 'Ibadah_Haji_Tahun_Ini_Lebih_Baik', '<p>KARANGANYAR- Sekertaris PP Muhammaadiyah Miftah menilai, penyelenggaraan musim haji tahun ini jauh lebih baik dibandingkan tahun sebelumnya. \n\nHanya saja, Miftah meminta kepada pemerintah, khususnya Kementerian Agama, mencari solusi transportasi haji di tanah suci.\n\n“Tahun ini penyelenggaran haji jauh lebih baik, hanya saja pemerintah harus memikirkan solusi untuk transportasi. Pengadaan transportasi mutlak diperlukan karena lokasi masjidil Haram relatif masih jauh dari pemondokan, seiring adanya pelebaran kawasan Masjdil Haram oleh Pemerintah Arab Saudi," papar Miftah di Karanganyar, Jawa Tengah.\n\nMiftah berterima kasih kepada Pemerintah yang telah memikirkan masalah pemondokan para jemaah haji. Tahun ini, pemondokan para jamaah haji jauh lebih bagus dibandingkan sebelumnya.\n\nDari sisi fasilitas jauh lebih komplit, belum lagi fasilitas perbankan di lokasi pemondokan yang memudahkan jamaah haji. "Jadi calon haji tidak perlu takut lagi bila membawa uang banyak, sudah dilengkapi linked perbankan," jelasnya.\n\nPP Muhammdiyah berharap pemerintah tetap mempertahankan fasilitas haji seperti tahun ini di tahun-tahun berikutnya.	 (kem)</p>', 1, 1, 'haji,umroh', 'publish', 1, '2013-10-01 00:00:00', '2013-10-01 00:00:00', 'circling_the_kabah_te.jpg', 'Ibadah Haji Tahun Ini Lebih Baik', 'Ibadah Haji Tahun Ini Lebih Baik', 1),
(3, 'http://haji.kemenag.go.id/assets/images/berita/gatot1.jpg', 'Jamaah Haji Nonkuota Menurun Drastis', 'jamaah_haji_nonkuota_menurun_drastis', '\n Makkah (Sinhat)--Jumlah haji Indonesia nonkuota pada musim haji 1434 hijriah menurun drastis karena aturan Pemerintah Saudi sangat ketat termasuk memberikan sanksi bagi penduduk setempat yang memfasilitasi mereka.\n \n Dubes Indonesia untuk Arab Saudi Gatot Abdullah Mansur kepada wartawan di Media Center Haji Daker Mekkah, Rabu, mengatakan jumlahnya sekarang sangat sedikit sekitar puluhan orang sementara biasanya mencapai ribuan orang.\n \n &ldquo;Orang Indonesia yang terbukti melanggar aturan menggunakan visa haji maka akan segera dideportasi dan mereka tidak boleh masuk ke Arab Saudi selama 10 tahun,&rdquo; katanya.\n \n Selain itu, ia menjelaskan, bagi penduduk setempat yang kedapatan memberikan fasilitas untuk haji bagi para mukiman (istilah untuk pelanggar visa haji) maka akan dibawa ke meja hijau dengan tuntutan satu tahun penjara serta fasilitas haji lainnya disita termasuk kendaraan yang mengangkut para mukimin.\n \n Ia mengungkapkan, saat ini Staf di Konsul Jenderal Republik Indonesia (KJRI) yang akan umroh saja tetap harus menggunakan jasa travel dan tidak bisa seperti dulu dengan hanya dikoordinir KJRI Jeddah. &ldquo;Ada seorang diplomat dari negara lain yang tidak tahu aturan umroh, tetap saja dihukum,&rdquo; katanya.\n \n Tidak hanya untuk negara lain, Pemerintah Arab Saudi juga mulai membatasi haji bagi warganya sendiri sampai 50 persen dari kuota sebelumnya.\n \n Pantauan di lapangan, masih ada jamaah haji yang tidak menggunakan visa haji sehingga otomatis mereka tidak mendapat fasilitas kendaraan antar jemput serta tidak mendapat maktab di Arafah dan Mina. Mereka biasanya tidur di masjid-masjid bahkan saat mabit mereka berdiam di sembarang tempat mulai dari trotoar jalan sampai kawasan perbukitan.(MCH/Rep)\n \n &nbsp;', 0, 2, NULL, 'publish', NULL, '2013-10-17 19:01:44', '2013-10-17 19:01:44', 'http://haji.kemenag.go.id/assets/images/berita/gatot1.jpg', 'Jamaah Haji Nonkuota Menurun Drastis', '\n Makkah (Sinhat)--Jumlah haji Indonesia nonkuota pada musim haji 1434 hijriah menurun drastis karena aturan Pemerintah Saudi sangat ketat termasuk memberikan sanksi bagi penduduk setempat yang memfas', 0);

-- --------------------------------------------------------

--
-- Table structure for table `news_categories`
--

CREATE TABLE IF NOT EXISTS `news_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `descriptions` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `news_categories`
--

INSERT INTO `news_categories` (`id`, `name`, `descriptions`, `created_at`, `updated_at`) VALUES
(1, 'umroh', 'kategori berita umroh', '2013-10-01 00:00:00', '2013-10-01 00:00:00'),
(2, 'haji', 'kategori berita haji', '2013-10-01 00:00:00', '2013-10-01 00:00:00'),
(3, 'curhat haji', 'kategori berita haji', '2013-10-01 00:00:00', '2013-10-01 00:00:00'),
(4, 'kisah haji', 'kategori berita haji', '2013-10-01 00:00:00', '2013-10-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `packets`
--

CREATE TABLE IF NOT EXISTS `packets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `attribute_page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_packets_attribute_pages1` (`attribute_page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `packets`
--

INSERT INTO `packets` (`id`, `name`, `title`, `description`, `created_at`, `updated_at`, `attribute_page_id`) VALUES
(2, 'bintang 5', NULL, ' <ul>\n<li>jarak 1 km</li>\n<li>makkah</li>\n<li>medinah</li>\n</ul>\n ', '2013-10-11 14:05:35', '2013-10-11 14:05:35', 20),
(3, 'bintang 3', NULL, '<ul>\n<li>jarak 1 km</li>\n<li>makkah</li>\n<li>medinah</li>\n</ul>', '2013-10-10 20:40:41', '2013-10-10 20:40:41', 20);

-- --------------------------------------------------------

--
-- Table structure for table `packet_prices`
--

CREATE TABLE IF NOT EXISTS `packet_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packet_id` int(11) NOT NULL,
  `min_single_price` bigint(20) DEFAULT NULL,
  `max_single_price` bigint(20) DEFAULT NULL,
  `min_double_price` bigint(20) DEFAULT NULL,
  `max_double_price` bigint(20) DEFAULT NULL,
  `min_triple_price` bigint(20) DEFAULT NULL,
  `max_triple_price` bigint(20) DEFAULT NULL,
  `min_quad_price` bigint(20) DEFAULT NULL,
  `max_quad_price` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` enum('active','nonactive') DEFAULT NULL,
  `kurs` varchar(45) DEFAULT NULL,
  `currency` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_packet_prices_1` (`packet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `packet_prices`
--

INSERT INTO `packet_prices` (`id`, `packet_id`, `min_single_price`, `max_single_price`, `min_double_price`, `max_double_price`, `min_triple_price`, `max_triple_price`, `min_quad_price`, `max_quad_price`, `user_id`, `created_at`, `updated_at`, `status`, `kurs`, `currency`) VALUES
(1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2013-10-09 19:25:12', '2013-10-09 19:25:12', 'nonactive', NULL, NULL),
(2, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-09 20:31:45', '2013-10-09 20:31:45', 'nonactive', NULL, NULL),
(3, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-09 20:35:10', '2013-10-09 20:35:10', 'nonactive', NULL, NULL),
(4, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-09 20:38:52', '2013-10-09 20:38:52', 'nonactive', NULL, NULL),
(5, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-09 20:39:04', '2013-10-09 20:39:04', 'nonactive', NULL, NULL),
(6, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-09 20:39:15', '2013-10-09 20:39:15', 'nonactive', NULL, NULL),
(7, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2013-10-10 20:40:41', '2013-10-10 20:40:41', 'active', NULL, NULL),
(8, 2, 1, 2, 3, 4, 5, 6, 7, 8, 1, '2013-10-10 20:43:23', '2013-10-10 20:43:23', 'nonactive', NULL, NULL),
(9, 2, 100, 200, 300, 400, 500, 600, 700, 800, 1, '2013-10-11 14:05:35', '2013-10-11 14:05:35', 'active', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `intitle` varchar(100) NOT NULL,
  `category` enum('static','dinamic','collaboration') NOT NULL DEFAULT 'static',
  `incontent` text NOT NULL,
  `url` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  `seo_keywords` text NOT NULL,
  `seo_description` text NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `frontend_menu_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `intitle`, `category`, `incontent`, `url`, `parent_id`, `date_created`, `date_updated`, `is_menu`, `seo_keywords`, `seo_description`, `is_admin`, `frontend_menu_order`) VALUES
(1, 'Profile       ', 'Profile       ', 'static', '<p  justify;">H. Didin Umarzen Besar di lingkungan Pesantren di Garut setelah menyelesaikan Sarjana Muda jurusan Akademi di Bandung. Atas jasa yang besar dari Ayahanda KH. Umarzen Th 1980 beliau berangkat ke Mekkah untuk bekerja di Sepupunya Muhamad Faruq Yamani, REEM TOUR Travel Mekkah. DI tempatkan sebagai Ticketing, setelah terlebih dahulu disekolahkan di Jeddah dan praktek di Malaysia Air Lines di Jeddah.</p>\n<p  justify;">REEM&nbsp; TOUR&nbsp; Travel&nbsp; Mekkah kemudian menjalin kemitraan&nbsp;&nbsp; dengan&nbsp;&nbsp; perusahan&nbsp;&nbsp; dari Indonesia&nbsp;&nbsp; yaitu&nbsp; PATUNA&nbsp; pimpinan&nbsp; Bp. H. Amirsyah&nbsp; Tabrani, sehingga REEM&nbsp; travel&nbsp; resmi&nbsp; menjadi&nbsp; perwakilan&nbsp; PT. PATUNA&nbsp; untuk&nbsp; di&nbsp; Saudi.<br />Dari&nbsp; sinilah H. Didin&nbsp; Umarzen&nbsp; dapat&nbsp; mengembangkan&nbsp; Akses&nbsp; Penyelenggaraan&nbsp; Haji&nbsp; dan&nbsp; Umrah&nbsp; seperti Akses&nbsp; ke Hotel, AirLines, Transportasi, dan Catering.<br />Pada tahun&nbsp; 1984 beliau kembali&nbsp; ke&nbsp; Indonesia,&nbsp; dan&nbsp; mendirikan perusahaan biro penyelenggara&nbsp; Haji&nbsp; dengan&nbsp; Nama&nbsp; PT. MEGA ARAFAH.&nbsp; Dengan&nbsp; Pembimbing&nbsp; Ibadahnya&nbsp; Bp. KH. AF Gazali SH.&nbsp; Perusahaan tersebut berkembang&nbsp; dengan&nbsp; baik,&nbsp; namun kemudian Kyai Haji menyarankan beliau untuk kembali&nbsp; ke Tanah Suci untuk berkosentrasi&nbsp; di Dakwah&nbsp; atau Mubaligh.<br />Pada tahun&nbsp; 1992&nbsp; beliau kembali&nbsp; ke&nbsp; Mekkah&nbsp; dan&nbsp; bekerja&nbsp; di&nbsp; Muasasah&nbsp; Osama&nbsp; Zeid&nbsp; Azraq.&nbsp;&nbsp; Di Divisi Haji&nbsp; &amp; Umrah&nbsp;&nbsp; dan&nbsp;&nbsp; langsung&nbsp; mengelola beberapa&nbsp; penyelenggara&nbsp; yang&nbsp; cukup&nbsp; ternama&nbsp; dari&nbsp; Bandung dan Jakarta. Di tahun&nbsp; 1995&nbsp; beliau kembali&nbsp; ke&nbsp; Bandung&nbsp; dan&nbsp; langsung&nbsp; berkerja&nbsp; di&nbsp; PT. KRISMA MUZDALIFAH.&nbsp;&nbsp; Sebagai Operator&nbsp; Umrah&nbsp; &amp;&nbsp; Haji&nbsp; sampai&nbsp; Tahun&nbsp; 2001. Pada Tahun&nbsp; 2001&nbsp; inilah kemudian beliau membeli&nbsp; PT. MADANIA SEMESTA WISATA, dengan menimbang beberapa faktor, sebagai berikut:</p>\n<ol  justify;">\n<li>Atas&nbsp; kemampuan&nbsp; yang&nbsp; kuat&nbsp; dan&nbsp; merasa&nbsp; sudah&nbsp; waktunya&nbsp; untuk memiliki&nbsp; Perusahaan sendiri.</li>\n<li>Beliau telah mengumpulkan dana yang pada akhirnya digunakan untuk membeli PT. MADANIA, walaupun&nbsp; dengan&nbsp; harga yang&nbsp; sangat&nbsp; mahal&nbsp; pada&nbsp; saat itu. Namun dengan pemikiran dan pertimbangan yang matang, karena setelah PT. MADANIA dan ( 9 PT ) lainnya&nbsp; tidak&nbsp; ada&nbsp; lagi Izin yang&nbsp; dikeluarkan&nbsp; oleh&nbsp; DEPAG.</li>\n<li>Saran dari&nbsp; Ustad Shamir&nbsp; Rokhaili&nbsp; pemilik&nbsp; Hotel Rawabi,&nbsp; Hotel&nbsp; Plaza&nbsp; Makkah&nbsp; &amp;&nbsp; Hotel&nbsp; Andalusia Mekkah&nbsp; Hotel yang&nbsp; selama&nbsp; di Mekkah&nbsp; digunakan jasanya oleh&nbsp; H. Didin Umrazen.</li>\n<li>Saran dari&nbsp; Bp H. Saifullah Rusyad &ndash; Pemilik&nbsp; PT. KRISMA MUZDALIFAH.</li>\n</ol>\n<p  justify;"><br /><strong>PT. MADANIA SEMESTA WISATA</strong>&nbsp; tumbuh dan berkembang pesat menjadi salah satu perusahaan penyelenggara Ibadah Haji dan Umrah di Indonesia yang mampu bertahan baik hingga dengan saat ini.</p>', 'profiles       ', 0, '2012-03-15 00:57:43', '2012-03-14 03:57:51', 1, '       profile madania semesta wisata     ', '     profile madania semesta wisata     ', 1, 0),
(7, 'Aktifitas', 'Aktifitas', 'static', '<p  0px; padding: 0px 0px 10px; color: rgb(0, 0, 0); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); ">\n Tatacara pelelangan adalah sebagaimana diatur dalam Peraturan Menteri Keuangan Republik Indonesia Nomor 93/PMK.06/2010 tentang Petunjuk Pelaksanaan Lelang.</p>\n<ol  0px 0px 0px 16px; padding: 0px 0px 10px; list-style-position: outside; color: rgb(0, 0, 0); font-family: Georgia, ''Times New Roman'', Times, serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 19px; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); ">\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Setiap pelaksanaan lelang harus dilakukan oleh dan/atau dihadapan Pejabat Lelang kecuali ditentukan lain oleh undang-undang atau peraturan pemerintah.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Untuk pelaksanaan lelang ditetapkan harga limit dan uang jaminan yang harus disetorkan oleh peserta lelang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Pengumuman lelang dilakukan melalui harian yang terbit di kota/kabupaten atau kota/kabupaten terdekat atau ibukotapropinsi atau ibukota negara dan beredar di wilayah kerja KPKNL atau wilayah Pejabat Lelang Kelas II &nbsp;tempat barang akan dilelang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Untuk dapat turut serta dalam pelelangan, para peserta lelang diwajibkan menyetor uang jaminan yang jumlahnya dicantumkan pejabat lelang, uang mana akan diperhitungkan dengan harga pembelian jika peserta lelang yang bersangkutan ditunjuk sebagai pembeli.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Penjualan lelang dilakukan dengan penawaran lisan dengan harga naik-naik.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Penawar/pembeli dianggap bersungguh-sungguh telah mengetahui apa yang telah ditawar/dibeli olehnya. Apabila terdapat kekurangan atau kerusakan baik yang terlihat atau tidak terlihat atau terdapat cacat lainnya terhadap barang yang telah dibelinya itu maka ia tidak berhak untuk menolak menarik diri kembali setelah pembeliannya disahkan dan melepaskan semua hak untuk meminta ganti kerugian berupa apapun juga.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Pembeli lelang adalah penawar tertinggi yang mencapai dan atau melampaui harga limit yang disahkan oleh Pejabat Lelang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Pembayaran dilaksanakan selambat-lambatnya 3 (tiga) hari kerja setelah pelaksanaan lelang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Pembeli tidak diperkenankan untuk menguasai barang yang telah dibelinya itu sebelum uang pembelian dipenuhi/dilunasi seluruhnya, jadi harga pokok, bea lelang dan uang miskin. Kepada pembeli lelang diserahkan tanda terima.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Dalam setiap pelaksanaan lelang dibuat Risalah Lelang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Barang terjual pada saat itu juga menjadi hak dan tanggungan pembeli dan apabila barang itu berupa tanah dan rumah, pembeli harus segera mengurus/membalik nama hak tersebut atas namanya.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Apabila yang dilelang itu adalah tanah/tanah dan rumah yang sedang ditempati/dikuasai oleh tersita lelang dan tersita lelang tidak bersedia menyerahkan tanah/tanah dan rumah itu secara kosong maka terlelang beserta keluarganya akan dikeluarkan dengan paksa apabila perlu dengan bantuan yang berwajib dari tanah/tanah dan rumah tersebut.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Termasuk orang-orang yang dikeluarkan dari tanah/tanah dan rumah adalah para penyewa, pembeli, orang yang mendapat hibah, yang memperoleh tanah/tanah dan rumah tersebut setelah tanah/tanah dan rumah tersebut disita dan sita telah didaftarkan sesuai dengan ketentuan undang-undang.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Mereka yang menyewa, menerima sebagai jaminan, membeli atau memperoleh tanah/tanah dan rumah tersebut sebelum dilakukan penyitaan, baik sita jaminan atau sita eksekutorial tidak dapat dikeluarkan secara paksa dari tahan/tanah dan rumah. Pembeli lelang harus menempuh jalan damai dengan mereka atau mengajukan gugatan ke pengadilan dengan prosedur biasa.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Hipotik atau hak tanggungan yang didaftarkan di kantor pertanahan setelah tanah disita maka tidak mempunyai kekuatan hukum.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Suatu pelelangan yang telah dilakukan sesuai dengan peraturan yang berlaku tidak dapat dibatalkan.</li>\n <li  0px 0px 0px 16px; padding: 0px; ">\n  Dalam hal terjadi kecurangan atau pelelangan dilaksanakan dengan ceroboh dan tidak sesuai dengan peraturan yang berlaku, pelelangan tersebut dapat dibatalkan melalui suatu gugatan yang diajukan kepada pengadilan negeri.</li>\n</ol>\n', 'aktifitas', 0, '2012-03-27 20:24:23', '2012-03-26 23:24:52', 0, '<p>\n kegiatan madania semesta wisata, travel umroh madania semesta wisata,&nbsp;</p>\n', '<p>\n kegiatan madania semesta wisata, travel umroh madania semesta wisata,</p>\n', 1, 0),
(8, 'Berita', 'Berita', 'dinamic', '<p>\n	berita</p>\n', 'news', 0, '2013-08-06 00:00:00', '2013-08-05 17:00:00', 1, '', '', 0, 0),
(9, 'Kontak Kami', 'Madani travel | Kontak Kami ', 'static', '<p>\n kontak kami</p>\n', 'contact_us', 0, '2013-08-06 00:00:00', '2013-08-05 17:00:00', 0, 'madania travel , umroh haji bandung , madaniatravel.com , umroh murah bandung 2013, jln cijagra bandung jawa barat indonesia', 'kontak kami madania travel umroh dan haji yang berada di kota bandung . hubungi kami madania tour travel ', 0, 0),
(10, 'Visi    ', 'Visi      ', 'static', '<p>Menjadi perusahaan dan mitra terbaik Pemerintah dalam bidang penyelenggaraan perjalanan ibadah Haji dan Umrah.</p>', 'visi', 1, '2013-08-08 00:00:00', '2013-08-07 17:00:00', 0, ' visi pt madania semesta wisata ', '  Menjadi perusahaan dan mitra terbaik Pemerintah dalam bidang penyelenggaraan perjalanan ibadah Haji dan Umrah. ', 1, 0),
(12, 'Gallery  ', 'Gallery', 'static', '<p>&nbsp;&nbsp; kegiatan selama umroh dan haji, foto kegiatan umroh dan haji madania travel, kegiatan yang dilakukan oleh madania semesta wisata ketika umroh dan haji</p>', 'Gallery_selama_kegiatan_umroh_dan_haji  ', 0, '2013-08-08 00:00:00', '2013-08-07 17:00:00', 1, ' kegiatan selama umroh dan haji, foto kegiatan umroh dan haji madania travel\n ', ' kegiatan selama umroh dan haji, foto kegiatan umroh dan haji madania travel, kegiatan yang dilakukan oleh madania semesta wisata ketika umroh dan haji\n ', 1, 0),
(13, 'testimonial ', 'Testimonial dari Jammah madania travel ', 'static', '<p>Halaman untuk mengelola Testimonial</p>', 'testimonial_dari_jamaah_madania_travel ', 7, '2013-08-08 00:00:00', '2013-08-07 17:00:00', 0, '    testimonial madania travel , pesan dan kesan madania semesta wisata  ', '      testimonial madania travel , pesan dan kesan madania semesta wisata  ', 0, 0),
(15, 'paket  ', 'paket  ', 'collaboration', '<p><a href="aaa.com" target="_blank">daftar paket</a></p>', 'daftar_paket  ', 0, '2013-09-02 00:00:00', '2013-09-01 17:00:00', 0, '    ', '    ', 1, 0),
(16, 'slideshow ', 'slideshow ', '', '<p>Halaman Pengaturan untuk merubah dan menambahkan slideshow Foto yang akan muncul pada halaman awal website madaniatravel.com.</p>\n<p>Silahkan Tambahkan Gambar beserta deskripsi yang diinginkan.</p>', 'slideshow', 0, '2013-09-03 00:00:00', '2013-09-02 19:24:48', 0, '', '', 1, 0),
(17, 'home', 'Madania travel', 'static', '<p>\n halaman home</p>\n', 'home', 0, '2013-09-26 02:00:45', '2013-09-25 19:00:49', 0, 'madania travel , umroh haji bandung , madaniatravel.com , umroh murah bandung 2013, jln cijagra bandung jawa barat indonesia', '\n kontak kami madania travel umroh dan haji yang berada di kota bandung . hubungi kami madania tour travel\n', 1, 0),
(18, 'Service ', 'Services ', 'static', '<p>\n Products Dan Services</p>\n', 'product_and_service_madania_travel ', 0, '2013-09-30 18:45:07', '2013-09-30 11:45:10', 0, 'Products Dan Services madania travel\n', 'Products Dan Services madania travel\n', 0, 2),
(19, 'FAQ    ', 'Faq ', 'static', '<p><span  #333333; font-family: arial, helvetica, clean, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: 16px; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: #ffffff; display: inline !important; float: none;">FAQ Adalah Suatu Tanya - Jawab Yang Mana Itu Bisa Di jawab Dan Bisa Share Secara Bersama - Sama Dengan Join Bersama Di Dalam Satu Komunitas Atau Join Di Suatu Bidang Usaha Yang Sama</span></p>', 'FAQ    ', 0, '2013-09-30 19:03:43', '2013-09-30 12:03:45', 1, '    FAQ madania travel umroh dan haji\n    ', '    FAQ seputar pertanyaan umroh dan haji saat mendaftarkan pada madania travel\n    ', 1, 3),
(20, 'latar belakang   ', 'latar belakang   ', 'static', '<p>Pertumbuhan calon jemaah haji Indonesia&nbsp; dalam setiap tahunnya mengalami kenaikan yang cukup signifikan. Dilihat dari calon jemaah haji yang menunggu 5 sampai 10 tahun&nbsp; mendaftar sebagai haji reguler. <br /><br />Sementara jatah quota haji yang diberikan oleh pemerintah Saudi Arabia untuk haji adalah 200 ribu dalam setiap tahunnya, sehingga&nbsp; keinginan masyarakat indonesia yang mayoritas beragama Islam untuk segera beribadah Haji jelas tidak tercukupi, mengingat kapasitas quota yang dberikan.<br />Ibadah haji adalah bagian penting dalam agama islam yang mana termasuk kepada rukun islam yang ke lima, sehingga wajib hukumnya bagi umat muslim yang sudah merasa mampu untuk melaksanakan ibadah haji.<br /><br />Kenaikan minat haji yang sangat signifikan tersebut mendorong masyarakat muslim Indonesia untuk segera mendaftarkan diri untuk menjadi calon jemaah haji resmi yang terdaftar di kementrian agama, untuk menghindari dari daftar calon jemaah haji waiting list yang terus bertambah dalam setiap harinya. <br />Hal tersebut sangat berdampak positif bagi biro-biro perjalanan wisata khususnya yang bergerak dalam pelayanan umroh dan haji khusus, dimana secara langsung dijadikan masyarakat idonesia solusi tepat dan cepat untuk merealisasikan keinginannya berkunjung ke baitullah. Mendaftarkan diri ke biro penyelenggara ibadah umroh dan haji khusus, baik dalam melaksanakan umroh ataupun haji khusus tergolong cepat dari pada mendaftar ke haji reguler ke kementrian agama walaupun dari segi biaya untuk mendaftar di Haji khusus sangat jauh lebih mahal. Hal ini dilihat dari semakin bertambahnya peningkatan calon jemaah umroh dan haji khusus yang terdaftar di PT Madania Semesta Wisata, dan PT Kharisma Musdalifah utama yang rata-rata dalam setiap bulannya dapat memberangkatkan 1000 jamaah umroh dalam satu bulan dan 300 jemaah haji dalam setiap tahunnya.<br /><br />Kepercayaan dari pemerintah dengan keluarnya ijin pariwisata dan ijin umroh&nbsp; didapatkan oleh Madania pada tahun 2000 dan ijin haji pada tahun 2003, memperlihatkan bahwa PT. Madania Semesta Wisata sudah sangat berpengalaman dalam memberikan pelayanan penyelenggaraan ibadah umroh &amp; haji khusus. Hal ini ditunjang pula dari&nbsp; sebelum keluarnya izin dari pemerintah, dimana H.Didin Umarzen selaku pendiri dari madania sudah menjalankan dan bergerak dalam memberikan pelayanan sejak tahun 1981 yaitu menjadi perwakilan resmi dari travel umroh &amp; haji di saudi arabia, juga bekerja untuk malaysia airlines dan saudi airlines dalam bagian ticketing.<br /><br />Madania umroh dan haji khusus sudah mempunyai&nbsp; 35 cabang atau perwakilan yang terbagi ke beberapa wilayah seperti Jawa tengah, Pekan Baru Riau, Sulawesi Tenggara, Sulawesi Selatan , Kalimantan dan kota-kota lainnya. Sehingga, Madania dapat dikatakan sebagai perusahaan yang bergerak dalam sektor real jasa pariwisata dan jasa lainnya yang saling berhubungan untuk tujuan memberikan pelayanan maksimal dalam keberadaannya dan mampu mampu menjadi jasa travel &ldquo;one Stop Travel Service&rdquo;&nbsp; .<br />Pengalaman dari memberikan pelayanan umroh dan haji membuat Madania mulai mengembangkan diri ke usaha lainnya yang menunjang keberadaan Madania, yakni:<br /><br /></p>\n<ol>\n<li>Travel Boutique : yakni pelayanan ticketing domestic dan international, Reservation hotel domestic dan international, Dokumen perjalanan, M.I.C.E, Outbond training Program bersama Adventure Indonesia (B.A.I) dan jasa lainnya.</li>\n<li>Trans Boutique : Yakni perusahaan yang menyewakan kendaraan umum seperti Bus (Jet Bus, Microbus, Seat 59, 49, 47, 35), Elf, dan Alphard.</li>\n<li>RM Samara Cigawir : Perusahaan yang bergerak pada kuliner dengan konsep rumah makan sunda dengan luas tanah sebesar 3 hektar yang bertempat di arah Km 45 Jl Bandung-Tasikmalaya tepatnya di daerah Limbangan Kab.Garut.</li>\n</ol>\n<p>&nbsp;</p>', 'latar_belakang', 1, '2013-09-30 00:00:00', '2013-09-30 16:15:23', 0, '   latar belakang madania semesta wisata\n   ', '   latar belakang madania semesta wisata   ', 1, 2),
(21, 'misi  ', 'Misi  ', 'static', '<ol>\n<li>Menggali potensi wisata negara yang belum termanfaatkan secara maksimal.</li>\n<li>Memperlihatkan nilai-nilai religius melalui wisata rohani.</li>\n<li>Menghasilkan nilai tambah/value added baik bagi perusahaan maupun Indonesia.</li>\n<li>Mampu menciptakan dan menjalin hubungan baik dengan calon jemaah, pelanggan, masyarakat dan Pemerintah.</li>\n<li>Menghasilkan produk pilihan wisata Haji dan Umrah yang inovatif dalam rangka menciptakan kepuasan pelanggan dengan harga yang kompetitif.</li>\n<li>Memperluas jaringan pemasaran di seluruh kota besar Indonesia.</li>\n</ol>', 'misi', 1, '2013-09-30 23:16:44', '2013-09-30 16:16:47', 0, '  misi pendirian pt madania semesta wisata   ', '  misi pendirian pt madania semesta wisata   ', 0, 3),
(22, 'legalitas  ', 'Legalitas  ', 'static', '<p><strong>Surat Izin Usaha Kepariwisataan (SIUK/SIUP)</strong><br />Nama &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; : PT. Madania Semesta Wisata<br />Alamat Kantor&nbsp;&nbsp; &nbsp;: GRAHA MADANIA Jl. Cijagra No 22B, Buah Batu, Bandung<br />No Registrasi&nbsp;&nbsp; &nbsp;&nbsp; : Izin usaha kepariwisataan, 556/Herr SIUK 344/BPPT/2011<br />Akte Pendirian&nbsp;&nbsp; &nbsp;: Notaris Dra. Siti Mariam dan Era Hardja. SH, no 30 tgl 29-4-2009</p>', 'legalitas', 1, '2013-09-30 23:18:02', '2013-09-30 16:18:05', 0, '  legalitas pendirian pt madania semesta wisata   ', '  legalitas pendirian pt madania semesta wisata   ', 0, 2),
(23, 'asita  ', 'Asita  ', 'static', '<p>Nama&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : PT. Madania Semesta Wisata<br />Alamat Kantor&nbsp;&nbsp; &nbsp;: GRAHA MADANIA Jl. Cijagra No 22B, Buah Batu, Bandung<br />NIA ASITA&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 0493/IX/2011<br /><br />NPWP&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 01.933.264.2-423.000<br />Nama&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : PT. Madania Semesta Wisata<br /><br /><strong>Tanda Daftar Perusahaan Perseroan Terbatas (TDP PT)</strong><br />Nomor TDP&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 101116310122<br />Nama Perusahaan&nbsp;&nbsp; &nbsp; : Madania Semesta Wisata, PT.<br />Status&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Pusat<br />Alamat&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : GRAHA MADANIA Jl. Cijagra No 22B, Buah Batu, Bandung</p>', 'asita', 1, '2013-09-30 23:19:04', '2013-09-30 16:19:01', 0, '  asita pendirian pt madania semesta wisata   ', '  asita pendirian pt madania semesta wisata   ', 0, 2),
(24, 'kerjasama', 'kerjasama', 'dinamic', '<p>\n kerjasama</p>\n', 'kerjasama', 0, '2013-10-05 20:11:46', '2013-10-05 13:11:49', 0, 'kerjasama', 'kerjasama', 1, 0),
(25, 'Area download ', 'Area download ', 'collaboration', '<p>halaman ini digunakan untuk menambahkan data atau file-file yang dapat di unduh oleh user.</p>', 'area_download ', 0, '2013-10-05 20:20:15', '2013-10-05 13:20:18', 0, ' Area download ', ' Area download ', 1, 0),
(26, 'iklan', 'iklan', 'static', '<p>\n iklan</p>\n', 'iklan', 0, '2013-10-05 20:42:58', '2013-10-05 13:43:01', 0, 'iklan', 'iklan', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE IF NOT EXISTS `provinces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `nama`) VALUES
(1, 'Jawa Barat'),
(2, 'DKI Jakarta'),
(3, 'Jawa Tengah'),
(4, 'Jawa Timur');

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE IF NOT EXISTS `religions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`id`, `name`, `date_created`, `date_updated`) VALUES
(1, 'Islam', '2013-05-27 00:00:00', '2013-05-27 00:00:00'),
(2, 'Nasrani', '2013-05-27 00:00:00', '2013-05-27 00:00:00'),
(3, 'Katolik', '2013-05-27 00:00:00', '2013-05-27 00:13:56'),
(4, 'Budha', '2013-05-27 00:14:07', '2013-05-27 00:14:10'),
(5, 'Hindu', '2013-05-27 00:14:07', '2013-05-27 00:14:10');

-- --------------------------------------------------------

--
-- Table structure for table `requirements`
--

CREATE TABLE IF NOT EXISTS `requirements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` enum('all','packet') NOT NULL DEFAULT 'all',
  `tag` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `requirements`
--

INSERT INTO `requirements` (`id`, `name`, `description`, `status`, `tag`, `created_at`, `updated_at`) VALUES
(1, 'pas photo', 'pas photo 3X4\n', 'all', 'umroh', '2013-10-04 15:07:56', '2013-10-04 15:07:58'),
(2, 'pas photo', 'pas photo 4x6\n', 'all', 'haji', '2013-10-04 15:39:20', '2013-10-04 15:39:23'),
(3, 'Membayar uang muka pada saat pendaftaran sebesar USD 500,- per orang.', '<p>\n <span  rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none; ">Membayar uang muka pada saat pendaftaran sebesar USD 500,- per orang.</span></p>\n', 'all', 'umroh', '2013-10-04 19:40:13', '2013-10-04 19:40:16'),
(4, 'Menyerahkan dokumen berupa', '<p>\n Menyerahkan dokumen berupa</p>\n<ul>\n <li>\n  Paspor asli yang masa berlakunya minimal 8 bulan dari tanggal berangkat dengan nama minimal terdiri dari 3 kata, contoh : Ahmad Abdullah Ismail.</li>\n <li>\n  Menyerahkan pas foto berwarna ukuran 3 x 4 = 4 lembar dan 4 x 6 = 6 lembar dengan background putih, focus muka 80 %, untuk wanita memakai kerudung.</li>\n <li>\n  Kartu keluarga Asli, Surat nikah Asli, Akta kelahiran Asli.</li>\n <li>\n  Foto Copy KTP2 lembar dan KTP asli bagi wanita yang berusia 45 tahun ke atas.</li>\n <li>\n  Kartu Kuning (vaksinasi meningitis)</li>\n</ul>\n', 'all', 'umroh', '2013-10-04 19:43:00', '2013-10-04 19:43:02'),
(5, ' Pembatalan yang dilakukan 2 ( dua) minggu sebelum Keberangkatan dikenakan Biaya 25% dari harga paket.', '<p>\n <span  rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none; "><span class="Apple-converted-space">&nbsp;</span>Pembatalan yang dilakukan 2 ( dua) minggu sebelum Keberangkatan dikenakan Biaya 25% dari harga paket.</span></p>\n', 'all', 'Pembatalan Umroh', '2013-10-04 19:45:57', '2013-10-04 19:45:59'),
(6, 'Pembatalan yang dilakukan 1 (satu) minggu sebelum keberangkatan dikenakan biaya 75% dari harga paket.', 'Pembatalan yang dilakukan 1 (satu) minggu sebelum keberangkatan dikenakan biaya 75% dari harga paket.\n', '', 'Pembatalan Umroh', '2013-10-04 19:46:54', '2013-10-04 19:46:56'),
(7, 'pembatalan yang dilakukan 5 sampai 1 hari sebelum keberangkatan dikenakan biaya 85 % dari harga paket', 'pembatalan yang dilakukan 5 sampai 1 hari sebelum keberangkatan dikenakan biaya 85 % dari harga paket', 'all', 'Pembatalan Umroh', '2013-10-04 19:47:36', '2013-10-04 19:47:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_group_users` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `group_id`) VALUES
(1, 'super_administrator', 'admin@localhost.com', '5a0cdef1627164252ad4f87c6b3395b0', 99),
(2, 'doren', 'doren@harfa.com', 'e3ac76b1900741e1246d9c30cb1333e1', 99),
(3, 'sekretaris', 'ghghgh', 'e3ac76b1900741e1246d9c30cb1333e1', 100),
(4, 'marketing', 'marketing@harfa.com', '9f389ec698c58b702c2cdae3be0bf194', 102);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `title` varchar(200) NOT NULL,
  `url` varchar(200) NOT NULL,
  `autoplay` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('active','nonactive') NOT NULL DEFAULT 'active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `name`, `title`, `url`, `autoplay`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Xl', 'xl', '//www.youtube.com/embed/D3dV1CrpnDA', 0, 'active', '2013-10-11 00:00:00', '2013-10-11 00:00:00'),
(2, 'Nort face ', 'Nort face ', '//www.youtube.com/embed/sLmjaimm2Ug', 0, 'nonactive', '2013-10-11 19:27:34', '2013-10-11 19:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `villages`
--

CREATE TABLE IF NOT EXISTS `villages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `province_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_kelurahan_kecamatan` (`district_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `villages`
--

INSERT INTO `villages` (`id`, `nama`, `district_id`, `province_id`, `city_id`) VALUES
(1, 'kuti', 2, 4, 4),
(2, 'papandayan', 1, 1, 1),
(3, 'parakaya', 1, 1, 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `packets`
--
ALTER TABLE `packets`
  ADD CONSTRAINT `fk_packets_attribute_pages1` FOREIGN KEY (`attribute_page_id`) REFERENCES `attribute_pages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `packet_prices`
--
ALTER TABLE `packet_prices`
  ADD CONSTRAINT `fk_packet_prices_1` FOREIGN KEY (`packet_id`) REFERENCES `packets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
